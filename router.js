const UrlPrettifier = require('next-url-prettifier').default

const routes = [
  {
    page: 'index',
    prettyUrl: ({ language }) => (language === 'fr' ? `/fr` : `/en`),
    prettyUrlPatterns: [
      {
        pattern: '/'
      },
      {
        pattern: '/:language'
      }
    ]
  },
  {
    page: 'account',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/account` : `/en/account`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/account'
      }
    ]
  },
  {
    page: 'activate',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/activate` : `/en/activate`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/activate'
      }
    ]
  },
  {
    page: 'b2b',
    prettyUrl: ({ language }) => (language === 'fr' ? `/fr/b2b` : `/en/b2b`),
    prettyUrlPatterns: [
      {
        pattern: '/:language/b2b'
      }
    ]
  },
  {
    page: 'b2c',
    prettyUrl: ({ language }) => (language === 'fr' ? `/fr/b2c` : `/en/b2c`),
    prettyUrlPatterns: [
      {
        pattern: '/:language/b2c'
      }
    ]
  },
  {
    page: 'catalog',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/catalog` : `/en/catalog`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/catalog'
      }
    ]
  },
  {
    page: 'cgv',
    prettyUrl: ({ language }) => (language === 'fr' ? `/fr/cgv` : `/en/cgv`),
    prettyUrlPatterns: [
      {
        pattern: '/:language/cgv'
      }
    ]
  },
  {
    page: 'checkout',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/checkout` : `/en/checkout`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/checkout'
      }
    ]
  },
  {
    page: 'cookies',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/cookies` : `/en/cookies`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/cookies'
      }
    ]
  },
  {
    page: 'faq',
    prettyUrl: ({ language }) => (language === 'fr' ? `/fr/faq` : `/en/faq`),
    prettyUrlPatterns: [
      {
        pattern: '/:language/faq'
      }
    ]
  },
  {
    page: 'legal',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/legal` : `/en/legal`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/legal'
      }
    ]
  },
  {
    page: 'login',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/login` : `/en/login`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/login'
      }
    ]
  },
  {
    page: 'order',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/order` : `/en/order`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/order'
      }
    ]
  },
  {
    page: 'payment',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/payment` : `/en/payment`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/payment'
      }
    ]
  },
  {
    page: 'product',
    prettyUrl: ({ language, id, name }) =>
      language === 'fr'
        ? `/fr/product/${id}/${name}`
        : `/en/product/${id}/${name}`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/product/:id/:name'
      }
    ]
  },
  {
    page: 'quote',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/quote` : `/en/quote`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/quote'
      }
    ]
  },
  {
    page: 'register',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/register` : `/en/register`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/register'
      }
    ]
  },
  {
    page: 'reinit',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/reinit` : `/en/reinit`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/reinit'
      }
    ]
  },
  {
    page: 'reset',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/reset` : `/en/reset`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/reset'
      }
    ]
  },
  {
    page: 'sav',
    prettyUrl: ({ language }) => (language === 'fr' ? `/fr/sav` : `/en/sav`),
    prettyUrlPatterns: [
      {
        pattern: '/:language/sav'
      }
    ]
  },
  {
    page: 'shipping',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/shipping` : `/en/shipping`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/shipping'
      }
    ]
  },
  {
    page: 'survey',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/survey` : `/en/survey`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/survey'
      }
    ]
  },
  {
    page: 'wishlist',
    prettyUrl: ({ language, id, name }) =>
      language === 'fr'
        ? `/fr/wishlist/${id}/${name}`
        : `/en/wishlist/${id}/${name}`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/wishlist/:id/:name'
      }
    ]
  },
  {
    page: 'wishlists',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/wishlists` : `/en/wishlists`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/wishlists'
      }
    ]
  },
  {
    page: 'sizes',
    prettyUrl: ({ language }) =>
      language === 'fr' ? `/fr/sizes` : `/en/sizes`,
    prettyUrlPatterns: [
      {
        pattern: '/:language/sizes'
      }
    ]
  }
]

const urlPrettifier = new UrlPrettifier(routes)
exports.default = routes
exports.Router = urlPrettifier
