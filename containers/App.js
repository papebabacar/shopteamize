import { compose, setDisplayName } from 'recompose'
import { createStructuredSelector } from 'reselect'
import { bindActionCreators } from 'redux'
import Template from 'components/Theme/Template'
import withRedux from 'next-redux-wrapper'
import { connect } from 'react-redux'
import { initStore } from '/store'

import {
  loginUser,
  logoutUser,
  setLanguage,
  setCheckedCookies
} from 'store/actions/user'
import { hideModal, showModal, updateModal } from 'store/actions/modal'
import {
  setCart,
  setDefaultCartQuantity,
  refreshCart,
  emptyCart,
  addCartItem,
  updateCartItem,
  removeCartItem
} from 'store/actions/cart'
import { setOrder, checkOrder, emptyOrder } from 'store/actions/order'
import { addToCatalog, setCatalog } from 'store/actions/catalog'
import {
  setWishlist,
  addToWishlist,
  updateWishlistItem,
  removeFromWishlist,
  emptyWishlist
} from 'store/actions/wishlist'
import {
  clearResults,
  searchItems,
  setSearchFilter,
  expandSearch
} from 'store/actions/search'

import { getUser } from 'store/selectors/user'
import { getModal } from 'store/selectors/modal'
import { getCart } from 'store/selectors/cart'
import { getOrder } from 'store/selectors/order'
import { getCatalog } from 'store/selectors/catalog'
import { getWishlist } from 'store/selectors/wishlist'
import { getSearch } from 'store/selectors/search'

const mapUserDispatchToProps = dispatch => {
  return {
    loginUser: bindActionCreators(loginUser, dispatch),
    logoutUser: bindActionCreators(logoutUser, dispatch),
    setLanguage: bindActionCreators(setLanguage, dispatch),
    setCheckedCookies: bindActionCreators(setCheckedCookies, dispatch)
  }
}
const mapCartDispatchToProps = dispatch => {
  return {
    setCart: bindActionCreators(setCart, dispatch),
    setDefaultCartQuantity: bindActionCreators(
      setDefaultCartQuantity,
      dispatch
    ),
    refreshCart: bindActionCreators(refreshCart, dispatch),
    emptyCart: bindActionCreators(emptyCart, dispatch),
    addCartItem: bindActionCreators(addCartItem, dispatch),
    updateCartItem: bindActionCreators(updateCartItem, dispatch),
    removeCartItem: bindActionCreators(removeCartItem, dispatch)
  }
}
const mapOrderDispatchToProps = dispatch => {
  return {
    setOrder: bindActionCreators(setOrder, dispatch),
    checkOrder: bindActionCreators(checkOrder, dispatch),
    emptyOrder: bindActionCreators(emptyOrder, dispatch)
  }
}
const mapCatalogDispatchToProps = dispatch => {
  return {
    addToCatalog: bindActionCreators(addToCatalog, dispatch),
    setCatalog: bindActionCreators(setCatalog, dispatch)
  }
}
const mapWishlistDispatchToProps = dispatch => {
  return {
    setWishlist: bindActionCreators(setWishlist, dispatch),
    addToWishlist: bindActionCreators(addToWishlist, dispatch),
    updateWishlistItem: bindActionCreators(updateWishlistItem, dispatch),
    removeFromWishlist: bindActionCreators(removeFromWishlist, dispatch),
    emptyWishlist: bindActionCreators(emptyWishlist, dispatch)
  }
}
const mapModalDispatchToProps = dispatch => {
  return {
    showModal: bindActionCreators(showModal, dispatch),
    updateModal: bindActionCreators(updateModal, dispatch),
    hideModal: bindActionCreators(hideModal, dispatch)
  }
}
const mapSearchDispatchToProps = dispatch => {
  return {
    clearResults: bindActionCreators(clearResults, dispatch),
    searchItems: bindActionCreators(searchItems, dispatch),
    setSearchFilter: bindActionCreators(setSearchFilter, dispatch),
    expandSearch: bindActionCreators(expandSearch, dispatch)
  }
}

const mapUserStateToProps = createStructuredSelector({
  user: getUser
})
const mapCartStateToProps = createStructuredSelector({
  cart: getCart
})
const mapOrderStateToProps = createStructuredSelector({
  order: getOrder
})
const mapCatalogStateToProps = createStructuredSelector({
  catalog: getCatalog
})
const mapWishliststateToProps = createStructuredSelector({
  wishlist: getWishlist
})
const mapModalStateToProps = createStructuredSelector({
  modal: getModal
})
const mapSearchStateToProps = createStructuredSelector({
  search: getSearch
})

export const withStore = withRedux(
  initStore,
  mapUserStateToProps,
  mapUserDispatchToProps
)
export const getUserStore = connect(
  mapUserStateToProps,
  mapUserDispatchToProps
)
export const getCartStore = connect(
  mapCartStateToProps,
  mapCartDispatchToProps
)
export const getOrderStore = connect(
  mapOrderStateToProps,
  mapOrderDispatchToProps
)
export const getCatalogStore = connect(
  mapCatalogStateToProps,
  mapCatalogDispatchToProps
)
export const getWishlistStore = connect(
  mapWishliststateToProps,
  mapWishlistDispatchToProps
)
export const getModalStore = connect(
  mapModalStateToProps,
  mapModalDispatchToProps
)
export const getSearchStore = connect(
  mapSearchStateToProps,
  mapSearchDispatchToProps
)

export default compose(setDisplayName('Template'))(Template)
