import { getModalStore } from 'containers/App'
import QuickView from 'components/QuickView'
import Detail from './Detail'

const Component = ({
  modal,
  showModal,
  hideModal,
  selectedRelayPoint,
  setSelectedRelayPoint
}) => (
  <QuickView closeAction={hideModal}>
    <Detail
      showModal={showModal}
      hideModal={hideModal}
      relayPoints={modal.data.relayPoints}
      selectedRelayPoint={selectedRelayPoint}
      setSelectedRelayPoint={setSelectedRelayPoint}
    />
  </QuickView>
)

export default getModalStore(Component)
