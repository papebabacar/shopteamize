import styled from 'styled-components'
import theme from 'components/Theme'

export const RelayPoints = styled.div`
  ${theme.media.laptop`max-width: calc( 100% - 64px);`};
  margin: 0 auto;
  padding: 1.625em 0;
`
export const Modal = styled.div`
  position: relative;
  outline: 0;
  width: 100%;
  background: white;
  text-align: left;
  display: inline-block;
  vertical-align: middle;
  box-sizing: border-box;
  max-width: 640px;
  margin: 0 auto;
  cursor: default;
`
export const Close = styled.div`
  position: absolute;
  top: 0.203125em;
  right: 0.203125em;
  line-height: 0.8125em;
  padding: 0.203125em 0.40625em;
  cursor: pointer;
  font-size: 150%;
  &:hover {
    color: #000;
  }
`
export const Label = styled.p``

export const Img = styled.img`
  width: 3.4375em;
`

export const Title = styled.h3`
  ${'' /* text-align: center; */};
`

export const RadioGroup = styled.div`
  display: block;
  padding: 0.40625em 0 0 1.625em;
  margin-bottom: 0.40625em;
`

export const Input = styled.input`
  margin-right: 0.40625em;
`
export const RadioLabel = styled.label``
