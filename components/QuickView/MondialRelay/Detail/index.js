import { Trans } from '@lingui/react'
import { compose, withState } from 'recompose'
import {
  Modal,
  RelayPoints,
  Title,
  Img,
  Input,
  RadioGroup,
  RadioLabel,
  Close
} from './style'
import { SubmitButton } from 'components/Theme'

const Detail = ({
  hideModal,
  relayPoints,
  relayPointId,
  setRelayPointId,
  setSelectedRelayPoint,
  selectedRelayPoint: { CP, Ville }
}) => (
  <Modal onClick={event => event.stopPropagation()}>
    <RelayPoints>
      <Title>
        <Img src="/static/img/relay.jpg" alt="Mondial Relay" />{' '}
        <Trans>Choose your relay point</Trans> : {CP} - {Ville}
      </Title>
      {relayPoints.map(({ Num, LgAdr1, LgAdr3 }) => (
        <RadioGroup key={Num}>
          <Input
            type="radio"
            id={Num}
            name="relay"
            value={Num}
            checked={relayPointId === Num}
            onChange={() => setRelayPointId(Num)}
          />
          <RadioLabel for={Num}>
            {LgAdr3}, {LgAdr1}
          </RadioLabel>
        </RadioGroup>
      ))}
      <SubmitButton
        block
        onClick={() => {
          setSelectedRelayPoint(
            relayPoints.find(({ Num }) => Num === relayPointId)
          )
          hideModal()
        }}
      >
        <Trans>Add relay point</Trans>
      </SubmitButton>
    </RelayPoints>
    <Close onClick={hideModal}>x</Close>
  </Modal>
)

const Component = compose(
  withState(
    'relayPointId',
    'setRelayPointId',
    ({ selectedRelayPoint: { Num } }) => Num
  )
)(Detail)

export default Component
