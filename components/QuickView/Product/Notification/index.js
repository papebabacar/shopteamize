import { Trans } from '@lingui/react'
import { Modal, Info, Actions, Close } from './style'
import { Button } from 'components/Theme'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const Component = ({ hideModal, user: { language } }) => (
  <Modal onClick={event => event.stopPropagation()}>
    <Info>
      <Trans>Your product have been successfully added to your cart</Trans>
    </Info>
    <Actions>
      <Button
        block
        width="80%"
        onClick={() =>
          hideModal() &&
          (document.location = `${
            Router.linkPage('checkout', { language }).as
          }?step=basket`)
        }
      >
        <Trans>SEE MY CART</Trans> ›
      </Button>
      <Button secondary block width="80%" onClick={() => hideModal()}>
        ‹ <Trans>CONTINUE MY SHOPPING</Trans>
      </Button>
    </Actions>
    <Close onClick={hideModal}>x</Close>
  </Modal>
)

export default getUserStore(Component)
