import { getModalStore } from 'containers/App'
import QuickView from 'components/QuickView'
import Detail from './Detail'
import Notification from './Notification'
import WishlistNotification from './WishlistNotification'

const Component = ({ modal, showModal, hideModal, fetchData }) => (
  <QuickView closeAction={hideModal}>
    {modal.data ? (
      Object.keys(modal.data).length > 0 ? (
        <Detail
          showModal={showModal}
          hideModal={hideModal}
          product={modal.data}
          fetchData={fetchData}
        />
      ) : (
        <Notification hideModal={hideModal} />
      )
    ) : (
      <WishlistNotification hideModal={hideModal} />
    )}
  </QuickView>
)

export default getModalStore(Component)
