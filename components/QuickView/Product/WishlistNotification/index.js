import { Trans } from '@lingui/react'
import { Modal, Info, Actions, Close } from './style'
import { Button } from 'components/Theme'
import { Router } from 'router'
import { getUserStore, getWishlistStore } from 'containers/App'

const Component = ({
  hideModal,
  user: { language, pathname },
  setWishlist
}) => (
  <Modal onClick={event => event.stopPropagation()}>
    <Info>
      <Trans>Your product have been successfully added to your wishlist.</Trans>
    </Info>
    <Actions>
      <Button
        block
        width="80%"
        onClick={() => {
          hideModal()
          if (pathname === '/catalog') {
            setWishlist({ visible: true })
          } else {
            document.location = `${
              Router.linkPage('catalog', { language }).as
            }?category=gourmand`
          }
        }}
      >
        <Trans>SEE MY WISHLIST</Trans> ›
      </Button>
      <Button secondary block width="80%" onClick={() => hideModal()}>
        ‹ <Trans>CONTINUE MY SHOPPING</Trans>
      </Button>
    </Actions>
    <Close onClick={hideModal}>x</Close>
  </Modal>
)

export default getUserStore(getWishlistStore(Component))
