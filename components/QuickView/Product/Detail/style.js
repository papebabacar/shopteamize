import styled from 'styled-components'
import { Flex } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Product = styled(Flex)`
  ${theme.media.laptop`max-width: calc( 100% - 64px);`};
  margin: 0 auto;
`
export const Modal = styled.div`
  position: relative;
  outline: 0;
  width: 100%;
  background: white;
  text-align: left;
  display: inline-block;
  vertical-align: middle;
  box-sizing: border-box;
  max-width: 1024px;
  margin: 0 auto;
  cursor: default;
`
export const Close = styled.div`
  position: absolute;
  top: 0.625em;
  right: 0.203125em;
  line-height: 0.8125em;
  padding: 0.203125em 0.40625em;
  cursor: pointer;
  font-size: 150%;
  &:hover {
    color: #000;
  }
`
