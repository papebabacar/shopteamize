import { Actions } from './style'
import { Button } from 'components/Theme'
import { Router } from 'router'
import { getUserStore } from 'containers/App'
import slugify from '@sindresorhus/slugify'

const Component = ({
  product,
  addCartItem,
  showModal,
  hideModal,
  user: { language }
}) => (
  <Actions>
    <Button
      onClick={() => {
        hideModal()
        document.location = `${
          Router.linkPage('product', {
            language,
            id: product.id,
            name: slugify(product.name)
          }).as
        }`
      }}
    >
      VOIR LE DETAIL DU PRODUIT
    </Button>
    <Button
      onClick={() =>
        addCartItem(product, 'products') && showModal({}, 'product')
      }
    >
      AJOUTER AU PANIER
    </Button>
  </Actions>
)

export default getUserStore(Component)
