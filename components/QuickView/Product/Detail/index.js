import { compose, withState, lifecycle } from 'recompose'
import { getUserStore, getCartStore } from 'containers/App'
import Info from 'components/Product/Detail/Info'
import Carousel from 'components/Product/Detail/Carousel'
import Share from 'components/Product/Detail/Share'
import { Modal, Product, Close } from './style'
import { getProduct } from 'api/muhajir/catalog'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'

const ProductView = ({
  product,
  size,
  variants,
  images,
  hideModal,
  user: { language },
  fetchData
}) => (
  <Modal onClick={event => event.stopPropagation()}>
    <Product padding={0} flex-phone={1} flex-tablet={2}>
      <Carousel mainImageUrl={product.mainImageUrl} images={images} />
      <Info
        product={{ ...product, size, variants }}
        quickView
        fetchData={fetchData}
      />
    </Product>
    <Share
      // eslint-disable-next-line no-undef
      shareUrl={`${APP_URL}${
        Router.linkPage('product', {
          language,
          id: product.id,
          name: slugify(product.name)
        }).as
      }`}
      image={product.mainImageUrl}
    />
    <Close onClick={hideModal}>x</Close>
  </Modal>
)

const Component = compose(
  getCartStore,
  getUserStore,
  withState('images', 'setImages', []),
  withState('size', 'setSize'),
  withState('variants', 'setVariants', []),
  lifecycle({
    async componentDidMount() {
      const {
        product: { id },
        setSize,
        setVariants,
        setImages,
        setDefaultCartQuantity,
        user: { language }
      } = this.props
      setDefaultCartQuantity(1)
      try {
        const {
          data: { size, variants, images }
        } = await getProduct(id, language)
        setSize(size)
        setVariants(variants)
        setImages(images)
      } catch (error) {
        // do something
      }
    }
  })
)(ProductView)

export default Component
