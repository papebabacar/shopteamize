import { compose, lifecycle } from 'recompose'
import { Modal } from './style'

const QuickView = ({ closeAction, children }) => (
  <Modal onClick={closeAction}>{children}</Modal>
)

const Component = compose(
  lifecycle({
    componentDidMount() {
      document.querySelector('html').style.overflow = 'hidden'
      document.querySelector('.header').style.position = 'static'
      window.addEventListener('keydown', onKeyPress(this.props.closeAction), {
        once: true
      })
    },
    componentWillUnmount() {
      document.querySelector('html').style.overflow = 'auto'
      document.querySelector('.header').style.position = 'sticky'
    }
  })
)(QuickView)

export default Component

const onKeyPress = closeAction => event => {
  event.keyCode === 27 && closeAction()
}
