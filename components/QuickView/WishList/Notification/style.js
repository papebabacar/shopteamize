import styled from 'styled-components'

export const Modal = styled.div`
  position: relative;
  outline: 0;
  width: 100%;
  background: white;
  text-align: left;
  display: inline-block;
  vertical-align: middle;
  box-sizing: border-box;
  max-width: 480px;
  margin: 0 auto;
  cursor: default;
`

export const Actions = styled.div`
  display: flex; /* establish flex container */
  flex-direction: column; /* make main axis vertical */
  justify-content: center; /* center items vertically, in this case */
  align-items: center;
  margin: 1.625em 0;
`
export const Info = styled.h2`
  text-align: center;
  padding: 1.625em 1.625em 0;
`

export const Close = styled.div`
  position: absolute;
  top: 0.625em;
  right: 0.203125em;
  line-height: 0.8125em;
  padding: 0.203125em 0.40625em;
  cursor: pointer;
  font-size: 150%;
  &:hover {
    color: #000;
  }
`
