import { Trans } from '@lingui/react'
import { Modal, Info, Actions, Close } from './style'
import { Button } from 'components/Theme'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const Component = ({ hideModal, user: { language } }) => (
  <Modal onClick={event => event.stopPropagation()}>
    <Info>
      <Trans>
        Your wishlist have been taken into care. You have 48H to order it.
      </Trans>
    </Info>
    <Actions>
      <Button
        block
        width="80%"
        onClick={() =>
          hideModal() &&
          (document.location = `${
            Router.linkPage('account', { language }).as
          }?view=wishlist`)
        }
      >
        <Trans>SEE MY WISHLISTS</Trans> ›
      </Button>
      <Button secondary block width="80%" onClick={() => hideModal()}>
        ‹ <Trans>CONTINUE MY SHOPPING</Trans>
      </Button>
    </Actions>
    <Close onClick={hideModal}>x</Close>
  </Modal>
)

export default getUserStore(Component)
