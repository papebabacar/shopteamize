import { getModalStore } from 'containers/App'
import QuickView from 'components/QuickView'
import Detail from './Detail'
import Notification from './Notification'
import FormNotification from './FormNotification'

const Component = ({ modal, showModal, hideModal }) => (
  <QuickView closeAction={hideModal}>
    {modal.data ? (
      Object.keys(modal.data).length > 0 ? (
        <Detail
          showModal={showModal}
          hideModal={hideModal}
          wishlist={modal.data}
        />
      ) : (
        <Notification hideModal={hideModal} />
      )
    ) : (
      <FormNotification hideModal={hideModal} />
    )}
  </QuickView>
)

export default getModalStore(Component)
