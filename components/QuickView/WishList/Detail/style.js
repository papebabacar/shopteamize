import styled from 'styled-components'
import theme from 'components/Theme'

export const Detail = styled.div`
  display: none;
  ${theme.media.tablet`
    display:block;
    margin: 0 auto;
    min-height: 36vh;
  `};
`
export const Modal = styled.div`
  position: relative;
  outline: 0;
  width: 100%;
  background: white;
  text-align: left;
  display: inline-block;
  vertical-align: middle;
  box-sizing: border-box;
  max-width: 1024px;
  margin: 0 auto;
  cursor: default;
`
export const Close = styled.div`
  position: absolute;
  top: 0.625em;
  right: 0.203125em;
  line-height: 0.8125em;
  padding: 0.203125em 0.40625em;
  cursor: pointer;
  font-size: 150%;
  &:hover {
    color: #000;
  }
`
