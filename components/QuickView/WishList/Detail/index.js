import Product from 'components/WishList/Detail/Product'
import Share from 'components/WishList/Detail/Share'
import Info from 'components/WishList/Detail/Info'
import { Modal, Detail, Close } from './style'
import { compose, withState, lifecycle } from 'recompose'
import { getWishlist } from 'api/muhajir/wishlist'
import { getUserStore } from 'containers/App'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'

const WishlistView = ({ wishlist, items, hideModal, user: { language } }) => (
  <Modal onClick={event => event.stopPropagation()}>
    <Info wishlist={wishlist} quickView />
    <Detail>
      {items.length > 0 && (
        <Product mainItemId={wishlist.mainImageSku} items={items} quickView />
      )}
    </Detail>
    <Share
      // eslint-disable-next-line no-undef
      shareUrl={`${APP_URL}${
        Router.linkPage('wishlist', {
          language,
          id: wishlist.id,
          name: slugify(getTitle(wishlist.labels, language))
        }).as
      }`}
      image={wishlist.mainImageUrl}
    />
    <Close onClick={hideModal}>x</Close>
  </Modal>
)

const Component = compose(
  getUserStore,
  withState('items', 'setItems', []),
  lifecycle({
    async componentDidMount() {
      try {
        const {
          wishlist: { id },
          setItems,
          user: { language }
        } = this.props
        const {
          data: { items }
        } = await getWishlist(id, language)
        setItems(items)
      } catch (error) {
        // do something
      }
    }
  })
)(WishlistView)

export default Component

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
