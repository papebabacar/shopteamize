import { Trans, withI18n } from '@lingui/react'
import { compose, withState, withHandlers } from 'recompose'
import { Form, withForm } from 'components/Theme/Form'
import Field from 'components/Theme/Form/Field'
import {
  AccountForm,
  SubmitButton,
  FormTitle,
  FormLabel,
  Link,
  Notification
} from 'components/Theme'
import { reinit } from 'api/account'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const ReinitPage = ({
  post,
  submitting,
  submitError,
  i18n,
  user: { language }
}) => (
  <AccountForm>
    <Form post={post} submitting={submitting}>
      <FormTitle>
        <Trans>Forgot your password?</Trans>
      </FormTitle>
      <FormLabel>
        <Trans>You will receive a link to reset your password</Trans>
      </FormLabel>
      <Field
        label="Email"
        name="username"
        value=""
        type="email"
        validations="isEmail"
        validationError={i18n.t`Please enter a valid email address`}
        placeholder="Email"
        required
      />
      <FormLabel right>
        <Link href={`${Router.linkPage('login', { language }).as}`}>
          ‹ <Trans>Back to login page</Trans>
        </Link>
      </FormLabel>
      {submitError && <Notification>{submitError}</Notification>}
      <SubmitButton disabled={submitting}>
        <Trans>Submit</Trans>
      </SubmitButton>
    </Form>
  </AccountForm>
)

const Component = compose(
  getUserStore,
  withI18n(),
  withState('submitError', 'setSubmitError'),
  withHandlers({
    post: ({
      setFormState,
      setSubmitError,
      i18n,
      user: { language }
    }) => async data => {
      setFormState(true)
      try {
        const { username: mail } = data
        await reinit(mail)
        document.location = `${Router.linkPage('login', { language }).as}`
      } catch (error) {
        setSubmitError(
          i18n.t`A technical error has occurred. Please reload the page`
        )
        setTimeout(() => setSubmitError(), 5000)
        setFormState(false)
      }
    }
  })
)(ReinitPage)

export default withForm(Component)
