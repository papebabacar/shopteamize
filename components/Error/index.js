import { Trans } from '@lingui/react'
import { Section, Title, Label, Link } from './style'
import BannerSteps from 'components/Home/BannerSteps'
import Popular from 'components/Push/WishList/Popular'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const Component = ({ statusCode, wishlists, user: { language } }) => (
  <>
    <Section>
      <Title>{statusCode}</Title>
      <Label>
        {statusCode === 404 ? (
          <Trans>
            Sorry...the page you are trying to access doesn't exist.
          </Trans>
        ) : (
          <Trans>
            Sorry...due to a technical problem, the page you want to view can
            not be displayed.
          </Trans>
        )}
      </Label>
      <Label>
        <Trans>
          You can go back to our{' '}
          <Link href={`${Router.linkPage('index', { language }).as}`}>
            homepage
          </Link>{' '}
          or begin a search
        </Trans>.
      </Label>
    </Section>
    {wishlists && <Popular items={wishlists} />}
    <BannerSteps />
  </>
)

export default getUserStore(Component)
