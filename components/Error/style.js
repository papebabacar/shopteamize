import styled from 'styled-components'

export const Section = styled.section`
  padding: 1.625em;
  text-align: center;
`
export const Title = styled.h1`
  font-size: 900%;
  margin-bottom: 0;
  color: #888;
`

export const Label = styled.p`
  font-weight: 900;
`

export const Link = styled.a`
  text-decoration: underline;
  color: rgb(145, 204, 138);
`
