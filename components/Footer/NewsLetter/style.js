import styled from 'styled-components'
import theme from 'components/Theme'
import { Box } from 'components/Theme/Layout/FlexBox'

const {
  settings: { padding, base, unit }
} = theme

export const Newsletter = styled(Box)`
  order: -1;
  flex: 1;
  ${theme.media.pad`order:0;flex: 0.5;`};
  width: 100%;
`

export const Label = styled.h3`
  display: block;
  text-align: center;
  ${theme.media.pad`
  padding: ${padding(base / 2, base / 2, 0)};
  text-align:left;`};
  margin: 0 auto;
  font-family: 'Helvetica Light';
  letter-spacing: -0.03125em;
`

export const Input = styled.input`
  background: #fff;
  padding: ${padding(base / 3, base)};
  width: 100%;
  display: inline-block;
`
export const Button = styled.button`
  position: relative;
  display: inline-block;
  margin-left: -${base + unit};
  top: ${base / 8 + unit};
  font-size: 200%;
  color: #444;
`
