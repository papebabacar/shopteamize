import { withI18n } from '@lingui/react'
import { Newsletter, Label, Input, Button } from './style'

const Component = ({ i18n }) => (
  <Newsletter>
    <Label>NEWSLETTER: </Label>
    <Input placeholder={i18n.t`your email address`} />
    <Button>✉</Button>
  </Newsletter>
)

export default withI18n()(Component)
