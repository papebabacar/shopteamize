import styled from 'styled-components'
import theme from 'components/Theme'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'

const { dark, light } = theme

export const Footer = styled(Flex.withComponent('footer'))`
  background: ${dark};
  color: ${light};
  margin-top: 0 !important;
  flex-direction: column;
  position: relative;
  padding-bottom: 0;
  ${theme.media.pad`flex-direction: row;padding-bottom: 1.625em;`};
`

export const LogoBox = styled(Box)`
  order: -1;
  flex: 1;
  width: 100%;
  ${theme.media.pad`order:1;flex: 1;padding:0.40625em 0 0 0`};
`

export const Logo = styled.img`
  height: 4.375em;
  margin: 0 auto;
  display: block;
`
export const Copyright = styled.div`
  font-family: 'Arimo';
  text-align: center;
  padding: 0.40625em;
  font-size: 70%;
  background: #202c28;
  color: white;
  width: 100%;
  ${theme.media.pad`position: absolute;bottom:0.40625em;left:0`};
`

export const Link = styled.a`
  color: #00ba4f;
  font-weight: 900;
`
