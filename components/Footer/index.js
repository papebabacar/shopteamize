import { Trans } from '@lingui/react'
import { Footer, LogoBox, Logo, Copyright, Link } from './style'
import CmsLinks from 'components/Footer/CmsLinks'
import SocialLinks from 'components/Footer/SocialLinks'
// import NewsLetter from 'components/Footer/NewsLetter'
import CookiesAgreement from 'components/CookiesAgreement'

const Component = () => (
  <>
    <Footer>
      <CmsLinks />
      <SocialLinks />
      {/* <NewsLetter /> */}
      <LogoBox>
        <Logo src="/static/img/logo.png" alt="shopteamize" />
      </LogoBox>
      <Copyright>
        <Link href="https://www.sn-ecommerce.fr">SN-Ecommerce</Link> &copy;
        2018. <Trans>All rights reserved</Trans>.
      </Copyright>
    </Footer>
    <CookiesAgreement />
  </>
)

export default Component
