import { Trans } from '@lingui/react'
import { CmsLinks, CmsLink } from './style'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const Component = ({ user: { language } }) => (
  <CmsLinks>
    <CmsLink href={`${Router.linkPage('order', { language }).as}`}>
      <Trans>Order</Trans>
    </CmsLink>
    <CmsLink href={`${Router.linkPage('payment', { language }).as}`}>
      <Trans>Payment</Trans>
    </CmsLink>
    <CmsLink href={`${Router.linkPage('shipping', { language }).as}`}>
      <Trans>Delivery</Trans>
    </CmsLink>
    <CmsLink href={`${Router.linkPage('faq', { language }).as}`}>
      <Trans>FAQ</Trans>
    </CmsLink>
    <CmsLink href={`${Router.linkPage('cgv', { language }).as}`}>
      <Trans>CGV</Trans>
    </CmsLink>
    <CmsLink href={`${Router.linkPage('sav', { language }).as}`}>
      <Trans>SAV</Trans>
    </CmsLink>
    <CmsLink href={`${Router.linkPage('legal', { language }).as}`}>
      <Trans>Legal notice</Trans>
    </CmsLink>
    <CmsLink href={`${Router.linkPage('cookies', { language }).as}`}>
      <Trans>Privacy notice</Trans>
    </CmsLink>
  </CmsLinks>
)

export default getUserStore(Component)
