import styled from 'styled-components'
import theme from 'components/Theme'
import { Box } from 'components/Theme/Layout/FlexBox'

const {
  settings: { base, unit }
} = theme

export const CmsLinks = styled(Box)`
  max-width: ${base * 15 + unit};
  flex: 1;
  margin: 1.625em auto 0;
  text-align: center;
  ${theme.media.pad`order:0;margin: 0.203125em 0;text-align:left;`};
  ${theme.media.laptop`flex: 0.5;`};
`
export const CmsLink = styled.a`
  padding: 0.203125em 0.40625em;
  border-left: 1px solid;
  line-height: ${base / 2};
  display: inline-block;
  margin-bottom: ${(base * 2) / 3 + unit};
  &:last-of-type {
    border-right: 1px solid;
  }
  font-family: 'Helvetica Light';
  letter-spacing: -0.03125em;
`
