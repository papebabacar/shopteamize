import {
  SocialLinks,
  SocialLink,
  FacebookIcon,
  InstagramIcon,
  Image
} from './style'

const Component = () => (
  <SocialLinks>
    <SocialLink
      href="https://www.facebook.com/MuhajirShoppers"
      target="_blank"
      rel="nofollow noopener noreferrer"
    >
      <FacebookIcon>
        <Image src="/static/img/facebook.png" alt="facebook" />{' '}
      </FacebookIcon>
    </SocialLink>
    <SocialLink
      href="https://www.instagram.com/muhajir.shoppers_uae"
      target="_blank"
      rel="nofollow noopener noreferrer"
    >
      <InstagramIcon>
        <Image src="/static/img/instagram.png" alt="instagram" />{' '}
      </InstagramIcon>
    </SocialLink>
  </SocialLinks>
)

export default Component
