import styled from 'styled-components'
import theme from 'components/Theme'
import { Box } from 'components/Theme/Layout/FlexBox'

export const SocialLinks = styled(Box)`
  text-align: center;
  flex: 1;
  width: 100%;
  margin: 1.625em 0;
  ${theme.media.pad`order:2;text-align: right;margin: 1.40625em 0.203125em;`};
  ${theme.media.laptop`flex: 0.5;`};
`

export const SocialLink = styled.a`
  display: inline-block;
  cursor: pointer;
`
export const FacebookIcon = styled.div`
  background: #3b5998;
`

export const InstagramIcon = styled.div`
  background: white;
`

export const Image = styled.img`
  width: 2.4375em;
  height: 2.4375em;
  object-fit: contain;
  border-radius: 50%;
`
