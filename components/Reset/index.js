import { Trans, withI18n } from '@lingui/react'
import { compose, withState, withHandlers } from 'recompose'
import { Form, withForm } from 'components/Theme/Form'
import Field from 'components/Theme/Form/Field'
import {
  AccountForm,
  SubmitButton,
  FormTitle,
  FormLabel,
  Link,
  Notification
} from 'components/Theme'
import { reset } from 'api/account'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const ResetPage = ({
  post,
  submitting,
  submitError,
  i18n,
  user: { language }
}) => (
  <AccountForm>
    <Form post={post} submitting={submitting}>
      <FormTitle>
        <Trans>New pasword</Trans>
      </FormTitle>
      <FormLabel>
        <Trans>Please set a new password for your account</Trans>
      </FormLabel>
      <Field
        label={<Trans>Password</Trans>}
        name="newPassword"
        value=""
        type="password"
        placeholder="*******"
        required
      />
      <Field
        label="Confirmation"
        name="confirmation"
        value=""
        type="password"
        placeholder="*******"
        required
        validations="equalsField:newPassword"
        validationError={i18n.t`The two passwords must be equal`}
      />
      <FormLabel right>
        <Link href={`${Router.linkPage('login', { language }).as}`}>
          ‹ <Trans>Back to login page</Trans>
        </Link>
      </FormLabel>
      {submitError && <Notification>{submitError}</Notification>}
      <SubmitButton disabled={submitting}>
        <Trans>Submit</Trans>
      </SubmitButton>
    </Form>
  </AccountForm>
)

const Component = compose(
  getUserStore,
  withI18n(),
  withState('submitError', 'setSubmitError'),
  withHandlers({
    post: ({
      activationKey,
      setFormState,
      setSubmitError,
      i18n,
      user: { language }
    }) => async data => {
      setFormState(true)
      try {
        const { newPassword } = data
        await reset({ key: activationKey, newPassword })
        document.location = `${Router.linkPage('login', { language }).as}`
      } catch (error) {
        setSubmitError(
          i18n.t`A technical error has occurred. Please reload the page`
        )
        setTimeout(() => setSubmitError(), 5000)
        setFormState(false)
      }
    }
  })
)(ResetPage)

export default withForm(Component)
