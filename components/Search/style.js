import styled from 'styled-components'

export const SearchBox = styled.div`
  position: relative;
  max-width: 346px;
  padding: 1.625em 0;
  margin: 0 auto;
`

export const Search = styled.input`
  display: ${props =>
    props.expanded || props.noResult ? 'inline-block' : 'none'};
  padding: 0.40625em;
  margin-left: 1.625em;
  vertical-align: top;
  margin-top: 0.40625em;
`
export const Link = styled.a`
  vertical-align: top;
  padding: 0.203125em 0.203125em 0.40625em 0.40625em;
  position: ${props =>
    props.noResult || props.expanded ? 'absolute' : 'static'};
  right: ${props => (props.noResult ? '0.8125em' : '0')};
  line-height: 0.8125em;
`

export const SearchResult = styled.div`
  display: none;
  padding: 0.40625em;
  background: white;
  position: absolute;
  top: 3.8125em;
  text-align: center;
  width: 320px;
  right: -0.8125em;
  box-shadow: 0px 3px 30px -5px rgba(0, 0, 0, 0.58);
  ${SearchBox}:hover &,
  ${Search}:focus + ${Link} + & {
    display: ${props => (props.expanded || props.noResult ? 'block' : 'none')};
    border: 1px solid rgb(26, 120, 34);
  }
`
export const Label = styled.p`
  color: #6ec485;
  font-size: 88%;
`

export const InlineBlock = styled.div`
  display: ${props =>
    props.expanded || props.noResult ? 'inline-block' : 'none'};
`

export const Icon = styled.img`
  height: 1.625rem;
  filter: ${props => (props.noResult || props.expanded ? 'invert(1)' : 'none')};
`
export const Select = styled.select`
  margin-top: 0.40625em;
  border-radius: 8px;
  padding: 0.40625em;
`

export const Option = styled.option``
