import { Card, Detail, PictureBox, Title, Price, Link } from './style'
import { Picture } from 'components/Theme'
import { Router } from 'router'
import { getUserStore } from 'containers/App'
import slugify from '@sindresorhus/slugify'

const Component = ({ product, currencyCode, user: { language } }) => (
  <Link
    href={`${
      Router.linkPage('product', {
        language,
        id: product.id,
        name: slugify(product.name)
      }).as
    }`}
  >
    <Card flex-phone={2} padding={0}>
      <PictureBox box-flex={0.3}>
        <Picture
          src={product.mainImageUrl}
          alt={product.name}
          size="4.875em"
          margin="0"
        />
      </PictureBox>
      <Detail box-flex={0.8}>
        <Price>
          {product.price} {currencyCode}
        </Price>
        <Title>{product.name}</Title>
      </Detail>
    </Card>
  </Link>
)

export default getUserStore(Component)
