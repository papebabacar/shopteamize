import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'

export const Link = styled.a`
  display: block;
  padding: 0.40625em 0;
  border-bottom: 1px solid #eee;
  font-size: 85%;
`

export const Card = styled(Flex)`
  background: white;
  text-align: left;
  color: #222;
`

export const PictureBox = styled(Box)``

export const Detail = styled(Box)`
  position: relative;
`

export const Title = styled.h3`
  margin: 0;
  overflow: hidden;
`
export const Label = styled.p`
  display: block;
  margin: 0.40625em;
`

export const Price = styled.p`
  display: block;
  margin: 0.40625em;
`

export const Icon = styled.img`
  width: 1.625em;
  margin: 0.40625em auto 0.40625em;
`
