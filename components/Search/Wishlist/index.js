import { Trans } from '@lingui/react'
import Timer from 'react-countdown-now'
import { Card, Detail, PictureBox, Title, Price, Icon, Link } from './style'
import { Picture } from 'components/Theme'
import { Router } from 'router'
import { getUserStore } from 'containers/App'
import slugify from '@sindresorhus/slugify'

const Component = ({ wishlist, currencyCode, user: { language } }) => (
  <Link
    href={`${
      Router.linkPage('wishlist', {
        language,
        id: wishlist.id,
        name: slugify(getTitle(wishlist.labels, language))
      }).as
    }`}
  >
    <Card flex-phone={2} padding={0}>
      <PictureBox box-flex={0.3}>
        <Picture
          src={wishlist.mainImageUrl}
          alt={getTitle(wishlist.labels, language)}
          size="4.875em"
          margin="0"
        />
      </PictureBox>
      <Detail box-flex={0.7}>
        <Title>{getTitle(wishlist.labels, language)}</Title>
        <Price>
          {wishlist.price} {currencyCode}
        </Price>
        <Icon src="/static/img/timer.png" />
        <b>
          <Timer
            date={wishlist.endDate}
            renderer={({ days, hours, minutes, seconds, completed }) =>
              completed ? (
                <Trans>Closed</Trans>
              ) : (
                `${Number(days) * 24 + Number(hours)}h ${minutes}m ${seconds}s`
              )
            }
          />
        </b>
      </Detail>
    </Card>
  </Link>
)

export default getUserStore(Component)

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
