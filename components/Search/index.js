import { Trans, withI18n } from '@lingui/react'
import {
  Link,
  Icon,
  SearchBox,
  Search,
  SearchResult,
  InlineBlock,
  Label,
  Select,
  Option
} from './style'
import { Button } from 'components/Theme'
import { Router } from 'router'
import { getUserStore, getSearchStore } from 'containers/App'
import Product from './Product'
import Wishlist from './Wishlist'

const Component = ({
  search: {
    expanded,
    currencyCode,
    processing,
    results,
    params: { filter, query: searchTerm, noResultInput }
  },
  searchItems,
  clearResults,
  setSearchFilter,
  noResult,
  expandSearch,
  i18n,
  user: { language }
}) => (
  <SearchBox>
    <InlineBlock expanded={expanded} noResult={noResult}>
      <Select
        value={filter}
        onChange={({ target: { value } }) =>
          setSearchFilter(value) &&
          searchTerm.length >= 3 &&
          searchItems({ query: searchTerm })
        }
      >
        <Option value="products">
          <Trans>Products</Trans>
        </Option>
        <Option value="wishlists">
          <Trans>Wishlists</Trans>
        </Option>
      </Select>
    </InlineBlock>
    <Search
      expanded={expanded}
      noResult={noResult}
      placeholder={i18n.t`enter your search`}
      onChange={({ target: { value: query } }) =>
        query.length >= 3
          ? searchItems({ query, noResultInput: noResult })
          : clearResults()
      }
      onKeyPress={({ key, target: { value: query } }) =>
        key === 'Enter' &&
        query.length >= 3 &&
        (document.location = `${
          filter === 'products'
            ? `${Router.linkPage('catalog', { language }).as}`
            : `${Router.linkPage('wishlists', { language }).as}`
        }?query=${searchTerm}`)
      }
    />
    <Link
      expanded={expanded}
      noResult={noResult}
      onClick={() => {
        if (noResult) {
          if (searchTerm.length >= 3 && noResultInput)
            document.location = `${
              filter === 'products'
                ? `${Router.linkPage('catalog', { language }).as}`
                : `${Router.linkPage('wishlists', { language }).as}`
            }?query=${searchTerm}`
        } else {
          if (!expanded) expandSearch(true)
          else if (!noResultInput) {
            if (!searchTerm.length) expandSearch(!expanded)
            else if (searchTerm.length >= 3)
              document.location = `${
                filter === 'products'
                  ? `${Router.linkPage('catalog', { language }).as}`
                  : `${Router.linkPage('wishlists', { language }).as}`
              }?query=${searchTerm}`
          }
        }
      }}
    >
      <Icon
        src="/static/img/search.png"
        expanded={expanded}
        noResult={noResult}
      />
    </Link>
    {searchTerm && (
      <SearchResult expanded={expanded} noResult={noResult}>
        {processing ? (
          <Label>
            <Trans>Searching</Trans>...
          </Label>
        ) : !results.length ? (
          <Label>
            <Trans>No results found</Trans>
          </Label>
        ) : (
          <>
            {results.map(
              item =>
                filter === 'products' ? (
                  <Product
                    key={item.id}
                    product={item}
                    currencyCode={currencyCode}
                  />
                ) : (
                  <Wishlist
                    key={item.id}
                    wishlist={item}
                    currencyCode={currencyCode}
                  />
                )
            )}
            <Button
              href={`${
                filter === 'products'
                  ? Router.linkPage('catalog', { language }).as
                  : Router.linkPage('wishlists', { language }).as
              }?query=${searchTerm}`}
            >
              <Trans>See all results</Trans>
            </Button>
          </>
        )}
      </SearchResult>
    )}
  </SearchBox>
)

export default getUserStore(getSearchStore(withI18n()(Component)))
