import { Trans } from '@lingui/react'
import Push from 'components/Push/WishList'

const Component = ({ items }) => (
  <Push title={<Trans>PACKS RAMADAN</Trans>} items={items} />
)

export default Component
