import { getModalStore } from 'containers/App'
import QuickView from 'components/QuickView/WishList'
import Push from 'components/Push'
import Card from 'components/WishList/Card'
import { Wishlist, Slide } from './style'

const Component = ({ title, items, modal, showModal }) =>
  items.length > 0 && (
    <Wishlist>
      <Push title={title}>
        {items.map(wishlist => (
          <Slide key={wishlist.id}>
            <Card wishlist={wishlist} showModal={showModal} />
          </Slide>
        ))}
      </Push>
      {modal.visible && modal.view === 'wishlist' && <QuickView />}
    </Wishlist>
  )
export default getModalStore(Component)
