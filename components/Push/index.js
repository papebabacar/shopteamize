import Carousel from 'nuka-carousel'
import { Push, Title, Arrow } from './style'
import Responsive from 'react-responsive'
import theme from 'components/Theme'
const {
  devices: { pad }
} = theme

const Component = ({ title, children }) => (
  <Push>
    <Title>{title} :</Title>
    <Responsive maxWidth={pad}>
      {onMobile =>
        onMobile ? (
          <Carousel
            wrapAround
            slidesToShow={2}
            slidesToScroll={2}
            renderCenterLeftControls={({ previousSlide }) => (
              <Arrow onClick={previousSlide}>‹</Arrow>
            )}
            renderCenterRightControls={({ nextSlide }) => (
              <Arrow onClick={nextSlide}>›</Arrow>
            )}
            renderBottomCenterControls={() => null}
          >
            {children}
          </Carousel>
        ) : (
          <Carousel
            wrapAround
            slidesToShow={4}
            slidesToScroll={4}
            renderCenterLeftControls={({ previousSlide }) => (
              <Arrow onClick={previousSlide}>‹</Arrow>
            )}
            renderCenterRightControls={({ nextSlide }) => (
              <Arrow onClick={nextSlide}>›</Arrow>
            )}
            renderBottomCenterControls={() => null}
          >
            {children}
          </Carousel>
        )
      }
    </Responsive>
  </Push>
)

export default Component
