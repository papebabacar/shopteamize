import { Trans } from '@lingui/react'
import Push from 'components/Push/Product'

const Component = ({ items }) => (
  <Push title={<Trans>RAMADAN PRODUCTS</Trans>} items={items} />
)

export default Component
