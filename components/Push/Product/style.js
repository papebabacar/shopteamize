import styled from 'styled-components'
import theme from 'components/Theme'
const {
  settings: { base, padding }
} = theme

export const Slide = styled.div`
  padding: ${padding(base / 2)};
`
export const Product = styled.div``
