import { getModalStore } from 'containers/App'
import QuickView from 'components/QuickView/Product'
import Push from 'components/Push'
import Card from 'components/Product/Card'
import { Product, Slide } from './style'

const Component = ({ title, items, modal, showModal }) =>
  items.length > 0 && (
    <Product>
      <Push title={title}>
        {items.map(product => (
          <Slide key={product.id}>
            <Card product={product} showModal={showModal} />
          </Slide>
        ))}
      </Push>
      {modal.visible && modal.view === 'product' && <QuickView />}
    </Product>
  )

export default getModalStore(Component)
