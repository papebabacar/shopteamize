import styled from 'styled-components'
import theme from 'components/Theme'

const {
  settings: { base, unit, padding }
} = theme

export const Push = styled.section`
  padding: ${padding(base / 2)};
`

export const Title = styled.h3`
  color: #aaa;
  padding-left: ${base + unit};
`
export const Arrow = styled.a`
  background: rgba(180, 180, 180, 0.45);
  font-size: 300%;
  font-weight: 100;
  color: #eee;
  &:hover {
    background: rgba(120, 120, 120, 0.85);
    color: #fff;
  }
`
