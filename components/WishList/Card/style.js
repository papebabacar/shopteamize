import styled, { css } from 'styled-components'
import { Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Card = styled(Box)`
  position: relative;
  display: block;
  padding: 1.625em 0.8125em;
  background: #fff;
  text-align: center;
`
export const Link = styled.a`
  display: block;
`
export const HighLight = styled.div`
  padding: 0.8125em;
  width: 100%;
  height: 100%;
  display: none;
  position: absolute;
  top: 0;
  left: 0;
  background: rgba(10, 10, 10, 0.85);
  color: white;
  text-align: center;
  hr {
    width: 70%;
    margin: 0.8125em auto;
  }
  ${props =>
    props.hover &&
    css`
      display: flex;
      flex-direction: column;
      justify-content: center;
    `};
`
export const Title = styled.h3`
  height: 2.4375em;
  margin: 0;
  overflow: hidden;
  text-overflow: -o-ellipsis-lastline;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  word-wrap: break-word;
`
export const Label = styled.p`
  display: block;
`

export const Price = styled.p`
  display: block;
  font-size: 125%;
`

export const Bold = styled.b``

export const OnlyDesk = styled.div`
  display: none;
  ${theme.media.tablet`    
    display: flex;
    flex-direction: column;
    justify-content: center;
  `};
`

export const Separator = styled.hr``

export const Icon = styled.img`
  width: 1.625em;
  margin: 0 auto;
`
