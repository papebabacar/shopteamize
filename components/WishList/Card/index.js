import { compose, withState } from 'recompose'
import { Trans } from '@lingui/react'
import { getUserStore, getCartStore } from 'containers/App'
import {
  Card,
  Link,
  HighLight,
  Title,
  Label,
  Price,
  Icon,
  Bold,
  Separator,
  OnlyDesk
} from './style'
import { Picture, Button } from 'components/Theme'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'
import dynamic from 'next/dynamic'

const Timer = dynamic(import('react-countdown-now'), {
  ssr: false,
  loading: () => null
})

const Component = ({
  wishlist,
  user: { language },
  cart: { currencyCode },
  addCartItem,
  showModal,
  hover,
  setHover
}) => (
  <Card
    onMouseEnter={() => {
      setTimeout(() => {
        setHover(true)
      }, 10)
    }}
    onMouseLeave={() => setHover(false)}
  >
    <Link
      onClick={event => {
        event.preventDefault()
        event.target.focus()
        setTimeout(() => {
          setHover(true)
        }, 1)
      }}
      href={`${
        Router.linkPage('wishlist', {
          language,
          id: wishlist.id,
          name: slugify(getTitle(wishlist.labels, language))
        }).as
      }`}
    >
      <Picture
        src={`${wishlist.mainImageUrl}`}
        alt={getTitle(wishlist.labels, language)}
        cover
      />
      <Title>{getTitle(wishlist.labels, language)}</Title>
      <Price>
        {wishlist.price} {currencyCode}
      </Price>
      <HighLight hover={hover}>
        <OnlyDesk>
          <Icon src="/static/img/timer.png" />
          <Bold>
            <Timer
              date={wishlist.endDate}
              renderer={({ days, hours, minutes, seconds, completed }) =>
                completed ? (
                  <Trans>Closed</Trans>
                ) : (
                  `${Number(days) * 24 +
                    Number(hours)}h ${minutes}m ${seconds}s`
                )
              }
            />
          </Bold>
          <Trans>Deadline</Trans>{' '}
          {getDeadLine(wishlist.endDate) > 1 && (
            <Trans>(-{getDeadLine(wishlist.endDate)} days)</Trans>
          )}
          <Separator />
          <Icon src="/static/img/share.png" />
          <Label>
            <Bold>
              {wishlist.participantsCount > wishlist.minParticipant
                ? wishlist.participantsCount
                : wishlist.minParticipant}
            </Bold>
            <br />
            <Trans>Participants</Trans>
          </Label>
          <Separator />
        </OnlyDesk>
        <Button
          transparent
          onClick={event => showModal(wishlist) && event.preventDefault()}
          padding="0"
        >
          <Icon src="/static/img/view.png" />
          <Label>
            <Trans>Preview</Trans>
          </Label>
        </Button>
        <Separator />
        <Button
          transparent
          onClick={() =>
            addCartItem(
              wishlist,
              wishlist.type === 'CUSTOMER_WISHLIST'
                ? 'wishlists'
                : 'shoppingIdeas'
            ) && showModal()
          }
          padding="0"
        >
          <Icon src="/static/img/basket.png" />
          <Label>
            <Trans>Participate</Trans>
          </Label>
        </Button>
      </HighLight>
    </Link>
  </Card>
)

export default compose(
  getUserStore,
  getCartStore,
  withState('hover', 'setHover')
)(Component)

const getDeadLine = endDate =>
  Number.parseInt(Number(new Date(endDate).getTime() - Date.now()) / 86400000) +
  1

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
