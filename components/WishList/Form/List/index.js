import { compose, lifecycle } from 'recompose'
import Product from './Product'
import {
  getCatalogStore,
  getWishlistStore,
  getModalStore
} from 'containers/App'

const ListPage = ({ items, products, fetchData }) =>
  Object.keys(items).map(id => (
    <Product
      key={id}
      id={id}
      product={{ ...products.find(product => product.id === Number(id)) }}
      quantity={items[id].quantity}
      fetchData={fetchData}
      products={products}
    />
  ))

let drake

const Component = compose(
  getCatalogStore,
  getWishlistStore,
  getModalStore,
  lifecycle({
    async componentDidMount() {
      const {
        fetchData,
        addToWishlist,
        showModal,
        catalog: { data: products }
      } = this.props
      fetchData()
      const dragula = await import('dragula')
      const dragBox = document.querySelector('.dragBox')
      const dropBox = document.querySelector('.dropBox')
      drake = dragula([dragBox, dropBox], {
        mirrorContainer: dragBox,
        copy: true
      })
      drake.on('drag', (element, target) => {
        element &&
          target &&
          target.classList.contains('dragBox') &&
          dropBox.classList.add('gu-dragging')
      })
      drake.on('drop', (element, target, source) => {
        drake.cancel(true)
        dropBox.classList.remove('gu-dragging')
        if (
          target &&
          target.classList.contains('dropBox') &&
          source.classList.contains('dragBox')
        ) {
          if (element.dataset.size) {
            showModal(
              {
                ...products.find(({ id }) => id === Number(element.dataset.id)),
                addToWishlist: true
              },
              'product'
            )
          } else {
            addToWishlist(Number(element.dataset.id), 1)
            fetchData()
          }
        }
      })
    },
    componentWillUnmount() {
      drake && drake.destroy()
    }
  })
)(ListPage)

export default Component
