import styled, { css } from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Product = styled(Flex)`
  position: relative;
  text-align: left;
  background: white;
  border-bottom: 1px solid #eee;
  &:nth-of-type(even) {
    background: #f2f2f2;
  }
  min-height: 6.25em;
`

export const Column = styled(Box)``

export const HighLight = styled.div`
  padding: 0.8125em 0.203125em;
  width: 100%;
  height: 100%;
  display: none;
  position: absolute;
  top: 0;
  left: 0;
  background: rgba(10, 10, 10, 0.85);
  color: white;
  text-align: center;
  ${props =>
    props.visible &&
    css`
      display: flex;
      flex-direction: column;
      justify-content: center;
    `};
`

export const Title = styled.h3`
  padding: 0 0.203125em;
  min-height: 4.46875em;
  ${theme.media.laptop`font-size: 88%;`};
`
export const Label = styled.p`
  padding: 0 0.203125em;
  margin-bottom: 0.203125em;
  display: block;
  font-weight: 900;
  ${theme.media.laptop`font-size: 88%;`};
`

export const Price = styled.p``

export const Close = styled.a`
  position: absolute;
  right: 0.203125em;
  top: 0.203125em;
  padding: 0 0.40625em;
`

export const Action = styled.a`
  font-size: 200%;
  line-height: 1.125em;
  display: none;
  ${Product}:hover & {
    display: block;
  }
  ${theme.media.laptop`
    font-size: 170%;
    line-height: 1em;
  `};
`
export const Star = styled(Action)`
  color: ${props => (props.isMain ? 'rgb(26,120,34)' : '#bbb')};
  display: ${props => (props.isMain ? 'block !important' : 'none')};
`
export const Update = styled.img`
  width: 0.625em;
`
export const Delete = styled.img`
  width: 0.625em;
`
