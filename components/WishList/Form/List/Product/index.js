import { Trans, withI18n } from '@lingui/react'
import { compose, withState, withHandlers } from 'recompose'
import {
  Product,
  Column,
  HighLight,
  Title,
  Label,
  Action,
  Star,
  Update,
  Delete
} from './style'
import { Picture, MiniButton, Size, NumericInput } from 'components/Theme'
import { getWishlistStore } from 'containers/App'

const ListItem = ({
  product: { id, name, price, mainImageUrl, size, variants = [] },
  wishlist: { currencyCode, mainItemId },
  quantity,
  setWishlist,
  addToWishlist,
  updateWishlistItem,
  removeFromWishlist,
  quantityToAdd,
  setQuantityToAdd,
  view,
  showView,
  selected,
  setSelected,
  i18n,
  fetchData
}) => (
  <Product data-id={id} data-size={size} isMain={id === mainItemId} padding={0}>
    <Column box-flex={0.2}>
      <Picture src={mainImageUrl} alt={name} size="100%" margin="0" cover />
    </Column>
    <Column box-flex={0.75}>
      <Label>
        {quantity} x {price} {currencyCode}
      </Label>
      <Title>
        {name}
        {size && ` - ${i18n.t`Size`} ${size}`}
      </Title>
    </Column>
    <Column box-flex={0.05}>
      <Star
        isMain={id === mainItemId}
        onClick={() => setWishlist({ mainItemId: id })}
        title={i18n.t`Main product`}
      >
        ★
      </Star>
      <Action
        onClick={() => {
          setQuantityToAdd(quantity)
          showView('update')
        }}
        title={i18n.t`Edit`}
      >
        <Update src="/static/img/edit.png" />
      </Action>
      <Action onClick={() => showView('delete')} title={i18n.t`Delete`}>
        <Delete src="/static/img/delete.png" />
      </Action>
    </Column>
    <HighLight visible={view === 'update'}>
      <Label>
        {size ? <Trans>Qty</Trans> : <Trans>Quantity</Trans>}
        {' : '}
        <NumericInput
          max-width="4em"
          padding="0.203125em"
          center
          value={quantityToAdd}
          onChange={event => {
            setQuantityToAdd(Number(event.target.value))
          }}
        />
        {size && (
          <>
            {' '}
            <Trans>Size</Trans>
            {' : '}
            <Size
              max-width="7.5em"
              padding="0.203125em"
              value={selected}
              onChange={event => setSelected(Number(event.target.value))}
              options={variants
                .reduce(
                  (sizes, { id: variantId, size }) => [
                    ...sizes,
                    { label: size.toUpperCase(), value: variantId }
                  ],
                  [{ label: size.toUpperCase(), value: id }]
                )
                .sort(
                  ({ label: label1 }, { label: label2 }) => label1 > label2
                )}
            />
          </>
        )}
      </Label>
      <Column>
        <MiniButton secondary onClick={() => showView()}>
          <Trans>Cancel</Trans>
        </MiniButton>
        <MiniButton
          onClick={async () => {
            if (Number(selected) !== Number(id)) {
              id === mainItemId && setWishlist({ mainItemId: selected })
              addToWishlist(selected, quantityToAdd)
              await removeFromWishlist(id)
              fetchData()
            } else {
              updateWishlistItem(id, quantityToAdd)
            }
            showView()
          }}
        >
          <Trans>Validate</Trans>
        </MiniButton>
      </Column>
    </HighLight>
    <HighLight visible={view === 'delete'}>
      <Label>
        <Trans>Confirm deletion</Trans>
      </Label>
      <Column>
        <MiniButton secondary onClick={() => showView()}>
          <Trans>Cancel</Trans>
        </MiniButton>
        <MiniButton onClick={() => removeFromWishlist(id) && showView()}>
          <Trans>Delete</Trans>
        </MiniButton>
      </Column>
    </HighLight>
  </Product>
)

const Component = compose(
  withI18n(),
  getWishlistStore,
  withState('view', 'showView'),
  withState('quantityToAdd', 'setQuantityToAdd', ({ quantity }) => quantity),
  withState('selected', 'setSelected', ({ id }) => id),
  withHandlers({
    getSelected: ({ product, selected }) => () => {
      const { variants = [] } = product
      return { ...product, ...variants.find(({ id }) => id === selected) }
    }
  })
)(ListItem)

export default Component
