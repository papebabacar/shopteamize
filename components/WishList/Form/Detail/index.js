import { Trans, withI18n } from '@lingui/react'
import { compose, lifecycle, withState, withHandlers } from 'recompose'
import { getUserStore, getCartStore, getWishlistStore } from 'containers/App'
import { addValidationRule } from 'formsy-react'
import { Detail, Title, Actions, Close } from './style'
import { SubmitButton, Notification } from 'components/Theme'
import { Form, withForm } from 'components/Theme/Form'
import Field from 'components/Theme/Form/Field'
import {
  getUserPendingWishlist,
  createWishlist,
  updateWishlist
} from 'api/muhajir/wishlist'
import { getUser } from 'api/account'

const wishlist = ({
  user: { language },
  wishlist,
  setWishlist,
  children,
  post,
  submitting,
  submitError,
  i18n
}) => (
  <Form post={post} submitting={submitting}>
    <Close onClick={() => setWishlist({ visible: false })}>X</Close>
    <Detail flex-no-gutter flex-phone={1}>
      <Title>
        {wishlist.id ? (
          <Trans>Edit your wishlist</Trans>
        ) : (
          <Trans>Create your new wishlist</Trans>
        )}
      </Title>
      <Field
        name="title"
        label={<Trans>Title</Trans>}
        required
        value={wishlist.title}
        blurAction={setWishlist}
        validations={{
          minLength: 5,
          maxLength: 35
        }}
        validationError={i18n.t`Please enter at least 5 to 35 characters`}
        title={i18n.t`Please give a title to this wishlist`}
      />
      <Field
        name="minParticipant"
        label="Participants"
        required
        type="number"
        min="3"
        max="10"
        value={wishlist.minParticipant}
        blurAction={setWishlist}
        validations="isInt,isValidParticipants"
        validationError={i18n.t`Please enter a number at between 3 and 10`}
        title={i18n.t`Please give the number of participants`}
      />
      <Field
        name="endDate"
        label={<Trans>End date</Trans>}
        required
        type="date"
        value={wishlist.endDate}
        blurAction={setWishlist}
        validations="isValidDate"
        validationError={i18n.t`Please select a date between the next seven days`}
        language={language}
        title={i18n.t`Please give an end date`}
      />
      {children}
      <Actions>
        <>
          {submitError && <Notification>{submitError}</Notification>}
          <SubmitButton disabled={submitting}>OK</SubmitButton>
        </>
      </Actions>
    </Detail>
  </Form>
)

const Component = compose(
  withForm,
  getUserStore,
  getCartStore,
  getWishlistStore,
  withI18n(),
  withState('submitError', 'setSubmitError'),
  withHandlers({
    post: ({
      cart: { pendingWishlist },
      user: { token, id: idUserInitiator, language: isoCodeLang },
      wishlist: { id, items, mainItemId: mainImageSku },
      products,
      setWishlist,
      setFormState,
      setSubmitError,
      i18n,
      removeCartItem,
      addCartItem,
      showModal,
      logoutUser
    }) => async ({ title, minParticipant, endDate }) => {
      if (Object.keys(items).length < 2) {
        setTimeout(() => setSubmitError(), 5000)
        return setSubmitError(
          i18n.t`Your wishlist should contain at least two products.`
        )
      }
      try {
        await getUser(token)
      } catch (error) {
        setWishlist({ submitMode: true })
        logoutUser()
        return
      }
      setFormState(true)
      try {
        const wishlist = {
          idUserInitiator,
          minParticipant,
          mainImageSku,
          items: Object.keys(items).map(id => {
            const {
              categories,
              name,
              price,
              weight,
              currencyCode,
              currencyRate,
              mainImageUrl
            } = products.find(product => product.id === Number(id))
            return {
              id,
              categories,
              mainImageUrl,
              name,
              price,
              weight,
              currencyCode,
              currencyRate,
              quantity: items[id].quantity
            }
          }),
          startDate: new Date(),
          endDate: new Date(new Date(endDate).setHours(23, 59, 59)),
          labels: [
            {
              title,
              isoCodeLang
            }
          ],
          type: 'CUSTOMER_WISHLIST'
        }
        const { data: newPendingWishlist } = id
          ? await updateWishlist({ ...wishlist, id })
          : await createWishlist(wishlist)
        setWishlist({ visible: false, submitMode: false })
        showModal(null)
        setFormState(false)
        if (Object.keys(pendingWishlist).length) {
          await removeCartItem(newPendingWishlist.id, 'pendingWishlist')
          await addCartItem(newPendingWishlist, 'wishlists')
        }
      } catch (error) {
        setWishlist({ submitMode: false })
        setFormState(false)
        setSubmitError(
          i18n.t`A technical error has occurred. Please reload the page`
        )
        setTimeout(() => setSubmitError(), 5000)
      }
    }
  }),
  lifecycle({
    async componentDidMount() {
      const {
        user: { id, language },
        setWishlist,
        submitMode,
        post
      } = this.props
      try {
        const { data: wishlist } = await getUserPendingWishlist(id, {
          lang: language
        })
        wishlist &&
          setWishlist({
            id: wishlist.id
          })
      } catch (error) {
        // do something
      }
      const { title, minParticipant, endDate } = this.props.wishlist
      submitMode && post({ title, minParticipant, endDate })
    },
    async UNSAFE_componentWillMount() {
      addValidationRule(
        'isValidParticipants',
        (values, number) => number >= 3 && number <= 10
      )
      addValidationRule('isValidDate', (values, date) => {
        const dateDiff = new Date(date).getTime() - Date.now()
        return dateDiff > 0 && dateDiff < 7 * 24 * 60 * 60 * 1000
      })
    }
  })
)(wishlist)

export default Component
