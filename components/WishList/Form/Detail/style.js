import styled from 'styled-components'

export const Detail = styled.section`
  position: relative;
  text-align: center;
`

export const Title = styled.h3`
  text-align: center;
`
export const Actions = styled.div`
  margin-top: 0.8125em;
`

export const Close = styled.a`
  position: absolute;
  padding: 0.203125em 0.40625em;
  background: #eee;
  top: 0.203125em;
  right: 0.203125em;
  z-index: 9999;
`
