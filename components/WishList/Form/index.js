import { compose } from 'recompose'
import { Trans } from '@lingui/react'
import {
  WishlistZone,
  DropZone,
  DropBox,
  ButtonToggle,
  Info,
  Image,
  Span
} from './style'
import Detail from './Detail'
import List from './List'
import { getUserStore, getWishlistStore, getModalStore } from 'containers/App'
import { Hidden, withHiddenState } from 'reas'
const { Toggle } = Hidden
import theme from 'components/Theme'
import QuickView from 'components/QuickView/WishList'
const {
  devices: { laptop }
} = theme
import Login from 'components/Login'
import Responsive from 'react-responsive'

const Form = ({
  user: { isLogged },
  wishlist: { items, visible, submitMode },
  products,
  fetchData,
  modal,
  showModal
}) => (
  <WishlistZone visible={visible}>
    <DropZone>
      {!isLogged && submitMode && <Login padding="1.625em 0" center />}
      {(isLogged || (!isLogged && !submitMode)) && (
        <Detail
          products={products}
          showModal={showModal}
          submitMode={submitMode}
          fetchData={fetchData}
        >
          <Products items={items} products={products} fetchData={fetchData} />
        </Detail>
      )}
    </DropZone>
    {modal.visible && modal.view === 'wishlist' && <QuickView />}
  </WishlistZone>
)

const Component = compose(
  getUserStore,
  getWishlistStore,
  getModalStore
)(Form)

export default Component

const Products = withHiddenState(({ hidden, items, products, fetchData }) => (
  <>
    <Responsive maxWidth={laptop}>
      {onMobile => (
        <>
          {onMobile && (
            <Toggle as={ButtonToggle} {...hidden}>
              {hidden.visible ? (
                <Span>
                  <Trans>Hide my products</Trans>
                </Span>
              ) : (
                <Span>
                  <Trans>See my products</Trans>
                </Span>
              )}
            </Toggle>
          )}
          <Hidden
            as={DropBox}
            {...hidden}
            destroy
            visible={onMobile ? hidden.visible : true}
          >
            {Object.keys(items).length === 0 && (
              <Info>
                <Image src="/static/img/drag.png" alt="drag" />{' '}
                <Trans>Drag and drop your products here</Trans>
              </Info>
            )}
            <List items={items} fetchData={fetchData} products={products} />
          </Hidden>
        </>
      )}
    </Responsive>
  </>
))
