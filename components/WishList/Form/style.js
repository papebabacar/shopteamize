import styled, { css } from 'styled-components'
import { Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const WishlistZone = styled(Box)`
  order: -1;
  transition: 60ms all ease;
  position: sticky;
  top: 0;
  ${props =>
    !props.visible &&
    css`
      max-height: 0;
      overflow: hidden;
    `} ${theme.media.laptop`
    height: 100vh;
    order: 1;    
    flex:0.3;
    ${props =>
      !props.visible &&
      css`
        max-height: auto;
        max-width: 0;
        overflow: hidden;
      `}
  `};
  ${theme.media.desktop`    
    flex:0.25;
  `};
`

export const DropZone = styled.section`
  background: #fff;
  height: 100%;
  padding: 0.8125em 0.203125em;
`

export const DropBox = styled.section.attrs({ className: 'dropBox' })`
  margin-top: 0.8125em;
  border: 2px dashed #f2f2f2 !important;
  height: 53vh;
  overflow-y: auto;
  background: #fff;
  position: relative;
  .gu-transit {
    display: none !important;
  }
  &.gu-dragging {
    border: 3px dashed rgb(120, 207, 139) !important;
  }
`
export const HelperZone = styled.p`
  height: 100%;
  background: #f2f2f2;
  text-align: center;
  padding: 55% 1.625em 0;
  margin: 0;
  width: 100%;
`
export const ButtonToggle = styled.a`
  position: relative;
  display: inline-block;
  margin: 0.8125em auto;
  padding: 0.40625em 0.8125em;
  color: white;
  background: rgb(120, 207, 139);
  border-radius: 0.8125em;
`
export const Span = styled.span`
  vertical-align: top;
  line-height: 1.40625em;
  font-size: 125%;
`
export const Info = styled.p`
  position: absolute;
  margin-top: 18vh;
  text-align: center;
  width: 100%;
  display: none;
  ${DropBox}.gu-dragging & {
    display: none;
  }
  ${theme.media.laptop`
    display: block;
  `};
`
export const Image = styled.img`
  width: 3.25em;
  display: block;
  margin: 0 auto;
  border-radius: 50%;
`
