import { Breadcrumb, BreadcrumbLink, BreadcrumbItem } from './style'
import { getUserStore } from 'containers/App'
import { Router } from 'router'

const Component = ({
  wishlist,
  category: { code, name: category },
  user: { language }
}) => (
  <Breadcrumb flex-width="100%">
    <BreadcrumbLink href={`${Router.linkPage('index', { language }).as}`}>
      Accueil
    </BreadcrumbLink>
    <BreadcrumbLink
      href={`${Router.linkPage('wishlists', { language }).as}?category=${code}`}
    >
      Envies shopping {category}
    </BreadcrumbLink>
    <BreadcrumbItem>{wishlist}</BreadcrumbItem>
  </Breadcrumb>
)

export default getUserStore(Component)
