import styled from 'styled-components'
import theme from 'components/Theme'

const {
  settings: { padding, base }
} = theme

export const Breadcrumb = styled.div`
  padding: ${padding(base / 2, base)};
  > *:last-child {
    font-weight: 900;
    &:after {
      display: none;
    }
  }
  ${theme.media.tablet`background: #fff;`};
  ${theme.media.laptop`padding-left: 10vw;`};
`

export const BreadcrumbItem = styled.span`
  padding: 0;
  color: #888;
  display: inline-block;
  &:after {
    content: '›';
    display: inline-block;
    padding: 0 0.40625em;
  }
`
export const BreadcrumbLink = styled(BreadcrumbItem.withComponent('a'))``
