import { compose } from 'recompose'
import { Trans } from '@lingui/react'
import Timer from 'react-countdown-now'
import { getUserStore, getModalStore, getCartStore } from 'containers/App'
import { Router } from 'router'
import {
  Wishlist,
  Block,
  Title,
  Label,
  Description,
  Price,
  CountDown,
  Members,
  Icon,
  Actions,
  ButtonList
} from './style'
import { Button } from 'components/Theme'
import slugify from '@sindresorhus/slugify'
import {
  initializeWishlistClick,
  initializeWishlistAddToCartClick
} from 'components/Theme/GTM'

const Component = ({
  wishlist,
  cart: { currencyCode },
  addCartItem,
  showModal,
  quickView,
  user: { language }
}) => (
  <Wishlist flex-phone={1} flex-tablet={2} quickView={quickView}>
    <Block>
      <Price>
        {wishlist.price} {currencyCode}
      </Price>
      <Title>{getTitle(wishlist.labels, language)}</Title>
      <Label>
        <Trans>Team</Trans>{' '}
        {wishlist.addressInitiator
          ? `${wishlist.addressInitiator.firstName} ${
              wishlist.addressInitiator.lastName
            }`
          : 'Shopteamize'}
      </Label>
      <Description quickView={quickView}>
        {wishlist.labels.length > 1
          ? wishlist.labels.find(({ isoCodeLang }) => isoCodeLang === language)
              .description
          : wishlist.labels[0].description}
      </Description>
    </Block>
    <Block>
      <CountDown>
        <Icon src="/static/img/timer-black.png" alt="account" />
        <b>
          <Timer
            date={wishlist.endDate}
            renderer={({ days, hours, minutes, seconds, completed }) =>
              completed ? (
                <Trans>Closed</Trans>
              ) : (
                `${Number(days) * 24 + Number(hours)}h ${minutes}m ${seconds}s`
              )
            }
          />
        </b>
        <br />
        <Trans>Deadline</Trans>{' '}
        {getDeadLine(wishlist.endDate) > 1 && (
          <Trans>(-{getDeadLine(wishlist.endDate)} days)</Trans>
        )}
      </CountDown>
      <Members>
        <Icon src="/static/img/share-black.png" alt="account" />
        <b>
          {wishlist.participantsCount > wishlist.minParticipant
            ? wishlist.participantsCount
            : wishlist.minParticipant}
        </b>
        <br />
        <Trans>Participants</Trans>
      </Members>
      <Actions>
        <Button
          block
          onClick={() =>
            addCartItem(
              wishlist,
              wishlist.type === 'CUSTOMER_WISHLIST'
                ? 'wishlists'
                : 'shoppingIdeas'
            ) &&
            showModal() &&
            initializeWishlistAddToCartClick(wishlist)
          }
        >
          <Trans>PARTICIPATE AT THIS WISHLIST</Trans>
        </Button>
        {quickView && (
          <ButtonList
            block
            onClick={() => {
              initializeWishlistClick(wishlist)
            }}
            href={`${
              Router.linkPage('wishlist', {
                language,
                id: wishlist.id,
                name: slugify(getTitle(wishlist.labels, language))
              }).as
            }`}
          >
            <Trans>SEE WISHLIST'S DETAILS</Trans>
          </ButtonList>
        )}
      </Actions>
    </Block>
  </Wishlist>
)

export default compose(
  getUserStore,
  getCartStore,
  getModalStore
)(Component)

const getDeadLine = endDate =>
  Number.parseInt(Number(new Date(endDate).getTime() - Date.now()) / 86400000) +
  1

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
