import styled, { css } from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme, { Button } from 'components/Theme'

export const Wishlist = styled(Flex)`
  background: white;
  ${theme.media.laptop`padding: ${props =>
    props.quickView ? '1.625em 4.875em 0' : '0 12.5vw'} ;`};
  ${theme.media.desktop`padding: ${props =>
    props.quickView ? '1.625em 4.875em 0' : '0 17.5vw'} ;`};
`

export const Block = styled(Box)``

export const Title = styled.h1``

export const Label = styled.p``

export const Description = styled.p`
  ${props =>
    props.quickView &&
    css`
      max-height: 9.4375em;
      margin: 0;
      overflow: hidden;
      text-overflow: -o-ellipsis-lastline;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 6;
      -webkit-box-orient: vertical;
      word-wrap: break-word;
    `};
`

export const Price = styled(Description)`
  font-weight: 900;
  font-family: 'Arimo';
  font-size: 115%;
  margin: 0;
`

export const Actions = styled.div`
  margin: 3.25em 0 0.8125em 0;
`

export const ButtonList = styled(Button)`
  background: rgb(110, 196, 133);
  margin-bottom: 0;
`

export const Members = styled.div`
  display: inline-block;
  font-size: 115%;
  width: 50%;
  color: #888;
  text-align: center;
  border-right: 1px solid #ccc;
  b {
    color: #222;
  }
  font-size: 95%;
`
export const CountDown = styled(Members)`
  border-left: 1px solid #ccc;
`
export const Icon = styled.img`
  width: 2.4375em;
  height: 2.4375em;
  object-fit: contain;
  margin: 0.203125em auto;
  display: block;
`
