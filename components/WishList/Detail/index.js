import BreadCrumb from './BreadCrumb'
import Product from './Product'
import Info from './Info'
import Share from './Share'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'
import { initializeWishlist } from 'components/Theme/GTM'
import { compose, lifecycle } from 'recompose'
import { getUserStore, getModalStore } from 'containers/App'

const WishlistPage = ({ wishlist, user: { language } }) => (
  <>
    <BreadCrumb
      wishlist={getTitle(wishlist.labels, language)}
      category={wishlist.category}
    />
    <Info wishlist={wishlist} />
    <Share
      // eslint-disable-next-line no-undef
      shareUrl={`${APP_URL}${
        Router.linkPage('wishlist', {
          language,
          id: wishlist.id,
          name: slugify(getTitle(wishlist.labels, language))
        }).as
      }`}
      image={wishlist.mainImageUrl}
    />
    <Product
      mainItemId={wishlist.mainImageSku}
      items={wishlist.items}
      language={language}
    />
  </>
)
const Component = compose(
  getUserStore,
  getModalStore,
  lifecycle({
    async componentDidMount() {
      const { wishlist } = this.props
      initializeWishlist(wishlist)
    }
  })
)(WishlistPage)

export default getModalStore(Component)

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
