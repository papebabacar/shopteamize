import { ShareButtons, generateShareIcon } from 'react-share'
import { Share, Social } from './style'
import { initializeFacebookShare } from 'components/Theme/GTM'
import { initializeTwitterShare } from 'components/Theme/GTM'
import { getUserStore } from 'containers/App'

const Component = ({ shareUrl, image, inline, user: { language } }) => (
  <Share inline={inline}>
    <Social>
      <FacebookShareButton
        url={shareUrl}
        quote={getTitle(language)}
        onClick={() => initializeFacebookShare(shareUrl)}
      >
        <FacebookIcon size={48} />
      </FacebookShareButton>
    </Social>
    <Social>
      <TwitterShareButton
        url={shareUrl}
        title={getTitle(language)}
        onClick={() => initializeTwitterShare(shareUrl)}
      >
        <TwitterIcon size={48} />
      </TwitterShareButton>
    </Social>
    <Social>
      <GooglePlusShareButton url={shareUrl}>
        <GooglePlusIcon size={48} />
      </GooglePlusShareButton>
    </Social>
    <Social>
      <PinterestShareButton
        url={shareUrl}
        media={image}
        windowWidth={1000}
        windowHeight={730}
      >
        <PinterestIcon size={48} />
      </PinterestShareButton>
    </Social>
    <Social>
      <WhatsappShareButton
        url={shareUrl}
        title={getTitle(language)}
        separator=":: "
      >
        <WhatsappIcon size={48} />
      </WhatsappShareButton>
    </Social>
    <Social>
      <EmailShareButton
        url={shareUrl}
        subject={getTitle(language)}
        body={shareUrl}
      >
        <EmailIcon size={48} />
      </EmailShareButton>
    </Social>
  </Share>
)

export default getUserStore(Component)

const getTitle = language =>
  language === 'fr'
    ? 'je te recommande cette envie shopping'
    : 'I suggest you this wishlist'

const {
  FacebookShareButton,
  GooglePlusShareButton,
  TwitterShareButton,
  PinterestShareButton,
  WhatsappShareButton,
  EmailShareButton
} = ShareButtons

const FacebookIcon = generateShareIcon('facebook')
const TwitterIcon = generateShareIcon('twitter')
const GooglePlusIcon = generateShareIcon('google')
const PinterestIcon = generateShareIcon('pinterest')
const WhatsappIcon = generateShareIcon('whatsapp')
const EmailIcon = generateShareIcon('email')
