import styled from 'styled-components'
import theme from 'components/Theme'

export const Share = styled.div`
  text-align: center;
  ${theme.media.laptop`
    position: ${props => (props.inline ? 'static' : 'absolute')};
    top: 30%;
    transform: translateY(-30%);
    display: inline-flex;
    flex-direction: ${props => (props.inline ? 'row' : 'column')};
  `};
`

export const Social = styled.div`
  display: inline-block;
  cursor: pointer;
`
