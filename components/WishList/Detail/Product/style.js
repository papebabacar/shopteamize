import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import styled from 'styled-components'
import theme from 'components/Theme'

export const Section = styled(Flex)`
  ${theme.media.tablet`padding: ${props =>
    props.quickView ? '0 1.625em' : '0 12.5vw'} ;`};
  ${theme.media.laptop`padding: ${props =>
    props.quickView ? '0 4.875em' : '0 12.5vw'} ;`};
  ${theme.media.desktop`padding: ${props =>
    props.quickView ? '0 4.875em' : '0 17.5vw'} ;`};
`

export const Product = styled(Box)``
