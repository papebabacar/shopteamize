import { compose, withState } from 'recompose'
import { Section, Product } from './style'
import Loader from 'components/Theme/Layout/Loader'
import Info from 'components/Product/Detail/Info'
import Carousel from 'components/Product/Detail/Carousel'

const Detail = ({ mainItemId, items, setIndex, currentIndex, quickView }) => {
  return items.length > 0 ? (
    <Section padding={0} flex-phone={1} flex-tablet={2} quickView={quickView}>
      <Product>
        <Carousel
          invert
          withActions={false}
          quickView={quickView}
          onSlide={setIndex}
          mainImageUrl={
            items.find(product => product.id === mainItemId).mainImageUrl
          }
          images={items
            .filter(product => product.id !== mainItemId)
            .reduce((images, product) => [...images, product.mainImageUrl], [])}
        />
      </Product>
      <Product>
        <Info
          product={
            items
              .filter(product => product.id !== mainItemId)
              .reduce((images, product) => [...images, product], [
                items.find(product => product.id === mainItemId)
              ])[currentIndex]
          }
          withActions={false}
        />
      </Product>
    </Section>
  ) : (
    <Section>
      <Loader />
    </Section>
  )
}

const Component = compose(withState('currentIndex', 'setIndex', 0))(Detail)

export default Component
