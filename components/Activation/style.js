import styled from 'styled-components'

export const Activation = styled.section`
  display: block;
  margin: 0 auto;
  max-width: 480px;
  padding: 4.875em 0;
`

export const Label = styled.p``

export const Link = styled.a`
  padding: 1.625em;
  text-align: center;
  display: block;
  color: #4040ec;
`
