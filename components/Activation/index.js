import { Trans } from '@lingui/react'
import { compose, withState, lifecycle } from 'recompose'
import { Activation, Label } from './style'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const ActivationPage = ({ success, timer }) => (
  <Activation>
    {success && (
      <Label>
        <Trans>Your account have been successfully activated.</Trans>
      </Label>
    )}
    {!success && (
      <Label>
        <Trans>Your activation link has expired.</Trans>
      </Label>
    )}
    <Label>
      <Trans>You will be redirected in {timer} seconds.</Trans>
    </Label>
  </Activation>
)

const Component = compose(
  getUserStore,
  withState('timer', 'setTimer', 5),
  lifecycle({
    componentDidMount() {
      let timer = 5
      const {
        setTimer,
        user: { language }
      } = this.props
      setInterval(() => {
        setTimer(timer <= 0 ? timer : --timer)
      }, 1000)
      setTimeout(() => {
        document.location = `${Router.linkPage('login', { language }).as}`
      }, timer * 1000)
    }
  })
)(ActivationPage)

export default Component
