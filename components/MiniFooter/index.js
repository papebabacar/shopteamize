import { Trans } from '@lingui/react'
import { Section, Widget, Label, Icon } from './style'

const Component = () => (
  <Section flex-phone={1} flex-tablet={3}>
    <Widget>
      <Label>
        <Icon src="/static/img/check.png" />
        <Trans>Secured payment</Trans>
      </Label>
    </Widget>
    <Widget>
      <Label>
        <Icon src="/static/img/check.png" />
        <Trans>Delivery at home by DHL</Trans>
      </Label>
    </Widget>
    <Widget>
      <Label>
        <Icon src="/static/img/check.png" />
        <Trans>7 days to change your mind</Trans>
      </Label>
    </Widget>
  </Section>
)

export default Component
