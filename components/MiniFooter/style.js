import styled from 'styled-components'
import theme from 'components/Theme'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'

const { dark, light } = theme

export const Section = styled(Flex)`
  background: ${dark};
  color: ${light};
  text-align: center;
  font-size: 80%;
`
export const Widget = styled(Box)``

export const Label = styled.p`
  margin-top: 0.8125em;
`

export const Icon = styled.img`
  height: 1.125rem;
  vertical-align: top;
  margin: 0 0.40625rem 0 0;
`
