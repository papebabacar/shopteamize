import React from 'react'
import ReactDOM from 'react-dom'
import paypal from 'paypal-checkout'
import { preparePayment } from 'api/muhajir/payment'

const PaypalButton = paypal.Button.driver('react', { React, ReactDOM })

export default ({ onAuthorize, locale }) => (
  <PaypalButton
    env="production"
    commit={true}
    payment={payment}
    onAuthorize={data => onAuthorize(data)}
    locale={locale}
    style={{
      size: 'medium'
    }}
  />
)

const payment = async () => {
  try {
    const {
      data: { id }
    } = await preparePayment()
    return id
  } catch (error) {
    //console.log(error)
  }
}
