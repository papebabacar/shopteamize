import { css } from 'styled-components'
import createTheme from 'styled-components-theme'

export const colors = {
  white: '#FFF',
  light: '#EFF3F2',
  black: '#111',
  dark: '#202c28',
  gray1: '#f2f2f2',
  gray2: '#F2F2F2',
  green1: '#00AD36',
  green2: '#007C24',
  green3: '#00C23F',
  green4: '#008326',
  green5: '#78CF8B',
  green6: '#019853',
  green7: '#6EC485',
  pink: '#FF12F4'
}

export const breakpoints = {
  phone: {
    max: 550
  },
  tablet: {
    min: 550,
    max: 750
  },
  pad: {
    min: 750,
    max: 960
  },
  laptop: {
    min: 960,
    max: 1200
  },
  desktop: {
    min: 1200,
    max: 1920
  },
  large: {
    min: 1200
  }
}

const devices = {
  phone: 0,
  tablet: 550,
  pad: 750,
  laptop: 960,
  desktop: 1280,
  large: 1920
}

const settings = {
  base: 1.625,
  unit: 'em',
  padding: (top = settings.base, right = top, bottom = top, left = right) =>
    `${top}${settings.unit} ${right}${settings.unit} ${bottom}${
      settings.unit
    } ${left}${settings.unit}`
}

const media = Object.keys(devices).reduce((mediaqueries, device) => {
  mediaqueries[device] = (...queries) => css`
    @media (min-width: ${devices[device]}px) {
      ${css(...queries)};
    }
  `
  return mediaqueries
}, {})

const theme = {
  media,
  devices,
  settings,
  ...createTheme(...Object.keys(colors))
}

export { Button } from './UI'
export { MiniButton } from './UI'
export { Size } from './UI'
export { SubmitButton } from './UI'
export { AccountForm } from './UI'
export { FormTitle } from './UI'
export { FormLabel } from './UI'
export { Link } from './UI'
export { Picture } from './UI'
export { Notification } from './UI'
export { Article } from './UI'
export { NumericInput } from './UI'
export { Tabs } from './UI/Tabs'
export default theme
