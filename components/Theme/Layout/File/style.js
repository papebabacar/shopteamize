import styled from 'styled-components'

export const Dropzone = styled.section`
  width: 100%;
  .files-dropzone-list {
    display: table-cell;
    vertical-align: middle;
    text-align: center;
    padding: 10px;
    border: 1px dashed #d3d3d3;
    cursor: pointer;
  }
  .files-list ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }
  .files-list-item {
    padding: 0.40625em;
    border: 1px solid #eee;
  }
  .files-list-item-preview {
    height: 60px;
    width: 60px;
    display: inline-block;
  }
  .files-list-item-preview-image {
    height: 100%;
    width: 100%;
    object-fit: contain;
  }
  .files-list-item-remove {
    height: 24px;
    width: 24px;
    display: inline-block;
    cursor: pointer;
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB3aWR0aD0iNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTM4IDEyLjgzbC0yLjgzLTIuODMtMTEuMTcgMTEuMTctMTEuMTctMTEuMTctMi44MyAyLjgzIDExLjE3IDExLjE3LTExLjE3IDExLjE3IDIuODMgMi44MyAxMS4xNy0xMS4xNyAxMS4xNyAxMS4xNyAyLjgzLTIuODMtMTEuMTctMTEuMTd6Ii8+PHBhdGggZD0iTTAgMGg0OHY0OGgtNDh6IiBmaWxsPSJub25lIi8+PC9zdmc+)
      no-repeat center center;
    background-size: 30px 30px;
    margin-top: 1em;
    float: right;
  }
`
export const Button = styled.a`
  display: inline-block;
  background: #eee;
  padding: 0.40625em;
  margin: 0.8125em 0;
  & + & {
    margin-left: 0.40625em;
  }
`
