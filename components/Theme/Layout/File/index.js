import { Trans } from '@lingui/react'
import { Dropzone, Button } from './style'
import Files from 'react-files'
import { Component, createRef } from 'react'

export default class File extends Component {
  constructor(props) {
    super(props)
    this.myRef = createRef()
    this.onChange = props.onChange
    this.state = {
      files: []
    }
  }

  onFilesChange = files => {
    this.onChange(files)
    this.setState({
      files
    })
  }

  filesRemoveOne = file => () => {
    this.myRef.current.removeFile(file)
  }

  filesRemoveAll = () => {
    this.myRef.current.removeFiles()
  }

  filesUpload = () => {
    this.myRef.current.openFileChooser()
  }

  render() {
    return (
      <Dropzone>
        <Files
          ref={this.myRef}
          className="files-dropzone-list"
          style={{ height: '100px' }}
          onChange={this.onFilesChange}
          onError={this.onFilesError}
          multiple
          maxFiles={10}
          maxFileSize={10000000}
          minFileSize={0}
          clickable
        >
          <Trans>Drop files here or click to upload</Trans>
        </Files>
        <Button onClick={this.filesRemoveAll}>
          <Trans>Remove All</Trans>
        </Button>
        <Button onClick={this.filesUpload}>
          <Trans>upload</Trans>
        </Button>
        {this.state.files.length > 0 && (
          <div className="files-list">
            <ul>
              {this.state.files.map(file => (
                <li className="files-list-item" key={file.id}>
                  <div className="files-list-item-preview">
                    {file.preview.type === 'image' ? (
                      <img
                        className="files-list-item-preview-image"
                        src={file.preview.url}
                      />
                    ) : (
                      <div className="files-list-item-preview-extension">
                        {file.extension}
                      </div>
                    )}
                  </div>
                  <a
                    id={file.id}
                    className="files-list-item-remove"
                    onClick={this.filesRemoveOne(file)}
                  />
                </li>
              ))}
            </ul>
          </div>
        )}
      </Dropzone>
    )
  }
}
