import styled, { css } from 'styled-components'
import theme from 'components/Theme'
import { space, maxWidth, color } from 'styled-system'

const {
  settings: { base: gutter, unit }
} = theme

export const Box = styled.div`
  ${props =>
    props['box-flex'] &&
    css`
      flex: ${props['box-flex']};
    `};
  ${props =>
    props['box-right'] &&
    css`
      margin-left: auto;
    `};
`

export const Flex = styled.div`
  display: flex;
  width: 100%;
  flex-flow: row wrap;
  & + ${() => Flex || null} {
    margin-top: ${({ padding = gutter }) => -1 * padding + unit};
  }
  ${({ padding = gutter, ['flex-phone']: items }) =>
    items &&
    theme.media.phone`
    & > ${Box} {
      width: calc( ${100 / items}% - ${padding + unit});
    }`};
  ${({ padding = gutter, ['flex-tablet']: items }) =>
    items &&
    theme.media.tablet`
    & > ${Box} {
      width: calc( ${100 / items}% - ${padding + unit});
    }`};
  ${({ padding = gutter, ['flex-pad']: items }) =>
    items &&
    theme.media.pad`
    & > ${Box} {
     width: calc( ${100 / items}% - ${padding + unit});
    }`};
  ${({ padding = gutter, ['flex-laptop']: items }) =>
    items &&
    theme.media.laptop`
    & > ${Box} {
      width: calc( ${100 / items}% - ${padding + unit});
    }`};
  ${({ padding = gutter, ['flex-desktop']: items }) =>
    items &&
    theme.media.desktop`
    & > ${Box} {
      width: calc( ${100 / items}% - ${padding + unit});
    }`};
  ${({ padding = gutter, ['flex-large']: items }) =>
    items &&
    theme.media.large`
    & > ${Box} {
      width: calc( ${100 / items}% - ${padding + unit});
    }`};
  ${({ padding = gutter, ...props }) =>
    props['flex-phone'] ||
    props['flex-tablet'] ||
    props['flex-pad'] ||
    props['flex-laptop'] ||
    props['flex-desktop'] ||
    props['flex-large']
      ? css`
          padding: ${padding + unit} 0 0 ${padding + unit};
          & > ${Box} {
            margin: 0 ${padding - 0.01 + unit} ${padding - 0.01 + unit} 0;
          }
        `
      : css`
          padding: ${padding + unit};
        `};
  ${color};
  ${space};
  ${maxWidth};
`
