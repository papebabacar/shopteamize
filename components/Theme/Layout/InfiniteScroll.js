import { createElement, Component } from 'react'
import PropTypes from 'prop-types'

export default class InfiniteScroll extends Component {
  static propTypes = {
    element: PropTypes.string,
    hasMore: PropTypes.bool,
    initialLoad: PropTypes.bool,
    isReverse: PropTypes.bool,
    loadMore: PropTypes.func.isRequired,
    pageStart: PropTypes.number,
    threshold: PropTypes.number,
    useCapture: PropTypes.bool,
    useWindow: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
      .isRequired,
    isRestart: PropTypes.bool
  }

  static defaultProps = {
    element: 'div',
    padding: 0,
    hasMore: false,
    initialLoad: true,
    pageStart: 0,
    threshold: 250,
    useWindow: true,
    isReverse: false,
    loadMore: false,
    useCapture: false,
    isRestart: false
  }

  constructor(props) {
    super(props)
    this.state = {}
    this.scrollListener = this.scrollListener.bind(this)
  }

  componentDidMount() {
    this.pageLoaded = this.props.pageStart
    if (this.props.hasMore) this.attachScrollListener()
  }

  static getDerivedStateFromProps({ isRestart, pageStart }) {
    isRestart && (this.pageLoaded = pageStart)
    return null
  }

  componentDidUpdate() {
    if (this.props.hasMore) this.attachScrollListener()
  }
  componentWillUnmount() {
    this.detachScrollListener()
  }

  detachScrollListener() {
    let scrollEl = window
    if (this.props.useWindow === false) {
      scrollEl = this.scrollComponent.parentNode
    }

    scrollEl.removeEventListener('scroll', this.scrollListener)
    scrollEl.removeEventListener('resize', this.scrollListener)
  }

  attachScrollListener() {
    if (!this.props.hasMore) {
      return
    }

    let scrollEl = window
    if (this.props.useWindow === false) {
      scrollEl = this.scrollComponent.parentNode
    }

    scrollEl.addEventListener('scroll', this.scrollListener)
    scrollEl.addEventListener('resize', this.scrollListener)

    if (this.props.initialLoad) {
      this.scrollListener()
    }
  }

  scrollListener() {
    const el = this.scrollComponent
    const scrollEl = window

    let offset
    if (this.props.useWindow) {
      const scrollTop =
        scrollEl.pageYOffset !== undefined
          ? scrollEl.pageYOffset
          : (
              document.documentElement ||
              document.body.parentNode ||
              document.body
            ).scrollTop
      if (this.props.isReverse) {
        offset = scrollTop
      } else {
        offset =
          this.calculateTopPosition(el) +
          (el.offsetHeight - scrollTop - window.innerHeight)
      }
    } else if (this.props.isReverse) {
      offset = el.parentNode.scrollTop
    } else {
      offset =
        el.scrollHeight - el.parentNode.scrollTop - el.parentNode.clientHeight
    }

    if (offset < Number(this.props.threshold)) {
      // Call loadMore after detachScrollListener to allow for non-async loadMore functions
      if (typeof this.props.loadMore === 'function' && this.props.hasMore) {
        this.props.loadMore((this.pageLoaded += 1))
      }
    }
  }

  calculateTopPosition(el) {
    if (!el) {
      return 0
    }
    return el.offsetTop + this.calculateTopPosition(el.offsetParent)
  }

  render() {
    const {
      children,
      element,
      // eslint-disable-next-line no-unused-vars
      hasMore,
      // eslint-disable-next-line no-unused-vars
      initialLoad,
      // eslint-disable-next-line no-unused-vars
      pageStart,
      // eslint-disable-next-line no-unused-vars
      threshold,
      // eslint-disable-next-line no-unused-vars
      useWindow,
      // eslint-disable-next-line no-unused-vars
      isReverse,
      // eslint-disable-next-line no-unused-vars
      loadMore,
      // eslint-disable-next-line no-unused-vars
      useCapture,
      // eslint-disable-next-line no-unused-vars
      isRestart,
      ...restOfProps
    } = this.props
    const props = {
      ...restOfProps,
      ref: node => (this.scrollComponent = node)
    }
    return createElement(element, props, children)
  }
}
