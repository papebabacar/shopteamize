import { compose, withState } from 'recompose'
import { Section, Tab, Button } from './style'
import { cloneElement } from 'react'

const Component = ({ tabIndex, setTabIndex, tabBreak, children }) => (
  <Section>
    <Tab breakPoint={tabBreak} role="tablist">
      {children.map(({ props: { title, onClick } }, index) => (
        <Button
          key={index}
          role="tab"
          selected={tabIndex === index}
          aria-selected={tabIndex === index}
          onClick={() => (onClick ? onClick() : setTabIndex(index))}
        >
          {title}
        </Button>
      ))}
      {children.map((child, index) =>
        cloneElement(child, { selected: tabIndex === index })
      )}
    </Tab>
  </Section>
)

Component.defaultProps = {
  tabBreak: '768px'
}

export const Tabs = compose(withState('tabIndex', 'setTabIndex', 0))(Component)
