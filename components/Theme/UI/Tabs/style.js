import styled from 'styled-components'

export const Section = styled.section`
  background: #fff;
  padding: 1.625em 0.8125em;
`

export const Tab = styled.div`
  width: 100%;
  margin: 0 auto;
  @media (min-width: 960px) {
    max-width: 88vw;
  }
  @media (min-width: 1280px) {
    max-width: 70vw;
  }
`

export const Button = styled.a`
  padding: 0.203125em 0.8125em;
  cursor: pointer;
  color: ${props => (props.selected ? 'black' : 'rgb(26,120,34)')};
  border-right: 1px solid rgb(26, 120, 34);
  &:last-of-type {
    border: none;
  }
  &:hover,
  &:focus,
  &:active {
    color: black;
    outline: none;
  }
`
