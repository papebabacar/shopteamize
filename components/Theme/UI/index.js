import styled, { css } from 'styled-components'

export const Button = styled.a`
  border: ${props => (props.border ? '1px solid' : 'none')};
  background: ${props => {
    return props.transparent
      ? 'none'
      : props.banner || props.promo
        ? 'white'
        : props.secondary
          ? 'rgb(120,207,139)'
          : props.intermediary
            ? 'rgb(1, 152, 83)'
            : 'rgb(26, 120, 34)'
  }};
  color: ${props =>
    props.banner ? '#ff12f4' : props.promo ? 'rgb(0,170,55)' : 'white'};
  display: ${props => (props.block ? 'block' : 'inline-block')};
  text-align: center;
  margin: ${props =>
    props.margin ? props.margin : props.block ? '0.8125em auto' : '0 auto'};
  width: ${props =>
    props.block ? (props.width ? props.width : '100%') : 'auto'};
  max-width: ${props =>
    props.block ? (props.width ? props.width : '24em') : 'none'};
  padding: ${props => (props.padding ? props.padding : '0.8125em 3.25em')};
  border-radius: 3.25em;
  :disabled {
    background: #777;
  }
  & + button,
  & + a {
    margin-left: ${props => (props.block ? 'auto' : '0.8125em')};
  }
`

export const MiniButton = styled(Button)`
  padding: 0 0.40625em;
  margin-left: 0.40625em;
  font-size: 80%;
`

export const SubmitButton = Button.withComponent('button')

export const AccountForm = styled.section`
  background: #fff;
  display: block;
  margin: 0 auto;
  width: 96%;
  max-width: 520px;
  padding: ${props => (props.padding ? props.padding : '1.625em')};
  border-top: 0.8125em solid #202c28;
  text-align: center;
`
export const FormTitle = styled.h1`
  text-align: center;
  color: #202c28;
  border-bottom: 1px solid #bcbcbc;
  font-size: 175%;
`
export const Notification = styled.p`
  background: #f38989;
  color: white;
  font-size: 88%;
  text-align: center;
  padding: 0.203125em 0.8125em;
  margin-bottom: 0.8125em;
`
export const FormLabel = styled.p`
  text-align: ${props => (props.right ? 'right' : 'center')};
  color: #888;
  font-size: 88%;
`

export const Link = styled.a`
  color: rgb(145, 204, 138);
  text-decoration: underline;
  font-weight: 900;
`
export const Article = styled.article`
  min-height: 75vh;
  background: white;
  padding: 0.8125em 1.625em;
  @media (min-width: 1024px) {
    padding: 0.8125em 15%;
  }
  @media (min-width: 1280px) {
    padding: 0.8125em 23%;
  }
`
export const Select = styled.select`
  background: ${props => (props.bg ? props.bg : 'white')};
  padding: ${props =>
    props.padding ? props.padding : '0.203125em 0.8125em 0.203125em 1.625em'};
  margin: 0.40625em 0;
  font-family: 'Arimo';
  font-size: 110%;
  display: inline-block;
  width: 50%;
  border-radius: 1.625em;
  border: 1px solid #eee;
  box-shadow: none;
  max-width: ${props => (props['max-width'] ? props['max-width'] : 'none')};
  text-align: ${props => (props.center ? 'center' : 'left')};
`

export const Option = styled.option``

export const NumericInput = styled(Select.withComponent('input')).attrs({
  type: 'number',
  min: 1,
  max: 999
})``

export const Size = ({ options, ...props }) => (
  <Select {...props}>
    {options.map(({ value, label }) => (
      <Option value={value} key={value}>
        {label}
      </Option>
    ))}
  </Select>
)

export const Picture = styled.img`
  object-fit: ${props => (props.cover ? 'cover' : 'contain')};
  width: 100%;
  margin: ${props => (props.margin ? props.margin : '0 0 1.625em 0')};
  height: ${props => (props.size ? props.size : '33vw')};
  ${props =>
    !props.size &&
    css`
      @media (min-width: 550px) {
        /* tablet*/
        height: 45vw;
      }
    `}
  }
  ${props =>
    !props.size &&
    css`
      @media (min-width: 750px) {
        /* tablet and beyond */
        height: ${props => (props.adapt ? '18vw' : '25vw')};
      }
    `}
  }
`
