import { compose, withState } from 'recompose'
import Formsy from 'formsy-react'
import Loader from 'components/Theme/Layout/Loader'

export const withForm = compose(withState('submitting', 'setFormState', false))

export const Form = ({ post, children, submitting }) => (
  <Formsy onValidSubmit={post}>
    {children} {submitting && <Loader />}
  </Formsy>
)
