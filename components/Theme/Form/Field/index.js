import { Component } from 'react'
import { withFormsy } from 'formsy-react'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import {
  Row,
  FieldSet,
  Label,
  Input,
  Select,
  Option,
  TextArea,
  ErrorMessage,
  Info,
  Description
} from './style'
import { getUserStore } from 'containers/App'
import dynamic from 'next/dynamic'
const Dropzone = dynamic(import('components/Theme/Layout/File'), {
  ssr: false,
  loading: () => null
})

class Field extends Component {
  constructor(props) {
    super(props)
    this.state = { showToolTip: true }
  }

  static defaultProps = {
    edit: true
  }

  changeValue = event => {
    this.props.setValue(event.currentTarget.value)
  }

  changeSelectedValue = event => {
    this.props.setValue(event.currentTarget.value)
    this.onBlur(event)
  }

  changeFiles = async files => {
    const data = await Promise.all(
      files.map(async file => ({
        imageAsByteArray: (await readUploadedFile(file)).split(',').pop(),
        imageContentType: file.type
      }))
    )
    this.props.setValue(data)
  }

  changeDay = date => {
    this.props.setValue(date)
    this.props.blurAction &&
      this.props.blurAction({
        [this.props.name]: date
      })
  }

  onBlur = event => {
    this.setState({ showToolTip: true })
    this.props.blurAction &&
      this.props.blurAction({
        [this.props.name]: event.currentTarget.value
      })
  }

  render() {
    return (
      <Row padding={0} flex-phone={2}>
        <FieldSet>
          <Label
            required={this.props.required && this.props.edit}
            title={this.props.title}
          >
            {this.props.label}
            {this.props.title && (
              <>
                {' '}
                <Info /> <Description>{this.props.title}</Description>{' '}
              </>
            )}
          </Label>
        </FieldSet>
        <FieldSet
          hasError={
            this.props.isFormSubmitted() &&
            !this.props.isValid() &&
            this.props.edit
          }
        >
          {this.props.edit ? (
            <>
              {this.props.type === 'select' ? (
                <Select
                  onChange={this.changeSelectedValue}
                  value={this.props.getValue()}
                  title={this.props.title}
                >
                  {this.props.options.map(({ value, label }) => (
                    <Option value={value} key={value}>
                      {label}
                    </Option>
                  ))}
                </Select>
              ) : this.props.type === 'date' ? (
                <DayPickerInput
                  inputProps={{ readOnly: true, title: this.props.title }}
                  keepFocus={false}
                  onDayChange={this.changeDay}
                  onBlur={this.onBlur}
                  placeholder=""
                  value={new Date(this.props.getValue())}
                  formatDate={date =>
                    Intl.DateTimeFormat(this.props.language, {
                      day: '2-digit',
                      month: 'long',
                      year: 'numeric'
                    }).format(date)
                  }
                  dayPickerProps={{
                    months: 'Janvier_Février_Mars_Avril_Mai_Juin_Juillet_Août_Septembre_Octobre_Novembre_Décembre'.split(
                      '_'
                    ),
                    weekdaysLong: 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split(
                      '_'
                    ),
                    weekdaysShort: 'dim._lun._mar._mer._jeu._ven._sam.'.split(
                      '_'
                    ),
                    firstDayOfWeek: 1
                  }}
                />
              ) : this.props.type === 'upload' ? (
                <Dropzone name={this.props.name} onChange={this.changeFiles} />
              ) : this.props.type === 'textarea' ? (
                <TextArea
                  onChange={this.changeValue}
                  onBlur={this.onBlur}
                  type={this.props.type}
                  placeholder={this.props.placeholder}
                  value={this.props.getValue()}
                  rows="10"
                  title={this.props.title}
                />
              ) : (
                <Input
                  onChange={this.changeValue}
                  onBlur={this.onBlur}
                  type={this.props.type}
                  placeholder={this.props.placeholder}
                  value={this.props.getValue()}
                  title={this.props.title}
                  min={this.props.min}
                  max={this.props.max}
                  readOnly={this.props.readOnly}
                />
              )}
            </>
          ) : (
            <Label left>
              {' : '}
              {this.props.type === 'password'
                ? '••••••••••••••••'
                : this.props.getValue()}
            </Label>
          )}
        </FieldSet>
        {this.props.isFormSubmitted() &&
          !this.props.isValid() &&
          this.state.showToolTip && (
            <ErrorMessage>
              {this.props.getErrorMessage()
                ? this.props.getErrorMessage()
                : this.props.user.language === 'fr'
                  ? 'Veuillez remplir ce champ'
                  : 'Please fill in this field'}
            </ErrorMessage>
          )}
      </Row>
    )
  }
}

export default getUserStore(withFormsy(Field))

const readUploadedFile = file => {
  const reader = new FileReader()

  return new Promise((resolve, reject) => {
    reader.onerror = () => {
      reader.abort()
      reject(new DOMException('Problem parsing input file.'))
    }

    reader.onload = () => {
      resolve(reader.result)
    }
    reader.readAsDataURL(file)
  })
}
