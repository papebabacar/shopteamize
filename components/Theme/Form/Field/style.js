import styled, { css } from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Row = styled(Flex)`
  justify-content: center;
  margin-top: 1.625em;
  & + & {
    margin-top: 0.8125em;
  }
  &:last-of-type {
    margin-bottom: 1.625em;
  }
`

export const FieldSet = styled(Box)`
  max-width: 22em;
  padding: 1px;
  position: relative;
  background: ${props => (props.hasError ? '#f38989' : 'none')};
  input,
  select {
    padding: 0.40625em;
    width: 100%;
    border-radius: 0.203125em;
    vertical-align: text-bottom;
  }
`

export const Label = styled.label`
  font-size: 90%;
  ${({ required }) =>
    required &&
    css`
      &:after {
        content: '*';
        display: inline-block;
        color: #f38989;
        position: relative;
        top: 0.325em;
      }
    `};
  padding: 0.3125em 0.40625em 0 0;
  text-align: ${props => (props.left ? 'left' : 'right')};
  display: block;
  line-height: 1.725rem;
  ${theme.media.laptop`    
    line-height: 1.40625em;
  `};
`

export const Input = styled.input`
  width: 100%;
  ${props =>
    props.readOnly &&
    css`
      text-align: center;
      font-family: 'Arimo';
      color: green;
      font-weight: 900;
      background: #dfefdf;
    `};
`

export const TextArea = styled.textarea`
  width: 100%;
  resize: none;
`

export const ErrorMessage = styled.p`
  color: #f38989;
  font-size: 80%;
  position: relative;
  margin: 0;
`
export const Select = styled.select``

export const Option = styled.option``

export const Description = styled.p`
  display: none;
`

export const Info = styled.span`
  width: 0.8125em;
  height: 0.8125em;
  background: url(/static/img/info.png);
  background-size: cover;
  display: inline-block;
  position: relative;
  margin-left: 0.203125em;
  right: 0.203125em;
  &:hover + ${Description} {
    display: block;
    position: absolute;
    top: -3.25em;
    left: 8%;
    line-height: 1.625em;
    padding: 0.8125em;
    background: rgba(35, 46, 42, 0.9);
    color: white;
    box-shadow: 0px 3px 30px -5px rgba(0, 0, 0, 0.58);
    min-width: 22em;
    font-size: 75%;
    z-index: 999999999999;
    text-align: center;
  }
`
