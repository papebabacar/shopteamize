/* DataLayer for product detail page display */
export const initializeProduct = product => {
  const {
    id,
    name,
    price,
    category: { label }
    //color,
    //size
  } = product
  const dataLayer = {
    ecommerce: {
      detail: {
        products: [
          {
            id,
            name,
            price,
            Category: label
            // variant: color,
            // dimension1: size
          }
        ]
      }
    }
  }

  initializeGTM({ dataLayer })
}

/* DataLayer for Wishlist detail page display */
export const initializeWishlist = wishlist => {
  const dataLayer = {
    ecommerce: {
      detail: {
        products: [
          {
            id: wishlist.id,
            name: wishlist.labels[0].title,
            price: wishlist.price,
            category: wishlist.category.name
          }
        ]
      }
    }
  }
  initializeGTM({ dataLayer })
}

/* DataLayer Wishlist add to cart */
export const initializeWishlistAddToCartClick = wishlist => {
  const dataLayer = {
    event: 'addToCart',
    ecommerce: {
      add: {
        products: [
          {
            id: wishlist.id,
            name: wishlist.labels[0].title,
            price: wishlist.price,
            category: wishlist.categories[0]
            //quantity
          }
        ]
      }
    }
  }
  initializeGTM({ dataLayer })
}

/* DataLayer for category product Impression page */
export const initializeCatalog = catalog => {
  const {
    //    totalCount,
    //    name,
    data,
    category: { label: category }
  } = catalog

  const dataLayer = {
    ecommerce: {
      impressions: data.map(({ id, name, price }, position) => ({
        id,
        name,
        price,
        category,
        position,
        list: category
      }))
    }
  }
  initializeGTM({ dataLayer })
}

/* DataLayer for  product click (impression) on quickview */
export const initializeProductClick = product => {
  const { id, name, price, categories } = product
  const dataLayer = {
    event: 'productClick',
    ecommerce: {
      click: {
        actionField: {
          list: categories[0]
        },
        products: [
          {
            id,
            name,
            price,
            category: categories[0]
          }
        ]
      }
    }
  }

  initializeGTM({ dataLayer })
}

/* DataLayer for  wishlist click (impression) on quickview */
export const initializeWishlistClick = wishlist => {
  const dataLayer = {
    event: 'productClick',
    ecommerce: {
      click: {
        actionField: {
          list: wishlist.categories[0]
        },
        products: [
          {
            id: wishlist.id,
            name: wishlist.labels[0].title,
            price: wishlist.price,
            category: wishlist.categories[0]
          }
        ]
      }
    }
  }

  initializeGTM({ dataLayer })
}

/* DataLayer Product add to cart */
export const initializeAddToCartClick = product => {
  const {
    id,
    name,
    price,
    color,
    size
    //quantity
    //attributes: { brand, description },
    //category:{label},
    //    categories
  } = product
  const dataLayer = {
    event: 'addToCart',
    ecommerce: {
      add: {
        products: [
          {
            id,
            name,
            price,
            brand: 'Shopteamize',
            //category: categories[0],
            variant: color, //TODO dynamic value ( Waiting for MCT implement)
            dimension1: size
            //quantity
          }
        ]
      }
    }
  }
  initializeGTM({ dataLayer })
}

/* DataLayer remove product from cart */
export const initializeRemoveFromCartClick = product => {
  const {
    id,
    name,
    //    currencyCode,
    price,
    //attributes: { brand, description },
    //category:{label},
    categories
  } = product
  const dataLayer = {
    event: 'removeFromCart',
    ecommerce: {
      remove: {
        products: [
          {
            id,
            name,
            price,
            category: categories[0]
            // position: '1', // TODO Dynamic value
            // variant: 'M', // TODO Dynamic value (Waiting for MCT implement)
            //quantity: 1 // TD DO Dynamic Value
          }
        ]
      }
    }
  }
  initializeGTM({ dataLayer })
}

/* DataLayer checkout (from cart to payment page) */
export const initializecheckout = (serverCart, step) => {
  const {
    products,
    //    pendingWishlist,
    customerWishlists,
    shoppingIdeas
    //    carrier,
    //    payment
  } = serverCart
  const articles = [...products, ...customerWishlists, ...shoppingIdeas].map(
    ({ quantity, product, wishlist }) => {
      const { id, name = wishlist.labels[0].title, price } = product || wishlist
      return { id, name, price, quantity }
    }
  )
  const dataLayer = {
    event: 'EEcheckout',
    ecommerce: {
      checkout: {
        actionField: {
          step: step
          //option: payment
        },
        products: articles
      }
    }
  }

  initializeGTM({ dataLayer })
}
const insertIfNotEmpty = item => (item ? [[item.id, item]] : [])

/* DataLayer Confirmation page */
export const initializeconfirmation = order => {
  const {
    pendingWishlist,
    products,
    wishlists,
    shoppingIdeas,
    total,
    totalShipping,
    paymentID
  } = order
  const articles = [
    ...insertIfNotEmpty(pendingWishlist),
    ...Object.entries(products),
    ...Object.entries(wishlists),
    ...Object.entries(shoppingIdeas)
  ].map(([id, { name, price, quantity }]) => {
    return { id, name, price, quantity }
  })
  const dataLayer = {
    event: 'EEtransaction',
    ecommerce: {
      purchase: {
        actionField: {
          id: paymentID, //transaction ID - mandatory
          revenue: total, //total including tax and shipping
          shipping: totalShipping
        },
        products: articles
      }
    }
  }

  initializeGTM({ dataLayer })
}

/* Datalayer tracking share Facebook button */

export const initializeFacebookShare = shareUrl => {
  const dataLayer = {
    event: 'socialInteraction',
    socialNetwork: 'Facebook',
    socialAction: 'Send',
    socialTarget: shareUrl
  }

  initializeGTM({ dataLayer })
}

/* Datalayer tracking share Twitter button */

export const initializeTwitterShare = shareUrl => {
  const dataLayer = {
    event: 'socialInteraction',
    socialNetwork: 'Twitter',
    socialAction: 'Tweet',
    socialTarget: shareUrl
  }

  initializeGTM({ dataLayer })
}

const initializeGTM = async dataLayer => {
  const TagManager = await import('react-gtm-module')

  TagManager.dataLayer(dataLayer)
}
