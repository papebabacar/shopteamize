import styled from 'styled-components'
import theme from 'components/Theme'

const { gray1, black } = theme

export const Template = styled.section`
  background: ${gray1};
  color: ${black};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 100vh;
`
