import { compose, lifecycle } from 'recompose'
import Head from 'next/head'
import { Template } from './style'
import { ThemeProvider } from 'styled-components'
import { colors, breakpoints } from 'components/Theme'
import { I18nProvider } from '@lingui/react'
import fr from 'locale/fr/messages.js'
import en from 'locale/en/messages.js'
//import { setLanguage, setPathname } from 'store/actions/user'

const App = ({
  children,
  title = 'Shopteamize - La plateforme qui optimise vos achats groupés',
  image = '/static/img/cover.jpg',
  description = 'Shopteamize est un site marchand spécialisé dans l’achat groupé de produits en provenance des pays musulmans ( Dubai, Arabie Saoudite, Maroc…). Shopteamize vous propose des idées shopping ramadan, avec des produits minutieusement sélectionnés pour inspirer vos envies shopping. Shop qui peut !',
  language
}) => (
  <I18nProvider language={language} catalogs={{ en, fr }}>
    <ThemeProvider theme={{ ...colors, ...breakpoints }}>
      <Template>
        <Head>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="og:image" content={image} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:card" content={title} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
          <meta property="twitter:creator" content="@sn_ecommerce" />
          <meta property="twitter:image" content={image} />
        </Head>

        {children}
      </Template>
    </ThemeProvider>
  </I18nProvider>
)

const Component = compose(
  lifecycle({
    async componentDidMount() {
      const TagManager = await import('react-gtm-module')
      const tagManagerArgs = {
        gtmId: 'GTM-W84FM8T'
      }
      TagManager.initialize(tagManagerArgs)
    }
  })
)(App)

export default Component
