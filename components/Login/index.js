import { Trans, withI18n } from '@lingui/react'
import { compose, lifecycle, withState, withHandlers } from 'recompose'
import { getUserStore, getCartStore } from 'containers/App'
import {
  AccountForm,
  SubmitButton,
  FormTitle,
  FormLabel,
  Link,
  Notification
} from 'components/Theme'
import { Router } from 'router'
import { Form, withForm } from 'components/Theme/Form'
import Field from 'components/Theme/Form/Field'
import { authenticate, getUser } from 'api/account'
import { getCurrentCart, mergeCart, synchronizeCarts } from 'api/muhajir/cart'

const LoginForm = ({
  post,
  submitting,
  submitError,
  i18n,
  user: { language },
  padding,
  center
}) => (
  <AccountForm padding={padding}>
    <Form post={post} submitting={submitting}>
      <FormTitle>
        <Trans>Sign in</Trans>
      </FormTitle>
      <FormLabel>
        <Trans>Not registred?</Trans>{' '}
        <Link href={`${Router.linkPage('register', { language }).as}`}>
          <Trans>create your account.</Trans>
        </Link>.
      </FormLabel>
      <Field
        label="Email"
        name="username"
        placeholder="Email"
        type="email"
        value=""
        required
        validations="isEmail"
        validationError={i18n.t`Please enter a valid email address`}
      />
      <Field
        label={<Trans>Password</Trans>}
        name="password"
        placeholder="*******"
        type="password"
        value=""
        required
      />
      <FormLabel right={!center}>
        <Link href={`${Router.linkPage('reinit', { language }).as}`}>
          <Trans>Forgot your password?</Trans>
        </Link>
      </FormLabel>
      {submitError && <Notification>{submitError}</Notification>}
      <SubmitButton disabled={submitting}>
        <Trans>Sign in</Trans>
      </SubmitButton>
    </Form>
  </AccountForm>
)

const Component = compose(
  withI18n(),
  getUserStore,
  getCartStore,
  withForm,
  lifecycle({
    async componentDidMount() {
      const {
        logoutUser,
        cart: { idSession }
      } = this.props
      try {
        await synchronizeCarts(idSession)
      } catch (error) {
        // do something
      }
      logoutUser()
    }
  }),
  withState('submitError', 'setSubmitError'),
  withHandlers({
    post: ({
      setFormState,
      redirect,
      loginUser,
      cart: { idSession },
      refreshCart,
      emptyCart,
      setSubmitError,
      i18n
    }) => async data => {
      setFormState(true)
      const { username, password } = data
      try {
        const {
          data: { id_token: token }
        } = await authenticate({
          username,
          password
        })
        const {
          data: { id, login, firstName, lastName }
        } = await getUser(token)
        loginUser({ token, id, login, firstName, lastName })
        try {
          const { data: serverCart } = idSession
            ? await mergeCart(idSession)
            : await getCurrentCart()
          refreshCart(serverCart)
        } catch (error) {
          emptyCart()
        }
        redirect && (document.location = redirect)
      } catch (error) {
        let errorMessage = i18n.t`Your email is unknown or your password incorrect, please check your entry or create an account`
        error.response &&
          error.response.data.detail === `User ${username} was not activated` &&
          (errorMessage = i18n.t`You must activate your account before, an email have been send please check your email`)
        setSubmitError(errorMessage)
        setTimeout(() => setSubmitError(), 5000)
        setFormState(false)
      }
    }
  })
)(LoginForm)

export default Component
