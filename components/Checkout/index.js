import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { getUserStore, getCartStore, getOrderStore } from 'containers/App'
import { getCurrentCart, getCart, setShipping } from 'api/muhajir/cart'
import { getUserAddresses } from 'api/muhajir/address'
import { getCarriers } from 'api/muhajir/shipping'
import { Checkout } from './style'
import BreadCrumb from 'components/Checkout/BreadCrumb'
import Basket from 'components/Checkout/Basket'
import Delivery from 'components/Checkout/Delivery'
import Payment from 'components/Checkout/Payment'
import Confirmation from 'components/Checkout/Confirmation'
import Login from 'components/Login'
import { Router } from 'router'

const CheckoutPage = ({
  steps,
  cart: { currentStep },
  serverCart,
  updateShipping,
  addresses,
  carriers,
  setAddresses,
  isLoaded,
  user: { language }
}) => (
  <Checkout>
    <BreadCrumb steps={steps} currentStep={currentStep} />
    {currentStep === 'basket' && isLoaded && <Basket serverCart={serverCart} />}
    {currentStep === 'login' && (
      <Login
        redirect={`${
          Router.linkPage('checkout', { language }).as
        }?step=delivery`}
      />
    )}
    {currentStep === 'delivery' &&
      isLoaded && (
        <Delivery
          serverCart={serverCart}
          updateShipping={updateShipping}
          setAddresses={setAddresses}
          addresses={addresses}
          carriers={carriers}
        />
      )}
    {currentStep === 'payment' &&
      isLoaded && <Payment serverCart={serverCart} />}
    {currentStep === 'confirmation' && <Confirmation />}
  </Checkout>
)

const Component = compose(
  getUserStore,
  getCartStore,
  getOrderStore,
  withState('isLoaded', 'setLoaded', false),
  withState('addresses', 'setAddresses', {}),
  withState('carriers', 'setCarriers', []),
  withState('serverCart', 'setServerCartData', {
    products: [],
    pendingWishlist: {},
    customerWishlists: [],
    shoppingIdeas: []
  }),
  withHandlers({
    updateShipping: ({
      cart: { shippingAddress, shippingOption },
      carriers,
      refreshCart,
      setServerCartData
    }) => async () => {
      try {
        const { data: serverCart } = await setShipping({
          shippingAddress,
          billingAddress: shippingAddress,
          carrier: carriers.find(({ name }) => name === shippingOption)
        })
        setServerCartData(serverCart)
        refreshCart(serverCart)
        return serverCart
      } catch (error) {
        // do something
      }
    }
  }),
  lifecycle({
    async componentDidMount() {
      const {
        user: { token },
        cart: { idSession, currentStep, shippingAddress },
        setCart,
        setServerCartData,
        updateShipping,
        refreshCart,
        setAddresses,
        setCarriers,
        setLoaded,
        emptyCart,
        emptyOrder
      } = this.props
      try {
        switch (currentStep) {
          case 'basket': {
            emptyOrder()
            const { data: serverCart } = token
              ? await getCurrentCart()
              : await getCart(idSession)
            setServerCartData(serverCart)
            refreshCart(serverCart)
            break
          }
          case 'delivery': {
            const { data: addresses } = await getUserAddresses()
            setAddresses(
              addresses.reduce(
                (addresses, address) => ({
                  ...addresses,
                  [address.id]: address
                }),
                {}
              )
            )
            const countries = [
              ...new Set(
                addresses.reduce(
                  (countriesCode, address) => [
                    ...countriesCode,
                    address.countryCode
                  ],
                  []
                )
              )
            ]
            const carriersArray =
              countries.length > 0 &&
              (await Promise.all(
                countries
                  .reduce(
                    (requests, countryIsoCode) => [
                      ...requests,
                      getCarriers({ countryIsoCode })
                    ],
                    []
                  )
                  .map(promise =>
                    promise.catch(() => ({
                      data: [{ id: null }]
                    }))
                  )
              ))

            const carriers = carriersArray
              ? carriersArray.reduce(
                  (carriers, { data }) => [...carriers, ...data],
                  []
                )
              : []
            setCarriers(carriers)
            const firstAddress = addresses.find(() => true)
            const firstCarrier = carriers.find(() => true)
            setCart({
              shippingAddress: firstAddress,
              carrier: firstCarrier,
              shippingOption: firstCarrier && firstCarrier.name
            })
            firstAddress && firstCarrier && (await updateShipping())
            break
          }
          case 'payment': {
            if (!shippingAddress) throw new Error('No shipping address')
            const { data: serverCart } = await getCurrentCart()
            setServerCartData(serverCart)
            refreshCart(serverCart)
            break
          }
        }
      } catch (error) {
        setCart({ currentStep: 'login' })
        emptyCart()
        emptyOrder()
      }
      setLoaded(true)
    }
  })
)(CheckoutPage)

export default Component
