import { Trans } from '@lingui/react'
import { compose, lifecycle } from 'recompose'
import {
  getUserStore,
  getCartStore,
  getOrderStore,
  getWishlistStore
} from 'containers/App'
import { initializeconfirmation } from 'components/Theme/GTM'
import {
  Confirmation,
  Detail,
  Title,
  Heading,
  Info,
  Block,
  Line,
  Link,
  Label,
  Description,
  config,
  columns
} from './style'
import { ContinueShopping } from 'components/Checkout/Actions'
import ReactTable from 'react-table'
import Share from 'components/WishList/Detail/Share'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'

const ConfirmationPage = ({
  order: {
    itemsCount,
    products,
    pendingWishlist,
    wishlists,
    shoppingIdeas,
    totalItems,
    totalShopping,
    totalShipping,
    total,
    currencyCode,
    payment
  },
  user: { language }
}) => (
  <>
    <Confirmation currencyCode={currencyCode}>
      <Detail>
        <Title>
          <Trans>
            Congratulations you have successfully completed your order.
          </Trans>
        </Title>
        <Info>
          <Trans>Thank you for the confidence you granted us.</Trans>
        </Info>
        {payment === 'paypal_account' && (
          <Info>
            <Trans>
              Your order is registered and will be processed by our teams.
            </Trans>
          </Info>
        )}
        {payment === 'credit_card' && (
          <Info>
            <Trans>
              Your wishlists will be delivered after expiry of the validity
              date.
            </Trans>
          </Info>
        )}
        <Info>
          <Trans>
            Your order will be processed upon receipt of your payment. A
            confirmation email has just been sent to your account.
          </Trans>
        </Info>
        <Info>
          <Trans>You can track you order progress by logging into</Trans>{' '}
          <Link
            href={`${Router.linkPage('account', { language }).as}?view=order`}
          >
            <Trans>your selfcare</Trans>
          </Link>.
        </Info>
        {pendingWishlist && (
          <>
            <Line />
            <Info>
              <Trans>Share your wishlist with your friends.</Trans>
            </Info>
            <Share
              inline // eslint-disable-next-line no-undef
              shareUrl={`${APP_URL}${
                Router.linkPage('wishlist', {
                  language,
                  id: pendingWishlist.id,
                  name: slugify(pendingWishlist.name)
                }).as
              }`}
              image={pendingWishlist.img}
            />
          </>
        )}
        <Line />
      </Detail>
      <Detail>
        <Heading>
          <Trans>Your items</Trans>
        </Heading>
      </Detail>
      <ReactTable
        data={[
          ...insertIfNotEmpty(pendingWishlist),
          ...Object.values(wishlists),
          ...Object.values(shoppingIdeas),
          ...Object.values(products)
        ]}
        columns={columns}
        {...config}
        pageSize={itemsCount || 5}
        sortable={false}
      />
      <Line />
      <Detail>
        <Heading>
          <Trans>Your order</Trans>
        </Heading>
        <Block padding={0} flex-phone={2}>
          <Label>
            <Trans>Total Cart</Trans>
          </Label>
          <Description>
            {totalItems} {currencyCode}
          </Description>
          <Label>
            <Trans>Shopping fees</Trans>
          </Label>
          <Description>
            {totalShopping} {currencyCode}
          </Description>
          <Label>
            <Trans>Shipping fees</Trans>
          </Label>
          <Description>
            {totalShipping} {currencyCode}
          </Description>
          <Label>
            <Trans>Total Order</Trans>
          </Label>
          <Description>
            {total} {currencyCode}
          </Description>
        </Block>
      </Detail>
      <Line />
      <Detail>
        <Heading>
          <Trans>Shipping method</Trans>
        </Heading>
        <Description>DHL</Description>
      </Detail>
      <Detail>
        <Heading>
          <Trans>Payment method</Trans>
        </Heading>
        <Description>{payment}</Description>
      </Detail>
    </Confirmation>
    <ContinueShopping />
  </>
)

const Component = compose(
  getUserStore,
  getCartStore,
  getOrderStore,
  getWishlistStore,
  lifecycle({
    async componentDidMount() {
      const {
        order,
        order: { pendingWishlist },
        emptyCart,
        emptyWishlist,
        checkOrder
      } = this.props
      emptyCart()
      pendingWishlist && emptyWishlist()
      checkOrder(true)
      initializeconfirmation(order)
    }
  })
)(ConfirmationPage)

export default Component

const insertIfNotEmpty = item => (item ? [item] : [])
