import styled from 'styled-components'
import { Picture } from 'components/Theme'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import { Trans } from '@lingui/react'

export const Confirmation = styled.section`
  padding: 1.625em 0.8125em;
  margin-bottom:1.625em;
  background: #fff;
  .currency {
    p:after {
       content: ' (${({ currencyCode }) => currencyCode})';
    }
  }
`
export const Detail = styled.section`
  padding: 0.8125em;
`

export const Title = styled.h1``

export const Heading = styled.h3``

export const Info = styled.p``

export const Line = styled.hr`
  margin: 3.25em 0 0.8125em;
  opacity: 0.125;
`
export const Link = styled.a`
  color: rgb(145, 204, 138);
  text-decoration: underline;
  font-weight: 900;
`

export const Block = styled(Flex)`
  justify-content: flex-end;
`

export const Label = styled(Box)`
  padding: 0.40625em 0;
`

export const Description = styled(Box)`
  font-weight: 900;
  padding: 0.40625em 0;
  text-align: right;
`

export const Actions = styled.div`
  text-align: center;
  margin-bottom: 0.8125em;
`

const Right = styled.p`
  text-align: right;
`
export const config = {
  getProps: () => ({ style: { border: 'none' } }),
  getTdProps: () => ({
    style: { padding: '0.8125em 1.625em 0.40625em 0.8125em' }
  }),
  getTheadThProps: () => ({
    style: { padding: '0.8125em 1.625em 0 0.8125em' }
  }),
  getTrGroupProps: () => ({
    style: { border: 'none' }
  }),
  getTheadTrProps: () => ({ style: { textAlign: 'left' } }),
  getPaginationProps: () => ({
    style: {
      boxShadow: 'none',
      border: '1px solid #eee',
      padding: '0.8125em'
    }
  }),
  className: '-highlight',
  showPagination: false,
  loadingText: <Trans>Loading your order</Trans>,
  defaultSorted: [
    {
      id: 'address',
      desc: false
    }
  ]
}

export const columns = [
  {
    Header: '',
    accessor: 'img',
    Cell: ({ value: imageUrl }) => (
      <Picture src={imageUrl} size="2.4375em" margin="0" />
    ),
    minWidth: 60,
    maxWidth: 80
  },
  {
    id: 'name',
    Header: 'Ref.',
    accessor: ({ name, color, size }) =>
      `${name}${color ? ' - ' + color.toUpperCase() : ''}${
        size ? ' - ' + size.toUpperCase() : ''
      }`,
    minWidth: 100
  },
  {
    id: 'address',
    Header: <Trans>Delivery address</Trans>,
    accessor: ({ address: { address1 } }) => address1,
    minWidth: 80
  },
  {
    id: 'quantity',
    Header: () => (
      <Right>
        <Trans>Quantity</Trans>
      </Right>
    ),
    Cell: ({ value }) => <Right>{value}</Right>,
    accessor: ({ quantity, itemsCount }) =>
      itemsCount ? `${quantity} x ${itemsCount} article(s)` : quantity,
    minWidth: 40
  },
  {
    headerClassName: 'currency',
    Header: () => (
      <Right>
        <Trans>Unit price</Trans>
      </Right>
    ),
    Cell: ({ value }) => <Right>{Number(value).toFixed(2)}</Right>,
    accessor: 'price',
    minWidth: 160,
    maxWidth: 170
  },
  {
    id: 'totalPrice',
    headerClassName: 'currency',
    Header: () => (
      <Right>
        <Trans>Sub total</Trans>
      </Right>
    ),
    Cell: ({ value }) => <Right>{Number(value).toFixed(2)}</Right>,
    accessor: ({ price, quantity }) => quantity * price,
    minWidth: 160,
    maxWidth: 170
  }
]
