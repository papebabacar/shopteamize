import { compose, withState } from 'recompose'
import { getUserStore, getCartStore } from 'containers/App'
import Address from './Address'
import {
  Card,
  Detail,
  PictureBox,
  Title,
  Label,
  Span,
  Price
  // Badge,
  // BadgeLabel,
  // BadgeButton,
} from './style'
import { Picture } from 'components/Theme'

const Item = ({
  itemType,
  item,
  addresses,
  user: { language },
  cart: {
    currencyCode,
    shippingAddress,
    [itemType]: {
      [item.id]: { quantity, isHomeDelivery = true, idAddress }
    }
  },
  updateCartItem,
  editItem,
  setEditingItem
}) => (
  <Card flex-phone={3} padding={0.40625}>
    <PictureBox box-flex={0.2}>
      <Picture src={item.mainImageUrl} size="100%" margin="0" cover />
    </PictureBox>
    <Detail box-flex={0.75}>
      <Price>
        {item.price} {currencyCode}
      </Price>
      <Title>
        {item.name || getTitle(item.labels, language)}{' '}
        {item.itemsCount && <Span>- {item.itemsCount} article(s)</Span>}
      </Title>
      <Label>
        {item.itemsCount
          ? `${quantity} x ${item.itemsCount} article(s)`
          : quantity}
      </Label>
      {item.color && <Label>{item.color.toUpperCase()}</Label>}
      {item.size && <Label>{item.size.toUpperCase()}</Label>}
      <Address
        itemType={itemType}
        item={item}
        editItem={editItem}
        setEditingItem={setEditingItem}
        shippingAddress={shippingAddress}
        addresses={addresses}
        updateCartItem={updateCartItem}
        idAddress={idAddress}
        isHomeDelivery={isHomeDelivery}
        quantity={quantity}
        withInitiator={
          item.type === 'CUSTOMER_WISHLIST' && item.currentStatus !== 'PENDING'
        }
      />
      {/* {editItem && (
        <Link onClick={() => setEditingAddress(true)}>
          <Trans>Add your new address</Trans>
        </Link>
      )} */}
      {/* {item.type === 'CUSTOMER_WISHLIST' &&
        item.currentStatus !== 'PENDING' &&
        isHomeDelivery && (
          <Badge>
            <BadgeLabel>
              <Trans>
                Shipping costs are shared. Do you want to be delivered to the
                initiator's address?
              </Trans>
            </BadgeLabel>
            <BadgeButton
              onClick={() =>
                updateCartItem(
                  item,
                  quantity,
                  itemType,
                  false,
                  item.addressInitiator
                )
              }
            >
              OK
            </BadgeButton>
          </Badge>
        )} */}
    </Detail>
    <Detail box-flex={0.05}>
      {/* {editItem && (
        <Button secondary onClick={() => setEditingItem(false)}>
          x
        </Button>
      )}
      {!editItem && (
        <Button onClick={() => setEditingItem(true)}>
          <Update src="/static/img/edit.png" />
        </Button>
      )} */}
    </Detail>
  </Card>
)
const Component = compose(
  withState('editItem', 'setEditingItem', false),
  getUserStore,
  getCartStore
)(Item)

export default Component

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
