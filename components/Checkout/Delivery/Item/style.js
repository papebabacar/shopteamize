import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'

export const Card = styled(Flex)`
  background: white;
  margin-bottom: 1.625em;
  & + & {
    margin-top: 0;
  }
`

export const Detail = styled(Box)`
  position: relative;
`
export const PictureBox = styled(Box)``

export const Title = styled.h3`
  display: block;
`

export const Label = styled.p`
  margin-top: 0.8125em;
  display: inline-block;
  padding: 0.203125em 0.8125em;
  margin-right: 1.625em;
  background: #eee;
  border-radius: 0.8125em;
  min-width: 4em;
  text-align: center;
  font-size: 90%;
`

export const Badge = styled.div`
  display: block;
  font-size: 88%;
  color: white;
  padding: 0.40625em 0.8125em;
  background: rgb(110, 196, 133);
  margin-bottom: 0;
`

export const BadgeLabel = styled.p`
  padding-right: 1.625em;
  margin-bottom: 0.203125em;
`

export const BadgeButton = styled.a`
  float: right;
  color: white;
  margin-top: -2.40625em;
`

export const Price = styled.span`
  font-weight: 900;
  font-family: 'Arimo';
`

export const Span = styled.span`
  font-weight: 100;
  color: #888;
`
export const Link = styled.a`
  text-decoration: underline;
`
export const Update = styled.img`
  width: 1.625em;
`
export const Button = styled.a`
  text-align: center;
  background: ${props =>
    props.confirm
      ? 'rgb(26, 120, 34)'
      : props.secondary
        ? 'rgb(120,207,139)'
        : '#eee'};
  color: ${props => (props.confirm || props.secondary ? 'white' : 'black')};
  width: 2em;
  height: 2.25em;
  padding: 0.3125em;
  display: block;
  margin-bottom: 0.8125em;
`
