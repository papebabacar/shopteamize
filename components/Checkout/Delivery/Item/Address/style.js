import styled from 'styled-components'

export const Description = styled.p`
  font-size: 88%;
`
export const Label = styled(Description)`
  font-weight: 900;
  margin: 0;
  font-family: ${props => (props.bolder ? 'Arimo' : 'Locke')};
`
export const InlineLabel = styled.span`
  margin-right: 0.8125em;
  display: inline-block;
`

export const Block = styled.section`
  margin-top: 0.8125em;
`

export const Link = styled.a`
  display: block;
  margin-bottom: 0.8125em;
  color: #4040ec;
  text-decoration: underline;
`
export const Select = styled.select`
  display: block;
  width: 100%;
  padding: 0.40625em 0;
  background: white;
  margin: 1.625em 0;
`
export const Option = styled.option``

export const Span = styled.span`
  color: rgb(26, 120, 34);
  font-weight: 900;
  font-family: 'Arimo';
`
