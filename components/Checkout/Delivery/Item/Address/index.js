import { Trans } from '@lingui/react'
import { Block, Label, Select, Option, Span } from './style'

const Component = ({
  itemType,
  item,
  quantity,
  shippingAddress,
  idAddress,
  isHomeDelivery,
  addresses,
  updateCartItem,
  editItem,
  setEditingItem,
  withInitiator
}) => (
  <Block>
    {editItem ? (
      <Block>
        <Label>
          <Trans>Registred addresses</Trans>
        </Label>
        <Select
          value={
            isHomeDelivery
              ? idAddress || shippingAddress.id
              : item.addressInitiator.id
          }
          onChange={event =>
            updateCartItem(
              item,
              quantity,
              itemType,
              withInitiator
                ? item.addressInitiator.id !==
                  Number.parseInt(event.target.value)
                : true,
              addresses[event.target.value] || item.addressInitiator
            ) && setEditingItem(false)
          }
        >
          {/* {withInitiator && (
            <Option value={item.addressInitiator.id}>
              <Trans>Wishlist owner's address</Trans>
            </Option>
          )} */}
          {Object.values(addresses).map(
            ({ id, alias, address1, city, country }) => (
              <Option key={id} value={id}>
                {alias} | {address1} {city} {country}
              </Option>
            )
          )}
        </Select>
      </Block>
    ) : (
      <Block>
        {itemType !== 'products' && (
          <Label>
            {item.addressInitiator ? (
              <>
                <Span>
                  <Trans>Team</Trans>{' '}
                </Span>
                {`${item.addressInitiator.firstName} ${
                  item.addressInitiator.lastName
                }`}
              </>
            ) : item.currentStatus === 'PENDING' ? (
              ''
            ) : (
              <Span>Team Shopteamize</Span>
            )}
          </Label>
        )}
        {isHomeDelivery ? (
          <Label>
            <Trans>Address</Trans> :{' '}
            {addresses[idAddress || shippingAddress.id].gender === 'MALE' ? (
              <Trans>MR</Trans>
            ) : (
              <Trans>MRS</Trans>
            )}{' '}
            {addresses[idAddress || shippingAddress.id].lastName}{' '}
            {addresses[idAddress || shippingAddress.id].firstName}
          </Label>
        ) : (
          <Label>
            {item.addressInitiator.gender === 'MALE' ? (
              <Trans>MR</Trans>
            ) : (
              <Trans>MRS</Trans>
            )}{' '}
            {item.addressInitiator.lastName} {item.addressInitiator.firstName}
          </Label>
        )}
        <Label bolder>
          {isHomeDelivery
            ? `${addresses[idAddress || shippingAddress.id].address1}, ${
                addresses[idAddress || shippingAddress.id].city
              }, ${addresses[idAddress || shippingAddress.id].country}`
            : `${item.addressInitiator.address1}, ${
                item.addressInitiator.city
              }, ${item.addressInitiator.country}`}
        </Label>
      </Block>
    )}
  </Block>
)

export default Component
