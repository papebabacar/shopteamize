import styled, { css } from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Delivery = styled(Flex)`
  position: relative;
`

export const Address = styled(Flex)`
  margin-bottom: 0.8125em;
`

export const Block = styled(Box)`
  display: ${props => (props.hidden ? 'none' : 'block')};
  ${theme.media.pad`
    flex: ${props => (props.takeAll ? 1 : 0.7)};
  `};
`

export const BlockAddress = styled(Box)`
  background: white;
  padding: 0.8125em;
  ${theme.media.pad`
    height: 9.875em;
    ${props =>
      props.shipping &&
      css`
        width: calc(50% - 0.8025em) !important;
        margin-left: 0.8125em !important;
        min-height: 9.875em;
        height: auto;
      `} 
  `};
`

export const Label = styled.p`
  color: #888;
  font-weight: 900;
`

export const RadioGroup = styled.div`
  display: block;
  padding: 0.40625em 0 0;
`

export const Input = styled.input`
  margin-right: 0.40625em;
`
export const RadioLabel = styled.label``

export const Link = styled.a`
  text-decoration: underline;
  margin-bottom: 1.625em;
  display: inline-block;
`
