import styled from 'styled-components'

export const RelayPoints = styled.div`
  padding: 0.40625em 1.625em;
`

export const Label = styled.p``

export const Img = styled.img`
  width: 2.4375em;
`

export const RelayInfo = styled.p`
  color: #6ec485;
  font-weight: 900;
  font-family: 'Arimo';
  margin-bottom: 0;
`

export const Link = styled.a`
  text-decoration: underline;
  font-weight: 900;
  display: inline-block;
  margin: 0.8125em 0 0;
`
