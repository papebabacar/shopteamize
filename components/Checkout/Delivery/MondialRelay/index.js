import { Trans } from '@lingui/react'
import { compose, withState } from 'recompose'
import { RelayPoints, Label, RelayInfo, Link, Img } from './style'
import QuickView from 'components/QuickView/MondialRelay'
import { getModalStore } from 'containers/App'

const MondialRelay = ({
  relayPoint: { Num, LgAdr1, LgAdr3, CP, Ville } = {},
  relayPoints,
  selectedRelayPoint,
  setSelectedRelayPoint,
  modal,
  showModal
}) => (
  <RelayPoints>
    {Num === '' ? (
      <Label>
        <Trans>No relay point available</Trans>
      </Label>
    ) : (
      <>
        <Label>
          <Img src="/static/img/relay.jpg" alt="Mondial relay" />
          <Trans>The closest relay point</Trans>
        </Label>

        <RelayInfo>{LgAdr1}</RelayInfo>
        <RelayInfo>{LgAdr3}</RelayInfo>
        <RelayInfo>
          {CP} - {Ville}
        </RelayInfo>
        <Link onClick={() => showModal({ relayPoints: relayPoints }, 'relay')}>
          <Trans>Choose another relay point</Trans>
        </Link>
      </>
    )}
    {modal.visible && (
      <QuickView
        setSelectedRelayPoint={setSelectedRelayPoint}
        selectedRelayPoint={selectedRelayPoint}
      />
    )}
  </RelayPoints>
)

const Component = compose(
  getModalStore,
  withState('editPoint', 'setEditPoint')
)(MondialRelay)

export default Component
