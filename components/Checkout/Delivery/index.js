import { Trans } from '@lingui/react'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { getCartStore } from 'containers/App'
import Item from './Item'
import MondialRelay from './MondialRelay'
import AddressForm from 'components/Account/Address/Form'
import {
  Delivery,
  Address,
  BlockAddress,
  Block,
  Label,
  Link,
  RadioGroup,
  Input,
  RadioLabel
} from './style'
import Bill from 'components/Checkout/Bill'
import { Select, Option } from './Item/Address/style'
import { setShipping } from 'api/muhajir/cart'
import { getAdressePointRelais } from 'api/muhajir/shipping/mondialrelay'

const DeliveryPage = ({
  serverCart,
  serverCart: { products, pendingWishlist, customerWishlists, shoppingIdeas },
  addresses,
  carriers,
  setEditingAddress,
  editAddress,
  cart: { shippingAddress: { id: idAddress } = {}, shippingOption },
  setCart,
  refreshCart,
  loadRelayPoints,
  relayPoints,
  selectedRelayPoint,
  setSelectedRelayPoint
}) => (
  <Delivery padding={0} flex-phone={1} flex-pad={editAddress ? 1 : 2}>
    <Block hidden={editAddress}>
      <Address flex-phone={1} flex-pad={2} padding={0}>
        <BlockAddress>
          <Trans>Choose your shipping address</Trans>
          <Select
            value={idAddress}
            onChange={async event => {
              setCart({ shippingAddress: addresses[event.target.value] })
              const { data: serverCart } = await setShipping({
                shippingAddress: addresses[event.target.value],
                billingAddress: addresses[event.target.value],
                carrier: carriers.find(({ name }) => name === shippingOption)
              })

              refreshCart(serverCart)
              await loadRelayPoints()
            }}
          >
            {Object.values(addresses).map(
              ({ id, alias, address1, city, country }) => (
                <Option key={id} value={id}>
                  {alias} | {address1} {city} {country}
                </Option>
              )
            )}
          </Select>
          <Link onClick={() => setEditingAddress(true)}>
            <Trans>Add your new address</Trans>
          </Link>
        </BlockAddress>
        <BlockAddress shipping>
          <Trans>Choose your shipping option</Trans>
          {carriers.map(({ id, name }) => (
            <RadioGroup key={id}>
              <Input
                type="radio"
                id={name}
                name="shipping"
                value={name}
                checked={shippingOption === name}
                onChange={async () => {
                  setCart({ shippingOption: name })
                  name === 'Mondial Relay' &&
                    !relayPoints.length &&
                    (await loadRelayPoints())
                }}
              />
              <RadioLabel for={name}>{name}</RadioLabel>
            </RadioGroup>
          ))}
          {shippingOption === 'Mondial Relay' &&
            relayPoints.length > 0 && (
              <MondialRelay
                relayPoints={relayPoints}
                relayPoint={selectedRelayPoint}
                selectedRelayPoint={selectedRelayPoint}
                setSelectedRelayPoint={setSelectedRelayPoint}
              />
            )}
        </BlockAddress>
      </Address>
      {pendingWishlist &&
        pendingWishlist.wishlist && (
          <Block>
            <Trans>MY WISHLIST</Trans>
            <Item
              itemType="pendingWishlist"
              item={pendingWishlist.wishlist}
              key={pendingWishlist.wishlist.id}
              addresses={addresses}
            />
          </Block>
        )}
      {customerWishlists.length > 0 && (
        <Block>
          <Label>
            <Trans>DESIRED WISHLISTS</Trans>
          </Label>
          {customerWishlists.map(({ wishlist }) => (
            <Item
              itemType="wishlists"
              item={wishlist}
              key={wishlist.id}
              addresses={addresses}
              setEditingAddress={setEditingAddress}
            />
          ))}
        </Block>
      )}
      {shoppingIdeas.length > 0 && (
        <Block>
          <Label>
            <Trans>SHOPPING IDEAS</Trans>
          </Label>
          {shoppingIdeas.map(({ wishlist }) => (
            <Item
              itemType="shoppingIdeas"
              item={wishlist}
              key={wishlist.id}
              addresses={addresses}
              setEditingAddress={setEditingAddress}
            />
          ))}
        </Block>
      )}
      {products.length > 0 && (
        <Block>
          <Label>
            <Trans>MY SELECTION OF PRODUCTS</Trans>
          </Label>
          {products.map(({ product }) => (
            <Item
              itemType="products"
              item={product}
              key={product.id}
              addresses={addresses}
              setEditingAddress={setEditingAddress}
            />
          ))}
        </Block>
      )}
    </Block>
    <Block hidden={!editAddress} takeAll={editAddress}>
      <AddressForm
        setEditing={setEditingAddress}
        id={null}
        reload
        withCancel={idAddress}
      />
    </Block>
    <Bill
      hidden={editAddress}
      nextStep="payment"
      serverCart={serverCart}
      showShippingDelay
    />
  </Delivery>
)

const Component = compose(
  getCartStore,
  withState(
    'editAddress',
    'setEditingAddress',
    ({ cart: { shippingAddress } }) => !shippingAddress
  ),
  withState('relayPoints', 'setRelayPoints', []),
  withState('selectedRelayPoint', 'setSelectedRelayPoint'),
  withHandlers({
    loadRelayPoints: ({
      setRelayPoints,
      setSelectedRelayPoint,
      cart: { shippingAddress, shippingOption },
      carriers,
      refreshCart,
      setServerCartData
    }) => async () => {
      try {
        const { data } = await getAdressePointRelais(shippingAddress.postCode)
        const parser = new DOMParser()
        const xmlDoc = parser.parseFromString(data, 'text/xml')
        const relayPoints = Array.from(
          xmlDoc.getElementsByTagName('WSI2_RecherchePointRelaisResult')[0]
            .childNodes
        )
          .slice(1)
          .map(relayPoint =>
            Array.from(relayPoint.childNodes).reduce(
              (attributes, { nodeName, innerHTML }) => ({
                ...attributes,
                [nodeName]: innerHTML
              }),
              {}
            )
          )
        setRelayPoints(relayPoints)
        setSelectedRelayPoint(relayPoints.find(() => 1))
      } catch (error) {
        // do something
      }
      try {
        const { data: serverCart } = await setShipping({
          shippingAddress,
          billingAddress: shippingAddress,
          carrier: carriers.find(({ name }) => name === shippingOption)
        })
        setServerCartData(serverCart)
        refreshCart(serverCart)
        return serverCart
      } catch (error) {
        // do something
      }
    }
  }),
  lifecycle({
    async componentDidMount() {
      const {
        loadRelayPoints,
        cart: { shippingOption }
      } = this.props
      shippingOption === 'Mondial Relay' && (await loadRelayPoints())
    }
  })
)(DeliveryPage)

export default Component
