import { Trans } from '@lingui/react'
import { compose, withHandlers } from 'recompose'
import { processPayment } from 'api/muhajir/payment'
import { Payment, Block, Label, Link } from './style'
import Bill from 'components/Checkout/Bill'
import { getUserStore, getCartStore, getOrderStore } from 'containers/App'
import { Router } from 'router'
import dynamic from 'next/dynamic'
const PayPalButton = dynamic(import('components/Theme/Paypal'), {
  ssr: false,
  loading: () => null
})

const PaymentPage = ({ user: { id: userID, language }, serverCart, buy }) => (
  <Payment padding={0} flex-phone={1} flex-pad={2}>
    <Block>
      <Label>
        <Trans>
          By clicking on Command, you declare that you have read our{' '}
          <Link href={`${Router.linkPage('cgv', { language }).as}`}>
            General Terms and Conditions of Sales
          </Link>{' '}
          and agree with them. You will be redirected to Paypal's secured
          payment site.
        </Trans>
      </Label>
      <PayPalButton
        onAuthorize={buy}
        userID={userID}
        locale={language === 'en' ? 'en_US' : 'fr_FR'}
      />
    </Block>
    <Bill serverCart={serverCart} />
  </Payment>
)
const Component = compose(
  getUserStore,
  getCartStore,
  getOrderStore,
  withHandlers({
    buy: ({
      user: { id: userID, language },
      cart: { totalItems },
      serverCart,
      setOrder
    }) => async ({ paymentID, payerID }) => {
      try {
        const {
          data: {
            state,
            payer: { payment_method: payment }
          }
        } = await processPayment({ paymentID, payerID, userID })
        state === 'approved' &&
          setOrder({
            ...serverCart,
            totalItems,
            paymentID,
            payment
          }) &&
          (document.location = `${
            Router.linkPage('checkout', { language }).as
          }?step=confirmation`)
      } catch (error) {
        //console.log(error)
      }
    }
  })
)(PaymentPage)

export default Component
