import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Payment = styled(Flex)`
  position: relative;
`

export const Block = styled(Box)`
  ${theme.media.pad`flex:0.7;`};
`
export const Label = styled.p`
  margin: 1.625em 0;
`

export const Link = styled.a`
  color: rgb(145, 204, 138);
  text-decoration: underline;
  font-weight: 900;
  display: inline-block;
`
