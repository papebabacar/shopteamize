import styled, { css } from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Steps = styled(Flex)`
  margin: 0.40625em 0 0.8125em;
  counter-reset: steps;
  height: 2.4375em;
  overflow: hidden;
`

export const Step = styled(Box)`
  flex: 1;
  position: relative;
  background: #bcbcbc;
  color: #4c4c4c;
  margin-right: 0.8125em;
  counter-increment: steps;
  text-align: center;
  &:before,
  &:after {
    content: '';
    display: block;
    position: absolute;
    top: 0;
    right: -0.8125em;
    height: 0;
    width: 0;
    border-top: 1.21875em solid transparent;
    border-left: 0.8125em solid #bcbcbc;
    border-bottom: 1.21875em solid transparent;
    z-index: 1;
  }
  &:before {
    border-left-color: #f2f2f2;
    left: 0;
  }
  &:last-child:after {
    border-top: 1.21875em solid #bcbcbc;
    border-left: 0.8125em solid #bcbcbc;
    border-bottom: 1.28125em solid #bcbcbc;
  }
  ${props =>
    props.isCurrent &&
    css`
      background: rgb(110, 196, 133);
      color: #fff;
      &:first-child:before {
        border-left-color: rgb(110, 196, 133);
      }
      &:after {
        border-left: 0.8125em solid rgb(110, 196, 133);
      }
      &:last-child:after {
        border-top: 1.21875em solid rgb(110, 196, 133);
        border-left: 0.8125em solid rgb(110, 196, 133);
        border-bottom: 1.28125em solid rgb(110, 196, 133);
      }
    `} ${props =>
    props.isDone &&
    css`
      background: rgb(145, 204, 138);
      color: rgb(26, 120, 34);
      &:first-child:before {
        border-left-color: rgb(145, 204, 138);
      }
      &:after {
        border-left: 0.8125em solid rgb(145, 204, 138);
      }
      &:last-child:after {
        border-top: 1.21875em solid #2dbe3d;
        border-left: 0.8125em solid #bcbcbc;
        border-bottom: 1.21875em solid #2dbe3d;
      }
    `};
  ${theme.media.pad`text-align: left;`};
`

export const BreadcrumbItem = styled.p`
  margin: 0;
  line-height: 2.4375em;
  &:before {
    content: counter(steps);
    display: inline-block;
    position: relative;
    margin: 0 0.8125em 0 1.625em;
    width: 1.625em;
    height: 1.625em;
    line-height: 1.625em;
    border: 1px solid;
    border-radius: 50%;
    text-align: center;
  }
`

export const BreadcrumbLink = styled(BreadcrumbItem.withComponent('a'))`
  &:before {
    top: 0;
  }
`
export const Label = styled.span`
  display: none;
  ${theme.media.pad`display:inline;`};
`
