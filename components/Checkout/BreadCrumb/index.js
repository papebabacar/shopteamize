import { Trans } from '@lingui/react'
import { Steps, Step, BreadcrumbItem, BreadcrumbLink, Label } from './style'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const Component = ({ steps, currentStep, user: { language } }) => (
  <Steps padding={0}>
    {steps.map((step, index) => {
      return (
        <Step
          key={step}
          isDone={index < steps.indexOf(currentStep)}
          isCurrent={step === currentStep}
        >
          {index < steps.indexOf(currentStep) &&
          step !== 'login' &&
          currentStep !== 'confirmation' ? (
            <BreadcrumbLink
              href={`${
                Router.linkPage('checkout', { language }).as
              }?step=${step}`}
            >
              <Label>
                {step === 'basket' && <Trans>Shopping Cart</Trans>}
                {step === 'login' && <Trans>Sign In</Trans>}
                {step === 'delivery' && <Trans>Shipping</Trans>}
                {step === 'payment' && <Trans>Payment</Trans>}
                {step === 'confirmation' && <Trans>Confirmation</Trans>}
              </Label>
            </BreadcrumbLink>
          ) : (
            <BreadcrumbItem>
              <Label>
                {step === 'basket' && <Trans>Shopping Cart</Trans>}
                {step === 'login' && <Trans>Sign In</Trans>}
                {step === 'delivery' && <Trans>Shipping</Trans>}
                {step === 'payment' && <Trans>Payment</Trans>}
                {step === 'confirmation' && <Trans>Confirmation</Trans>}
              </Label>
            </BreadcrumbItem>
          )}
        </Step>
      )
    })}
  </Steps>
)

export default getUserStore(Component)
