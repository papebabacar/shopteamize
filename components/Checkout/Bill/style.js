import styled from 'styled-components'
import theme from 'components/Theme'
import { Box } from 'components/Theme/Layout/FlexBox'

export const Bill = styled(Box)`
  margin-top: 3.25em;
  padding: 1.625em 0.40625em;
  position: relative;
  order: -1;
  display: ${props => (props.hidden ? 'none' : 'block')};
  ${theme.media.pad`
    flex:0.3;
    order:1;
  `};
`
export const Sticky = styled.div`
  position: sticky;
  top: 3.25em;
`

export const Line = styled.div`
  padding: 0.8125em 0.8125em 0;
`

export const Discount = styled.del`
  color: #ec4040;
`

export const Label = styled.p`
  width: 50%;
  display: inline-block;
  &:last-of-type {
    text-align: right;
    vertical-align: bottom;
  }
`

export const Total = styled(Line)`
  font-weight: 900;
  font-family: 'Arvo';
  font-size: 125%;
`
export const Pub = styled.p`
  font-weight: 900;
  background: white;
  padding: 0.40625em;
  color: rgb(26, 120, 34);
  text-align: center;
  font-style: italic;
`
export const PubImg = styled.img`
  width: 1.625em;
  position: relative;
  margin-top: 0.203125em;
  left: -0.8125em;
`
