import { Trans } from '@lingui/react'
import { Checkout } from '../Actions'
import {
  Bill,
  Sticky,
  Line,
  Label,
  Total,
  Discount,
  Pub,
  PubImg
} from './style'
import { getCartStore } from 'containers/App'

const Component = ({
  serverCart,
  cart: {
    pendingWishlist,
    wishlists,
    shoppingIdeas,
    totalQuantity,
    totalItems = 0,
    totalShopping = 0,
    totalShipping = 0,
    total = 0,
    currencyCode,
    currentStep,
    shoppingPrice
  },
  nextStep,
  disabled,
  onClick,
  hidden,
  showPub,
  showShippingDelay
}) => (
  <Bill hidden={hidden}>
    <Sticky>
      <Line>
        <Label>
          <Trans>Total items</Trans> ({totalQuantity})
        </Label>
        <Label>
          {Number(totalItems).toFixed(2)} {currencyCode}
        </Label>
      </Line>
      {totalShopping > 0 && (
        <Line>
          <Label>
            {Object.keys(pendingWishlist).length > 0 ||
            Object.keys(wishlists).length > 0 ||
            Object.keys(shoppingIdeas).length > 0 ? (
              <Trans>
                Shopping fees<br />shoptimized
              </Trans>
            ) : (
              <Trans>Shopping fees</Trans>
            )}
          </Label>
          <Label>
            {shoppingPrice && (
              <Discount>
                {shoppingPrice}
                {currencyCode}
              </Discount>
            )}{' '}
            {totalShopping} {currencyCode}
          </Label>
        </Line>
      )}
      {currentStep !== 'basket' &&
        totalShipping > 0 && (
          <Line>
            <Label>
              <Trans>Shipping fees</Trans>
            </Label>
            <Label>
              {totalShipping} {currencyCode}
            </Label>
          </Line>
        )}
      <Total>
        <Label>
          <Trans>TOTAL</Trans>
        </Label>
        <Label>
          {currentStep === 'basket'
            ? Number(totalItems + totalShopping).toFixed(2)
            : Number(total).toFixed(2)}{' '}
          {currencyCode}
        </Label>
      </Total>
      {nextStep &&
        totalItems && (
          <Checkout
            step={currentStep}
            nextStep={nextStep}
            disabled={disabled}
            onClick={onClick}
            serverCart={serverCart}
          />
        )}
      {showPub && (
        <Pub>
          <PubImg src="/static/img/info.png" alt="info" />
          <Trans>
            Add products without any others additional shopping fees.
          </Trans>
        </Pub>
      )}
      {showShippingDelay && (
        <Pub>
          <PubImg src="/static/img/info.png" alt="info" />
          <Trans>Shipping in five open days</Trans>
        </Pub>
      )}
    </Sticky>
  </Bill>
)

export default getCartStore(Component)
