import { Trans } from '@lingui/react'
import { Actions, Arrow } from './style'
import { Button, SubmitButton } from 'components/Theme'
import { initializecheckout } from 'components/Theme/GTM'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

export const Checkout = getUserStore(
  ({ nextStep, disabled, onClick, step, serverCart, user: { language } }) => (
    <Actions>
      <SubmitButton
        block
        disabled={disabled}
        onClick={() => {
          if (disabled) return
          initializecheckout(serverCart, step)
          onClick
            ? onClick()
            : (document.location = `${
                Router.linkPage('checkout', { language }).as
              }?step=${nextStep}`)
        }}
      >
        <Trans>VALIDATE</Trans> <Arrow>›</Arrow>
      </SubmitButton>
    </Actions>
  )
)

export const ContinueShopping = () => (
  <Button secondary href="/">
    <Arrow>‹</Arrow> <Trans>CONTINUE MY SHOPPING</Trans>
  </Button>
)
