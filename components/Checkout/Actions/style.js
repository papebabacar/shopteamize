import styled from 'styled-components'

export const Actions = styled.div``

export const Arrow = styled.span`
  font-size: 200%;
  position: relative;
  top: 0.0625em;
  line-height: 0.625em;
`
