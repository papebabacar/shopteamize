import { Trans } from '@lingui/react'
import Product from './Product'
import Wishlist from './WishList/'
import { getCartStore } from 'containers/App'
import { Label, Cart, List, EmptyCart } from './style'
import Bill from 'components/Checkout/Bill'
import { ContinueShopping } from 'components/Checkout/Actions'

const Component = ({
  serverCart,
  cart: {
    products,
    pendingWishlist,
    wishlists,
    shoppingIdeas,
    totalQuantity,
    currencyCode
  },
  updateCartItem,
  removeCartItem
}) => (
  <>
    <Cart padding={0} flex-phone={1} flex-pad={2}>
      <List isEmpty={totalQuantity === 0}>
        {Object.keys(pendingWishlist).length > 0 && (
          <Label>
            <Trans>MY WISHLIST</Trans>
          </Label>
        )}
        {Object.keys(pendingWishlist).map(id => {
          const data = serverCart.pendingWishlist.wishlist
          return (
            data && (
              <Wishlist
                key={id}
                wishlist={data}
                quantity={pendingWishlist[id].quantity}
                currencyCode={currencyCode}
                updateCartItem={updateCartItem}
                removeCartItem={removeCartItem}
              />
            )
          )
        })}
        {Object.keys(wishlists).length > 0 && (
          <Label>
            <Trans>DESIRED WISHLISTS</Trans>
          </Label>
        )}
        {Object.keys(wishlists).map(id => {
          const data = serverCart.customerWishlists.find(
            ({ wishlist }) => wishlist.id === Number(id)
          )
          return (
            data && (
              <Wishlist
                key={id}
                wishlist={data.wishlist}
                quantity={wishlists[id].quantity}
                currencyCode={currencyCode}
                updateCartItem={updateCartItem}
                removeCartItem={removeCartItem}
              />
            )
          )
        })}
        {Object.keys(shoppingIdeas).length > 0 && (
          <Label>
            <Trans>SHOPPING IDEAS</Trans>
          </Label>
        )}
        {Object.keys(shoppingIdeas).map(id => {
          const data = serverCart.shoppingIdeas.find(
            ({ wishlist }) => wishlist.id === Number(id)
          )
          return (
            data && (
              <Wishlist
                key={id}
                wishlist={data.wishlist}
                quantity={shoppingIdeas[id].quantity}
                currencyCode={currencyCode}
                updateCartItem={updateCartItem}
                removeCartItem={removeCartItem}
              />
            )
          )
        })}
        {Object.keys(products).length > 0 && (
          <Label>
            <Trans>MY SELECTION OF PRODUCTS</Trans>
          </Label>
        )}
        {Object.keys(products).map(id => {
          const data = serverCart.products.find(
            ({ product }) => product.id === Number(id)
          )
          return (
            data && (
              <Product
                key={id}
                product={data.product}
                quantity={products[id].quantity}
                currencyCode={currencyCode}
                updateCartItem={updateCartItem}
                removeCartItem={removeCartItem}
              />
            )
          )
        })}
        {totalQuantity === 0 && (
          <EmptyCart>
            <Trans>Your basket is empty</Trans>
          </EmptyCart>
        )}
      </List>
      {totalQuantity > 0 && (
        <Bill
          nextStep="delivery"
          serverCart={serverCart}
          showPub={
            Object.keys(wishlists).length > 0 ||
            Object.keys(shoppingIdeas).length > 0
          }
        />
      )}
    </Cart>
    <ContinueShopping />
  </>
)

export default getCartStore(Component)
