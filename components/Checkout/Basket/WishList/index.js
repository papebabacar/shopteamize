import { Trans, withI18n } from '@lingui/react'
import { compose, withState } from 'recompose'
import {
  Card,
  Detail,
  PictureBox,
  Title,
  Label,
  Span,
  Info,
  Line,
  Price,
  Link,
  Button,
  Update
} from './style'
import { NumericInput, Picture } from 'components/Theme'
import { getUserStore } from 'containers/App'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'

const Wishlist = ({
  wishlist,
  quantity,
  currencyCode,
  updateCartItem,
  removeCartItem,
  view,
  showView,
  quantityToAdd,
  setQuantityToAdd,
  user: { language },
  i18n
}) => (
  <Card flex-phone={3} padding={0.40625}>
    <PictureBox box-flex={0.2}>
      <Picture
        src={wishlist.mainImageUrl}
        alt={getTitle(wishlist.labels, language)}
        size="9.5em"
        margin="0"
        cover
      />
    </PictureBox>
    <Detail box-flex={0.75}>
      <Price>
        {wishlist.price} {currencyCode}
      </Price>
      <Link
        href={
          wishlist.currentStatus === 'PENDING'
            ? `${
                Router.linkPage('account', { language }).as
              }?view=wishlist&edit=${wishlist.id}`
            : `${
                Router.linkPage('wishlist', {
                  language,
                  id: wishlist.id,
                  name: slugify(getTitle(wishlist.labels, language))
                }).as
              }`
        }
      >
        <Title>{getTitle(wishlist.labels, language)}</Title>
      </Link>
      <Line>
        {wishlist.addressInitiator ? (
          <>
            <Span>
              <Trans>Team</Trans>{' '}
            </Span>
            {`${wishlist.addressInitiator.firstName} ${
              wishlist.addressInitiator.lastName
            }`}
          </>
        ) : wishlist.currentStatus === 'PENDING' ? (
          ''
        ) : (
          <Span>Team Shopteamize</Span>
        )}
      </Line>
      {view === 'update' ? (
        <Line>
          <Trans>Quantity</Trans>
          {' : '}
          <NumericInput
            bg="#eee"
            max-width="6em"
            value={quantityToAdd}
            onChange={event => {
              setQuantityToAdd(Number(event.target.value))
            }}
          />
        </Line>
      ) : (
        <Label>
          {quantity} x {wishlist.itemsCount} article(s)
        </Label>
      )}
      {view === 'delete' && (
        <Info>
          <Trans>Would you like to confirm the suppression?</Trans>
        </Info>
      )}
    </Detail>
    {!view && (
      <Detail box-flex={0.05}>
        <Button onClick={() => showView('delete')} title={i18n.t`Delete`}>
          <Update src="/static/img/delete.png" />
        </Button>
        <Button
          onClick={() => {
            setQuantityToAdd(quantity)
            showView('update')
          }}
          title={i18n.t`Update`}
        >
          <Update src="/static/img/edit.png" />
        </Button>
      </Detail>
    )}
    {view === 'update' && (
      <Detail box-flex={0.05}>
        <Button secondary onClick={() => showView()} title={i18n.t`Close`}>
          X
        </Button>
        <Button
          confirm
          onClick={() => {
            updateCartItem(
              wishlist,
              quantityToAdd,
              wishlist.type === 'CUSTOMER_WISHLIST'
                ? wishlist.currentStatus === 'PENDING'
                  ? 'pendingWishlist'
                  : 'wishlists'
                : 'shoppingIdeas'
            )
            showView()
          }}
          title={i18n.t`Confirm`}
        >
          OK
        </Button>
      </Detail>
    )}
    {view === 'delete' && (
      <Detail box-flex={0.05}>
        <Button secondary onClick={() => showView()} title={i18n.t`Close`}>
          X
        </Button>
        <Button
          confirm
          onClick={() =>
            removeCartItem(
              wishlist.id,
              wishlist.type === 'CUSTOMER_WISHLIST'
                ? wishlist.currentStatus === 'PENDING'
                  ? 'pendingWishlist'
                  : 'wishlists'
                : 'shoppingIdeas'
            )
          }
          title={i18n.t`Confirm`}
        >
          OK
        </Button>
      </Detail>
    )}
  </Card>
)
const Component = compose(
  withI18n(),
  getUserStore,
  withState('view', 'showView', false),
  withState('quantityToAdd', 'setQuantityToAdd', ({ quantity }) => quantity)
)(Wishlist)

export default Component

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
