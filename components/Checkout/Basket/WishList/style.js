import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'

export const Card = styled(Flex)`
  background: white;
  margin-bottom: 1.625em;
  & + & {
    margin-top: 0;
  }
`

export const PictureBox = styled(Box)``

export const Detail = styled(Box)`
  position: relative;
`

export const Title = styled.h3`
  display: block;
`

export const Info = styled.p`
  height: 1.625em;
  display: block;
  font-weight: 900;
  color: rgb(26, 120, 34);
`

export const Line = styled.p`
  margin-bottom: 0;
`

export const Label = styled.p`
  margin-top: 0.8125em;
  display: inline-block;
  padding: 0.203125em 0.8125em;
  margin-right: 0.40625em;
  background: #eee;
  border-radius: 0.8125em;
  min-width: 4em;
  text-align: center;
  font-size: 90%;
`
export const Price = styled.span`
  font-weight: 900;
  font-family: 'Arimo';
`

export const Link = styled.a`
  display: block;
`

export const Span = styled.span`
  color: rgb(26, 120, 34);
  font-weight: 900;
  font-family: 'Arimo';
`

export const Button = styled.a`
  text-align: center;
  background: ${props =>
    props.confirm
      ? 'rgb(26, 120, 34)'
      : props.secondary
        ? 'rgb(120,207,139)'
        : '#eee'};
  color: ${props => (props.confirm || props.secondary ? 'white' : 'black')};
  width: 2em;
  height: 2.25em;
  padding: 0.3125em;
  display: block;
  margin-bottom: 0.8125em;
`

export const Update = styled.img`
  width: 1em;
`
