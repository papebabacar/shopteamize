import { Trans, withI18n } from '@lingui/react'
import { compose, withState } from 'recompose'
import {
  Card,
  Detail,
  PictureBox,
  Title,
  Label,
  Info,
  Line,
  Price,
  Link,
  Button,
  Update
} from './style'
import { NumericInput, Picture } from 'components/Theme'
import { Router } from 'router'
import { getUserStore } from 'containers/App'
import slugify from '@sindresorhus/slugify'
// import { initializeRemoveFromCartClick } from 'components/Theme/GTM'

const Product = ({
  product,
  quantity,
  currencyCode,
  updateCartItem,
  removeCartItem,
  view,
  showView,
  quantityToAdd,
  setQuantityToAdd,
  user: { language },
  i18n
}) => (
  <Card flex-phone={3} padding={0.40625}>
    <PictureBox box-flex={0.2}>
      <Picture
        src={product.mainImageUrl}
        alt={product.name}
        size="9.5em"
        margin="0"
        cover
      />
    </PictureBox>
    <Detail box-flex={0.75}>
      <Price>
        {product.price} {currencyCode}
      </Price>
      <Link
        href={`${
          Router.linkPage('product', {
            language,
            id: product.id,
            name: slugify(product.name)
          }).as
        }`}
      >
        <Title>{product.name}</Title>
      </Link>
      {view === 'update' ? (
        <Line>
          <Trans>Quantity</Trans>
          {' : '}
          <NumericInput
            bg="#eee"
            max-width="6em"
            value={quantityToAdd}
            onChange={event => {
              setQuantityToAdd(Number(event.target.value))
            }}
          />
        </Line>
      ) : (
        <>
          <Label>{quantity}</Label>
          {product.color && <Label>{product.color.toUpperCase()}</Label>}
          {product.size && <Label>{product.size.toUpperCase()}</Label>}
        </>
      )}
      {view === 'delete' && (
        <Info>
          <Trans>Would you like to confirm the suppression?</Trans>
        </Info>
      )}
    </Detail>
    {!view && (
      <Detail box-flex={0.05}>
        <Button onClick={() => showView('delete')} title={i18n.t`Delete`}>
          <Update src="/static/img/delete.png" />
        </Button>
        <Button
          onClick={() => {
            setQuantityToAdd(quantity)
            showView('update')
          }}
          title={i18n.t`Update`}
        >
          <Update src="/static/img/edit.png" />
        </Button>
      </Detail>
    )}
    {view === 'update' && (
      <Detail box-flex={0.05}>
        <Button secondary onClick={() => showView()} title={i18n.t`Close`}>
          X
        </Button>
        <Button
          confirm
          onClick={() => {
            updateCartItem(product, quantityToAdd, 'products')
            showView()
          }}
          title={i18n.t`Confirm`}
        >
          OK
        </Button>
      </Detail>
    )}
    {view === 'delete' && (
      <Detail box-flex={0.05}>
        <Button secondary onClick={() => showView()} title={i18n.t`Close`}>
          X
        </Button>
        <Button
          confirm
          onClick={() => removeCartItem(product.id, 'products')}
          title={i18n.t`Confirm`}
        >
          OK
        </Button>
      </Detail>
    )}
  </Card>
)

const Component = compose(
  withI18n(),
  getUserStore,
  withState('view', 'showView'),
  withState('quantityToAdd', 'setQuantityToAdd', ({ quantity }) => quantity)
)(Product)

export default Component
