import styled from 'styled-components'
import theme from 'components/Theme'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'

export const Cart = styled(Flex)`
  .react-numeric-input {
    width: 7em;
    margin: 0 auto;
    display: block;
    > input {
      font-size: 115% !important;
      width: 6em;
      line-height: 1.625em !important;
    }
  }
`

export const List = styled(Box)`
  ${theme.media.pad`flex:${props => (props.isEmpty ? 1 : 0.7)};`};
`

export const Label = styled.p`
  color: #888;
  font-weight: 900;
`

export const EmptyCart = styled.p`
  background: white;
  text-align: center;
  padding: 18vh 0;
  font-weight: 900;
`
