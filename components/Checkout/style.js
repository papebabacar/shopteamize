import styled from 'styled-components'

export const Checkout = styled.section`
  min-height: 74vh;
  max-width: 1120px;
  margin: 0 auto 1.625em;
  width: 100%;
`
