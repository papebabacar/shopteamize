import styled from 'styled-components'

export const CookiesPanel = styled.div`
  color: #ddd;
  font-size: 80%;
  text-align: center;
  background: rgba(40, 40, 40, 0.85);
  padding: 1.625em 0.8125em;
  display: ${props => (props.hidden ? 'none' : 'block')};
  position: fixed;
  bottom: 0;
  width: 100%;
  border-left: 2px solid rgb(26, 120, 34);
  a {
    font-size: 100%;
  }
`
export const Link = styled.a`
  text-decoration: underline;
  margin-right: 1.625em;
`
