import { Trans } from '@lingui/react'
import { getUserStore } from 'containers/App'
import { Router } from 'router'
import { CookiesPanel, Link } from './style'
import { MiniButton } from 'components/Theme'

const Component = ({
  user: { checkedCookies, language },
  setCheckedCookies
}) => (
  <CookiesPanel hidden={checkedCookies}>
    <Trans>
      By continuing to visit this site, you accept the use of cookies to offer
      you services and offers tailored to your interests.{' '}
      <Link
        onClick={() => setCheckedCookies(true)}
        href={`${Router.linkPage('cookies', { language }).as}`}
      >
        See more.
      </Link>
    </Trans>
    <MiniButton onClick={() => setCheckedCookies(true)}>
      <Trans>Ok I agree</Trans>
    </MiniButton>
  </CookiesPanel>
)

export default getUserStore(Component)
