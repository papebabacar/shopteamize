import { Trans, withI18n } from '@lingui/react'
import { compose, withState, withHandlers } from 'recompose'
import {
  AccountForm,
  SubmitButton,
  FormLabel,
  Link,
  FormTitle,
  Notification
} from 'components/Theme'
import { Form, withForm } from 'components/Theme/Form'
import Field from 'components/Theme/Form/Field'
import { register } from 'api/account'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const RegisterForm = ({
  post,
  submitting,
  submitError,
  i18n,
  user: { language }
}) => (
  <AccountForm>
    <Form post={post} submitting={submitting}>
      <FormTitle>
        <Trans>Create your account</Trans>
      </FormTitle>
      <Field
        label="Email"
        name="email"
        value=""
        placeholder="Email"
        required
        validations="isEmail"
        validationError={i18n.t`Please enter a valid email address`}
      />
      <Field
        label={<Trans>Last name</Trans>}
        name="lastName"
        value=""
        placeholder={i18n.t`Last name`}
        required
      />
      <Field
        label={<Trans>First name</Trans>}
        name="firstName"
        value=""
        placeholder={i18n.t`First name`}
        required
      />
      <Field
        label={<Trans>Password</Trans>}
        name="password"
        type="password"
        value=""
        placeholder="*******"
        required
      />
      <Field
        label="Confirmation"
        name="confirmation"
        type="password"
        value=""
        placeholder="*******"
        required
        validations="equalsField:password"
        validationError={i18n.t`The two passwords must be equal`}
      />
      <FormLabel right>
        <Link href={`${Router.linkPage('login', { language }).as}`}>
          ‹ <Trans>Back to login page</Trans>
        </Link>
      </FormLabel>
      {submitError && <Notification>{submitError}</Notification>}
      <SubmitButton disabled={submitting}>
        <Trans>Submit</Trans>
      </SubmitButton>
    </Form>
  </AccountForm>
)

const Component = compose(
  getUserStore,
  withI18n(),
  withState('submitError', 'setSubmitError'),
  withHandlers({
    post: ({
      setFormState,
      setSubmitError,
      i18n,
      user: { language }
    }) => async data => {
      setFormState(true)
      try {
        const { email, firstName, lastName, password } = data
        await register({
          email,
          firstName,
          lastName,
          login: email,
          password,
          langKey: 'fr'
        })
        document.location = `${Router.linkPage('login', { language }).as}`
      } catch (error) {
        setSubmitError(
          i18n.t`A technical error has occurred. Please reload the page`
        )
        setTimeout(() => setSubmitError(), 5000)
        setFormState(false)
      }
    }
  })
)(RegisterForm)

export default withForm(Component)
