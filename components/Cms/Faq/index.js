import { Article } from 'components/Theme'
import { Section, Title } from './style'

const divStyle = {
  minWidth: '100%'
}

const Component = ({ title, description }) => (
  <Article>
    <Title>{title}</Title>
    <div className="video-wrapper" style={divStyle}>
      <iframe
        width="100%"
        height="360"
        src="https://www.youtube.com/embed/wp--LwH29Oc"
        frameBorder="0"
        allow="autoplay; encrypted-media"
        allowFullScreen
      />
    </div>
    <br />
    <div className="video-wrapper" style={divStyle}>
      <iframe
        width="100%"
        height="360"
        src="https://www.youtube.com/embed/wBEvzQF0zag"
        frameBorder="0"
        allow="autoplay; encrypted-media"
        allowFullScreen
      />
    </div>
    <br />
    <Section dangerouslySetInnerHTML={{ __html: description }} />
  </Article>
)

export default Component
