import { Iframe } from './style'

const Component = () => (
  <Iframe
    src="https://docs.google.com/forms/d/e/1FAIpQLSfE0pkdz6Mo8nHgyn2iuVtR2mzqwmloeXfStUBdxHtUB4qRzg/viewform?embedded=true"
    //width="760"
    // height="500"
    frameborder="0"
    marginheight="0"
    marginwidth="0"
  >
    Chargement en cours...
  </Iframe>
)

export default Component
