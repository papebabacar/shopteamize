import styled from 'styled-components'

export const Iframe = styled.iframe`
  min-height: 100%;
  height: 100vh;
  margin: 3.25em auto;
`
