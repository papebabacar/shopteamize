import { Article } from 'components/Theme'
import { Section, Title } from './style'

const Component = ({ title, description }) => (
  <Article>
    <Title>{title}</Title>
    <Section dangerouslySetInnerHTML={{ __html: description }} />
  </Article>
)

export default Component
