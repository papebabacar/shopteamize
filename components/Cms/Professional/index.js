import { Trans } from '@lingui/react'
import { Article } from 'components/Theme'
import { Section, Title, Span } from './style'

const Component = ({ title, description }) => (
  <Article>
    <Title>
      {title}{' '}
      <Span>
        (<Trans>feature unavailable at this time</Trans>)
      </Span>
    </Title>
    <Section dangerouslySetInnerHTML={{ __html: description }} />
  </Article>
)

export default Component
