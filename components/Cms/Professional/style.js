import styled from 'styled-components'

export const Section = styled.section``

export const Title = styled.h1``

export const Span = styled.span`
  color: #ec4040;
  font-size: 50%;
`
