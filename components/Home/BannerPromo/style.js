import styled from 'styled-components'
import theme from 'components/Theme'

export const Section = styled.section`
  background: white;
  background: linear-gradient(
      145deg,
      rgba(255, 255, 255, 0.83),
      rgba(255, 255, 255, 0.5),
      #67181800
    ),
    url(/static/img/promo.png);
  background-repeat: no-repeat;
  background-position: right;
  min-height: 30vh;
  color: #222;
  margin: 0 1.625em;
  background-size: auto 100%;
  ${theme.media.tablet`
    background-image: linear-gradient(
      145deg,
      rgba(255, 255, 255, 0.83),
      rgba(255, 255, 255, 0.95),
      rgba(255, 255, 255, 0.099)
    ),
    url(/static/img/promo.png);
  `};
`
export const Content = styled.section`
  display: block;
  padding: 3.25em 0.40625em;
  text-align: center;
  max-width: 40em;
  margin: 0 auto;
  ${theme.media.pad`padding: 3.25em 6.125em;display: inline-block;`};
`
export const Title = styled.h1`
  margin-bottom: 0.8125em;
  font-family: 'Courier New';
  color: #d02f85;
  font-size: 200%;
  ${theme.media.pad`    
    font-size:150%;
  `};
`
