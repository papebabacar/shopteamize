import { Trans } from '@lingui/react'
import { Section, Content, Title } from './style'
import { Button } from 'components/Theme'
import { getUserStore } from 'containers/App'

const Component = ({ user: { language } }) => (
  <Section>
    <Content>
      <Title>
        <Trans>
          Prepare Eid el fitr !<br /> Enjoy the latest items of <br />Shopteamize
          !
        </Trans>
      </Title>
      <Button promo border href={`${language}/catalog?category=clothing`}>
        <Trans>SEE MORE</Trans>
      </Button>
    </Content>
  </Section>
)

export default getUserStore(Component)
