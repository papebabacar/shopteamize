import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

const {
  settings: { padding, base },
  green1,
  green2
} = theme

export const Section = styled(Flex)``

export const Card = styled(Box)`
  text-align: center;
  padding: ${padding(base, base / 2, base / 2)};
  color: #fff;
  &:first-of-type {
    background: ${green1};
  }
  &:last-of-type {
    background: ${green2};
  }
`
export const Title = styled.h1`
  font-size: 2.03125em;
  line-height: 1.1171875em;
  margin-bottom: 0.1015625em;
`

export const Description = styled.p`
  font-family: 'Helvetica Light';
  letter-spacing: -0.03125em;
`
export const Link = styled.a``
