import { Trans } from '@lingui/react'
import { Section, Card, Title, Link, Description } from './style'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const Component = ({ user: { language } }) => (
  <Section flex-phone={1} flex-tablet={2}>
    <Card>
      <Link href={`${Router.linkPage('b2b', { language }).as}`}>
        <Title>
          <Trans>Professional</Trans>
        </Title>
        <Description>
          <Trans>Looking for a product out of our catalog?</Trans>
        </Description>
      </Link>
    </Card>
    <Card>
      <Link href={`${Router.linkPage('b2c', { language }).as}`}>
        <Title>
          <Trans>Particular</Trans>
        </Title>
        <Description>
          <Trans>Wishing to create or join a wishlist?</Trans>
        </Description>
      </Link>
    </Card>
  </Section>
)

export default getUserStore(Component)
