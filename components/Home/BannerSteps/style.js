import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

const {
  settings: { padding, base, unit },
  green1,
  green2,
  green5
} = theme

export const Section = styled(Flex)``

export const Content = styled(Flex)`
  background: linear-gradient(135deg, ${green2}, ${green1});
  color: white;
`

export const Card = styled(Box)`
  text-align: center;
  position: relative;
  &:after {
    opacity: 0.3;
    content: '';
    position: absolute;
    display: block;
    bottom: 0;
    height: 1px;
    width: calc(100% - ${2 * base + unit});
    left: ${base + unit};
    background: ${green5};
    ${theme.media.pad`
      top: ${base + unit};
      left:auto;
		  right: 0;
      width: 1px;
      height: calc(100% - ${2 * base + unit});
    `};
  }
  &:last-of-type {
    &:after {
      display: none;
    }
  }
`
export const Step = styled(Flex)`
  padding: ${padding(base, base / 4, base, base / 9)};
  flex-flow: nowrap;
  ${theme.media.pad`
    flex-direction: column;
  `};
`

export const Icon = styled(Box.withComponent('img'))`
  align-self: center;
  margin: -${base / 2 + unit} ${base + unit} 0 ${base + unit};
  width: ${(base * 12) / 7 + unit};
  ${theme.media.tablet`
    margin-right:  ${base * 3 + unit};
  `};
  ${theme.media.pad`
    margin: ${base + unit} auto;
    height: 3em;
    width: auto;
  `};
`

export const Description = styled(Box.withComponent('p'))`
  align-self: center;
  font-family: 'Helvetica Light';
  letter-spacing: -0.03125em;
  line-height: 1.3625em;
  text-transform: uppercase;
  padding: 0 0.8125em;
  text-align: left;
  ${theme.media.pad`
    text-align: center;
  `};
`
