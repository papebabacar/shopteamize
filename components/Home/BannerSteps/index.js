import { Trans } from '@lingui/react'
import { Section, Content, Card, Step, Icon, Description } from './style'

const Component = () => (
  <Section>
    <Content padding={0} flex-phone={1} flex-pad={5}>
      <Card>
        <Step>
          <Icon src="/static/img/wishlist.png" />
          <Description>
            <Trans>
              Create or participate<br />at a<br />wishlist
            </Trans>
          </Description>
        </Step>
      </Card>
      <Card>
        <Step>
          <Icon src="/static/img/share.png" />
          <Description>
            <Trans>
              Share your wishlist<br />and invite<br />your friends
            </Trans>
          </Description>
        </Step>
      </Card>
      <Card>
        <Step>
          <Icon src="/static/img/basket-checked.png" />
          <Description>
            <Trans>
              Your wishlist<br />becomes a group<br />purchase
            </Trans>
          </Description>
        </Step>
      </Card>
      <Card>
        <Step>
          <Icon src="/static/img/shopping.png" />
          <Description>
            <Trans>
              A shopper does<br />the shopping<br />for you
            </Trans>
          </Description>
        </Step>
      </Card>
      <Card>
        <Step>
          <Icon src="/static/img/shipping.png" />
          <Description>
            <Trans>
              Receive your products<br />at home<br />in a lower cost
            </Trans>
          </Description>
        </Step>
      </Card>
    </Content>
  </Section>
)

export default Component
