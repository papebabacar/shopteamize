import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

const {
  settings: { padding, base, unit }
} = theme

export const Banner = styled(Flex)``

export const Pub = styled(Box)`
  background: url(/static/img/pub.jpg);
  background-size: cover;
  min-height: 26vh;
  color: #fff;
`
export const Content = styled.div`
  display: block;
  padding: ${padding((base * 3) / 2, base * 2, 0)};
  text-align: center;
  max-width: ${25 * base + unit};
`
export const Title = styled.h1`
  font-size: 2.3125em;
  margin-bottom: 0.3125em;
`

export const Description = styled.p`
  font-size: 110%;
`
