import { Banner, Pub, Content, Title, Description } from './style'

const Component = () => (
  <Banner flex-phone={1}>
    <Pub>
      <Content>
        <Title>Publicité</Title>
        <Description>
          lorem ipsum dolor consectetur elit. lorem ipsum dolor consectetur
          elit. lorem ipsum dolor consectetur elit.
        </Description>
      </Content>
    </Pub>
  </Banner>
)

export default Component
