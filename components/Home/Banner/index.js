import { Trans } from '@lingui/react'
import Carousel from 'nuka-carousel'
import {
  Slide,
  Iframe,
  Content,
  Title,
  Link,
  Description,
  Dots,
  Dot
} from './style'
import Actions from './Actions'
import { getUserStore } from 'containers/App'

const Component = ({ slides, user: { language } }) => (
  <Carousel
    autoplay
    wrapAround
    autoplayInterval={10000}
    renderCenterLeftControls={() => null}
    renderCenterRightControls={() => null}
    renderBottomCenterControls={({
      slideCount,
      slidesToScroll,
      goToSlide,
      currentSlide
    }) => (
      <Dots>
        {[...Array(Number(Number(slideCount / slidesToScroll).toFixed()))].map(
          (key, index) => (
            <Dot
              key={index}
              selected={currentSlide === index}
              onClick={() => goToSlide(index)}
            >
              •
            </Dot>
          )
        )}
      </Dots>
    )}
  >
    <Slide image="/static/img/video.jpg">
      <Content>
        <Title>
          <Trans>How to Shopteamize, check our tutorial.</Trans>
        </Title>
        <Iframe
          src="https://www.youtube.com/embed/wp--LwH29Oc?rel=0&autoplay=1"
          frameBorder="0"
          allow="autoplay; encrypted-media"
          allowFullScreen
        />
      </Content>
    </Slide>
    <Slide image="/static/img/cover.jpg">
      <Content>
        <Title>
          <Trans>
            Shopteamize<br />by grouping your purchases
          </Trans>{' '}
          !
        </Title>
        <Actions language={language} />
      </Content>
    </Slide>
    {slides.map(({ image, title, description, url }) => (
      <Slide image={image} key={title}>
        <Content>
          <Link href={`${language}${url}`}>
            {' '}
            <Title>{title}</Title>
            <Description>{description}</Description>
          </Link>
          <Actions language={language} />
        </Content>
      </Slide>
    ))}
  </Carousel>
)

export default getUserStore(Component)
