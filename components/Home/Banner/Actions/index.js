import { Trans } from '@lingui/react'
import { Description, Actions, ActionsBox, Info } from './style'
import { Button } from 'components/Theme'

const Component = ({ language }) => (
  <Actions flex-phone={1} flex-pad={2}>
    <ActionsBox>
      <Button
        banner
        href={`${language}/catalog?category=gourmand`}
        width="18em"
        block
      >
        <Trans>
          CREATE A<br />WISHLIST
        </Trans>
        <Info
          onClick={event => event.preventDefault()}
          english={language === 'en'}
        />
        <Description>
          <Trans>
            A wishlist is a group order !<br />
            Create your own wishlist, invite people to join your order and
            benefit from an optimized final cost.<br />
            <br />In one word "Shopteamize" !
          </Trans>
        </Description>
      </Button>
    </ActionsBox>
    <ActionsBox>
      <Button
        banner
        href={`${language}/wishlists?category=gourmand`}
        width="18em"
        block
      >
        <Trans>
          JOIN A<br />WISHLIST
        </Trans>
        <Info
          onClick={event => event.preventDefault()}
          english={language === 'en'}
        />
        <Description>
          <Trans>
            A wishlist is a group order !<br />Participate at a whislist, add
            products to your shopping cart and get a very optimized cost.<br />
            <br />Buy "Shopteamize"
          </Trans>
        </Description>
      </Button>
    </ActionsBox>
  </Actions>
)

export default Component
