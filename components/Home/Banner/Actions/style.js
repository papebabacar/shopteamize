import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'

export const Description = styled.p``

export const Info = styled.span`
  width: 1.625em;
  height: 1.625em;
  background: url(/static/img/info.png);
  background-size: cover;
  display: inline-block;
  position: absolute;
  margin-top: ${props => (props.english ? '-0.203125em' : '-0.6125em')};
  right: 0.6125em;
`

export const Actions = styled(Flex)``

export const ActionsBox = styled(Box)`
  & > a {
    position: relative;
    text-align: center;
    line-height: 1.6125em;
  }
  ${Description} {
    display: none;
  }
  &:hover ${Description} {
    display: block;
    position: absolute;
    top: -11.75em;
    left: -5em;
    line-height: 1.625em;
    padding: 0.8125em;
    background: rgba(35, 46, 42, 0.9);
    color: white;
    box-shadow: 0px 3px 30px -5px rgba(0, 0, 0, 0.58);
    min-width: 26em;
  }
`
