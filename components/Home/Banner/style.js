import styled from 'styled-components'
import theme from 'components/Theme'

const {
  settings: { base, unit }
} = theme

export const Slide = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: ${props =>
    props.image
      ? `
    linear-gradient(
      rgba(10, 10, 10, 0.2625),
      rgba(10, 10, 10, 0.425),
      rgba(10, 10, 10, 0.6125)
    ),
    url(${props.image})`
      : '#f2f2f2'};
  background-size: cover;
  background-position: center;
  color: #fff;
  padding: 0.203125em 0.8125em;
  text-align: right;
  min-height: 82vh;
  ${theme.media.pad`
    min-height: auto;
    height: 84vh;
  `};
`

export const Content = styled.section`
  display: block;
  text-align: center;
  max-width: 53em;
  margin: 0 auto;
`
export const Title = styled.h1`
  font-family: 'Courier New';
  font-size: 200%;
  ${theme.media.pad`    
    font-size:233%;
    margin-bottom: 0.040625em;
  `};
  color: white;
`
export const Link = styled.a``

export const Description = styled.p`
  font-size: 110%;
  margin-bottom: 1.625em;
  display: none;
  ${theme.media.pad`
    display: block;
  `};
`

export const Dots = styled.div`
  width: 100vw;
  text-align: center;
`
export const Dot = styled.button`
  font-size: 250%;
  padding: ${base / 16 + unit};
  line-height: ${base / 4 + unit};
  border: 1px solid #ccc;
  border-radius: 50%;
  color: ${props => (props.selected ? '#ccc' : 'transparent')};
  margin: -${base + unit} ${base / 8 + unit} 0;
  vertical-align: middle;
`
export const Iframe = styled.iframe`
  height: 67vh;
  width: 100%;
  ${theme.media.pad`
    height: 50vh;
  `};
  margin: 0 auto;
`
