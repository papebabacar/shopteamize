import { Trans, withI18n } from '@lingui/react'
import Field from 'components/Theme/Form/Field'
import { Heading } from '../style'

const Component = ({ i18n }) => (
  <>
    <Heading>
      <Trans>Customer information</Trans>
    </Heading>
    <Field
      name="gender"
      value={genderOptions[0].value}
      label={<Trans>Gender</Trans>}
      type="select"
      required
      options={genderOptions}
    />
    <Field name="lastname" value="" label={<Trans>Name</Trans>} required />
    <Field
      name="firstname"
      value=""
      label={<Trans>Firstname</Trans>}
      required
    />
    <Field
      label="Email"
      name="email"
      value=""
      placeholder="Email"
      required
      validations="isEmail"
      validationError={i18n.t`Please enter a valid email address`}
    />
    <Field name="phone" value="" label={<Trans>Telephone</Trans>} required />
    <Field
      name="applicantStatus"
      value={statusOptions[0].value}
      label={<Trans>Status</Trans>}
      type="select"
      required
      options={statusOptions}
    />
  </>
)

export default withI18n()(Component)

const genderOptions = [
  { value: 'MALE', label: <Trans>MR</Trans> },
  { value: 'FEMALE', label: <Trans>MRS</Trans> }
]
const statusOptions = [
  { value: 'PROFESSIONAL', label: <Trans>Professional</Trans> },
  { value: 'PARTICULAR', label: <Trans>Particular</Trans> }
]
