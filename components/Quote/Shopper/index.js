import { Trans, withI18n } from '@lingui/react'
import Field from 'components/Theme/Form/Field'
import { Heading, Label } from '../style'

const Component = () => (
  <>
    <Heading>
      <Trans>Shopper's mission</Trans>
    </Heading>
    <Label>
      <Trans>Would you like to assign this mission to a shopper.</Trans>
    </Label>
    <Label>
      <Trans>This service will cost you</Trans> 25 euros
    </Label>
    <Field
      name="shopperOption"
      value={shopperOptions[0].value}
      label={<Trans>This misson to a shopper</Trans>}
      type="select"
      required
      options={shopperOptions}
    />
  </>
)

export default withI18n()(Component)

const shopperOptions = [
  { value: true, label: <Trans>YES</Trans> },
  { value: false, label: <Trans>NO</Trans> }
]
