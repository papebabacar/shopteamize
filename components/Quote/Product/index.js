import { Trans } from '@lingui/react'
import Field from 'components/Theme/Form/Field'
import { Tabs } from 'components/Theme'
import { Heading, TabTitle, Tab } from '../style'

const Component = ({ categories, natures, products, setProducts }) => (
  <>
    <Heading>
      <Trans>Product information</Trans>
    </Heading>
    <Tabs>
      {[
        ...products.map((_, index) => (
          <Tab key={index} title={<Trans>Product {index + 1}</Trans>}>
            <TabTitle>
              <Trans>Product {index + 1}</Trans>
            </TabTitle>
            <Field
              name={`category[${index}]`}
              value={categories.length && categories[0].code}
              label={<Trans>Category of the product</Trans>}
              type="select"
              required
              options={categories.map(({ code: value, name: label }) => ({
                value,
                label
              }))}
            />
            <Field
              name={`nature[${index}]`}
              value={natures.length && natures[0].id}
              label={<Trans>Nature of the product</Trans>}
              type="select"
              required
              options={natures.map(({ id: value, name: label }) => ({
                value,
                label
              }))}
            />
            <Field
              name={`productGender[${index}]`}
              value={genderOptions[0].value}
              label={<Trans>Gender of the product</Trans>}
              type="select"
              required
              options={genderOptions}
            />
            <Field
              name={`description[${index}]`}
              value=""
              label={<Trans>Product description</Trans>}
              type="textarea"
              required
            />
            <Field
              name={`imagesBytes[${index}]`}
              value=""
              type="upload"
              label={<Trans>Photo of the product</Trans>}
            />
            <Field
              name={`catalogOption[${index}]`}
              value={catalogOptions[0].value}
              label={<Trans>Product</Trans>}
              type="select"
              required
              options={catalogOptions}
            />
            <Field
              name={`quantity[${index}]`}
              value=""
              label={<Trans>Quantity</Trans>}
            />
          </Tab>
        )),
        <Tab
          key={products.length + 1}
          onClick={() => setProducts([...products, {}])}
          title={<Trans>Add product</Trans>}
        />
      ]}
    </Tabs>
    <Field name="budget" value="" label={<Trans>Budjet</Trans>} required />
    <Field
      name="newsLetterOption"
      value={newsOptions[0].value}
      label={
        <Trans>
          Would you like to get new informations about shopteamize's catalog?
        </Trans>
      }
      type="select"
      required
      options={newsOptions}
    />
  </>
)

export default Component

const catalogOptions = [
  { value: true, label: <Trans>In the catalog</Trans> },
  { value: false, label: <Trans>Not in the catalog</Trans> }
]
const newsOptions = [
  { value: true, label: <Trans>YES</Trans> },
  { value: false, label: <Trans>NO</Trans> }
]
const genderOptions = [
  { value: 0, label: <Trans>Men</Trans> },
  { value: 1, label: <Trans>Women</Trans> },
  { value: 2, label: <Trans>Children</Trans> }
]
