import styled from 'styled-components'

export const Quote = styled.section`
  background: #fff;
  display: block;
  margin: 0 auto;
  width: 96%;
  padding: 1.625em;
  border-top: 0.40625em solid #202c28;
  min-height: 75vh;
`

export const Title = styled.h1`
  text-align: center;
  color: #202c28;
  border-bottom: 1px solid #bcbcbc;
  font-size: 212%;
`
export const TabTitle = styled.h2`
  text-align: center;
  padding: 0.8125em;
  color: #202c28;
  border-bottom: 1px solid #bcbcbc;
`
export const Heading = styled.h2`
  margin: 0.8125em 0;
  padding: 0.40625em;
  background: #202c28;
  color: white;
`
export const Label = styled.p`
  text-align: ${props => (props.right ? 'right' : 'center')};
  display: block;
  color: #888;
  font-size: 88%;
`

export const Tab = styled.div`
  display: ${props => (props.selected ? 'block' : 'none')};
`
export const Actions = styled.div`
  text-align: center;
  max-width: 26em;
  margin: 1.625em auto;
`
