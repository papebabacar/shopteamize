import { Trans, withI18n } from '@lingui/react'
import { compose, withState, withHandlers } from 'recompose'
import { Form, withForm } from 'components/Theme/Form'
import { Quote, Title, Label, Actions } from './style'
import { Button, SubmitButton, Notification } from 'components/Theme'
import Customer from './Customer'
import Product from './Product'
import Shipping from './Shipping'
import Shopper from './Shopper'
import { createQuote } from 'api/muhajir/quote'
import { Router } from 'router'

const QuotePage = ({
  confirmation,
  language,
  post,
  submitting,
  submitError,
  categories,
  carriers,
  natures,
  products,
  setProducts
}) => (
  <Quote>
    {confirmation ? (
      <>
        <Title>
          <Trans>Quote request confirmation</Trans>
        </Title>
        <Label>
          <Trans>
            Your request has been send successfully. You can check your email
            for details.
          </Trans>
        </Label>
        <Actions>
          <Button
            secondary
            block
            onClick={() =>
              (document.location = `${
                Router.linkPage('index', { language }).as
              }`)
            }
          >
            ‹ <Trans>CONTINUE MY SHOPPING</Trans>
          </Button>
          <Button
            block
            onClick={() =>
              (document.location = `${
                Router.linkPage('quote', { language }).as
              }`)
            }
          >
            <Trans>SUBMIT ANOTHER QUOTE REQUEST</Trans> ›
          </Button>
        </Actions>
      </>
    ) : (
      <Form post={post} submitting={submitting}>
        <Title>
          <Trans>Quote request form</Trans>
        </Title>
        <Label>
          <Trans>
            Please fill in these fields below to complete your request.
          </Trans>
        </Label>
        <Customer />
        <Product
          categories={categories}
          natures={natures}
          products={products}
          setProducts={setProducts}
        />
        <Shipping carriers={carriers} language={language} />
        <Shopper />
        {submitError && <Notification>{submitError}</Notification>}
        <SubmitButton disabled={submitting} block>
          <Trans>Submit your quote request</Trans>
        </SubmitButton>
      </Form>
    )}
  </Quote>
)

const Component = compose(
  withI18n(),
  withState('products', 'setProducts', [{}]),
  withState('submitError', 'setSubmitError'),
  withHandlers({
    post: ({
      setFormState,
      setSubmitError,
      i18n,
      language: langKey,
      products
    }) => async ({
      gender,
      firstname,
      lastname,
      email,
      phone,
      applicantStatus,
      address1,
      address2,
      city,
      country = 'France',
      budget,
      countryCode,
      shopperOption,
      newsLetterOption,
      catalogOption,
      category,
      nature,
      description,
      productGender,
      quantity,
      deliveryMethod,
      imagesBytes
    }) => {
      setFormState(true)
      try {
        await createQuote({
          langKey,
          email,
          firstname,
          lastname,
          gender,
          applicantStatus,
          budget,
          phone,
          shopperOption,
          newsLetterOption,
          products: products.map((_, index) => ({
            catalogOption: catalogOption[index],
            category: category[index],
            description: description[index],
            gender: productGender[index],
            id: 0,
            imagesBytes: imagesBytes[index],
            nature: { id: nature[index] },
            quantity: quantity[index]
          })),
          addres: {
            address1,
            address2,
            city,
            country,
            countryCode
          },
          deliveryMethod: {
            id: deliveryMethod
          }
        })
        document.location = `${
          Router.linkPage('quote', { language: langKey }).as
        }?confirmation=true`
      } catch (error) {
        setSubmitError(
          i18n.t`A technical error has occurred. Please reload the page`
        )
        setTimeout(() => setSubmitError(), 5000)
        setFormState(false)
      }
    }
  })
)(QuotePage)

export default withForm(Component)
