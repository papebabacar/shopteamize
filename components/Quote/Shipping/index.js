import { Trans, withI18n } from '@lingui/react'
import Field from 'components/Theme/Form/Field'
import { Heading } from '../style'

const Component = ({ i18n, carriers, language }) => (
  <>
    <Heading>
      <Trans>Shipping information</Trans>
    </Heading>
    <Field
      name="address1"
      label={<Trans>Shipping Address</Trans>}
      required
      value=""
      validations="minLength:5"
      validationError={i18n.t`The address should be at least 5 characters`}
    />
    <Field name="city" label={<Trans>City</Trans>} required value="" />
    <Field
      name="countryCode"
      label={<Trans>Country</Trans>}
      required
      value={countryOptions[0].value}
      type="select"
      options={countryOptions(language)}
    />
    <Field
      name="deliveryMethod"
      value={carriers.length && carriers[0].id}
      label={<Trans>Shipping mode</Trans>}
      type="select"
      required
      options={carriers.map(({ id: value, nameFr, nameEn }) => ({
        value,
        label: language === 'en' ? nameEn : nameFr
      }))}
    />
  </>
)

export default withI18n()(Component)

const countryOptions = () => [{ value: 0, label: 'TODO' }]
