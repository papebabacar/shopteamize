import styled from 'styled-components'
import { Flex } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const ProductsBox = styled.div`
  flex: 1;
  ${theme.media.laptop`
    flex: ${props => (props.stretched ? '0.7' : '1')};
  `};
  ${theme.media.desktop`    
    flex: ${props => (props.stretched ? '0.75' : '1')};
  `};
`

export const DragAndDrop = styled(Flex)`
  min-height: 74vh;
  .gu-mirror {
    position: fixed !important;
    margin: 0 !important;
    z-index: 9999 !important;
    opacity: 0.8;
  }
  .gu-hide {
    display: none !important;
  }
  .gu-unselectable {
    -webkit-user-select: none !important;
    -moz-user-select: none !important;
    -ms-user-select: none !important;
    user-select: none !important;
  }

  .gu-transit {
    display: none !important;
  }
`
