import { compose, withState, withHandlers } from 'recompose'
import InfiniteScroll from 'components/Theme/Layout/InfiniteScroll'
import { DragAndDrop, ProductsBox } from './style'
import Products from 'components/Catalog/List/Product'
import Wishlists from 'components/Catalog/List/WishList'
import { getCatalogStore, getUserStore, getWishlistStore } from 'containers/App'
import Form from 'components/WishList/Form'
import { getProduct } from 'api/muhajir/catalog'

const Catalog = ({
  itemType,
  catalog: { hasMore },
  loadMore,
  fetchData,
  products,
  user: { pathname },
  wishlist: { visible }
}) => (
  <DragAndDrop padding={0} flex-phone={1} flex-laptop={2}>
    <ProductsBox stretched={visible && pathname === '/catalog'}>
      <InfiniteScroll pageStart={0} loadMore={loadMore} hasMore={hasMore}>
        {itemType === 'products' && <Products fetchData={fetchData} />}
        {itemType === 'wishlists' && <Wishlists />}
      </InfiniteScroll>
    </ProductsBox>
    {itemType === 'products' && (
      <Form fetchData={fetchData} products={products} />
    )}
  </DragAndDrop>
)

const Component = compose(
  getCatalogStore,
  getUserStore,
  getWishlistStore,
  withState('products', 'setProducts', ({ catalog: { data: products } }) =>
    products.filter(product => !product.size)
  ),
  withHandlers({
    loadMore: ({
      getItems,
      catalog: { params },
      addToCatalog,
      setCatalog
    }) => async page => {
      try {
        const nextParams = {
          ...params,
          page
        }
        setCatalog({ params: nextParams, hasMore: false, isLoading: true })
        const { data: newItems } = await getItems(nextParams)
        addToCatalog(newItems)
      } catch (error) {
        // do something
      }
    },
    fetchData: ({
      user: { language },
      wishlist: { items },
      products,
      setProducts
    }) => async () => {
      try {
        const newProducts = await Promise.all(
          Object.keys(items)
            .filter(id => !products.find(product => product.id === Number(id)))
            .map(id => getProduct(id, language))
            .map(promise =>
              promise.catch(() => ({
                data: undefined
              }))
            )
        )
        newProducts.length &&
          setProducts([
            ...products,
            ...newProducts.map(({ data: products }) => products)
          ])
      } catch (error) {
        // do something
      }
    }
  })
)(Catalog)

export default getCatalogStore(Component)
