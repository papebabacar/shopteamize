import styled from 'styled-components'

export const SearchBox = styled.section`
  padding: 15vh 0;
  max-width: 42em;
  margin: 0 auto;
  text-align: center;
  background: white;
`
export const Title = styled.h1`
  text-align: center;
  border-bottom: 1px solid #bcbcbc;
`
