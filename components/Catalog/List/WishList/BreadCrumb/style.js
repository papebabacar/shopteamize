import styled from 'styled-components'
import { Box } from 'components/Theme/Layout/FlexBox'

export const Breadcrumb = styled(Box)`
  > *:last-child {
    font-weight: 900;
    font-family: 'Arimo';
    &:after {
      display: none;
    }
  }
`

export const BreadcrumbItem = styled.p`
  padding: 0;
  color: #888;
  display: inline-block;
  &:after {
    content: '›';
    display: inline-block;
    padding: 0 0.40625em;
  }
`
export const BreadcrumbLink = styled(BreadcrumbItem.withComponent('a'))``

export const Span = styled.span``

export const BackButton = styled.a`
  border: 1px solid #aaa;
  padding: 0.203125em 0.40625em;
  font-size: 133%;
`
