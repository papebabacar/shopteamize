import { Trans, Plural } from '@lingui/react'
import {
  Breadcrumb,
  BreadcrumbLink,
  BreadcrumbItem,
  Span,
  BackButton
} from './style'
import Responsive from 'react-responsive'
import theme from 'components/Theme'
import { getUserStore } from 'containers/App'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'

const {
  devices: { pad }
} = theme

const Component = ({
  product,
  category,
  totalCount,
  query,
  user: { language }
}) => (
  <Breadcrumb>
    <Responsive maxWidth={pad}>
      {onMobile =>
        onMobile ? (
          <BackButton onClick={() => history.go(-1)}>
            ‹ <Trans>Back</Trans>
          </BackButton>
        ) : (
          <>
            <BreadcrumbLink
              href={`${Router.linkPage('index', { language }).as}`}
            >
              <Trans>Home</Trans>
            </BreadcrumbLink>
            {product ? (
              <>
                <BreadcrumbLink
                  href={`${
                    Router.linkPage('catalog', { language }).as
                  }?category=${category.code}`}
                >
                  {category.name}
                </BreadcrumbLink>
                <BreadcrumbLink
                  href={`${
                    Router.linkPage('product', {
                      language,
                      id: product.id,
                      name: slugify(product.name)
                    }).as
                  }`}
                >
                  {product.name}
                </BreadcrumbLink>
                <BreadcrumbItem>
                  <Trans>Wishlists associated</Trans> (
                  <Plural
                    value={totalCount}
                    _0={<Trans>No results found</Trans>}
                    _1={<Trans># wishlist</Trans>}
                    other={<Trans># wislists</Trans>}
                  />)
                </BreadcrumbItem>
              </>
            ) : (
              <BreadcrumbItem>
                {query ? (
                  <Trans>Your search : {query}</Trans>
                ) : (
                  <Span>
                    <Trans>Wishlists</Trans> {category.name}
                  </Span>
                )}{' '}
                (
                <Plural
                  value={totalCount}
                  _0={<Trans>No results found</Trans>}
                  _1={<Trans># wishlist</Trans>}
                  other={<Trans># wislists</Trans>}
                />)
              </BreadcrumbItem>
            )}
          </>
        )
      }
    </Responsive>
  </Breadcrumb>
)

export default getUserStore(Component)
