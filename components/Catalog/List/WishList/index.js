import { Trans } from '@lingui/react'
import QuickView from 'components/QuickView/WishList'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import { SearchBox, Title } from './style'
import Card from 'components/WishList/Card'
import BreadCrumb from './BreadCrumb'
import SortInput from './SortInput'
import Search from 'components/Search'
import { getCatalogStore, getModalStore } from 'containers/App'
import Loader from 'components/Theme/Layout/Loader'

const Component = ({
  catalog: { totalCount, params, data, isLoading, product, category },
  modal,
  showModal
}) => (
  <Box box-flex={1}>
    <Flex flex-phone={1} flex-tablet={2}>
      <BreadCrumb
        product={product}
        category={category}
        query={params.query}
        totalCount={totalCount}
      />
      <SortInput params={params} />
    </Flex>
    {data.length ? (
      <Flex flex-phone={2} flex-tablet={2} flex-pad={4}>
        {data.map(wishlist => (
          <Card wishlist={wishlist} key={wishlist.id} showModal={showModal} />
        ))}
      </Flex>
    ) : (
      <SearchBox>
        <Title>
          <Trans>
            We are sorry but no results were found for your research.
          </Trans>
        </Title>
        <Search noResult={true} />
      </SearchBox>
    )}

    {isLoading && <Loader />}
    {modal.visible && modal.view === 'wishlist' && <QuickView />}
  </Box>
)

export default getModalStore(getCatalogStore(Component))
