import { Trans } from '@lingui/react'
import { SortInput, Select, Option } from './style'
import encode from 'query-string-encode'

const Component = ({ params }) => (
  <SortInput>
    <Select
      defaultValue={params.sort}
      onChange={event => {
        if (event.target.value === 'false') {
          //eslint-disable-next-line no-unused-vars
          const { sort: _, ...newParams } = params
          document.location = '?' + encode({ ...newParams, page: 0 })
        } else {
          document.location =
            '?' + encode({ ...params, sort: event.target.value, page: 0 })
        }
      }}
    >
      <Option value={false}>
        <Trans>Sort by</Trans>...
      </Option>
      <Option value="price,asc">
        <Trans>Ascending prices</Trans>
      </Option>
      <Option value="price,desc">
        <Trans>Descending prices</Trans>
      </Option>
    </Select>
  </SortInput>
)

export default Component
