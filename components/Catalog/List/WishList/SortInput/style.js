import styled from 'styled-components'
import { Box } from 'components/Theme/Layout/FlexBox'

export const SortInput = styled(Box)`
  text-align: right;
`
export const Select = styled.select`
  display: inline-block;
  padding: 0 0.8125em;
  margin-right: 0.8125em;
  border-radius: 0.8125em;
  background: #fff;
`
export const Option = styled.option``
