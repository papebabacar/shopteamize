import { Trans } from '@lingui/react'
import { SortInput, Select, Option } from './style'
import encode from 'query-string-encode'

const Component = ({ params, attributes: { colors = [], sizes = [] } }) => (
  <SortInput>

    {params.category === 'clothing' && (
      <Select
        defaultValue={params.gender}
        onChange={event => {
          if (event.target.value === 'false') {
            //eslint-disable-next-line no-unused-vars
            const { gender: _, ...newParams } = params
            document.location = '?' + encode({ ...newParams, page: 0 })
          } else {
            document.location =
              '?' + encode({ ...params, gender: event.target.value, page: 0 })
          }
        }}
      >
        <Option value={false}>
          <Trans>Gender...</Trans>
        </Option>
        <Option value='woman'>
          <Trans>Women</Trans>
        </Option>
        <Option value='men'>
          <Trans>Men</Trans>
        </Option>
        <Option value='child'>
          <Trans>Children</Trans>
        </Option>
        <Option value='mixed'>
          <Trans>Mixed</Trans>
        </Option>
      </Select>
    )}

    {colors.length > 0 && (
      <Select
        defaultValue={params.color}
        onChange={event => {
          if (event.target.value === 'false') {
            //eslint-disable-next-line no-unused-vars
            const { color: _, ...newParams } = params
            document.location = '?' + encode({ ...newParams, page: 0 })
          } else {
            document.location =
              '?' + encode({ ...params, color: event.target.value, page: 0 })
          }
        }}
      >
        <Option value={false}>
          <Trans>Color</Trans>...
        </Option>
        {colors.map(code => (
          <Option key={code} value={code}>
            {code}
          </Option>
        ))}
      </Select>
    )}
    {sizes.length > 0 && (
      <Select
        defaultValue={params.productSize}
        onChange={event => {
          if (event.target.value === 'false') {
            //eslint-disable-next-line no-unused-vars
            const { productSize: _, ...newParams } = params
            document.location = '?' + encode({ ...newParams, page: 0 })
          } else {
            document.location =
              '?' +
              encode({ ...params, productSize: event.target.value, page: 0 })
          }
        }}
      >
        <Option value={false}>
          <Trans>Size</Trans>...
        </Option>
        {sizes.map(code => (
          <Option key={code} value={code}>
            {code}
          </Option>
        ))}
      </Select>
    )}
    {/* <Select
      defaultValue={getDefaultValue(params)}
      onChange={event => {
        let minPrice
        let maxPrice
        JSON.parse(event.target.value).map((value, index) => {
          index === 0 && (minPrice = value)
          index === 1 && (maxPrice = value)
        })
        document.location =
          '?' + encode({ ...params, minPrice, maxPrice, page: 0 })
      }}
    >
      <Option value="[0]">
        <Trans>Price</Trans>...
      </Option>
      <Option value="[0,50]">
        <Trans>From</Trans> 0 <Trans>to</Trans> 50 euros
      </Option>
      <Option value="[50,100]">
        <Trans>From</Trans> 50 <Trans>to</Trans> 100 euros
      </Option>
      <Option value="[100,150]">
        <Trans>From</Trans> 100 <Trans>to</Trans> 150 euros
      </Option>
      <Option value="[150]">
        <Trans>From</Trans> 150 euros <Trans>and more</Trans>
      </Option>
    </Select> */}
    <Select
      defaultValue={params.sort}
      onChange={event => {
        if (event.target.value === 'false') {
          //eslint-disable-next-line no-unused-vars
          const { sort: _, ...newParams } = params
          document.location = '?' + encode({ ...newParams, page: 0 })
        } else {
          document.location =
            '?' + encode({ ...params, sort: event.target.value, page: 0 })
        }
      }}
    >
      <Option value={false}>
        <Trans>Sort by</Trans>...
      </Option>
      <Option value="price,asc">
        <Trans>Ascending prices</Trans>
      </Option>
      <Option value="price,desc">
        <Trans>Descending prices</Trans>
      </Option>
    </Select>
  </SortInput>
)

export default Component

// const getDefaultValue = params => {
//   let defaultValue = '[0'
//   Object.keys(params).map(param => {
//     param === 'minPrice' && (defaultValue = `[${params[param]}`)
//     param === 'maxPrice' && (defaultValue = `${defaultValue},${params[param]}`)
//   })
//   return defaultValue
// }
