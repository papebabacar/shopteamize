import { Trans, Plural } from '@lingui/react'
import { Breadcrumb, BreadcrumbLink, BreadcrumbItem, BackButton } from './style'
import Responsive from 'react-responsive'
import theme from 'components/Theme'
import { Router } from 'router'
import { getUserStore } from 'containers/App'
const {
  devices: { pad }
} = theme

const Component = ({
  category: { name },
  totalCount,
  query,
  user: { language }
}) => (
  <Breadcrumb>
    <Responsive maxWidth={pad}>
      {onMobile =>
        onMobile ? (
          <BackButton onClick={() => history.go(-1)}>
            ‹ <Trans>Back</Trans>
          </BackButton>
        ) : (
          <>
            <BreadcrumbLink
              href={`${Router.linkPage('index', { language }).as}`}
            >
              <Trans>Home</Trans>
            </BreadcrumbLink>
            <BreadcrumbItem>
              {query ? <Trans>Your search : {query}</Trans> : name} (<Plural
                value={totalCount}
                _0={<Trans>No results found</Trans>}
                _1={<Trans># product</Trans>}
                other={<Trans># products</Trans>}
              />)
            </BreadcrumbItem>
          </>
        )
      }
    </Responsive>
  </Breadcrumb>
)

export default getUserStore(Component)
