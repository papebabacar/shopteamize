import { Trans } from '@lingui/react'
import {
  getWishlistStore,
  getCatalogStore,
  getModalStore
} from 'containers/App'
import QuickView from 'components/QuickView/Product'
import Card from 'components/Product/Card'
import BreadCrumb from './BreadCrumb'
import SortInput from './SortInput'
import { SearchBox, Title } from './style'
import Search from 'components/Search'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import Loader from 'components/Theme/Layout/Loader'
import { compose, lifecycle } from 'recompose'
import { initializeCatalog } from 'components/Theme/GTM'

const CategoryProductPage = ({
  catalog: { totalCount, params, attributes, data, isLoading, category },
  modal,
  showModal,
  wishlist: { visible },
  fetchData
}) => (
  <Box box-flex={1}>
    <Flex flex-phone={1} flex-tablet={2}>
      <BreadCrumb
        category={category}
        query={params.query}
        totalCount={totalCount}
      />
      <SortInput params={params} attributes={attributes} />
    </Flex>
    {data.length ? (
      <Flex flex-phone={2} flex-tablet={2} flex-pad={4} className="dragBox">
        {data.map(product => (
          <Card
            product={product}
            key={product.id}
            showModal={showModal}
            adapt={visible}
          />
        ))}
      </Flex>
    ) : (
      <SearchBox>
        <Title>
          <Trans>
            We are sorry but no results were found for your research.
          </Trans>
        </Title>
        <Search noResult={true} expanded={true} />
      </SearchBox>
    )}
    {isLoading && <Loader />}
    {modal.visible &&
      modal.view === 'product' && <QuickView fetchData={fetchData} />}
  </Box>
)

const Component = compose(
  getWishlistStore,
  getModalStore,
  getCatalogStore,
  lifecycle({
    async componentDidMount() {
      const { catalog } = this.props
      initializeCatalog(catalog)
    }
  })
)(CategoryProductPage)

export default Component
