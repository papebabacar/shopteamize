import styled from 'styled-components'
import theme from 'components/Theme'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'

const { dark, light } = theme

export const Section = styled(Flex).attrs({ className: 'header' })`
  background: ${dark};
  color: ${light};
  text-align: center;
  font-size: 80%;
  padding: 0.8125em;
`
export const Widget = styled(Box)``

export const Label = styled.p`
  padding: 1.625em 0;
`

export const Icon = styled.img`
  height: 1.3125rem;
  vertical-align: top;
  margin: -0.3125rem 0.40625rem 0 0;
  ${theme.media.laptop`margin: -0.125rem 0.40625rem 0 2.25em;`};
`
export const Logo = styled.img`
  height: 4.375em;
  margin: 0.203125em auto;
  display: block;
`
