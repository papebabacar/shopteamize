import { Trans } from '@lingui/react'
import { Section, Widget, Label, Logo, Icon } from './style'

const Component = () => (
  <Section flex-phone={3} padding={0}>
    <Widget>
      <Label>
        <Icon src="/static/img/phone.png" />
        <Trans>Need help? +971 55 56 03 076</Trans>
      </Label>
    </Widget>
    <Widget>
      <Logo src="/static/img/logo.png" alt="shopteamize" />
    </Widget>
    <Widget>
      <Label>
        <Icon src="/static/img/lock.png" />
        <Trans>Garanteed Safe checkout</Trans>
      </Label>
    </Widget>
  </Section>
)

export default Component
