import { Trans } from '@lingui/react'
import { compose } from 'recompose'
import {
  UserLinks,
  UserLink,
  UserInfo,
  Badge,
  Layer,
  Link,
  Icon,
  Language,
  Switch,
  OptionItem,
  OptionLink,
  Selected,
  Span
} from './style'
import { getUserStore, getCartStore, getWishlistStore } from 'containers/App'
import { Router } from 'router'
import { synchronizeCarts } from 'api/muhajir/cart'
// import Search from 'components/Search'

const Component = ({
  user: { isLogged, pathname, pathParams, language },
  cart: { idSession, totalQuantity: cartQuantity },
  wishlist: { category, visible, totalQuantity: wishlistQuantity },
  setWishlist,
  logoutUser
}) => (
  <UserLinks>
    {/* <UserInfo>
      <Search />
    </UserInfo> */}
    {cartQuantity > 0 ? (
      <UserLink
        href={`${Router.linkPage('checkout', { language }).as}?step=basket`}
        highlighted={pathname === '/checkout'}
      >
        <Icon src="/static/img/basket.png" />
        <Badge>{cartQuantity}</Badge>
      </UserLink>
    ) : (
      <UserInfo>
        <Icon src="/static/img/basket.png" />
      </UserInfo>
    )}
    <UserLink
      highlighted={visible && pathname === '/catalog'}
      onClick={() => {
        pathname === '/catalog'
          ? setWishlist({ visible: !visible })
          : setWishlist({ visible: true }) &&
            (document.location = `${
              Router.linkPage('catalog', { language }).as
            }?category=${category ? category : 'gourmand'}`)
      }}
    >
      <Icon src="/static/img/wishlist.png" />
      {wishlistQuantity > 0 && <Badge>{wishlistQuantity}</Badge>}
    </UserLink>
    <UserInfo highlighted={pathname === '/account'} connected={isLogged}>
      <Icon src="/static/img/user.png" />
      <Layer>
        <Link
          onClick={() => {
            document.location = isLogged
              ? Router.linkPage('account', { language }).as
              : Router.linkPage('login', { language }).as
          }}
        >
          {isLogged ? <Trans>My account</Trans> : <Trans>Log in</Trans>}
        </Link>
        <Link
          onClick={async () => {
            if (isLogged) {
              try {
                await synchronizeCarts(idSession)
              } catch (error) {
                // do something
              }
              logoutUser()
              document.location.reload()
            } else {
              document.location = Router.linkPage('register', { language }).as
            }
          }}
        >
          {isLogged ? <Trans>Log out</Trans> : <Trans>Sign up</Trans>}
        </Link>
      </Layer>
    </UserInfo>
    <UserInfo>
      <Language>
        <Selected>
          {language}
          <Span>ˇ</Span>
        </Selected>
        <Switch>
          <OptionItem>{language === 'en' ? 'En' : 'Fr'}</OptionItem>
          <OptionLink
            href={
              Router.linkPage(pathname.replace('/', ''), {
                language: language === 'en' ? 'fr' : 'en',
                ...pathParams
              }).as
            }
          >
            {language === 'en' ? 'Fr' : 'En'}
          </OptionLink>
        </Switch>
      </Language>
    </UserInfo>
  </UserLinks>
)
export default compose(
  getUserStore,
  getCartStore,
  getWishlistStore
)(Component)
