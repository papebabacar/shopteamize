import styled, { css } from 'styled-components'
import theme from 'components/Theme'
import { Box } from 'components/Theme/Layout/FlexBox'

const {
  settings: { base, unit }
} = theme

export const UserLinks = styled(Box)`
  ${theme.media.pad`margin-left: auto; margin-right: 0.40625em`};
`

export const Badge = styled.span`
  position: absolute;
  right: ${(base * 2) / 3 + unit};
  top: ${base / 2 + unit};
  padding: 0;
  font-weight: 900;
  padding: 0 0.203125em;
`
export const Layer = styled.div`
  position: absolute;
  display: none;
  background: white;
  color: #222;
  padding: 0.40625em;
  top: 4.375em;
  left: -2.8125em;
  min-width: 10em;
  text-align: center;
  box-shadow: 0px 3px 30px -5px rgba(0, 0, 0, 0.58);
  z-index: 99999999999;
`
export const Link = styled.a`
  margin: 0.8125em 0;
  & + & {
    border-to: 1px solid #eee;
  }
  display: block;
`

export const UserLink = styled.a`
  min-width: 3em;
  vertical-align: top;
  position: relative;
  border-left: 1px solid #777;
  display: inline-block;
  height: 100%;
  > ${Badge} {
    margin-top: 0.8125rem;
    line-height: 1em;
    background: rgba(10, 10, 10, 0.75);
    color: #ff12f4;
    right: 0.8125em;
  }
  ${props =>
    props.highlighted &&
    css`
      background: #fff;
      @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
        /* IE10+ CSS styles go here */
        background: transparent;
      }
      ${Icon} {
        filter: brightness(0);
      }
    `};
  ${props =>
    props.connected &&
    css`
      ${Icon} {
        filter: contrast(0) sepia(100%) hue-rotate(115deg);
      }
    `};
  padding: 0.203125em 0.6125em;
  ${theme.media.tablet`padding: 0.40625em 0.8125em;`};
  ${theme.media.pad`padding: 0.40625em;`};
  ${theme.media.laptop`padding: 0.525em 0.8125em;`};
`
export const UserInfo = UserLink.withComponent('div').extend`
  > ${Badge} {
	 	color: white;
	}
  :hover ${Layer} {
	 	display: block;
	}
`

export const Icon = styled.img`
  height: 1.625rem;
  vertical-align: top;
  margin-top: 0.6125rem;
`

export const Switch = styled.div`
  position: absolute;
  width: 100%;
  top: 1.40625em;
  visibility: hidden;
  border: 1px solid #888;
`
export const Language = styled.div`
  position: relative;
  font-family: 'Arimo';
  &:hover {
    ${Switch} {
      visibility: visible;
    }
  }
`

export const Selected = styled.p`
  margin-top: 0.9rem;
  padding: 0 0.203125em;
  text-transform: capitalize;
  cursor: pointer;
`
export const Span = styled.span`
  font-size: 250%;
  position: relative;
  top: 0.45em;
  line-height: 0;
  font-family: 'Courier New';
`

export const OptionItem = styled.p`
  background: #202c28;
  display: block;
  height: 100%;
  width: 100%;
  padding: 0 0.203125em;
  margin: 0;
  box-shadow: 0 2px 0 rgba(0, 0, 0, 0.06);
`

export const OptionLink = styled(OptionItem.withComponent('a'))``
