import { stack as Menu } from 'react-burger-menu'
import { Header, Logo, Link, Nav, Hamburger } from './style'
import MenuLinks from 'components/Header/MenuLinks'
import UserLinks from 'components/Header/UserLinks'

const Component = ({ categories, wishlistCategories }) => (
  <Header padding={0.8125}>
    <Hamburger>
      <Menu>
        <MenuLinks
          categories={categories}
          wishlistCategories={wishlistCategories}
        />
      </Menu>
    </Hamburger>
    <Link href="/">
      {' '}
      <Logo src="/static/img/logo.png" alt="shopteamize" />
    </Link>
    <Nav box-right>
      <MenuLinks
        categories={categories}
        wishlistCategories={wishlistCategories}
      />
    </Nav>
    <UserLinks />
  </Header>
)

export default Component
