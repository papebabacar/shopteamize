import { Trans } from '@lingui/react'
import {
  Menu,
  SubMenu,
  MenuItem,
  MenuLink,
  ArrowDown,
  Content,
  ContentItem,
  Link
} from './style'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const Component = ({ categories, user: { language } }) => (
  <Menu class="menu">
    <MenuItem>
      <MenuLink href={`${Router.linkPage('index', { language }).as}`}>
        <Trans>Home</Trans>
      </MenuLink>
    </MenuItem>
    <MenuItem>
      <MenuLink>
        <Trans>Products</Trans>
        <ArrowDown>ˇ</ArrowDown>
      </MenuLink>
      <SubMenu>
        <Content padding={0.40625} flex-phone={1} flex-pad={5}>
          {categories.map(({ code, name }) => (
            <ContentItem key={code}>
              <Link
                href={`${
                  Router.linkPage('catalog', { language }).as
                }?category=${code}`}
              >
                {name}
              </Link>
            </ContentItem>
          ))}
        </Content>
      </SubMenu>
    </MenuItem>
    <MenuItem>
      <MenuLink href={`${Router.linkPage('wishlists', { language }).as}`}>
        <Trans>Packs Ramadan</Trans>
        {/* <ArrowDown>ˇ</ArrowDown> */}
      </MenuLink>
      {/* <SubMenu>
        <Content padding={0.8125} flex-phone={2} flex-pad={5} flex-desktop={7}>
          {categories
            .filter(({ code }) => wishlistCategories.includes(code))
            .map(({ code, name }) => (
              <ContentItem key={code}>
                <Link
                  href={`${
                    Router.linkPage('wishlists', { language }).as
                  }?category=${code}`}
                >
                  {name}
                </Link>
              </ContentItem>
            ))}
        </Content>
      </SubMenu> */}
    </MenuItem>
    <MenuItem>
      <MenuLink
        href={`${
          Router.linkPage('catalog', { language }).as
        }?category=gourmand`}
      >
        <Trans>Create my wishlist</Trans>
      </MenuLink>
    </MenuItem>
    {/* <MenuItem>
      <MenuLink href={`${Router.linkPage('quote', {language}).as}`}>
        <Trans>Quote</Trans>
      </MenuLink>
    </MenuItem> */}
  </Menu>
)

export default getUserStore(Component)
