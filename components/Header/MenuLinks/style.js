import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Menu = styled.ul`
  display: block;
  ${theme.media.pad`display: flex;`};
`

export const MenuLink = styled.a`
  display: block;
  color: #000;
  position: relative;
  padding: 1.525em 0.40625em;
  white-space: nowrap;
  font-family: 'Helvetica Light';
  letter-spacing: -0.03125em;
  border-left: 1px solid #777;
  &:last-of-type {
    border-right: 1px solid #777;
  }
  ${theme.media.pad`color: #fff;`};
  ${theme.media.laptop`padding:1.4375em 0.6125em;`};
`

export const MenuItem = styled.li`
  display: block;
  text-align: center;
  touch-action: none;
  &:hover {
    background-color: white;
    ${MenuLink} {
      color: rgb(26, 120, 34);
    }
  }
`

export const SubMenu = styled.ul`
  z-index: 99999999999;
  display: none;
  position: relative;
  width: 100%;
  background-color: #fff;
  color: #222;
  box-shadow: 0 2px 0 rgba(0, 0, 0, 0.06);
  text-align: center;
  padding: 0.8125em 0.40625em;
  ${MenuItem}:hover & {
    display: block;
    ${theme.media.pad`
        display: flex;
				position: absolute;
        left: 0;
        top: 4.8125em;
    `};
  }
`

export const ArrowDown = styled.span`
  font-size: 250%;
  line-height: 0;
  position: relative;
  top: 0.5125em;
  font-family: 'Courier New';
`
export const Content = styled(Flex)`
  text-align: center;
  justify-content: flex-start;
  padding: 0.8125em 0 0 1.625em !important;
`
export const ContentItem = styled(Box)``

export const Link = styled.a`
  color: rgb(26, 120, 34);
  font-weight: 900;
  text-transform: capitalize;
`
