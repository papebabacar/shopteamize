import styled from 'styled-components'
import theme from 'components/Theme'

const { dark, light } = theme

export const Header = styled.header.attrs({ className: 'header' })`
  display: flex;
  background: ${dark};
  color: ${light};
  position: sticky;
  top: 0;
  z-index: 99999;
  width: 100%;
  padding: 0.40625em 0;
`

export const Link = styled.a`
  margin: 0 0.8125em 0 2.4375em;
  padding-left: 1.40625em;
  margin: 0 auto;
`

export const Logo = styled.img`
  height: 4.25em;
`

export const Nav = styled.nav`
  ${theme.media.phone`display:none;`};
  ${theme.media.pad`display:inline-flex;`};
`

export const Hamburger = styled.div`
  position: relative;
  top: -1em;
  left: -0.8125em;
  ${theme.media.pad`display:none;`};
`
