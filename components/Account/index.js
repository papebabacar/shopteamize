import { Trans } from '@lingui/react'
import { SelfCare, Menu, Main, Link, Icon, Span } from './style'
import { getUserStore, getCartStore } from 'containers/App'
import Address from './Address'
import Wishlist from './WishList'
import Order from './Order'
import Info from './Info'
import { Router } from 'router'
import { synchronizeCarts } from 'api/muhajir/cart'

const Component = ({
  params,
  params: { view = 'info' },
  user,
  logoutUser,
  cart: { idSession }
}) => (
  <>
    <SelfCare padding={0.40625} flex-phone={2}>
      <Menu>
        <Link
          href={`${
            Router.linkPage('account', { language: user.language }).as
          }?view=info`}
          selected={view === 'info'}
        >
          <Icon src="/static/img/account.png" alt="account" />
          <Span>
            <Trans>My Account</Trans>
          </Span>
        </Link>
        <Link
          href={`${
            Router.linkPage('account', { language: user.language }).as
          }?view=address`}
          selected={view === 'address'}
        >
          <Icon src="/static/img/adresses.png" alt="addresses" />
          <Span>
            <Trans>My addresses</Trans>
          </Span>
        </Link>
        <Link
          href={`${
            Router.linkPage('account', { language: user.language }).as
          }?view=wishlist`}
          selected={view === 'wishlist'}
        >
          <Icon src="/static/img/wishlists.png" alt="wishlists" />
          <Span>
            <Trans>My wishlists</Trans>
          </Span>
        </Link>
        <Link
          href={`${
            Router.linkPage('account', { language: user.language }).as
          }?view=order`}
          selected={view === 'order'}
        >
          <Icon src="/static/img/orders.png" alt="orders" />
          <Span>
            <Trans>My orders</Trans>
          </Span>
        </Link>
        <Link
          onClick={async () => {
            try {
              await synchronizeCarts(idSession)
            } catch (error) {
              // do something
            }
            logoutUser()
            document.location.reload()
          }}
        >
          <Icon src="/static/img/logout.jpg" alt="logout" />
          <Span>
            <Trans>Log out</Trans>
          </Span>
        </Link>
      </Menu>
      <Main>
        {view === 'info' && <Info user={user} params={params} />}
        {view === 'address' && <Address params={params} />}
        {view === 'wishlist' && <Wishlist user={user} params={params} />}
        {view === 'order' && <Order user={user} params={params} />}
      </Main>
    </SelfCare>
  </>
)

export default getUserStore(getCartStore(Component))
