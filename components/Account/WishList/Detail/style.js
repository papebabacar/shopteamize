import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import { Picture } from 'components/Theme'
import { Trans } from '@lingui/react'

export const Detail = styled.section`
  margin-bottom: 0.8125em;
  .currency {
    p:after {
       content: ' (${({ currencyCode }) => currencyCode})';
    }
  }
`

export const Info = styled(Flex)`
  margin-top: 1.625em;
  border-top: 1px solid #eee;
  text-align: right;
`

export const Label = styled(Box)`
  padding: 0.40625em 0;
`

export const Description = styled(Box)`
  font-weight: 900;
  padding: 0.40625em 0;
`

export const Actions = styled.div`
  text-align: center;
  margin-top: 3.25em;
`

const Right = styled.p`
  text-align: right;
`

export const config = {
  getProps: () => ({ style: { border: 'none' } }),
  getTrGroupProps: () => ({ style: { border: 'none' } }),
  getTheadTrProps: () => ({ style: { textAlign: 'left' } }),
  getPaginationProps: () => ({
    style: {
      boxShadow: 'none',
      border: '1px solid #eee',
      padding: '0.8125em'
    }
  }),
  showPagination: false,
  className: '-highlight',
  pageSizeOptions: [5, 10, 15, 20],
  defaultSorted: [
    {
      id: 'name'
    }
  ],
  previousText: '‹‹',
  nextText: '››',
  loadingText: <Trans>Loading your products</Trans>,
  noDataText: <Trans>You have no products</Trans>,
  pageText: 'Page',
  ofText: 'sur',
  rowsText: 'lignes'
}

export const columns = [
  {
    id: 'mainImageUrl',
    Header: '',
    accessor: ({ mainImageUrl, categories }) => ({
      mainImageUrl,
      category: categories.find(() => true)
    }),
    sortMethod: (a, b) => a.category.localeCompare(b.category),
    Cell: props => (
      <Picture src={props.value.mainImageUrl} size="2.4375em" margin="0" />
    ),
    minWidth: 32,
    maxWidth: 40
  },
  {
    Header: <Trans>Product</Trans>,
    accessor: 'name',
    minWidth: 100
  },
  {
    Header: () => (
      <Right>
        <Trans>Quantity</Trans>
      </Right>
    ),
    Cell: ({ value }) => <Right>{value}</Right>,
    accessor: 'quantity',
    minWidth: 20
  },
  {
    headerClassName: 'currency',
    Header: () => (
      <Right>
        <Trans>Unit price</Trans>
      </Right>
    ),
    Cell: ({ value }) => <Right>{Number(value).toFixed(2)}</Right>,
    accessor: 'price',
    minWidth: 160,
    maxWidth: 170
  },
  {
    id: 'totalPrice',
    headerClassName: 'currency',
    Header: () => (
      <Right>
        <Trans>Total price</Trans>
      </Right>
    ),
    Cell: ({ value }) => <Right>{Number(value).toFixed(2)}</Right>,
    accessor: ({ price, quantity }) => quantity * price,
    minWidth: 160,
    maxWidth: 170
  }
]
