import { Trans } from '@lingui/react'
import { compose, withState, lifecycle } from 'recompose'
import {
  Detail,
  Info,
  Label,
  Description,
  Actions,
  config,
  columns
} from './style'
import { Button } from 'components/Theme'
import ReactTable from 'react-table'
import { deleteUserWishlist } from 'api/muhajir/wishlist'
import { getUserStore, getCartStore, getWishlistStore } from 'containers/App'
import { Router } from 'router'
import Loader from 'components/Theme/Layout/Loader'
import { getUserWishlist } from 'api/muhajir/wishlist'
import slugify from '@sindresorhus/slugify'

const DetailPage = ({
  wishlistId,
  wishlistData,
  setWishlist,
  user: { id: userId, language },
  cart: { currencyCode, pendingWishlist },
  addCartItem,
  removeCartItem,
  emptyWishlist
}) => (
  <Detail currencyCode={currencyCode}>
    {wishlistData.items ? (
      <>
        <ReactTable
          data={wishlistData.items}
          columns={columns}
          {...config}
          pageSize={wishlistData.items.length || 5}
          sortable={false}
        />
        <Info padding={0.8125} flex-phone={2} flex-tablet={4}>
          <Label>
            <Trans>Start date :</Trans>
          </Label>
          <Description>
            {new Date(wishlistData.startDate).toLocaleString(language, {
              weekday: 'long',
              year: 'numeric',
              month: 'long',
              day: 'numeric'
            })}
          </Description>
          <Label>
            <Trans>End date :</Trans>
          </Label>
          <Description>
            {new Date(wishlistData.endDate).toLocaleString(language, {
              weekday: 'long',
              year: 'numeric',
              month: 'long',
              day: 'numeric'
            })}
          </Description>
          <Label>
            <Trans>Status :</Trans>
          </Label>
          <Description>
            {wishlistData.currentStatus === 'PENDING' ? (
              <Trans>Pending</Trans>
            ) : wishlistData.currentStatus === 'PUBLISHED' ? (
              <Trans>Published</Trans>
            ) : wishlistData.currentStatus === 'CLOSED' ? (
              <Trans>Closed</Trans>
            ) : (
              wishlistData.currentStatus
            )}
          </Description>
          <Label>
            <Trans>Total price :</Trans>
          </Label>
          <Description>
            {wishlistData.price} {currencyCode}
          </Description>
        </Info>
        {wishlistData.currentStatus === 'PENDING' && (
          <Actions>
            <Button
              secondary
              onClick={async () => {
                try {
                  await deleteUserWishlist(userId, wishlistId)
                  Object.keys(pendingWishlist).length &&
                    removeCartItem(wishlistId, 'pendingWishlist')
                  emptyWishlist()
                  document.location = `${
                    Router.linkPage('account', { language }).as
                  }?view=wishlist`
                } catch (error) {
                  // do something
                }
              }}
            >
              <Trans>Delete</Trans>
            </Button>
            <Button
              intermediary
              onClick={() =>
                setWishlist({
                  id: wishlistData.id,
                  category: wishlistData.categories.find(() => true),
                  visible: true,
                  title: wishlistData.labels[0].title,
                  endDate: wishlistData.endDate,
                  mainItemId: wishlistData.mainImageSku,
                  items: wishlistData.items.reduce(
                    (products, { id, quantity }) => ({
                      ...products,
                      [id]: { quantity }
                    }),
                    {}
                  )
                }) &&
                (document.location = `${
                  Router.linkPage('catalog', { language }).as
                }?category=${wishlistData.categories.find(() => true)}`)
              }
            >
              <Trans>Edit</Trans>
            </Button>
            <Button
              onClick={() => {
                Object.keys(pendingWishlist).length === 0 &&
                  addCartItem(wishlistData, 'wishlists')
                setTimeout(
                  () =>
                    (document.location = `${
                      Router.linkPage('checkout', { language }).as
                    }?step=basket`),
                  3000
                )
              }}
            >
              <Trans>Order</Trans>
            </Button>
          </Actions>
        )}
        {wishlistData.currentStatus === 'PUBLISHED' && (
          <Actions>
            <Button
              onClick={() =>
                (document.location = `${
                  Router.linkPage('wishlist', {
                    language,
                    id: wishlistData.id,
                    name: slugify(getTitle(wishlistData.labels, language))
                  }).as
                }`)
              }
            >
              <Trans>See detail page </Trans>
            </Button>
          </Actions>
        )}
      </>
    ) : (
      <Loader />
    )}
  </Detail>
)

const Component = compose(
  getUserStore,
  getCartStore,
  getWishlistStore,
  withState('wishlistData', 'setWishlistData', {}),
  lifecycle({
    async componentDidMount() {
      const {
        wishlistId,
        setWishlistData,
        user: { id: idUser, language: lang }
      } = this.props
      const { data: wishlist } = await getUserWishlist(idUser, wishlistId, {
        lang
      })
      setWishlistData(wishlist)
    }
  })
)(DetailPage)

export default Component

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
