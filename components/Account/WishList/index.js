import { Trans } from '@lingui/react'
import { Section, Title } from './style'
import List from './List'
import Detail from './Detail'

const Component = ({ params: { edit } }) => (
  <Section>
    <Title>
      {edit ? <Trans>Track my wishlist</Trans> : <Trans>My wishlists</Trans>}
    </Title>
    {edit ? <Detail wishlistId={edit} /> : <List />}
  </Section>
)

export default Component
