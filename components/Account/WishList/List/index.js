import { compose, withState, lifecycle } from 'recompose'
import { List, config, columns } from './style'
import ReactTable from 'react-table'
import { getUserStore, getCartStore } from 'containers/App'
import { Router } from 'router'
import Loader from 'components/Theme/Layout/Loader'
import { getUserWishlists } from 'api/muhajir/wishlist'

const ListPage = ({
  isLoaded,
  wishlists,
  addCartItem,
  cart: { currencyCode, pendingWishlist },
  user: { language }
}) => (
  <List currencyCode={currencyCode}>
    {isLoaded ? (
      <ReactTable
        data={wishlists}
        columns={columns(language)}
        {...config}
        pendingWishlist={pendingWishlist}
        addCartItem={addCartItem}
        getTdProps={({ data, addCartItem, pendingWishlist }, rowInfo) => {
          return {
            onClick: event => {
              const id = event.target.id === 'addCartItem' && rowInfo.row.id
              if (!id) return
              const wishlist = data.find(
                ({ id: wishlistId }) => wishlistId === id
              )
              Object.keys(pendingWishlist).length === 0 &&
                addCartItem(wishlist, 'wishlists')
              setTimeout(
                () =>
                  (document.location = `${
                    Router.linkPage('checkout', { language }).as
                  }?step=basket`),
                3000
              )
            }
          }
        }}
        sortable={false}
      />
    ) : (
      <Loader />
    )}
  </List>
)

const Component = compose(
  getUserStore,
  getCartStore,
  withState('wishlists', 'setWishlists'),
  withState('isLoaded', 'setLoaded', false),
  lifecycle({
    async componentDidMount() {
      const {
        user: { language: lang, id },
        setWishlists,
        setLoaded
      } = this.props
      try {
        const { data: wishlists } = await getUserWishlists(id, {
          lang
        })
        setWishlists(wishlists)
      } catch (error) {
        // do something
      }
      setLoaded(true)
    }
  })
)(ListPage)

export default Component
