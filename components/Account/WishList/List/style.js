import { Trans } from '@lingui/react'
import styled from 'styled-components'
import { Picture } from 'components/Theme'
import { MiniButton } from 'components/Theme'

export const List = styled.section`
  margin-bottom: 0.8125em;
  .currency {
    p:after {
       content: ' (${({ currencyCode }) => currencyCode})';
    }
  }
`

const Link = styled.a`
  color: rgb(145, 204, 138);
  text-decoration: underline;
  font-weight: 900;
`

const Center = styled.p`
  text-align: center;
`

const Right = styled.p`
  text-align: right;
`

export const config = {
  getProps: () => ({ style: { border: 'none' } }),
  getTrGroupProps: () => ({ style: { border: 'none' } }),
  getTheadTrProps: () => ({ style: { textAlign: 'left' } }),
  getPaginationProps: () => ({
    style: {
      boxShadow: 'none',
      border: '1px solid #eee',
      padding: '0.8125em'
    }
  }),
  className: '-highlight',
  pageSizeOptions: [5, 10, 15, 20],
  defaultPageSize: 5,
  defaultSorted: [
    {
      id: 'currentStatus',
      desc: false
    }
  ],
  previousText: '‹‹',
  nextText: '››',
  loadingText: <Trans>Loading your wishlists</Trans>,
  noDataText: <Trans>You have no wishlists</Trans>,
  pageText: <Trans>Page</Trans>,
  ofText: <Trans>of</Trans>,
  rowsText: <Trans>rows</Trans>
}

export const columns = language => [
  {
    id: 'mainImageUrl',
    Header: '',
    accessor: ({ mainImageUrl, category }) => ({
      mainImageUrl,
      category
    }),
    sortMethod: (a, b) => a.category.localeCompare(b.category),
    Cell: ({ value: { mainImageUrl } }) => (
      <Picture src={mainImageUrl} size="2.4375em" margin="0" />
    ),
    minWidth: 32,
    maxWidth: 40
  },
  {
    id: 'title',
    Header: <Trans>Title</Trans>,
    accessor: ({ labels }) => getTitle(labels, language),
    minWidth: 160
  },
  {
    Header: <Trans>Start date</Trans>,
    accessor: 'startDate',
    Cell: ({ value: startDate }) =>
      new Date(startDate).toLocaleString('fr-FR', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      })
  },
  {
    Header: <Trans>End date</Trans>,
    accessor: 'endDate',
    Cell: ({ value: endDate }) =>
      new Date(endDate).toLocaleString('fr-FR', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      })
  },
  {
    headerClassName: 'currency',
    Header: () => (
      <Right>
        <Trans>Total price</Trans>
      </Right>
    ),
    Cell: ({ value: price }) => <Right>{Number(price).toFixed(2)}</Right>,
    accessor: 'price',
    minWidth: 110,
    maxWidth: 120
  },
  {
    Header: () => (
      <Center>
        <Trans>Status</Trans>
      </Center>
    ),
    accessor: 'currentStatus',
    Cell: ({ value: currentStatus }) => (
      <Center>
        {currentStatus === 'PENDING' ? (
          <Trans>Pending</Trans>
        ) : currentStatus === 'PUBLISHED' ? (
          <Trans>Published</Trans>
        ) : currentStatus === 'CLOSED' ? (
          <Trans>Closed</Trans>
        ) : (
          currentStatus
        )}
        {currentStatus === 'PENDING' && (
          <MiniButton id="addCartItem">
            <Trans>Place the order</Trans>
          </MiniButton>
        )}
      </Center>
    ),
    sortMethod: (a, b) => {
      return a === 'PENDING' ? -1 : b === 'PENDING' ? 1 : -1
    },
    minWidth: 190,
    maxWidth: 210
  },
  {
    Header: '',
    accessor: 'id',
    Cell: ({ value: id }) => (
      <Link href={`${document.location}&edit=${id}`}>
        <Trans>Details</Trans>
      </Link>
    ),
    minWidth: 60,
    maxWidth: 70
  }
]

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
