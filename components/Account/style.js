import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const SelfCare = styled(Flex)`
  min-height: 74.25vh;
  background: #fff;
  border-top: 0.40625em solid #202c28;
`
export const Span = styled.span`
  display: none;
`

export const Icon = styled.img`
  width: 2em;
  height: 2em;
  object-fit: contain;
  margin: 0.203125em 0.40625em 0;
`

export const Menu = styled(Box)`
  padding: 0.8125em 0.40625em;
  flex: 0.125;
  background: #eee;
  ${theme.media.pad`
    flex: 0.2;
    ${Icon}{
      display: inline;
      float: left;
      margin: 0 0.203125em 0.203125em 0;
    }
    ${Span}{
      display: inline;
    }
  `};
`

export const Main = styled(Box)`
  background: #fff;
  min-height: 74.25vh;
  padding-left: 0.8125em;
  border-left: 1px solid #eee;
  flex: 1;
`

export const Link = styled.a`
  display: block;
  margin-bottom: 0.8125em;
  padding: 0.05em;
  border-left: ${props => (props.selected ? '4px solid #065606' : 'none')};
  font-weight: ${props => (props.selected ? '900' : '100')};
  ${theme.media.pad`
    padding: 0.40625em 0.8125em;
    margin-bottom: 0.40625em;
  `};
`
