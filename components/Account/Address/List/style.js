import { Trans } from '@lingui/react'
import styled from 'styled-components'
import { removeAddress } from 'api/muhajir/address'
import { Router } from 'router'

const Link = styled.a`
  color: rgb(145, 204, 138);
  text-decoration: underline;
  font-weight: 900;
`

const Label = styled.p``

const Right = styled.p`
  text-align: right;
`
export const config = {
  getProps: () => ({ style: { border: 'none' } }),
  getTdProps: () => ({
    style: { padding: '0.8125em 1.625em 0.40625em 0.8125em' }
  }),
  getTheadThProps: () => ({
    style: { padding: '0.8125em 1.625em 0 0.8125em' }
  }),
  getTrGroupProps: () => ({
    style: { border: 'none' }
  }),
  getTheadTrProps: () => ({ style: { textAlign: 'left' } }),
  getPaginationProps: () => ({
    style: {
      boxShadow: 'none',
      border: '1px solid #eee',
      padding: '0.8125em'
    }
  }),
  className: '-highlight',
  showPagination: false,
  defaultSorted: [
    {
      id: 'address1',
      desc: false
    }
  ],
  loadingText: <Trans>Loading...</Trans>,
  noDataText: (
    <Trans>You have no addresses, please add your first address</Trans>
  )
}

export const columns = language => [
  {
    Header: <Trans>Alias</Trans>,
    accessor: 'alias',
    minWidth: 130,
    maxWidth: 150
  },
  {
    Header: <Trans>First and last names</Trans>,
    id: 'name',
    accessor: ({ gender, lastName, firstName }) => (
      <Label>
        {gender === 'MALE' ? <Trans>MR</Trans> : <Trans>MRS</Trans>} {lastName}{' '}
        {firstName}
      </Label>
    ),

    minWidth: 160,
    maxWidth: 170
  },
  {
    Header: <Trans>Address</Trans>,
    accessor: 'address1',
    minWidth: 120
  },
  {
    Header: <Trans>City</Trans>,
    accessor: 'city',
    minWidth: 100,
    maxWidth: 110
  },
  {
    Header: <Trans>Country</Trans>,
    accessor: 'countryCode',
    minWidth: 90,
    maxWidth: 100
  },
  {
    Header: () => (
      <Right>
        <Trans>Zip code</Trans>
      </Right>
    ),
    Cell: ({ value: postCode }) => <Right>{postCode}</Right>,
    accessor: 'postCode',
    minWidth: 60
  },
  {
    Header: '',
    accessor: 'id',
    Cell: ({ value: id }) => (
      <Link
        onClick={async () => {
          await removeAddress(id)
          document.location.reload()
        }}
      >
        <Trans>Delete</Trans>
      </Link>
    ),
    minWidth: 110,
    maxWidth: 120
  },
  {
    Header: '',
    accessor: 'id',
    Cell: ({ value: id }) => (
      <Link
        onClick={() =>
          (document.location = `${
            Router.linkPage('account', { language }).as
          }?view=address&edit=${id}`)
        }
      >
        <Trans>Edit</Trans>
      </Link>
    ),
    minWidth: 90,
    maxWidth: 100
  }
]
