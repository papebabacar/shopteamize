import { config, columns } from './style'
import ReactTable from 'react-table'
import { getUserStore } from 'containers/App'

const Component = ({ data, user: { language } }) => (
  <ReactTable
    data={data}
    columns={columns(language)}
    {...config}
    pageSize={data.length || 5}
    sortable={false}
  />
)

export default getUserStore(Component)
