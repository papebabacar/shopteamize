import styled from 'styled-components'

export const Address = styled.section`
  padding: 0.40625em 0.8125em;
`

export const Title = styled.h1`
  padding: 0.40625em;
  border: 1px solid #eee;
  border-bottom: 4px solid;
`

export const Link = styled.a`
  color: rgb(145, 204, 138);
  text-decoration: underline;
  font-weight: 900;
  display: inline-block;
  margin: 3.25em 0 0;
`
