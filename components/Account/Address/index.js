import { Trans, withI18n } from '@lingui/react'
import { compose, withState, lifecycle } from 'recompose'
import { Address, Title, Link } from './style'
import List from './List'
import Form from './Form'
import Loader from 'components/Theme/Layout/Loader'
import { getUserAddresses } from 'api/muhajir/address'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const AddressPage = ({
  isLoaded,
  addresses,
  params: { edit },
  user: { language }
}) => (
  <Address>
    <Title>
      <Trans>My addresses</Trans>
    </Title>
    {isLoaded ? (
      edit ? (
        <Form
          {...addresses.find(({ id }) => id === Number(edit))}
          setEditing={() =>
            (document.location = `${
              Router.linkPage('account', { language }).as
            }?view=address`)
          }
          withCancel
        />
      ) : (
        <>
          <List data={addresses} />
          <Link
            onClick={() =>
              (document.location = `${
                Router.linkPage('account', { language }).as
              }?view=address&edit=new`)
            }
          >
            {addresses.length ? (
              <Trans>Add your new address</Trans>
            ) : (
              <Trans>Create your new address</Trans>
            )}
          </Link>
        </>
      )
    ) : (
      <Loader />
    )}
  </Address>
)

const Component = compose(
  getUserStore,
  withI18n(),
  withState('addresses', 'setAddresses', []),
  withState('isLoaded', 'setLoaded', false),
  lifecycle({
    async componentDidMount() {
      const { setAddresses, setLoaded } = this.props
      try {
        const { data: addresses } = await getUserAddresses()
        setAddresses(addresses)
      } catch (error) {
        // do something
      }
      setLoaded(true)
    }
  })
)(AddressPage)

export default Component
