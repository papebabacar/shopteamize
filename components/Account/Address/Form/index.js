import { Trans, withI18n } from '@lingui/react'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { getUserStore } from 'containers/App'
import { Info, Actions } from './style'
import { FormTitle, Button, SubmitButton, Notification } from 'components/Theme'
import { Form, withForm } from 'components/Theme/Form'
import Field from 'components/Theme/Form/Field'
import { createAddress, updateAddress } from 'api/muhajir/address'
import { getCountries } from 'api/muhajir/shipping'

const genderOptions = [
  { value: 'MALE', label: <Trans>MR</Trans> },
  { value: 'FEMALE', label: <Trans>MRS</Trans> }
]

const Address = ({
  countries,
  id,
  alias = '',
  gender = genderOptions[0].value,
  firstName = '',
  lastName = '',
  phone = '',
  address1 = '',
  city = '',
  countryCode = 'FRA',
  postCode = '',
  post,
  submitting,
  setEditing,
  submitError,
  withCancel,
  i18n
}) => (
  <Info flex-no-gutter flex-phone={1}>
    <Form post={post} submitting={submitting}>
      <FormTitle>
        {id ? (
          <Trans>Edit your address</Trans>
        ) : (
          <Trans>Create a new address</Trans>
        )}
      </FormTitle>
      <Field name="alias" label={<Trans>Alias</Trans>} required value={alias} />
      <Field
        name="gender"
        label={<Trans>Gender</Trans>}
        type="select"
        required
        value={gender}
        options={genderOptions}
      />
      <Field
        name="lastName"
        label={<Trans>Name</Trans>}
        required
        value={lastName}
      />
      <Field
        name="firstName"
        label={<Trans>Firstname</Trans>}
        required
        value={firstName}
      />
      <Field
        name="countryCode"
        label={<Trans>Country</Trans>}
        required
        value={countryCode}
        type="select"
        options={countries.reduce(
          (countriesOptions, { name, code }) => [
            ...countriesOptions,
            { value: code, label: name }
          ],
          []
        )}
      />
      <Field name="city" label={<Trans>City</Trans>} required value={city} />
      <Field
        name="postCode"
        label={<Trans>Postal code</Trans>}
        required
        value={postCode}
        validations="isNumeric"
        validationError={i18n.t`The postal code should contain only numeric values`}
      />
      <Field
        name="phone"
        label={<Trans>Telephone</Trans>}
        required
        value={phone}
        validations="minLength:7"
        validationError={i18n.t`The phone number should be at least 7 characters`}
      />
      <Field
        name="address1"
        label={<Trans>Address</Trans>}
        required
        value={address1}
        validations="minLength:5"
        validationError={i18n.t`The address should be at least 5 characters`}
      />
      {submitError && <Notification>{submitError}</Notification>}
      <Actions>
        {withCancel && (
          <Button secondary onClick={() => setEditing(false)}>
            <Trans>Cancel</Trans>
          </Button>
        )}
        <SubmitButton disabled={submitting}>
          <Trans>Submit</Trans>
        </SubmitButton>
      </Actions>
    </Form>
  </Info>
)

const Component = compose(
  getUserStore,
  withForm,
  withI18n(),
  withState('submitError', 'setSubmitError'),
  withState('countries', 'setCountries', []),
  withHandlers({
    post: ({
      countries,
      user,
      id,
      setEditing,
      setFormState,
      setSubmitError,
      i18n,
      reload
    }) => async data => {
      setFormState(true)
      try {
        const { data: address } = id
          ? await updateAddress({
              ...data,
              id,
              country: countries.find(({ code }) => code === data.countryCode)
                .name,
              customerId: user.id
            })
          : await createAddress({
              ...data,
              country: countries.find(({ code }) => code === data.countryCode)
                .name,
              customerId: user.id
            })
        setEditing(address)
        reload && document.location.reload()
      } catch (error) {
        setSubmitError(
          i18n.t`A technical error has occurred. Please reload the page`
        )
        setTimeout(() => setSubmitError(), 5000)
        setFormState(false)
      }
    }
  }),
  lifecycle({
    async componentDidMount() {
      const {
        user: { language: lang },
        setCountries
      } = this.props
      try {
        const { data: countries } = await getCountries({ lang })
        setCountries(countries)
      } catch (error) {
        // do something
      }
    }
  })
)(Address)

export default Component
