import styled from 'styled-components'

export const Info = styled.section`
  max-width: 42em;
  margin: 0 auto;
  background: white;
  padding: 0.8125em 1.625em;
`

export const Actions = styled.div`
  margin: 1.625em 0;
  text-align: center;
`
