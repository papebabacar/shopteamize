import { Trans } from '@lingui/react'
import { compose, withState, lifecycle } from 'recompose'
import { Detail, Info, Label, Description, config, columns } from './style'
import ReactTable from 'react-table'
import { getOrderByUser } from 'api/muhajir/order'
import { getWishlists } from 'api/muhajir/wishlist'

const DetailPage = ({
  currencyCode,
  user: { language },
  order: {
    items,
    createdDate,
    currentStatus,
    totalShipping = 0,
    totalShopping = 0,
    total = 0,
    totalItems = Number(total - totalShipping - totalShopping).toFixed(2),
    payment: { paymentType, id: paymentId },
    shipping: { carrierName, id: shippingId }
  }
}) => (
  <Detail>
    <ReactTable
      loading={!items}
      data={items}
      columns={columns(language)}
      {...config}
      pageSize={(items && items.length) || 5}
      sortable={false}
    />
    <Info padding={0.8125} flex-phone={2} flex-tablet={4}>
      <Label>
        <Trans>Creation date :</Trans>
      </Label>
      <Description>
        {new Date(createdDate).toLocaleString(language, {
          weekday: 'long',
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })}
      </Description>
      <Label>
        <Trans>Status :</Trans>
      </Label>
      <Description>{currentStatus}</Description>
      <Label>
        <Trans>Payment / Tracking Number :</Trans>
      </Label>
      <Description>
        {paymentType} <Trans>/ Ref.</Trans>
        {paymentId}
      </Description>
      <Label>
        <Trans>Carrier / Tracking number:</Trans>
      </Label>
      <Description>
        {carrierName} <Trans>/ Ref.</Trans>
        {shippingId}
      </Description>
      <Label>
        <Trans>Subtotal :</Trans>
      </Label>
      <Description>
        {totalItems} {currencyCode}
      </Description>
      <Label>
        <Trans>Shipping fees :</Trans>
      </Label>
      <Description>
        {totalShipping} {currencyCode}
      </Description>
      <Label>
        <Trans>Shopping fees :</Trans>
      </Label>
      <Description>
        {totalShopping} {currencyCode}
      </Description>
      <Label>
        <Trans>Total paid :</Trans>
      </Label>
      <Description>
        {total} {currencyCode}
      </Description>
    </Info>
  </Detail>
)

const Component = compose(
  withState('order', 'setOrder', {
    payment: {},
    shipping: {}
  }),
  lifecycle({
    async componentDidMount() {
      const {
        orderId,
        user: { id: userId, language: lang },
        setOrder
      } = this.props
      try {
        const { data: order } = await getOrderByUser(userId, orderId)
        const { products, customerWishlists, shoppingIdeas } = order
        const wishlistQuantities = {}
        const wishlistsIds = [
          ...customerWishlists.map(
            ({ wishlistId, quantity }) =>
              (wishlistQuantities[wishlistId] = quantity) && wishlistId
          ),
          ...shoppingIdeas.map(
            ({ wishlistId, quantity }) =>
              (wishlistQuantities[wishlistId] = quantity) && wishlistId
          )
        ]
        const { data: wishlists } = await getWishlists({
          ids: wishlistsIds.toString(),
          lang
        })
        setOrder({
          ...order,
          items: [
            ...products,
            ...wishlists.map(wishlist => ({
              ...wishlist,
              quantity: wishlistQuantities[wishlist.id]
            }))
          ]
        })
      } catch (error) {
        // do something
      }
    }
  })
)(DetailPage)

export default Component
