import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'
import { Trans } from '@lingui/react'

export const Detail = styled.section`
  margin-bottom: 0.8125em;
`

export const Info = styled(Flex)`
  margin: 1.625em 0;
  text-align: right;
  justify-content: flex-end;
`

export const Label = styled(Box)`
  padding: 0.40625em 0;
`

export const Description = styled(Box)`
  font-weight: 900;
  padding: 0.40625em 0;
`

export const Actions = styled.div`
  text-align: center;
  margin-bottom: 0.8125em;
`

const Right = styled.p`
  text-align: right;
`

export const config = {
  getProps: () => ({ style: { border: 'none' } }),
  getTdProps: () => ({
    style: { padding: '0.8125em 1.625em 0.40625em 0.8125em' }
  }),
  getTheadThProps: () => ({
    style: { padding: '0.8125em 1.625em 0 0.8125em' }
  }),
  getTrGroupProps: () => ({
    style: { border: 'none' }
  }),
  getTheadTrProps: () => ({ style: { textAlign: 'left' } }),
  getPaginationProps: () => ({
    style: {
      boxShadow: 'none',
      border: '1px solid #eee',
      padding: '0.8125em'
    }
  }),
  showPagination: false,
  className: '-highlight',
  defaultSorted: [
    {
      id: 'name'
    }
  ],
  loadingText: <Trans>Loading your articles</Trans>,
  noDataText: <Trans>You have no articles</Trans>
}

export const columns = language => [
  {
    id: 'name',
    Header: <Trans>Article</Trans>,
    accessor: ({ name, labels }) => name || getTitle(labels, language),
    minWidth: 160
  },
  {
    Header: () => (
      <Right>
        <Trans>Quantity</Trans>
      </Right>
    ),
    Cell: ({ value: quantity }) => <Right>{quantity}</Right>,
    accessor: 'quantity',
    minWidth: 100
  },
  {
    headerClassName: 'currency',
    Header: () => (
      <Right>
        <Trans>Unit price</Trans>
      </Right>
    ),
    Cell: ({ value: price }) => <Right>{price}</Right>,
    accessor: 'price',
    minWidth: 150,
    maxWidth: 180
  },
  {
    id: 'totalPrice',
    headerClassName: 'currency',
    Header: () => (
      <Right>
        <Trans>Total price</Trans>
      </Right>
    ),
    Cell: ({ value: total }) => <Right>{total}</Right>,
    accessor: ({ price, quantity }) => price * quantity,
    minWidth: 150,
    maxWidth: 180
  }
]

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
