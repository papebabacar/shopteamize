import { Trans } from '@lingui/react'
import styled from 'styled-components'

const Link = styled.a`
  color: rgb(145, 204, 138);
  text-decoration: underline;
  font-weight: 900;
`

const Right = styled.p`
  text-align: right;
`

export const config = {
  getProps: () => ({ style: { border: 'none' } }),
  getTdProps: () => ({
    style: { padding: '0.8125em 1.625em 0.40625em 0.8125em' }
  }),
  getTheadThProps: () => ({
    style: { padding: '0.8125em 1.625em 0 0.8125em' }
  }),
  getTrGroupProps: () => ({
    style: { border: 'none' }
  }),
  getTheadTrProps: () => ({ style: { textAlign: 'left' } }),
  getPaginationProps: () => ({
    style: {
      boxShadow: 'none',
      border: '1px solid #eee',
      padding: '0.8125em'
    }
  }),
  className: '-highlight',
  pageSizeOptions: [5, 10, 15, 20],
  defaultPageSize: 5,
  defaultSorted: [
    {
      id: 'createdDate',
      desc: true
    }
  ],
  previousText: '‹‹',
  nextText: '››',
  loadingText: <Trans>Loading your orders</Trans>,
  noDataText: <Trans>You have no orders</Trans>,
  pageText: <Trans>Page</Trans>,
  ofText: <Trans>of</Trans>,
  rowsText: <Trans>rows</Trans>
}

export const columns = [
  {
    Header: () => (
      <Right>
        <Trans>Ref</Trans>
      </Right>
    ),
    Cell: ({ value: id }) => <Right>{id}</Right>,
    accessor: 'id',
    minWidth: 70,
    maxWidth: 100
  },
  {
    Header: <Trans>Created date</Trans>,
    accessor: 'createdDate',
    Cell: ({ value: createdDate }) =>
      new Date(createdDate).toLocaleString('fr-FR', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      })
  },
  {
    headerClassName: 'currency',
    Header: () => (
      <Right>
        <Trans> Total price </Trans>
      </Right>
    ),
    Cell: ({ value: total }) => <Right>{Number(total).toFixed(2)}</Right>,
    accessor: 'total',
    minWidth: 150,
    maxWidth: 180
  },
  {
    Header: <Trans>Status</Trans>,
    accessor: 'currentStatus',
    Cell: ({ value: currentStatus }) =>
      currentStatus === 'PAYMENT_SUCCESS' ? (
        <Trans>PAYMENT_SUCCESS</Trans>
      ) : (
        <Trans>PAYMENT_IN_PROGRESS</Trans>
      ),
    minWidth: 60
  },
  {
    Header: <Trans>Payment</Trans>,
    accessor: 'payment.paymentType',
    minWidth: 80
  },
  {
    Header: <Trans>Shipping</Trans>,
    accessor: 'shipping.carrierName',
    minWidth: 60
  },
  {
    Header: '',
    accessor: 'id',
    Cell: ({ value: id }) => (
      <Link href={`${document.location}&edit=${id}`}>
        <Trans>Details</Trans>
      </Link>
    ),
    minWidth: 90,
    maxWidth: 100
  }
]
