import { compose, withState, lifecycle } from 'recompose'
import { getUserOrders } from 'api/muhajir/order'
import { getCartStore } from 'containers/App'
import { config, columns } from './style'
import ReactTable from 'react-table'

const List = ({ orders }) => (
  <ReactTable
    loading={!orders}
    data={orders}
    columns={columns}
    {...config}
    sortable={false}
  />
)

const Component = compose(
  getCartStore,
  withState('orders', 'setOrders'),
  lifecycle({
    async componentDidMount() {
      const { user, setOrders } = this.props
      try {
        const { data: orders } = await getUserOrders(user.id)
        setOrders(orders)
      } catch (error) {
        // do something
      }
    }
  })
)(List)

export default Component
