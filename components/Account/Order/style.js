import styled from 'styled-components'

export const Section = styled.section`
  padding: 0.40625em 0.8125em;
  .currency {
    p:after {
       content: ' (${({ currencyCode }) => currencyCode})';
    }
  }
`

export const Title = styled.h1`
  padding: 0.40625em;
  border: 1px solid #eee;
  border-bottom: 4px solid;
`
