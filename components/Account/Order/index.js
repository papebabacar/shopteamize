import { Trans } from '@lingui/react'
import { Section, Title } from './style'
import List from './List'
import Detail from './Detail'
import { getCartStore } from 'containers/App'

const Component = ({ user, cart: { currencyCode }, params: { edit } }) => (
  <Section currencyCode={currencyCode}>
    <Title>
      {edit ? (
        <Trans>Track my order - Ref. {edit}</Trans>
      ) : (
        <Trans>My orders</Trans>
      )}
    </Title>
    {edit ? (
      <Detail user={user} currencyCode={currencyCode} orderId={edit} />
    ) : (
      <List user={user} />
    )}
  </Section>
)

export default getCartStore(Component)
