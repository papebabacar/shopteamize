import { Trans, withI18n } from '@lingui/react'
import { compose, withState, withHandlers } from 'recompose'
import {
  Home,
  Title,
  Section,
  Block,
  Heading
  /* , Link, Actions */
} from './style'
// import { SubmitButton, Notification } from 'components/Theme'
import { getUserStore } from 'containers/App'
import { Form, withForm } from 'components/Theme/Form'
import Field from 'components/Theme/Form/Field'
import { authenticate, updateUser, updatePassword } from 'api/account'

const Info = ({
  user: { login, firstName, lastName },
  postInfo,
  postEmail,
  postPassword,
  // submitting,
  // submitError,
  // setSubmitError,
  editInfo,
  // setEditInfo,
  editEmail,
  // setEditEmail,
  editPassword,
  // setEditPassword,
  i18n
}) => (
  <Home>
    <Title>
      <Trans>My account</Trans>
    </Title>
    <Heading>
      <Trans>My personal info</Trans>
    </Heading>
    <Form post={postInfo}>
      <Section padding={0.40625} flex-phone={1}>
        <Field
          name="lastName"
          label={<Trans>Last name</Trans>}
          required
          value={lastName}
          edit={editInfo}
        />
        <Field
          name="firstName"
          label={<Trans>First name</Trans>}
          required
          value={firstName}
          edit={editInfo}
        />
        {/* {editInfo && (
          <Actions>
            {submitError && <Notification>{submitError}</Notification>}
            <SubmitButton
              onClick={() => {
                setEditInfo(false)
                setSubmitError(false)
              }}
            >
              <Trans>Cancel</Trans>
            </SubmitButton>
            <SubmitButton disabled={submitting}>
              <Trans>Submit</Trans>
            </SubmitButton>
          </Actions>
        )}
        {!editInfo &&
          !editEmail &&
          !editPassword && (
            <Link onClick={() => setEditInfo(true)}>
              <Trans>Click here to modify</Trans>
            </Link>
          )} */}
      </Section>
    </Form>
    <Heading>
      <Trans>My account information</Trans>
    </Heading>
    <Form post={editEmail ? postEmail : postPassword}>
      <Section flex-phone={1}>
        {!editPassword && (
          <Block>
            <Field
              label={
                editEmail ? <Trans>Actual email</Trans> : <Trans>Email</Trans>
              }
              name="email"
              placeholder="Email"
              type="email"
              value={login}
              required
              validations="isEmail"
              validationError={i18n.t`Please enter a valid email address`}
              edit={editEmail}
            />
            {editEmail && (
              <Field
                label={<Trans>New email address</Trans>}
                name="newEmail"
                placeholder={i18n.t`New email address`}
                type="email"
                value=""
                required
                validations="isEmail"
                validationError={i18n.t`Please enter a valid email address`}
              />
            )}
            {editEmail && (
              <Field
                label="Confirmation"
                name="confirmation"
                type="email"
                value=""
                placeholder={i18n.t`New email address`}
                required
                validations="equalsField:newEmail"
                validationError={i18n.t`The two passwords must be equal`}
              />
            )}
          </Block>
        )}
        {/* {!editInfo &&
          !editEmail &&
          !editPassword && (
            <Link onClick={() => setEditEmail(true)}>
              <Trans>Click here to modify</Trans>
            </Link>
          )} */}
        {/* <Block>
          <Field
            label={
              editPassword ? (
                <Trans>Actual password</Trans>
              ) : (
                <Trans>Password</Trans>
              )
            }
            name="password"
            placeholder="*******"
            type="password"
            value=""
            required
            edit={editEmail || editPassword}
          />
          {!editEmail &&
            editPassword && (
              <Field
                label={<Trans>New password</Trans>}
                name="newPassword"
                placeholder="*******"
                type="password"
                value=""
                required
              />
            )}
          {!editEmail &&
            editPassword && (
              <Field
                label="Confirmation"
                name="confirmation"
                type="password"
                value=""
                placeholder="*******"
                required
                validations="equalsField:newPassword"
                validationError={i18n.t`The two passwords should be equal`}
              />
            )}
        </Block> */}
        {/* {!editInfo &&
          !editEmail &&
          !editPassword && (
            <Link onClick={() => setEditPassword(true)}>
              <Trans>Click here to modify</Trans>
            </Link>
          )}
        {(editEmail || editPassword) && (
          <Actions>
            {submitError && <Notification>{submitError}</Notification>}
            <SubmitButton
              onClick={() => {
                setEditEmail(false)
                setEditPassword(false)
                setSubmitError(false)
              }}
            >
              Annuler
            </SubmitButton>
            <SubmitButton disabled={submitting}>
              <Trans>Submit</Trans>
            </SubmitButton>
          </Actions>
        )} */}
      </Section>
    </Form>
  </Home>
)

const Component = compose(
  withI18n(),
  getUserStore,
  withForm,
  withState('submitError', 'setSubmitError'),
  withState('editInfo', 'setEditInfo', false),
  withState('editEmail', 'setEditEmail', false),
  withState('editPassword', 'setEditPassword', false),
  withHandlers({
    postInfo: ({
      setFormState,
      setSubmitError,
      loginUser,
      user: { id, login },
      i18n
    }) => async data => {
      setFormState(true)
      try {
        const { firstName: newFirstName, lastName: newLastName } = data
        const {
          data: { firstName, lastName }
        } = await updateUser({
          id,
          login,
          firstName: newFirstName,
          lastName: newLastName
        })
        loginUser({ firstName, lastName })
        document.location.reload()
      } catch (error) {
        let errorMessage = i18n.t`A technical error has occurred. Please reload the page`
        setSubmitError(errorMessage)
        setTimeout(() => setSubmitError(), 5000)
        setFormState(false)
      }
    },
    postEmail: ({
      setFormState,
      setSubmitError,
      loginUser,
      user: { id, firstName, lastName },
      i18n
    }) => async data => {
      setFormState(true)
      try {
        const { email, newEmail, password } = data
        await authenticate({
          username: email,
          password
        })
        await updateUser({
          id,
          firstName,
          lastName,
          login: newEmail,
          email: newEmail
        })
        const {
          data: { id_token: newToken }
        } = await authenticate({
          username: newEmail,
          password
        })
        loginUser({ token: newToken, login: newEmail, email: newEmail })
        document.location.reload()
      } catch (error) {
        setFormState(false)
        let errorMessage = i18n.t`A technical error has occurred. Please reload the page`
        setTimeout(() => setSubmitError(), 5000)
        setSubmitError(errorMessage)
      }
    },
    postPassword: ({
      setFormState,
      setSubmitError,
      user: { login: username },
      loginUser,
      i18n
    }) => async data => {
      setFormState(true)
      try {
        const { password, newPassword } = data
        await authenticate({
          username,
          password
        })
        await updatePassword({
          newPassword
        })
        const {
          data: { id_token: token }
        } = await authenticate({
          username,
          password: newPassword
        })
        loginUser({ token })
      } catch (error) {
        setFormState(false)
        let errorMessage = i18n.t`A technical error has occurred. Please reload the page`
        setTimeout(() => setSubmitError(), 5000)
        setSubmitError(errorMessage)
      }
    }
  })
)(Info)

export default Component
