import styled from 'styled-components'
import { Flex, Box } from 'components/Theme/Layout/FlexBox'

export const Home = styled.section`
  padding: 0.40625em 0.8125em;
`
export const Title = styled.h1`
  padding: 0.40625em;
  border: 1px solid #eee;
  border-bottom: 4px solid;
`
export const Section = styled(Flex)`
  padding: 0.40625em 0;
`

export const Block = styled(Box)``

export const Heading = styled.h2`
  text-align: center;
  background: #202c28;
  color: white;
  padding: 0.203125em 0;
`

export const Link = styled.a`
  color: rgb(145, 204, 138);
  text-decoration: underline;
  font-weight: 900;
  display: inline-block;
  margin: 0 auto;
`
export const Actions = styled.div`
  text-align: center;
  flex: 1;
  margin-bottom: 0.8125em;
`
