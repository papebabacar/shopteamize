import BreadCrumb from './BreadCrumb'
import Info from './Info'
import Tabs from './Tabs'
import Carousel from './Carousel'
import Share from './Share'
import { getUserStore, getModalStore } from 'containers/App'
import { Detail } from './style'
import QuickView from 'components/QuickView/Product'
import { compose, lifecycle } from 'recompose'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'
import { initializeProduct } from 'components/Theme/GTM'
import {
  JSONLD,
  AggregateRating,
  GenericCollection,
  Product,
  Review,
  Author,
  Location,
  Rating
} from 'react-structured-data'

const ProductPage = ({ product, terms, modal, user: { language } }) => (
  <>
    <BreadCrumb product={product.name} category={product.category} />
    <Detail padding={0} flex-phone={1} flex-tablet={2}>
      <Carousel
        mainImageUrl={product.mainImageUrl}
        images={product.images}
        withActions
      />
      <Info product={product} language={language} detail />
    </Detail>
    <Share
      // eslint-disable-next-line no-undef
      shareUrl={`${APP_URL}${
        Router.linkPage('product', {
          language,
          id: product.id,
          name: slugify(product.name)
        }).as
      }`}
      image={product.mainImageUrl}
    />
    <Tabs
      tabs={[
        { title: 'Description', description: product.description },
        ...terms
      ]}
    />
    {modal.visible && modal.view === 'product' && <QuickView />}
    <JSONLD>
      <Product name={product.name}>
        <AggregateRating ratingValue={product.category} />
        <GenericCollection type="review">
          <Review
            name="It's awesome"
            reviewBody="This is Great! My family loves it"
            datePublished="11/22/1963"
          >
            <Author name="Jerry" />
            <Location name="Chicago, IL" />
            <Rating ratingValue={5} />
          </Review>
          <Review
            name="Very cool"
            reviewBody="I like this a lot. Very cool product"
            datePublished="11/22/1963"
          >
            <Author name="Cool Carl" />
            <Location name="Chicago, IL" />
            <Rating ratingValue={4} />
          </Review>
        </GenericCollection>
      </Product>
    </JSONLD>
  </>
)
const Component = compose(
  getUserStore,
  getModalStore,
  lifecycle({
    async componentDidMount() {
      const { product } = this.props
      initializeProduct(product)
    }
  })
)(ProductPage)

export default getModalStore(Component)
