import styled, { css } from 'styled-components'
import theme from 'components/Theme'
import { Box } from 'components/Theme/Layout/FlexBox'

export const Info = styled(Box)`
  padding: ${props => (props.withActions ? '1.625em' : '0.8125em 2.4375em')};
  ${theme.media.tablet`padding: ${props =>
    props.withActions ? '3.25em 1.625em 0' : '4.5em 1.25em 0.8125em'};`};
  ${theme.media.laptop`padding: ${props =>
    props.withActions ? '3.25em 2.4375em 0' : '4.5em 1.25em 0.8125em'};`};
  margin: 0 !important;
`

export const Title = styled.h1`
  margin: 0;
  ${props =>
    !props.withActions &&
    css`
      font-size: 115%;
    `};
`

export const Description = styled.div`
  margin-bottom: 0.8125em;
  display: ${props => (props.hideOnMobile ? 'none' : 'block')};
  ${theme.media.tablet`display:block`};
  ${props =>
    !props.withActions &&
    props.quickView &&
    css`
      height: 8.6875em;
      overflow: hidden;
    `};
`

export const Label = styled.p`
  margin-bottom: 0;
  display: inline-block;
  width: 50%;
  color: #888;
`
export const Color = styled.p`
  background: white;
  padding: 0.203125em 0.8125em 0.203125em 1.625em;
  margin: 0.40625em 0;
  font-family: 'Arimo';
  font-size: 110%;
  display: inline-block;
  width: 50%;
  border-radius: 1.625em;
  border: 1px solid #eee;
`

export const Price = styled.h3`
  font-weight: 900;
`

export const Links = styled.div`
  text-align: ${props => (props.withActions ? 'center' : 'left')};
`

export const Link = styled.a`
  display: inline-block;
  text-decoration: underline;
  margin: 0.8125em 0.8125em 0 0;
`
export const Actions = styled.div`
  margin: 0.40625em 0;
  ${theme.media.tablet`margin: 3.25em 0 1.625em;`};
`
