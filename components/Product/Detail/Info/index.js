import { Trans } from '@lingui/react'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import {
  Info,
  Title,
  Description,
  Price,
  Label,
  Links,
  Link,
  Actions,
  Color
} from './style'
import { Button, Size, NumericInput } from 'components/Theme'
import {
  getUserStore,
  getModalStore,
  getCartStore,
  getWishlistStore
} from 'containers/App'
import { getWishlists } from 'api/muhajir/wishlist'
import {
  initializeProductClick,
  initializeAddToCartClick
} from 'components/Theme/GTM'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'

const Product = ({
  product,
  detail,
  wishlists,
  cart: { currencyCode, quantityToAdd: quantity },
  setDefaultCartQuantity,
  addCartItem,
  showModal,
  withActions = true,
  selected,
  setSelected,
  getSelected,
  user: { language },
  quickView,
  addToWishlist,
  fetchData
}) => (
  <Info withActions={withActions}>
    <Price>
      {getSelected().price} {currencyCode}
    </Price>
    <Title withActions={withActions}>{product.name}</Title>
    <Description
      withActions={withActions}
      dangerouslySetInnerHTML={{ __html: product.shortDescription }}
      hideOnMobile={quickView}
    />
    {product.color && (
      <>
        <Label>
          <Trans>COLOR</Trans>
        </Label>
        <Color>{product.color.toUpperCase()}</Color>
      </>
    )}
    {product.size && (
      <Label>
        <Trans>SIZE</Trans>
      </Label>
    )}
    {withActions &&
      product.size && (
        <Size
          value={selected}
          onChange={event => setSelected(Number(event.target.value))}
          options={product.variants
            .reduce(
              (sizes, { id, size }) => [
                ...sizes,
                { label: size.toUpperCase(), value: id }
              ],
              [{ label: product.size.toUpperCase(), value: product.id }]
            )
            .sort(({ label: label1 }, { label: label2 }) => label1 > label2)}
        />
      )}
    {withActions && (
      <>
        <Label>
          <Trans>QUANTITY</Trans>
        </Label>
        <NumericInput
          value={quantity}
          onChange={event => {
            setDefaultCartQuantity(Number(event.target.value))
          }}
        />
      </>
    )}
    {withActions && (
      <Actions>
        <Button
          block
          intermediary
          onClick={async () => {
            if (product.addToWishlist) {
              await addToWishlist(getSelected().id, quantity)
              showModal(null, 'product')
              fetchData()
            } else {
              addCartItem(getSelected(), 'products')
              showModal({}, 'product')
              initializeAddToCartClick(getSelected())
            }
          }}
        >
          {product.addToWishlist ? (
            <Trans>ADD TO MY WISHLIST</Trans>
          ) : (
            <Trans>ADD TO CART</Trans>
          )}
        </Button>
        {withActions &&
          wishlists.length > 0 && (
            <Button
              block
              secondary
              href={
                wishlists.length === 1
                  ? `${
                      Router.linkPage('wishlist', {
                        language,
                        id: wishlists[0].id,
                        name: slugify(getTitle(wishlists[0].labels, language))
                      }).as
                    }`
                  : `${Router.linkPage('wishlists', { language }).as}?id=${
                      getSelected().id
                    }`
              }
            >
              <Trans>ASSOCIATED WHISHLIST</Trans>
            </Button>
          )}
      </Actions>
    )}
    <Links withActions={withActions}>
      {!detail && (
        <Link
          onClick={() => {
            initializeProductClick(getSelected())
          }}
          href={`${
            Router.linkPage('product', {
              language,
              id: getSelected().id,
              name: slugify(getSelected().name)
            }).as
          }`}
        >
          <Trans>See product's details</Trans>
        </Link>
      )}
      {withActions &&
        product.size && (
          <Link
            href={`${
              Router.linkPage('sizes', {
                language
              }).as
            }`}
          >
            <Trans>Size guide</Trans>
          </Link>
        )}
    </Links>
  </Info>
)

const Component = compose(
  getUserStore,
  getModalStore,
  getCartStore,
  getWishlistStore,
  withState('wishlists', 'setWishLists', []),
  withState('selected', 'setSelected', ({ product: { id } }) => id),
  withHandlers({
    getSelected: ({ product, selected }) => () => {
      const { variants = [] } = product
      return { ...product, ...variants.find(({ id }) => id === selected) }
    }
  }),
  lifecycle({
    async componentDidMount() {
      const {
        product: { id },
        withActions = true,
        setWishLists
      } = this.props
      if (withActions) {
        try {
          const { data: wishlists } = await getWishlists({
            id: id
          })
          setWishLists(wishlists)
        } catch (error) {
          // do something
        }
      }
    }
  })
)(Product)

export default Component

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
