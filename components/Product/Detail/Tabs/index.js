import { Tabs } from 'components/Theme'
import { Description } from './style'

const Component = ({ tabs }) => (
  <Tabs>
    {tabs.map(({ title, description }) => (
      <Description
        key={title}
        title={title.toUpperCase()}
        dangerouslySetInnerHTML={{ __html: description }}
      />
    ))}
  </Tabs>
)

export default Component
