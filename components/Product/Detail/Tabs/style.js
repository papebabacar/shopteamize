import styled from 'styled-components'

export const Title = styled.h2``

export const Description = styled.section`
  padding: 1.625em 0.8125em;
  display: ${props => (props.selected ? 'block' : 'none')};
`
