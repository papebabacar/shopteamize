import { Trans } from '@lingui/react'
import { Breadcrumb, BreadcrumbLink, BreadcrumbItem } from './style'
import { Router } from 'router'
import { getUserStore } from 'containers/App'

const Component = ({
  product,
  category: { code, name: category },
  user: { language }
}) => (
  <Breadcrumb box-flex={1}>
    <BreadcrumbLink href={`${Router.linkPage('index', { language }).as}`}>
      <Trans>Home</Trans>
    </BreadcrumbLink>
    <BreadcrumbLink
      href={`${Router.linkPage('catalog', { language }).as}?category=${code}`}
    >
      {category}
    </BreadcrumbLink>
    <BreadcrumbItem selected>{product}</BreadcrumbItem>
  </Breadcrumb>
)

export default getUserStore(Component)
