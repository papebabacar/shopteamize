import styled from 'styled-components'
import { Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

const {
  settings: { padding, base }
} = theme

export const Breadcrumb = styled(Box)`
  padding: ${padding(base / 2, base)};
  > *:last-child {
    font-weight: 900;
    font-family: 'Arimo';
    &:after {
      display: none;
    }
  }
  ${theme.media.tablet`background: #fff;`};
  ${theme.media.laptop`padding-left: 10vw;`};
`

export const BreadcrumbItem = styled.p`
  padding: 0;
  color: #888;
  display: inline-block;
  &:after {
    content: '›';
    display: inline-block;
    padding: 0 0.40625em;
  }
  font-weight: ${props => (props.selected ? '900' : '100')};
`
export const BreadcrumbLink = styled(BreadcrumbItem.withComponent('a'))``
