import styled from 'styled-components'
import { Box } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Carousel = styled(Box)`
  max-width: 90%;
  margin: 0 auto !important;
  ${theme.media.tablet`max-width:none`};
  .image-gallery-content {
    position: relative;
    display: flex;
    flex-direction: column;
    .image-gallery-slide-wrapper {
      position: static;
      .image-gallery-left-nav,
      .image-gallery-right-nav {
        top: ${props => (props.invert ? '36px' : 'auto')};
        bottom: ${props => (props.invert ? 'auto' : '-12px')};
        padding: 0;
        &:before {
          background: rgba(180, 180, 180, 0.45);
          font-size: 32px;
          font-weight: 100;
          color: #eee;
          padding: 12px 4px;
        }
      }
      .image-gallery-slide img {
        width: 40vw;
        height: 40vw;
        object-fit: cover;
        display: block;
        margin: 0 auto;
        ${theme.media.tablet`
          width: ${props =>
            !props.withActions ? (props.quickView ? '15em' : '100%') : '100%'};
          height: ${props =>
            !props.withActions
              ? props.quickView
                ? '15em'
                : '27em'
              : '29.25em'};`};
      }
    }
    .image-gallery-thumbnails-wrapper {
      order: ${props => (props.invert ? '-1' : '1')};
      .image-gallery-thumbnails {
        padding: 0;
        order: -1;
        .image-gallery-thumbnails-container {
          display: flex;
          .image-gallery-thumbnail {
            vertical-align: middle;
            box-sizing: content-box;
            width: auto;
            border: none;
            &.active {
              > div {
                img {
                  filter: brightness(1);
                  opacity: 1;
                  border-color: transparent;
                  border-bottom: none;
                  border-left: 1px solid #ccc;
                  border-right: 1px solid #ccc;
                }
              }
            }
            > div {
              img {
                height: 4.25em;
                width: 4.25em;
                object-fit: cover;
                filter: blur(1.6);
                opacity: 0.5;
              }
            }
          }
        }
      }
    }
  }
  .image-gallery-content.fullscreen {
    .image-gallery-slide .image-gallery-image {
      width: 100vw;
      height: 90vh;
      padding: 0 8vw;
      background: white;
      img {
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
    }
  }
`
