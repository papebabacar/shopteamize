import ImageGallery from 'react-image-gallery'
import { Carousel } from './style'

const Component = ({
  mainImageUrl,
  images,
  onSlide,
  invert,
  withActions,
  quickView
}) => (
  <Carousel invert={invert} withActions={withActions} quickView={quickView}>
    <ImageGallery
      items={images.reduce(
        (carouselImages, imageUrl) => [
          ...carouselImages,
          { original: imageUrl, thumbnail: imageUrl }
        ],
        [{ original: mainImageUrl, thumbnail: mainImageUrl }]
      )}
      lazyLoad
      showFullscreenButton={withActions}
      showPlayButton={false}
      onSlide={onSlide}
      showThumbnails={images.length > 0}
    />
  </Carousel>
)

export default Component
