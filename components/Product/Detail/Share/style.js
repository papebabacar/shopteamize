import styled from 'styled-components'
import theme from 'components/Theme'

export const Share = styled.div`
  text-align: center;
  ${theme.media.laptop`
    position: absolute;
    top: 40%;
    transform: translateY(-33%);
    display: inline-flex;
    flex-direction: column;
  `};
`

export const Social = styled.div`
  display: inline-block;
  cursor: pointer;
`
