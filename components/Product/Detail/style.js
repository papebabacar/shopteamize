import styled from 'styled-components'
import { Flex } from 'components/Theme/Layout/FlexBox'
import theme from 'components/Theme'

export const Detail = styled(Flex)`
  margin: 0 auto;
  background: linear-gradient(#f2f2f2, #efefef 95%, #d2d2d2);
  box-shadow: inset 5px -38px 8px -39px rgba(140, 140, 140, 0.58);
  ${theme.media.laptop`max-width: 88vw;`};
  ${theme.media.desktop`max-width: 70vw;`};
`
