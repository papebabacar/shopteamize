import { Trans } from '@lingui/react'
import { compose, withState } from 'recompose'
import { Card, HighLight, Title, Label, Price, Icon, Link } from './style'
import { Picture, Button } from 'components/Theme'
import { getUserStore, getCartStore, getWishlistStore } from 'containers/App'
import { Router } from 'router'
import slugify from '@sindresorhus/slugify'

const Component = ({
  product,
  cart: { currencyCode },
  user: { language },
  addCartItem,
  addToWishlist,
  showModal,
  hover,
  setHover,
  adapt
}) => (
  <Card
    data-id={product.id}
    data-size={product.size}
    onMouseEnter={() => {
      setTimeout(() => {
        setHover(true)
      }, 1)
    }}
    onMouseLeave={() => setHover(false)}
  >
    <Link
      onClick={event => {
        event.preventDefault()
        event.target.focus()
        setTimeout(() => {
          setHover(true)
        }, 1)
      }}
      href={`${
        Router.linkPage('product', {
          language,
          id: product.id,
          name: slugify(product.name)
        }).as
      }`}
    >
      <Picture
        src={product.mainImageUrl}
        alt={product.name}
        cover
        adapt={adapt}
      />
      <Title>{product.name}</Title>
      <Price>
        {product.price} {currencyCode}
      </Price>
      <HighLight hover={hover}>
        <Button
          transparent
          onClick={() =>
            product.size
              ? showModal({ ...product, addToWishlist: true }, 'product')
              : addToWishlist(product.id, 1) && showModal(null, 'product')
          }
          padding="0"
        >
          <Icon src="/static/img/wishlist.png" />
          <Label>
            <Trans>Add to my wishlist</Trans>
          </Label>
        </Button>
        <hr />
        <Button
          transparent
          onClick={() => showModal(product, 'product')}
          padding="0"
        >
          <Icon src="/static/img/view.png" />
          <Label>
            <Trans>Preview</Trans>
          </Label>
        </Button>
        <hr />
        <Button
          transparent
          onClick={() =>
            !product.model
              ? addCartItem(product, 'products') && showModal({}, 'product')
              : showModal(product, 'product')
          }
          padding="0"
        >
          <Icon src="/static/img/basket.png" />
          <Label>
            <Trans>Add to cart</Trans>
          </Label>
        </Button>
      </HighLight>
    </Link>
  </Card>
)

export default compose(
  getUserStore,
  getCartStore,
  getWishlistStore,
  withState('hover', 'setHover')
)(Component)
