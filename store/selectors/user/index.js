import { createSelector } from 'reselect'

const getState = state => state

export const getUser = createSelector(getState, ({ user }) => user)
