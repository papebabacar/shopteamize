import { createSelector } from 'reselect'

const getState = state => state

export const getSearch = createSelector(
  getState,
  ({ search, cart: { currencyCode } }) => {
    search.currencyCode = currencyCode
    return search
  }
)
