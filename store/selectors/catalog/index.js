import { createSelector } from 'reselect'

const getState = state => state

export const getCatalog = createSelector(getState, ({ catalog }) => catalog)
