import { createSelector } from 'reselect'

const getState = state => state

export const getOrder = createSelector(
  getState,
  ({ order /*, cart: { currencyCode }*/ }) => {
    const { products, pendingWishlist, wishlists, shoppingIdeas } = order
    order.pendingWishlistCount = !pendingWishlist ? 0 : 1
    order.productsCount = Object.keys(products).length
    order.wishlistsCount = Object.keys(wishlists).length
    order.shoppingIdeasCount = Object.keys(shoppingIdeas).length
    order.itemsCount =
      order.productsCount +
      order.pendingWishlistCount +
      order.wishlistsCount +
      order.shoppingIdeasCount
    order.currencyCode = '€'
    return order
  }
)
