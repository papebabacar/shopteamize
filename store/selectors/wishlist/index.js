import { createSelector } from 'reselect'

const getState = state => state

export const getWishlist = createSelector(
  getState,
  ({ wishlist, cart: { currencyCode } }) => {
    const { items } = wishlist
    wishlist.totalQuantity = 0
    Object.values(items).map(
      ({ quantity }) => (wishlist.totalQuantity += quantity)
    )
    wishlist.currencyCode = currencyCode
    return wishlist
  }
)
