import { createSelector } from 'reselect'

const getState = state => state

export const getCart = createSelector(getState, ({ cart }) => {
  const { products, pendingWishlist, wishlists, shoppingIdeas } = cart
  cart.totalQuantity = 0
  cart.totalItems = 0
  Object.values({
    ...products,
    ...pendingWishlist,
    ...wishlists,
    ...shoppingIdeas
  }).map(({ quantity, price, itemsCount = 1 }) => {
    cart.totalQuantity += quantity * itemsCount
    cart.totalItems += quantity * price
  })
  cart.totalItems = Number(cart.totalItems).toFixed(2) * 1
  cart.total =
    Number(cart.totalItems + cart.totalShopping + cart.totalShipping).toFixed(
      2
    ) * 1
  cart.currencyCode = '€'
  return cart
})
