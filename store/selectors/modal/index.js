import { createSelector } from 'reselect'

const getState = state => state

export const getModal = createSelector(getState, ({ modal }) => modal)
