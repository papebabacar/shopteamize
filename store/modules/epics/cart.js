import * as types from 'store/actions/types/cart'
import { refreshCart } from 'store/actions/cart'
import { Observable } from 'rxjs'
import { ajax } from 'rxjs/observable/dom/ajax'

// eslint-disable-next-line no-undef
const URL = `${API_URL}/shoppingcart/api/muhajir`

const addCartItemEpic = (action$, store) => {
  return action$.ofType(types.CART_ITEM_ADD).switchMap(({ item, itemType }) => {
    const {
      user: { token },
      cart: { id, idSession, quantityToAdd: quantity }
    } = store.getState()
    const data = {
      [`${getTypeItem(itemType)}`]: item,
      cartId: token ? id : idSession,
      quantity
    }
    return ajax
      .post(`${URL}/cart-item-${getType(itemType)}`, data, {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
      .catch(() => Observable.empty())
      .map(({ response: serverCart }) => refreshCart(serverCart))
  })
}

const updateCartItemEpic = (action$, store) => {
  return action$
    .ofType(types.CART_ITEM_UPDATE)
    .switchMap(({ item, quantity, itemType, isHomeDelivery, address }) =>
      Observable.timer(600).switchMap(() => {
        const {
          user: { token },
          cart: { id, idSession }
        } = store.getState()
        const data = {
          [`${getTypeItem(itemType)}`]: item,
          cartId: token ? id : idSession,
          quantity,
          isHomeDelivery,
          address
        }
        return ajax
          .put(`${URL}/cart-item-${getType(itemType)}`, data, {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
          })
          .catch(() => Observable.empty())
          .switchMap(({ response: serverCart }) =>
            updateShippingCost(serverCart, token)
          )
      })
    )
}

const deleteCartItemEpic = (action$, store) => {
  return action$
    .ofType(types.CART_ITEM_REMOVE)
    .switchMap(({ itemId, itemType }) => {
      const {
        user: { token },
        cart: { id, idSession }
      } = store.getState()
      return ajax
        .delete(
          `${URL}/cart-item-${getType(itemType)}/${
            token ? id : idSession
          }/${itemId}`,
          {
            Authorization: `Bearer ${token}`
          }
        )
        .catch(() => Observable.empty())
        .switchMap(({ response: serverCart }) =>
          updateShippingCost(serverCart, token)
        )
    })
}

const updateShippingCost = (serverCart, token) => {
  if (!serverCart || !token) return Observable.of(refreshCart(serverCart))
  return serverCart.carrier && serverCart.shippingAddress
    ? ajax
        .post(
          `${URL}/carts/shipping`,
          {
            billingAddress: serverCart.shippingAddress,
            carrier: serverCart.carrier,
            shippingAddress: serverCart.shippingAddress
          },
          {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
          }
        )
        .catch(() => Observable.empty())
        .map(({ response: serverCart }) => refreshCart(serverCart))
    : Observable.of(refreshCart(serverCart))
}

const getTypeItem = itemType =>
  itemType === 'products' ? 'product' : 'wishlist'
const getType = itemType => (itemType === 'products' ? 'products' : 'wishlists')

export default [addCartItemEpic, updateCartItemEpic, deleteCartItemEpic]
