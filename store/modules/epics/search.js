import * as types from 'store/actions/types/search'
import { getResults, clearResults } from '/store/actions/search'
import { Observable } from 'rxjs'
import { ajax } from 'rxjs/observable/dom/ajax'

const endpoints = {
  // eslint-disable-next-line no-undef
  products: `${API_URL}/catalog/api/_search/products`,
  // eslint-disable-next-line no-undef
  wishlists: `${API_URL}/wishlist/api/muhajir/_search/wishlists`
}

const searchEpic = (action$, store) => {
  return action$
    .ofType(types.SEARCH_ITEMS)
    .map(action => action.params)
    .distinctUntilChanged()
    .switchMap(
      ({
        query = store.getState().search.params.query,
        filter: itemType = store.getState().search.params.filter,
        language = store.getState().user.language
      }) =>
        Observable.timer(300) // debounce
          .takeUntil(action$.ofType(types.SEARCH_CLEAR_ITEMS))
          .mergeMap(() =>
            ajax
              .getJSON(
                `${endpoints[itemType]}?query=${query}&size=5&lang=${language}`
              )
              .retryWhen(attempts =>
                attempts
                  .scan((attemptsCount, error) => {
                    if (attemptsCount >= 3) throw error
                    return attemptsCount + 1
                  }, 1)
                  .delay(2000)
              )
              .catch(() => Observable.of(clearResults()).map(() => []))
              .map(products => getResults(products))
          )
    )
}

export default searchEpic
