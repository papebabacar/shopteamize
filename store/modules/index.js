import { combineEpics } from 'redux-observable'
import cartEpics from './epics/cart'
import searchEpic from './epics/search'

export default combineEpics(...cartEpics, searchEpic)
