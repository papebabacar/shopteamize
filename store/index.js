import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createEpicMiddleware } from 'redux-observable'
import epics from './modules'
import createReducers from './reducers'
import { paths, setCookieOnServer } from './persist'
import reduxCookiesMiddleware from 'redux-cookies-middleware'

const epicMiddleware = createEpicMiddleware(epics)

export const initStore = (state, { req }) => {
  return createStore(
    createReducers(req),
    state,
    composeWithDevTools(
      applyMiddleware(
        epicMiddleware,
        reduxCookiesMiddleware(paths, {
          setCookie: (name, value) => setCookieOnServer(req, name, value)
        })
      )
    )
  )
}
