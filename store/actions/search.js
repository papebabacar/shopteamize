import * as types from './types/search'

export const searchItems = params => ({
  type: types.SEARCH_ITEMS,
  params
})

export const getResults = results => ({
  type: types.SEARCH_GET_ITEMS,
  results
})

export const clearResults = () => ({
  type: types.SEARCH_CLEAR_ITEMS
})

export const setSearchFilter = filter => ({
  type: types.SEARCH_SET_FILTER,
  filter
})

export const expandSearch = visible => ({
  type: types.SEARCH_EXPAND,
  visible
})
