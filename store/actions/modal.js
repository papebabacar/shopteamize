import * as types from './types/modal'

export const showModal = (data = {}, view = 'wishlist') => ({
  type: types.MODAL_SHOW,
  data,
  view
})

export const updateModal = (data = {}) => ({
  type: types.MODAL_UPDATE,
  data
})

export const hideModal = () => ({
  type: types.MODAL_HIDE
})
