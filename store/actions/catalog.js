import * as types from './types/catalog'

export const setCatalog = data => ({
  type: types.CATALOG_SET,
  data
})

export const addToCatalog = items => ({
  type: types.CATALOG_ITEM_ADD,
  items
})

export const emptyCatalog = () => ({
  type: types.CATALOG_ITEM_EMPTY
})
