import * as types from './types/order'

export const setOrder = data => ({
  type: types.ORDER_SET,
  data
})

export const checkOrder = () => ({
  type: types.ORDER_CHECK
})

export const emptyOrder = () => ({
  type: types.ORDER_EMPTY
})
