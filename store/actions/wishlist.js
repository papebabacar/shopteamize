import * as types from './types/wishlist'

export const setWishlist = data => ({
  type: types.WISHLIST_SET,
  data
})

export const addToWishlist = (id, quantity) => ({
  type: types.WISHLIST_PRODUCT_ADD,
  id,
  quantity
})

export const updateWishlistItem = (id, quantity) => ({
  type: types.WISHLIST_PRODUCT_UPDATE,
  id,
  quantity
})

export const removeFromWishlist = id => ({
  type: types.WISHLIST_PRODUCT_REMOVE,
  id
})

export const emptyWishlist = () => ({
  type: types.WISHLIST_EMPTY
})
