import * as types from './types/cart'

export const setCart = data => ({
  type: types.CART_SET,
  data
})

export const refreshCart = serverCart => ({
  type: types.CART_REFRESH,
  serverCart
})

export const emptyCart = () => ({
  type: types.CART_EMPTY
})

export const setDefaultCartQuantity = quantity => ({
  type: types.CART_SET_DEFAULT_QUANTITY,
  quantity
})

export const addCartItem = (item, itemType = 'wishlists') => ({
  type: types.CART_ITEM_ADD,
  item,
  itemType
})

export const updateCartItem = (
  item,
  quantity,
  itemType = 'wishlists',
  isHomeDelivery,
  address
) => ({
  type: types.CART_ITEM_UPDATE,
  item,
  quantity,
  itemType,
  isHomeDelivery,
  address
})

export const removeCartItem = (itemId, itemType = 'wishlists') => ({
  type: types.CART_ITEM_REMOVE,
  itemId,
  itemType
})
