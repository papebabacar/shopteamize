import * as types from './types/user'

export const loginUser = data => ({
  type: types.USER_LOGIN,
  data
})

export const logoutUser = () => ({
  type: types.USER_LOGOUT
})

export const setLanguage = language => ({
  type: types.USER_SET_LANGUAGE,
  language
})

export const setCheckedCookies = checkedCookies => ({
  type: types.USER_SET_COOKIES,
  checkedCookies
})

export const setPathname = pathname => ({
  type: types.USER_SET_PATHNAME,
  pathname
})

export const setPathParams = pathParams => ({
  type: types.USER_SET_PATHPARAMS,
  pathParams
})
