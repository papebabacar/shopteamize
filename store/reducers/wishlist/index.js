import * as types from 'store/actions/types/wishlist'
import { synchronizeState } from 'store/persist'

const createWishlist = req => {
  const initialData = {
    title: '',
    minParticipant: 5,
    items: {},
    mainItemId: null,
    quantityToAdd: 1,
    visible: true,
    submitMode: false,
    endDate: new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
  }
  const initialState = synchronizeState(req, { wishlist: initialData })

  return (wishlist = initialState.wishlist, action) => {
    switch (action.type) {
      case types.WISHLIST_SET:
        return {
          ...wishlist,
          ...action.data
        }
      case types.WISHLIST_PRODUCT_ADD:
        return {
          ...wishlist,
          items: {
            ...wishlist.items,
            [`${action.id}`]: {
              quantity: wishlist.items[`${action.id}`]
                ? wishlist.items[action.id].quantity + action.quantity
                : action.quantity
            }
          },
          mainItemId: wishlist.mainItemId ? wishlist.mainItemId : action.id
        }
      case types.WISHLIST_PRODUCT_UPDATE:
        return {
          ...wishlist,
          items: {
            ...wishlist.items,
            [`${action.id}`]: {
              quantity: action.quantity
            }
          }
        }
      case types.WISHLIST_PRODUCT_REMOVE: {
        // eslint-disable-next-line no-unused-vars
        const { [`${action.id}`]: _, ...items } = wishlist.items
        const restItemsId = Object.keys(items)
        return {
          ...wishlist,
          items,
          mainItemId:
            wishlist.mainItemId === action.id
              ? restItemsId.length > 0
                ? Number(restItemsId[0])
                : null
              : wishlist.mainItemId
        }
      }
      case types.WISHLIST_EMPTY:
        return initialData
      default:
        return wishlist
    }
  }
}

export default createWishlist
