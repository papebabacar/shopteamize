import * as types from 'store/actions/types/cart'
import { synchronizeState } from 'store/persist'

const createCart = req => {
  const initialData = {
    products: {},
    pendingWishlist: {},
    wishlists: {},
    shoppingIdeas: {},
    totalQuantity: 0,
    totalShopping: 0,
    totalShipping: 0,
    total: 0,
    shippingAddress: null,
    quantityToAdd: 1
  }

  const initialState = synchronizeState(req, { cart: initialData })

  return (cart = initialState.cart, action) => {
    switch (action.type) {
      case types.CART_SET:
        return {
          ...cart,
          ...action.data
        }
      case types.CART_SET_DEFAULT_QUANTITY:
        return {
          ...cart,
          quantityToAdd:
            action.quantity >= 1 && action.quantity <= 999 ? action.quantity : 1
        }
      case types.CART_ITEM_ADD:
        return {
          ...cart,
          [action.itemType]: {
            ...cart[action.itemType],
            [action.item.id]: {
              price: action.item.price,
              itemsCount: action.item.itemsCount,
              quantity: cart[action.itemType][action.item.id]
                ? cart[action.itemType][action.item.id].quantity +
                  cart.quantityToAdd
                : cart.quantityToAdd
            }
          }
        }
      case types.CART_ITEM_UPDATE:
        return {
          ...cart,
          [action.itemType]: {
            ...cart[action.itemType],
            [action.item.id]: {
              ...cart[action.itemType][action.item.id],
              price: action.item.price,
              quantity: action.quantity,
              isHomeDelivery: action.isHomeDelivery,
              idAddress: action.address && action.address.id
            }
          }
        }
      case types.CART_ITEM_REMOVE: {
        // eslint-disable-next-line no-unused-vars
        const { [action.itemId]: _, ...remainingItems } = cart[action.itemType]
        return {
          ...cart,
          [action.itemType]: remainingItems
        }
      }
      case types.CART_EMPTY:
        return {
          ...initialData,
          idSession: cart.idSession,
          currentStep: cart.currentStep
        }
      case types.CART_REFRESH: {
        const { idSession } = cart
        const {
          cartId,
          products,
          pendingWishlist,
          customerWishlists,
          shoppingIdeas,
          totalShopping,
          totalShipping,
          shoppingPrice
        } = action.serverCart
        return {
          ...cart,
          id: cartId,
          idSession: idSession || cartId,
          quantityToAdd: 1,
          products: products.reduce(
            (products, { quantity, product: { id, price }, address }) => ({
              ...products,
              [id]: { price, quantity, idAddress: address && address.id }
            }),
            {}
          ),
          pendingWishlist: pendingWishlist
            ? {
                [pendingWishlist.wishlist.id]: {
                  price: pendingWishlist.wishlist.price,
                  itemsCount: pendingWishlist.wishlist.itemsCount,
                  quantity: pendingWishlist.quantity,
                  isHomeDelivery: pendingWishlist.isHomeDelivery,
                  idAddress:
                    pendingWishlist.address && pendingWishlist.address.id
                }
              }
            : {},
          wishlists: transFormWishlists(customerWishlists),
          shoppingIdeas: transFormWishlists(shoppingIdeas),
          totalShopping,
          totalShipping,
          shoppingPrice
        }
      }
      default:
        return cart
    }
  }
}

export default createCart

const transFormWishlists = wishlists =>
  wishlists.reduce(
    (
      wishlists,
      { quantity, wishlist: { id, price, itemsCount }, isHomeDelivery, address }
    ) => ({
      ...wishlists,
      [id]: {
        price,
        isHomeDelivery,
        itemsCount,
        quantity,
        idAddress: address && address.id
      }
    }),
    {}
  )
