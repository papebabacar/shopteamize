import * as types from 'store/actions/types/search'

const initialState = {
  params: { query: '', filter: 'products' },
  processing: false,
  expanded: false,
  results: []
}

const reducer = (search = initialState, action) => {
  switch (action.type) {
    case types.SEARCH_ITEMS:
      return {
        ...search,
        processing: true,
        params: { ...search.params, ...action.params }
      }
    case types.SEARCH_GET_ITEMS:
      return {
        ...search,
        processing: false,
        results: action.results
      }
    case types.SEARCH_SET_FILTER:
      return {
        ...search,
        params: { ...search.params, filter: action.filter }
      }
    case types.SEARCH_CLEAR_ITEMS:
      return {
        ...initialState,
        expanded: search.expanded,
        params: {
          ...initialState.params,
          filter: search.params.filter
        }
      }
    case types.SEARCH_EXPAND:
      return {
        ...search,
        expanded: action.visible
      }
    default:
      return search
  }
}

export default reducer
