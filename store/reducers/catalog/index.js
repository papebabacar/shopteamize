import * as types from 'store/actions/types/catalog'

const initialState = {
  data: [],
  hasMore: false,
  isLoading: false,
  totalCount: 0
}

const reducer = (catalog = initialState, action) => {
  switch (action.type) {
    case types.CATALOG_SET:
      return {
        ...catalog,
        ...action.data
      }
    case types.CATALOG_ITEM_ADD:
      return {
        ...catalog,
        data: [...catalog.data, ...action.items],
        hasMore: catalog.data.length + action.items.length < catalog.totalCount,
        isLoading: false
      }

    case types.CATALOG_ITEM_EMPTY:
      return initialState
    default:
      return catalog
  }
}

export default reducer
