import { combineReducers } from 'redux'
import createUser from './user'
import createCart from './cart'
import createOrder from './order'
import createWishlist from './wishlist/'
import modal from './modal'
import catalog from './catalog'
import search from './search'

const createReducers = req =>
  combineReducers({
    catalog,
    modal,
    user: createUser(req),
    cart: createCart(req),
    order: createOrder(req),
    search,
    wishlist: createWishlist(req)
  })

export default createReducers
