import * as types from 'store/actions/types/modal'

const initialState = {
  data: null,
  view: 'wishlist',
  visible: false
}

const reducer = (modal = initialState, action) => {
  switch (action.type) {
    case types.MODAL_SHOW:
      return {
        ...modal,
        data: action.data,
        view: action.view,
        visible: true
      }
    case types.MODAL_UPDATE:
      return {
        ...modal,
        data: { ...modal.data, ...action.data }
      }
    case types.MODAL_HIDE:
      return initialState
    default:
      return modal
  }
}

export default reducer
