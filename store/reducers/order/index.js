import * as types from 'store/actions/types/order'
import { synchronizeState } from 'store/persist'

const createOrder = req => {
  const initialData = {
    products: {},
    pendingWishlist: null,
    wishlists: {},
    shoppingIdeas: {},
    totalQuantity: 0,
    totalShopping: 0,
    totalShipping: 0,
    total: 0,
    shippingAddress: null,
    carrier: null
  }

  const initialState = synchronizeState(req, { order: initialData })

  return (order = initialState.order, action) => {
    switch (action.type) {
      case types.ORDER_SET: {
        const {
          products,
          pendingWishlist,
          customerWishlists,
          shoppingIdeas,
          totalItems,
          totalShopping,
          totalShipping,
          total,
          paymentID,
          payment,
          shippingAddress
        } = action.data
        return {
          ...order,
          products: products.reduce(
            (
              products,
              {
                quantity,
                product: { id, name, price, mainImageUrl, color, size },
                address
              }
            ) => ({
              ...products,
              [id]: {
                name,
                price,
                color,
                size,
                quantity,
                address: address
                  ? {
                      address1: `${address.address1}, ${address.city}, ${
                        address.country
                      }`
                    }
                  : {
                      address1: `${shippingAddress.address1}, ${
                        shippingAddress.city
                      }, ${shippingAddress.country}`
                    },
                img: mainImageUrl
              }
            }),
            {}
          ),
          pendingWishlist: pendingWishlist && {
            id: pendingWishlist.wishlist.id,
            name: pendingWishlist.wishlist.labels[0].title,
            price: pendingWishlist.wishlist.price,
            quantity: pendingWishlist.quantity * pendingWishlist.itemsCount,
            address: pendingWishlist.address
              ? {
                  address1: `${pendingWishlist.address.address1}, ${
                    pendingWishlist.address.city
                  }, ${pendingWishlist.address.country}`
                }
              : {
                  address1: `${shippingAddress.address1}, ${
                    shippingAddress.city
                  }, ${shippingAddress.country}`
                },
            img: pendingWishlist.wishlist.mainImageUrl
          },
          wishlists: transFormWishlists(customerWishlists, shippingAddress),
          shoppingIdeas: transFormWishlists(shoppingIdeas, shippingAddress),
          paymentID,
          payment,
          totalItems,
          totalShopping,
          totalShipping,
          total
        }
      }
      case types.ORDER_CHECK:
        return { ...order, checked: true }
      case types.ORDER_EMPTY:
        return { ...initialData }
      default:
        return order
    }
  }
}

export default createOrder

const transFormWishlists = (wishlists, shippingAddress) =>
  wishlists.reduce(
    (
      wishlists,
      {
        quantity,
        wishlist: {
          id,
          labels,
          price,
          itemsCount,
          mainImageUrl,
          addressInitiator
        },
        isHomeDelivery,
        address
      }
    ) => ({
      ...wishlists,
      [id]: {
        name: labels[0].title,
        price,
        quantity: quantity,
        itemsCount: itemsCount,
        address: !isHomeDelivery
          ? {
              address1: `${addressInitiator.address1}, ${
                addressInitiator.city
              }, ${addressInitiator.country}`
            }
          : address
            ? {
                address1: `${address.address1}, ${address.city}, ${
                  address.country
                }`
              }
            : {
                address1: `${shippingAddress.address1}, ${
                  shippingAddress.city
                }, ${shippingAddress.country}`
              },
        img: mainImageUrl
      }
    }),
    {}
  )
