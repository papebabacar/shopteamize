import * as types from 'store/actions/types/user'
import { synchronizeState } from 'store/persist'

const createUser = req => {
  const initialData = {}
  const initialState = synchronizeState(req, { user: initialData })
  return (user = initialState.user, action) => {
    switch (action.type) {
      case types.USER_LOGIN:
        return {
          ...user,
          ...action.data,
          isLogged: true
        }
      case types.USER_LOGOUT:
        return {
          ...initialData,
          pathname: user.pathname,
          language: user.language,
          checkedCookies: user.checkedCookies
        }
      case types.USER_SET_LANGUAGE:
        return {
          ...user,
          language: action.language
        }
      case types.USER_SET_COOKIES:
        return {
          ...user,
          checkedCookies: action.checkedCookies
        }
      case types.USER_SET_PATHNAME:
        return {
          ...user,
          pathname: action.pathname,
          pathParams: null
        }
      case types.USER_SET_PATHPARAMS:
        return {
          ...user,
          pathParams: action.pathParams
        }
      default:
        return user
    }
  }
}

export default createUser
