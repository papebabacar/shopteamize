import Cookies from 'universal-cookie'
import { getStateFromCookies } from 'redux-cookies-middleware'

export const paths = {
  user: { name: 'user' },
  cart: { name: 'cart' },
  order: { name: 'order' },
  wishlist: { name: 'wishlist' }
}

// reads a cookie from the express request object.
const getCookieOnServer = (req, name) => {
  const cookies = req ? new Cookies(req.headers.cookie) : new Cookies()
  return JSON.stringify(cookies.get(name))
}

// sets cookie using the express response object.
export const setCookieOnServer = (req, name, value) => {
  const cookies = req ? new Cookies(req.headers.cookie) : new Cookies()
  cookies.set(name, value, {
    path: '/',
    maxAge: ['user', 'cart'].includes(name) ? 860400000 : null
  })
}

export const synchronizeState = (req, initialState) =>
  getStateFromCookies(initialState, paths, name => getCookieOnServer(req, name))
