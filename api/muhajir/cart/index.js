import 'api'
import { get, post } from 'axios'

const URL = 'shoppingcart/api/muhajir'

export const getCurrentCart = () => {
  return get(`${URL}/carts`)
}

export const getCart = id => {
  return get(`${URL}/carts/${id}`)
}

export const mergeCart = id => {
  return get(`${URL}/carts/merge/${id}`)
}

export const synchronizeCarts = id => {
  return get(`${URL}/carts/logout/${id}`)
}

export const setShipping = params => {
  return post(`${URL}/carts/shipping`, params, {
    'Content-Type': 'application/json'
  })
}

export const checkout = params => {
  return post(`${URL}/carts/payment`, params, {
    'Content-Type': 'application/json'
  })
}
