import 'api'
import { get } from 'axios'
import encode from 'query-string-encode'

const URL = 'catalog/api'

export const getProductCategories = params => {
  return get(`${URL}/categories?${encode(params)}`)
}

export const getProducts = params => {
  return get(`${URL}/products?${encode(params)}`)
}

export const getNewProducts = params => {
  return get(`${URL}/products/news?${encode(params)}`)
}

export const getProduct = (id, lang) => {
  return get(`${URL}/products/${id}?lang=${lang}`)
}

export const getAttributes = params => {
  return get(`${URL}/attributes?${encode(params)}`)
}

export const searchProducts = params => {
  return get(`${URL}/_search/products?${encode(params)}`)
}
