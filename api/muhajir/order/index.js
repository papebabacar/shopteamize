import 'api'
import { get } from 'axios'
import encode from 'query-string-encode'

const URL = 'order/api/muhajir'

export const getUserOrders = (userId, params) => {
  return get(`${URL}/users/${userId}/orders?${encode(params)}`)
}

export const getOrderByUser = (userId, orderId) => {
  return get(`${URL}/users/${userId}/orders/${orderId}`)
}
