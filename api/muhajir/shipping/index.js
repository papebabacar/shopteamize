import 'api'
import { get } from 'axios'
import encode from 'query-string-encode'

const URL = 'shipping/api'

export const getCarriers = params => get(`${URL}/carriers?${encode(params)}`)

export const getCarrier = id => get(`${URL}/carriers/${id}`)

export const getCountries = params => get(`${URL}/countries?${encode(params)}`)
