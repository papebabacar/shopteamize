import 'api'
import { post } from 'axios'
import md5 from 'md5'

const URL = 'https://api.mondialrelay.com/Web_Services.asmx'
const ENSEIGNE = 'BDTEST13'
const PRIVATEKEY = 'PrivateK'

export const getAdressePointRelais = (postCode, country = 'FR') =>
  post(
    `${URL}`,
    `
			<soap:Envelope
			xmlns:soap="http://www.w3.org/2003/05/soap-envelope"
			xmlns:web="http://www.mondialrelay.fr/webservice/">
			<soap:Header/>
			<soap:Body>
				<web:WSI2_RecherchePointRelais>
					<web:Enseigne>${ENSEIGNE}</web:Enseigne>
					<web:Pays>${country}</web:Pays>
					<web:CP>${postCode}</web:CP>
					<web:Security>${md5(
            ENSEIGNE + country + postCode + PRIVATEKEY
          ).toUpperCase()}</web:Security>
				</web:WSI2_RecherchePointRelais>
			</soap:Body>
			</soap:Envelope>
		`,
    {
      mode: 'cors',
      headers: {
        'Content-Type': 'application/soap+xml; charset=utf-8'
      }
    }
  )
