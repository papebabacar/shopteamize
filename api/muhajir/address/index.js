import 'api'
import { get, post, put, delete as remove } from 'axios'

const URL = 'api'

export const getAddressesByUser = id => {
  return get(`${URL}/addresses/${id}`)
}

export const getUserAddresses = () => {
  return get(`${URL}/user-addresses`)
}

export const createAddress = params => {
  return post(`${URL}/addresses`, params)
}

export const updateAddress = params => {
  return put(`${URL}/addresses`, params)
}

export const removeAddress = id => {
  return remove(`${URL}/addresses/${id}`)
}
