import 'api'
import { get } from 'axios'

const URL = 'catalog/api/cms'

export const getSlides = lang => {
  return get(`${URL}/${lang}/sliders`)
}

export const getTerms = lang => {
  return get(`${URL}/${lang}/product-pages`)
}

export const getPagesCms = (lang, page) => {
  return get(`${URL}/${lang}/pages/${page}`)
}
