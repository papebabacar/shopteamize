import 'api'
import { get, post } from 'axios'

const URL = '/quote/api/quotes'

export const createQuote = params => {
  return post(URL, params)
}

export const getCarriers = () => {
  return get(`${URL}/carriers`)
}

export const getNatures = () => {
  return get(`${URL}/product-natures`)
}
