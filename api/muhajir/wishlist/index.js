import 'api'
import { get, post, put, delete as remove } from 'axios'
import encode from 'query-string-encode'

const URL = 'wishlist/api/muhajir'

export const getWishlistCategories = () => {
  return get(`${URL}/wishlists/categories`)
}

export const getWishlists = params => {
  return get(`${URL}/wishlists?${encode(params)}`)
}

export const getWishlist = (id, lang) => {
  return get(`${URL}/wishlists/${id}?lang=${lang}`)
}

export const updateWishlist = params => {
  return put(`${URL}/wishlists`, params)
}

export const createWishlist = params => {
  return post(`${URL}/wishlists`, params)
}

export const getUserWishlists = (idUser, params) => {
  return get(`${URL}/users/${idUser}/wishlists?${encode(params)}`)
}

export const getUserWishlist = (idUser, idWishlist, params) => {
  return get(`${URL}/users/${idUser}/wishlists/${idWishlist}?${encode(params)}`)
}

export const getUserPendingWishlist = (idUser, params) => {
  return get(`${URL}/users/${idUser}/wishlists/pending?${encode(params)}`)
}

export const deleteUserWishlist = (idUser, idWishlist) => {
  return remove(`${URL}/users/${idUser}/wishlists/${idWishlist}`)
}

export const searchWishlists = params => {
  return get(`${URL}/_search/wishlists?${encode(params)}`)
}
