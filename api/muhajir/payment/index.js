import 'api'
import { post } from 'axios'
import encode from 'query-string-encode'

const URL = 'shoppingcart/api/payment'

export const preparePayment = () => {
  return post(`${URL}/paypal/create`)
}

export const processPayment = params => {
  return post(`${URL}/paypal/execute?${encode(params)}`)
}
