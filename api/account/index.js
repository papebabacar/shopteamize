import 'api'
import { get, post, put } from 'axios'
import encode from 'query-string-encode'

const URL = 'api'

export const register = params => {
  return post(`${URL}/register`, params)
}

export const authenticate = params => {
  return post(`${URL}/authenticate`, params)
}

export const activate = params => {
  return get(`${URL}/activate?${encode(params)}`)
}

export const reinit = mail => {
  return post(`${URL}/account/reset-password/init`, mail, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export const reset = params => {
  return post(`${URL}/account/reset-password/finish`, params)
}

export const getUser = token => {
  return get(`${URL}/account`, {
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
}

export const updateUser = user => {
  return put(`${URL}/account`, user, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export const updatePassword = params => {
  return post(`${URL}/account/change-password`, params)
}

export const getUsers = params => {
  return get(`${URL}/users?${encode(params)}`)
}
