import axios from 'axios'

// eslint-disable-next-line no-undef
axios.defaults.baseURL = API_URL

// Automatically extract token from cookies
// Add authorization headers to client side requests
// Only browser client side requests

axios.interceptors.request.use(async config => {
  const Cookies = await import('universal-cookie')
  const cookies = new Cookies()
  const user = cookies.get('user')
  const token = user && user.token
  token && (config.headers.Authorization = `Bearer ${token}`)
  return config
})
