module.exports = {
  languageData: {
    plurals: function(n, ord) {
      if (ord) return n == 1 ? 'one' : 'other'
      return n >= 0 && n < 2 ? 'one' : 'other'
    }
  },
  messages: {
    'your email address': 'votre adresse email',
    MR: 'M.',
    MRS: 'Mme',
    'Edit your address': 'Modifiez votre adresse',
    'Create a new address': 'Cr\xE9ez une nouvelle adresse',
    Alias: 'Alias',
    Gender: 'Civilit\xE9',
    Name: 'Nom',
    Firstname: 'Pr\xE9nom',
    Country: 'Pays',
    City: 'Ville',
    'Postal code': 'Code postal',
    'The postal code should contain only numeric values':
      'Le code postal est un champ num\xE9rique',
    Telephone: 'T\xE9l\xE9phone',
    'The phone number should be at least 7 characters':
      'Votre num\xE9ro de t\xE9l\xE9phone doit comporter au moins 7 caract\xE8res',
    Address: 'Adresse',
    'The address should be at least 5 characters':
      'Veuillez saisir cinq caract\xE8res au moins',
    Cancel: 'Annuler',
    Submit: 'Valider',
    'A technical error has occurred. Please reload the page':
      'Une erreur technique est survenue. Veuillez recharger la page.',
    'My addresses': "Mon carnet d'adresse",
    'Add your new address': 'Ajoutez une nouvelle adresse',
    'Create your new address': 'Cr\xE9ez votre premi\xE8re adresse',
    'Loading...': 'Chargement...',
    'You have no addresses, please add your first address':
      "Vous n'avez pas d'adresses, veuillez ajouter votre premi\xE8re adresse",
    'First and last names': 'Nom et pr\xE9noms(s)',
    'Zip code': 'Code postal',
    Delete: 'Suppression',
    Edit: 'Modifier',
    'My Account': 'Mon Compte',
    'My wishlists': 'Mes envies shopping',
    'My orders': 'Mes commandes',
    'Log out': 'D\xE9connexion',
    'My account': 'Mon compte',
    'My personal info': 'Mes donn\xE9es personnelles',
    'Last name': 'Pr\xE9nom',
    'First name': 'Nom',
    'My account information': 'Informations du compte',
    'Actual email': 'Email actuel',
    Email: 'Email',
    'Please enter a valid email address': 'Veuillez renseigner un email valide',
    'New email address': 'Nouveau Email',
    'The two passwords must be equal':
      'Les deux mots de passe doivent \xEAtre identiques',
    'Creation date :': 'Date de cr\xE9ation',
    'Status :': 'Statut',
    'Payment / Tracking Number :': 'Paiement / Num\xE9ro de suivi',
    '/ Ref.': '/ Ref.',
    'Carrier / Tracking number:': 'Livraison / Num\xE9ro de suivi',
    'Subtotal :': 'Sous-total :',
    'Shipping fees :': 'Frais de livraison :',
    'Shopping fees :': 'Frais de shopping :',
    'Total paid :': 'Total commande :',
    'Loading your articles': 'Chargement de vos articles',
    'You have no articles': "Vous n'avez aucun article",
    Article: 'Article',
    Quantity: 'Quantit\xE9',
    'Unit price': 'Prix unitaire',
    'Total price': 'Prix total',
    'Track my order - Ref. {edit}': function(a) {
      return ['Suivre ma commande - Ref. ', a('edit')]
    },
    'Loading your orders': 'Chargement de vos commandes',
    'You have no orders': "Vous n'avez aucune commande",
    Page: 'Page',
    of: 'sur',
    rows: 'lignes',
    Ref: 'Ref',
    'Created date': 'Date de cr\xE9ation',
    Status: 'Etat',
    PAYMENT_SUCCESS: 'Pay\xE9',
    PAYMENT_IN_PROGRESS: 'En attente',
    Payment: 'Paiement',
    Shipping: 'Livraison',
    Details: 'D\xE9tails',
    'Start date :': 'Date de d\xE9but',
    'End date :': 'Date de fin',
    Pending: 'En attente',
    Published: 'Publi\xE9e',
    Closed: 'Ferm\xE9e',
    'Total price :': 'Prix total',
    Order: 'Commande',
    'See detail page': 'Voir la page d\xE9tail',
    'Loading your products': 'Chargement de vos produits',
    'You have no products': "Vous n'avez aucun produit",
    Product: 'Produit',
    'Track my wishlist': 'Suivre mon envie shopping pas \xE0 pas',
    'Loading your wishlists': "Chargement de vos listes d'envie",
    'You have no wishlists': "Vous n'avez aucune liste d'envie",
    Title: 'Titre',
    'Start date': 'Date de d\xE9but',
    'End date': 'Date de fin',
    'Place the order': 'Commander',
    'Your account have been successfully activated.':
      'Votre compte est activ\xE9 avec succ\xE9s.',
    'Your activation link has expired.': "Le lien d'activation a expir\xE9.",
    'You will be redirected in {timer} seconds.': function(a) {
      return [
        'Vous allez \xEAtre automatiquement redirig\xE9 au bout de ',
        a('timer'),
        ' secondes.'
      ]
    },
    Back: 'Retour',
    Home: 'Accueil',
    'Your search : {query}': function(a) {
      return ['Votre recherche : ', a('query')]
    },
    '{totalCount, plural, =0 {No results found} =1 {# product} other {# products}}': function(
      a
    ) {
      return [
        a('totalCount', 'plural', {
          0: 'Aucun r\xE9sulat trouv\xE9',
          1: ['#', ' produit'],
          other: ['#', ' produits']
        })
      ]
    },
    Preview: 'Aper\xE7u',
    'Add to cart': 'Ajouter',
    'We are sorry but no results were found for your research.':
      'D\xE9sol\xE9, aucun article disponible pour votre recherche.',
    Color: 'Couleur',
    Size: 'Taille',
    'Sort by': 'Trier par',
    'Ascending prices': 'Prix croissant',
    'Descending prices': 'Prix d\xE9croissant',
    'Wishlists associated': 'Listes associ\xE9es',
    '{totalCount, plural, =0 {No results found} =1 {# wishlist} other {# wislists}}': function(
      a
    ) {
      return [
        a('totalCount', 'plural', {
          0: 'Aucun r\xE9sulat trouv\xE9',
          1: ['#', ' envie shopping'],
          other: ['#', ' envies shopping']
        })
      ]
    },
    Wishlists: "Liste d'envies",
    Deadline: 'Ech\xE9ance',
    Participants: 'Participants',
    Participate: 'Participer',
    VALIDATE: 'VALIDER',
    'CONTINUE MY SHOPPING': 'CONTINUER MON SHOPPING',
    'MY WISHLIST': 'MON ENVIE SHOPPING',
    'DESIRED WISHLISTS': 'ENVIES SHOPPING',
    'SHOPPING IDEAS': 'IDEES SHOPPING',
    'MY SELECTION OF PRODUCTS': 'MA SELECTION DE PRODUITS',
    'Your basket is empty': 'Votre panier est vide',
    'Would you like to confirm the suppression?':
      'Voulez-vous confirmer la suppression?',
    Update: 'Modifier',
    Close: 'Fermer',
    Confirm: 'Confirmer',
    Team: 'Team',
    'Total items': 'Total articles',
    'Shopping fees<0/>shoptimized': 'Frais de shopping<0/>shopteamiz\xE9s',
    'Shopping fees': 'Frais de shopping',
    'Shipping fees': 'Frais de livraison',
    TOTAL: 'TOTAL',
    'Add products without any others additional shopping fees.':
      'Ajoutez des produits sans frais de shopping suppl\xE9mentaires',
    'Shipping in five open days': 'Livraison sous 5 jours ouvr\xE9s',
    'Shopping Cart': 'Panier',
    'Sign In': 'Connexion',
    Confirmation: 'Confirmation',
    'Congratulations you have successfully completed your order.':
      'F\xE9licitations! vous avez termin\xE9 votre commande avec succ\xE8s.',
    'Thank you for the confidence you granted us.':
      'Nous vous remercions pour votre confiance. ',
    'Your order is registered and will be processed by our teams.':
      'Votre commande est enregistr\xE9e et sera trait\xE9e par nos \xE9quipes.',
    'Your wishlists will be delivered after expiry of the validity date.':
      'Vos envies shopping vous seront livr\xE9es apr\xE8s expiration de la date de validit\xE9.',
    'Your order will be processed upon receipt of your payment. A confirmation email has just been sent to your account.':
      'Votre commande sera trait\xE9e d\xE8s r\xE9ception de votre paiement. Un mail de confirmation vous a \xE9t\xE9 envoy\xE9 dans votre bo\xEEte mail.',
    'You can track you order progress by logging into':
      'Vous pouvez suivre votre commande en vous connectant \xE0',
    'your selfcare': 'votre espace personnel',
    'Share your wishlist with your friends.':
      'Partagez cette envie shopping avec vos ami(e)s.',
    'Your items': 'Vos articles',
    'Your order': 'Votre commande',
    'Total Cart': 'Total panier',
    'Total Order': 'Total commande',
    'Shipping method': 'Mode de livraison',
    'Payment method': 'Mode de paiment',
    'Loading your order': 'Chargement de votre commande',
    'Delivery address': 'Adresse de livraison',
    'Sub total': 'Sous total',
    'Choose your shipping address': 'Choisissez votre adresse de livraison',
    'Choose your shipping option': 'Choisissez votre option de livraison',
    'Registred addresses': 'Adresses enregitr\xE9es',
    'No relay point available': 'Aucun point relais disponible',
    'The closest relay point': 'Le point relais le plus proche',
    'Choose another relay point': 'Changez de point relais',
    "By clicking on Command, you declare that you have read our <0>General Terms and Conditions of Sales</0> and agree with them. You will be redirected to Paypal's secured payment site.":
      'En validant le paiement, vous d\xE9clarez avoir pris connaissance des <0>conditions g\xE9n\xE9rales de vente</0> et \xEAtre en accord avec celles-si. Vous allez \xEAtre redirig\xE9 vers le site de paiement s\xE9curis\xE9 de PayPal.',
    'feature unavailable at this time':
      'fonctionnalit\xE9 indisponible pour le moment',
    'By continuing to visit this site, you accept the use of cookies to offer you services and offers tailored to your interests. <0>See more.</0>':
      'En poursuivant votre navigation sur ce site, vous acceptez l\u2019utilisation de cookies afin de vous proposer des services et des offres adapt\xE9s \xE0 vos centres d\u2019int\xE9r\xEAt. <0>En savoir plus.</0>',
    'Ok I agree': "Ok j'accepte",
    "Sorry...the page you are trying to access doesn't exist.":
      "D\xE9sol\xE9...la page que vous souhaitez consulter n'est malheureusement pas disponible.",
    'Sorry...due to a technical problem, the page you want to view can not be displayed.':
      "D\xE9sol\xE9...suite \xE0 un incident technique, la page que vous souhaitez consulter ne peut s'afficher.",
    'You can go back to our <0>homepage</0> or begin a search':
      "Vous pouvez retourner \xE0 l'<0>accueil</0> ou faire une recherche",
    Delivery: 'Livraison',
    FAQ: 'FAQ',
    CGV: 'CGV',
    SAV: 'SAV',
    'Legal notice': 'Mentions l\xE9gales',
    'Privacy notice': 'Cookies',
    'All rights reserved': 'Tous droits r\xE9serv\xE9s',
    Products: 'Produits',
    'Packs Ramadan': 'Coffrets Ramadan',
    'Create my wishlist': 'Je cr\xE9e mon envie',
    'Log in': 'Connexion',
    'Sign up': 'Cr\xE9er mon compte',
    'CREATE A<0/>WISHLIST': 'JE CREE<0/>UNE ENVIE SHOPPING',
    'A wishlist is a group order !<0/>Create your own wishlist, invite people to join your order and benefit from an optimized final cost.<1/><2/>In one word "Shopteamize" !':
      "Une envie shopping est une commande group\xE9e !<0/>Cr\xE9er votre propre envie, invitez vos ami(e)s \xE0 participer et b\xE9neficiez d'un co\xFBt final optimis\xE9.<1/><2/>En un mot Shopteamize !",
    'JOIN A<0/>WISHLIST': 'JE PARTICIPE A<0/>UNE ENVIE SHOPPING',
    'A wishlist is a group order !<0/>Participate at a whislist, add products to your shopping cart and get a very optimized cost.<1/><2/>Buy "Shopteamize"':
      'Une envie shopping est une commande group\xE9e !<0/>Participez \xE0 une envie shopping, rajoutez des produits \xE0 votre panier et b\xE9neficiez de co\xFBts \xE0 moindre frais<1/><2/>Achetez shopteamiz\xE9 !',
    'How to Shopteamize, check our tutorial.':
      'Comment shopteamizer vos achats ?',
    'Shopteamize<0/>by grouping your purchases':
      'Shopteamize<0/>en groupant tes achats provenant de Duba\xEF',
    'Prepare Eid el fitr !<0/> Enjoy the latest items of <1/>Shopteamize !':
      'Pr\xE9parer Eid el fitr !<0/>Profitez des nouveaut\xE9s<1/>Shopteamize',
    'SEE MORE': 'EN SAVOIR PLUS',
    'Create or participate<0/>at a<1/>wishlist':
      'Cr\xE9ez ou participez<0/>\xE0 une<1/>envie shopping',
    'Share your wishlist<0/>and invite<1/>your friends':
      'Partagez votre envie<0/>shopping et invitez<1/>vos ami(e)s',
    'Your wishlist<0/>becomes a group<1/>purchase':
      'Votre envie shopping<0/>se transforme en achat<1/>group\xE9',
    'A shopper does<0/>the shopping<1/>for you':
      'Un shopper r\xE9alise<0/>le shopping<1/>pour vous',
    'Receive your products<0/>at home<1/>in a lower cost':
      'Recevez vos produits<0/>chez vous<1/>\xE0 moindre co\xFBt',
    Professional: 'Professionnel',
    'Looking for a product out of our catalog?':
      'Vous d\xE9sirez un produit hors catalogue?',
    Particular: 'Particulier',
    'Wishing to create or join a wishlist?':
      'Vous d\xE9sirez cr\xE9er ou participer \xE0 une envie shopping?',
    'Sign in': 'Connexion',
    'Not registred?': 'Pas inscrit ?',
    'create your account.': 'Cr\xE9er votre compte',
    Password: 'Mot de passe',
    'Forgot your password?': 'Mot de passe oubli\xE9 ?',
    'Your email is unknown or your password incorrect, please check your entry or create an account':
      'Votre email est inconnu ou votre mot de passe incorrect, veuillez v\xE9rifier votre saisie ou cr\xE9er un nouveau compte.',
    'You must activate your account before, an email have been send please check your email':
      "Vous devez activer votre compte d'abord, un mail vient d'\xEAtre envoy\xE9 \xE0 votre adresse email, veuillez v\xE9rifier vos mails.",
    'Secured payment': 'Paiement s\xE9curis\xE9',
    'Delivery at home by DHL': 'Livraison \xE0 domicile par DHL',
    '7 days to change your mind': "7 jours pour changer d'avis",
    'Need help? +971 55 56 03 076': "Besoin d'aide ? +971 55 56 03 076",
    'Garanteed Safe checkout': 'Paiement s\xE9curis\xE9',
    'Add to my wishlist': 'Ajouter \xE0 mon envie',
    COLOR: 'COULEUR',
    SIZE: 'TAILLE',
    QUANTITY: 'QUANTITE',
    'ADD TO MY WISHLIST': 'AJOUTER A MON ENVIE',
    'ADD TO CART': 'AJOUTER AU PANIER',
    'ASSOCIATED WHISHLIST': 'ENVIES SHOPPING ASSOCIEES',
    "See product's details": 'Voir les d\xE9tails du produit',
    'Size guide': 'Guide des tailles',
    'See details': 'Voir les d\xE9tails',
    'RAMADAN PRODUCTS': 'PRODUITS RAMADAN',
    'PACKS RAMADAN': 'COFFRETS RAMADAN',
    'Choose your relay point': 'S\xE9lectionnez votre point relais',
    'Add relay point': 'Ajouter le point relais',
    'Your product have been successfully added to your cart':
      'Votre produit a bien \xE9t\xE9 ajout\xE9 au panier',
    'SEE MY CART': 'VOIR MON PANIER',
    'Your product have been successfully added to your wishlist.':
      'votre produit a bien \xE9t\xE9 ajout\xE9 \xE0 votre envie shopping.',
    'SEE MY WISHLIST': 'VOIR MON ENVIE',
    'Your wishlist have been taken into care. You have 48H to order it.':
      'Votre envie shopping a bien \xE9t\xE9 prise en compte. Vous avez 48H pour passer votre commande.',
    'SEE MY WISHLISTS': 'VOIR MES ENVIES SHOPPING',
    'Your wishlist have been successfully added to your cart':
      'Votre envie shopping a bien \xE9t\xE9 ajout\xE9e au panier',
    'Customer information': 'Donn\xE9es personnelles',
    'Quote request confirmation': 'Confirmation de demande de devis',
    'Your request has been send successfully. You can check your email for details.':
      'Votre demande a \xE9t\xE9 envoy\xE9e avec succ\xE9s. Veuillez v\xE9rifier votre email pour les d\xE9tails',
    'SUBMIT ANOTHER QUOTE REQUEST': 'ENVOYER UN AUTRE DEVIS',
    'Quote request form': 'Formulaire de demande de devis',
    'Please fill in these fields below to complete your request.':
      'Veuillez remplir les champs ci-dessous pour compl\xE9ter votre demande.',
    'Submit your quote request': 'Envoyer le devis',
    'Product information': 'Information du produit',
    'Product {0}': function(a) {
      return ['Produit ', a('0')]
    },
    'Category of the product': 'Cat\xE9gorie du produit',
    'Nature of the product': 'Nature du produit',
    'Gender of the product': 'Genre du produit',
    'Product description': 'Description du produit',
    'Photo of the product': 'Photo du produit',
    'Add product': 'Ajouter un produit',
    Budjet: 'Budjet',
    "Would you like to get new informations about shopteamize's catalog?":
      'Voulez-vous recevoir les news de Shopteamize ?',
    'In the catalog': 'Dans le catalogue',
    'Not in the catalog': 'Hors catalog',
    YES: 'OUI',
    NO: 'NON',
    Men: 'Hommes',
    Women: 'Femmes',
    Children: 'Enfants',
    'Shipping information': 'Information de livraison',
    'Shipping Address': 'Adresse de livraison',
    'Shipping mode': 'Mode de livraison',
    "Shopper's mission": 'Mission du Shopper',
    'Would you like to assign this mission to a shopper.':
      'Voulez-vous assigner cette mission \xE0 un shopper ?',
    'This service will cost you': 'Le service vous co\xFBtera',
    'This misson to a shopper': 'La mission \xE0 un shopper',
    'Create your account': 'Cr\xE9er votre compte',
    'Back to login page': 'Retour \xE0 la page de connexion',
    'You will receive a link to reset your password':
      'Votre recevrez un lien de r\xE9initialisation de votre mot de passe',
    'New pasword': 'Nouveau mot de passe',
    'Please set a new password for your account':
      'Veuillez renseigner un nouveau mot de passe pour votre compte',
    'enter your search': 'votre recherche',
    Searching: 'Recherche',
    'No results found': 'Aucun r\xE9sultat trouv\xE9',
    'See all results': 'Voir tous les r\xE9sultats',
    'Drop files here or click to upload': 'Glissez-d\xE9posez vos images ici',
    'Remove All': 'Supprimer',
    upload: 'Charger',
    '(-{0} days)': function(a) {
      return ['(-', a('0'), ' jours)']
    },
    'PARTICIPATE AT THIS WISHLIST': 'PARTICIPER A LA COMMANDE',
    "SEE WISHLIST'S DETAILS": 'VOIR LES DETAILS',
    'Edit your wishlist': 'Modifiez votre envie',
    'Create your new wishlist': 'Cr\xE9ez votre envie shopping',
    'Please enter at least 5 to 35 characters':
      'Ce champ doit \xEAtre compris entre 5 \xE0 35 caract\xE8res',
    'Please give a title to this wishlist':
      'Donnez un titre \xE0 votre envie shopping',
    'Please enter a number at between 3 and 10':
      'Veuillez entrer un nombre entre 3 et 10',
    'Please give the number of participants':
      'Nombre de participants souhait\xE9s',
    'Please select a date between the next seven days':
      'Veuillez choisir une date dans les 7 prochains jours',
    'Please give an end date': 'Date de cl\xF4ture souhait\xE9e',
    'Your wishlist should contain at least two products.':
      'Votre envie shopping doit contenir au moins deux produits',
    'Hide my products': 'Masquer mes articles',
    'See my products': 'Afficher mes articles',
    'Drag and drop your products here': 'Glissez-d\xE9posez vos produits ici',
    'Main product': 'Produit vedette',
    Qty: 'Qt\xE9',
    Validate: 'Valider',
    'Confirm deletion': 'Confirmer la suppression',
    'Follow your wishes': 'Suivez vos envies',
    'Create my list': 'Je cr\xE9e ma liste'
  }
}
