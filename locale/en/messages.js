module.exports = {
  languageData: {
    plurals: function(n, ord) {
      var s = String(n).split('.'),
        v0 = !s[1],
        t0 = Number(s[0]) == n,
        n10 = t0 && s[0].slice(-1),
        n100 = t0 && s[0].slice(-2)
      if (ord)
        return n10 == 1 && n100 != 11
          ? 'one'
          : n10 == 2 && n100 != 12
            ? 'two'
            : n10 == 3 && n100 != 13
              ? 'few'
              : 'other'
      return n == 1 && v0 ? 'one' : 'other'
    }
  },
  messages: {
    'your email address': 'your email address',
    MR: 'MR',
    MRS: 'MRS',
    'Edit your address': 'Edit your address',
    'Create a new address': 'Create a new address',
    Alias: 'Alias',
    Gender: 'Gender',
    Name: 'Name',
    Firstname: 'Firstname',
    Country: 'Country',
    City: 'City',
    'Postal code': 'Postal code',
    'The postal code should contain only numeric values':
      'The postal code should contain only numeric values',
    Telephone: 'Telephone',
    'The phone number should be at least 7 characters':
      'The phone number should be at least 7 characters',
    Address: 'Address',
    'The address should be at least 5 characters':
      'The address should be at least 5 characters',
    Cancel: 'Cancel',
    Submit: 'Submit',
    'A technical error has occurred. Please reload the page':
      'A technical error has occurred. Please reload the page',
    'My addresses': 'My addresses',
    'Add your new address': 'Add your new address',
    'Create your new address': 'Create your new address',
    'Loading...': 'Loading...',
    'You have no addresses, please add your first address':
      'You have no addresses, please add your first address',
    'First and last names': 'First and last names',
    'Zip code': 'Zip code',
    Delete: 'Delete',
    Edit: 'Edit',
    'My Account': 'My Account',
    'My wishlists': 'My wishlists',
    'My orders': 'My orders',
    'Log out': 'Log out',
    'My account': 'My account',
    'My personal info': 'My personal info',
    'Last name': 'Last name',
    'First name': 'First name',
    'My account information': 'My account information',
    'Actual email': 'Actual email',
    Email: 'Email',
    'Please enter a valid email address': 'Please enter a valid email address',
    'New email address': 'New email address',
    'The two passwords must be equal': 'The two passwords must be equal',
    'Creation date :': 'Creation date :',
    'Status :': 'Status :',
    'Payment / Tracking Number :': 'Payment / Tracking Number :',
    '/ Ref.': '/ Ref.',
    'Carrier / Tracking number:': 'Carrier / Tracking number:',
    'Subtotal :': 'Subtotal :',
    'Shipping fees :': 'Shipping fees :',
    'Shopping fees :': 'Shopping fees :',
    'Total paid :': 'Total paid :',
    'Loading your articles': 'Loading your articles',
    'You have no articles': 'You have no articles',
    Article: 'Article',
    Quantity: 'Quantity',
    'Unit price': 'Unit price',
    'Total price': 'Total price',
    'Track my order - Ref. {edit}': function(a) {
      return ['Track my order - Ref. ', a('edit')]
    },
    'Loading your orders': 'Loading your orders',
    'You have no orders': 'You have no orders',
    Page: 'Page',
    of: 'of',
    rows: 'rows',
    Ref: 'Ref',
    'Created date': 'Created date',
    Status: 'Status',
    PAYMENT_SUCCESS: 'Paid',
    PAYMENT_IN_PROGRESS: 'PAYMENT_IN_PROGRESS',
    Payment: 'Payment',
    Shipping: 'Shipping',
    Details: 'Details',
    'Start date :': 'Start date :',
    'End date :': 'End date :',
    Pending: 'Pending',
    Published: 'Published',
    Closed: 'Closed',
    'Total price :': 'Total price :',
    Order: 'Order',
    'See detail page': 'See detail page',
    'Loading your products': 'Loading your products',
    'You have no products': 'You have no products',
    Product: 'Product',
    'Track my wishlist': 'Track my wishlist',
    'Loading your wishlists': 'Loading your wishlists',
    'You have no wishlists': 'You have no wishlists',
    Title: 'Title',
    'Start date': 'Start date',
    'End date': 'End date',
    'Place the order': 'Place the order',
    'Your account have been successfully activated.':
      'Your account have been successfully activated.',
    'Your activation link has expired.': 'Your activation link has expired.',
    'You will be redirected in {timer} seconds.': function(a) {
      return ['You will be redirected in ', a('timer'), ' seconds.']
    },
    Back: 'Back',
    Home: 'Home',
    'Your search : {query}': function(a) {
      return ['Your search : ', a('query')]
    },
    '{totalCount, plural, =0 {No results found} =1 {# product} other {# products}}': function(
      a
    ) {
      return [
        a('totalCount', 'plural', {
          0: 'No results found',
          1: ['#', ' product'],
          other: ['#', ' products']
        })
      ]
    },
    Preview: 'Preview',
    'Add to cart': 'Add to cart',
    'We are sorry but no results were found for your research.':
      'We are sorry but no results were found for your research.',
    Color: 'Color',
    Size: 'Size',
    'Sort by': 'Sort by',
    'Ascending prices': 'Ascending prices',
    'Descending prices': 'Descending prices',
    'Wishlists associated': 'Wishlists associated',
    '{totalCount, plural, =0 {No results found} =1 {# wishlist} other {# wislists}}': function(
      a
    ) {
      return [
        a('totalCount', 'plural', {
          0: 'No results found',
          1: ['#', ' wishlist'],
          other: ['#', ' wislists']
        })
      ]
    },
    Wishlists: 'Wishlists',
    Deadline: 'Deadline',
    Participants: 'Participants',
    Participate: 'Participate',
    VALIDATE: 'VALIDATE',
    'CONTINUE MY SHOPPING': 'CONTINUE MY SHOPPING',
    'MY WISHLIST': 'MY WISHLIST',
    'DESIRED WISHLISTS': 'DESIRED WISHLISTS',
    'SHOPPING IDEAS': 'SHOPPING IDEAS',
    'MY SELECTION OF PRODUCTS': 'MY SELECTION OF PRODUCTS',
    'Your basket is empty': 'Your basket is empty',
    'Would you like to confirm the suppression?':
      'Would you like to confirm the suppression?',
    Update: 'Update',
    Close: 'Close',
    Confirm: 'Confirm',
    Team: 'Team',
    'Total items': 'Total items',
    'Shopping fees<0/>shoptimized': 'Shopping fees<0/>shoptimized',
    'Shopping fees': 'Shopping fees',
    'Shipping fees': 'Shipping fees',
    TOTAL: 'TOTAL',
    'Add products without any others additional shopping fees.':
      'Add products without any others additional shopping fees.',
    'Shipping in five open days': 'Shipping in five open days',
    'Shopping Cart': 'Shopping Cart',
    'Sign In': 'Sign In',
    Confirmation: 'Confirmation',
    'Congratulations you have successfully completed your order.':
      'Congratulations you have successfully completed your order.',
    'Thank you for the confidence you granted us.':
      'Thank you for the confidence you granted us.',
    'Your order is registered and will be processed by our teams.':
      'Your order is registered and will be processed by our teams.',
    'Your wishlists will be delivered after expiry of the validity date.':
      'Your wishlists will be delivered after expiry of the validity date.',
    'Your order will be processed upon receipt of your payment. A confirmation email has just been sent to your account.':
      'Your order will be processed upon receipt of your payment. A confirmation email has just been sent to your account.',
    'You can track you order progress by logging into':
      'You can track you order progress by logging into',
    'your selfcare': 'your selfcare',
    'Share your wishlist with your friends.':
      'Share your wishlist with your friends.',
    'Your items': 'Your items',
    'Your order': 'Your order',
    'Total Cart': 'Total Cart',
    'Total Order': 'Total Order',
    'Shipping method': 'Shipping method',
    'Payment method': 'Payment method',
    'Loading your order': 'Loading your order',
    'Delivery address': 'Delivery address',
    'Sub total': 'Sub total',
    'Choose your shipping address': 'Choose your shipping address',
    'Choose your shipping option': 'Choose your shipping option',
    'Registred addresses': 'Registred addresses',
    'No relay point available': 'No relay point available',
    'The closest relay point': 'The closest relay point',
    'Choose another relay point': 'Choose another relay point',
    "By clicking on Command, you declare that you have read our <0>General Terms and Conditions of Sales</0> and agree with them. You will be redirected to Paypal's secured payment site.":
      "By clicking on Command, you declare that you have read our <0>General Terms and Conditions of Sales</0> and agree with them. You will be redirected to Paypal's secured payment site.",
    'feature unavailable at this time': 'feature unavailable at this time',
    'By continuing to visit this site, you accept the use of cookies to offer you services and offers tailored to your interests. <0>See more.</0>':
      'By continuing to visit this site, you accept the use of cookies to offer you services and offers tailored to your interests. <0>See more.</0>',
    'Ok I agree': 'Ok I agree',
    "Sorry...the page you are trying to access doesn't exist.":
      "Sorry...the page you are trying to access doesn't exist.",
    'Sorry...due to a technical problem, the page you want to view can not be displayed.':
      'Sorry...due to a technical problem, the page you want to view can not be displayed.',
    'You can go back to our <0>homepage</0> or begin a search':
      'You can go back to our <0>homepage</0> or begin a search',
    Delivery: 'Delivery',
    FAQ: 'FAQ',
    CGV: 'CGV',
    SAV: 'SAV',
    'Legal notice': 'Legal notice',
    'Privacy notice': 'Privacy notice',
    'All rights reserved': 'All rights reserved',
    Products: 'Products',
    'Packs Ramadan': 'Packs Ramadan',
    'Create my wishlist': 'Create my wishlist',
    'Log in': 'Log in',
    'Sign up': 'Sign up',
    'CREATE A<0/>WISHLIST': 'CREATE A WISHLIST',
    'A wishlist is a group order !<0/>Create your own wishlist, invite people to join your order and benefit from an optimized final cost.<1/><2/>In one word "Shopteamize" !':
      'A wishlist is a group order !<0/>Create your own wishlist, invite people to join your order and benefit from an optimized final cost.<1/><2/>In one word "Shopteamize" !',
    'JOIN A<0/>WISHLIST': 'JOIN A WISHLIST',
    'A wishlist is a group order !<0/>Participate at a whislist, add products to your shopping cart and get a very optimized cost.<1/><2/>Buy "Shopteamize"':
      'A wishlist is a group order !<0/>Participate at a whislist, add products to your shopping cart and get a very optimized cost.<1/><2/>Buy "Shopteamize"',
    'How to Shopteamize, check our tutorial.':
      'How to Shopteamize, check our tutorial.',
    'Shopteamize<0/>by grouping your purchases':
      'Shopteamize<0/>by grouping your shopping from Duba\xEF',
    'Prepare Eid el fitr !<0/> Enjoy the latest items of <1/>Shopteamize !':
      'Prepare Eid el fitr !<0/> Enjoy the latest items of <1/>Shopteamize !',
    'SEE MORE': 'SEE MORE',
    'Create or participate<0/>at a<1/>wishlist':
      'Create or participate<0/>at a<1/>wishlist',
    'Share your wishlist<0/>and invite<1/>your friends':
      'Share your wishlist<0/>and invite<1/>your friends',
    'Your wishlist<0/>becomes a group<1/>purchase':
      'Your wishlist<0/>becomes a group<1/>shopping',
    'A shopper does<0/>the shopping<1/>for you':
      'A shopper does<0/>the shopping<1/>for you',
    'Receive your products<0/>at home<1/>in a lower cost':
      'Receive your products<0/>at home<1/>in a lower cost',
    Professional: 'Professional',
    'Looking for a product out of our catalog?':
      'Looking for a product out of our catalog?',
    Particular: 'Particular',
    'Wishing to create or join a wishlist?': 'Create or join a wishlist?',
    'Sign in': 'Sign in',
    'Not registred?': 'Not registred?',
    'create your account.': 'create your account.',
    Password: 'Password',
    'Forgot your password?': 'Forgot your password?',
    'Your email is unknown or your password incorrect, please check your entry or create an account':
      'Your email is unknown or your password incorrect, please check your entry or create an account',
    'You must activate your account before, an email have been send please check your email':
      'You must activate your account before, an email have been send please check your email',
    'Secured payment': 'Secured payment',
    'Delivery at home by DHL': 'Delivery at home by DHL',
    '7 days to change your mind': '7 days to change your mind',
    'Need help? +971 55 56 03 076': 'Need help? +971 55 56 03 076',
    'Garanteed Safe checkout': 'Garanteed Safe checkout',
    'Add to my wishlist': 'Add to my wishlist',
    COLOR: 'COLOR',
    SIZE: 'SIZE',
    QUANTITY: 'QUANTITY',
    'ADD TO MY WISHLIST': 'ADD TO MY WISHLIST',
    'ADD TO CART': 'ADD TO CART',
    'ASSOCIATED WHISHLIST': 'ASSOCIATED WHISHLIST',
    "See product's details": "See product's details",
    'Size guide': 'Size guide',
    'See details': 'See details',
    'RAMADAN PRODUCTS': 'RAMADAN PRODUCTS',
    'PACKS RAMADAN': 'PACKS RAMADAN',
    'Choose your relay point': 'Choose your relay point',
    'Add relay point': 'Add relay point',
    'Your product have been successfully added to your cart':
      'Your product have been successfully added to your cart',
    'SEE MY CART': 'SEE MY CART',
    'Your product have been successfully added to your wishlist.':
      'Your product have been successfully added to your wishlist.',
    'SEE MY WISHLIST': 'SEE MY WISHLIST',
    'Your wishlist have been taken into care. You have 48H to order it.':
      'Your wishlist have been taken into care. You have 48H to order it.',
    'SEE MY WISHLISTS': 'SEE MY WISHLISTS',
    'Your wishlist have been successfully added to your cart':
      'Your wishlist have been successfully added to your cart',
    'Customer information': 'Customer information',
    'Quote request confirmation': 'Quote request confirmation',
    'Your request has been send successfully. You can check your email for details.':
      'Your request has been send successfully. You can check your email for details.',
    'SUBMIT ANOTHER QUOTE REQUEST': 'SUBMIT ANOTHER QUOTE REQUEST',
    'Quote request form': 'Quote request form',
    'Please fill in these fields below to complete your request.':
      'Please fill in these fields below to complete your request.',
    'Submit your quote request': 'Submit your quote request',
    'Product information': 'Product information',
    'Product {0}': function(a) {
      return ['Product ', a('0')]
    },
    'Category of the product': 'Category of the product',
    'Nature of the product': 'Nature of the product',
    'Gender of the product': 'Gender of the product',
    'Product description': 'Product description',
    'Photo of the product': 'Photo of the product',
    'Add product': 'Add product',
    Budjet: 'Budjet',
    "Would you like to get new informations about shopteamize's catalog?":
      "Would you like to get new informations about shopteamize's catalog?",
    'In the catalog': 'In the catalog',
    'Not in the catalog': 'Not in the catalog',
    YES: 'YES',
    NO: 'NO',
    Men: 'Men',
    Women: 'Women',
    Children: 'Children',
    'Shipping information': 'Shipping information',
    'Shipping Address': 'Shipping Address',
    'Shipping mode': 'Shipping mode',
    "Shopper's mission": "Shopper's mission",
    'Would you like to assign this mission to a shopper.':
      'Would you like to assign this mission to a shopper.',
    'This service will cost you': 'This service will cost you',
    'This misson to a shopper': 'This misson to a shopper',
    'Create your account': 'Create your account',
    'Back to login page': 'Back to login page',
    'You will receive a link to reset your password':
      'You will receive a link to reset your password',
    'New pasword': 'New pasword',
    'Please set a new password for your account':
      'Please set a new password for your account',
    'enter your search': 'enter your search',
    Searching: 'Searching',
    'No results found': 'No results found',
    'See all results': 'See all results',
    'Drop files here or click to upload': 'Drop files here or click to upload',
    'Remove All': 'Remove All',
    upload: 'upload',
    '(-{0} days)': function(a) {
      return ['(-', a('0'), ' days)']
    },
    'PARTICIPATE AT THIS WISHLIST': 'PARTICIPATE AT THIS WISHLIST',
    "SEE WISHLIST'S DETAILS": "SEE WISHLIST'S DETAILS",
    'Edit your wishlist': 'Edit your wishlist',
    'Create your new wishlist': 'Create your new wishlist',
    'Please enter at least 5 to 35 characters':
      'Please enter at least 5 to 35 characters',
    'Please give a title to this wishlist':
      'Please give a title to this wishlist',
    'Please enter a number at between 3 and 10':
      'Please enter a number at between 3 and 10',
    'Please give the number of participants':
      'Please give the number of participants',
    'Please select a date between the next seven days':
      'Please select a date between the next seven days',
    'Please give an end date': 'Please give an end date',
    'Your wishlist should contain at least two products.':
      'Your wishlist should contain at least two products.',
    'Hide my products': 'Hide my products',
    'See my products': 'See my products',
    'Drag and drop your products here': 'Drag and drop your products here',
    'Main product': 'Main product',
    Qty: 'Qty',
    Validate: 'Validate',
    'Confirm deletion': 'Confirm deletion',
    'Follow your wishes': 'Follow your wishes',
    'Create my list': 'Create my list'
  }
}
