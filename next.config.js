const webpack = require('webpack')
const withCSS = require('@zeit/next-css')
const TargetsPlugin = require('targets-webpack-plugin')

module.exports = withCSS({
  useFileSystemPublicRoutes: false,
  webpack: (config, { dev }) => {
    config.plugins.push(
      new webpack.DefinePlugin({
        APP_URL: JSON.stringify('https://www.shopteamize.com'),
        API_URL: JSON.stringify('https://gateway.shopteamize.com')
      })
    )
    if (!dev) {
      config.plugins.push(
        new TargetsPlugin({
          browsers: [
            'Chrome >= 52',
            'FireFox >= 44',
            'Safari >= 7',
            'Explorer 11',
            'last 4 Edge versions'
          ]
        })
      )
    }
    return config
  }
})
