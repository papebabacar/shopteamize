const express = require('express')
const next = require('next')
const Router = require('./router').Router
// const cache = require('./cache')
const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({
  dev
})
const handle = app.getRequestHandler()
const url = process.env.API_URL || 'not defined'

app.prepare().then(() => {
  const server = express()
  const options = {
    root: __dirname + '/static/'
  }
  server.get('/robots.txt', (req, res) =>
    res.status(200).sendFile('robots.txt', {
      ...options,
      headers: { 'Content-Type': 'text/plain;charset=UTF-8' }
    })
  )
  server.get('/sitemap.xml', (req, res) =>
    res.status(200).sendFile('sitemap.xml', {
      ...options,
      headers: { 'Content-Type': 'text/xml;charset=UTF-8' }
    })
  )
  server.get('/favicon.ico', (req, res) =>
    res.status(200).sendFile('favicon.ico', {
      ...options,
      headers: { 'Content-Type': 'image/x-icon' }
    })
  )
  Router.forEachPattern((page, pattern, defaultParams) =>
    server.get(pattern, (req, res) =>
      app.render(req, res, `/${page}`, {
        ...defaultParams,
        ...req.query,
        ...req.params
      })
    )
  )

  server
    // .use(cache(app))
    .use((request, response) => handle(request, response))
    .listen(port, error => {
      if (error) throw error
      // eslint-disable-next-line
      console.log(`> Ready on port ${port} with api url ${url}`)
    })
})
