import App, { withStore } from 'containers/App'
import Quote from 'components/Quote'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { getCarriers, getNatures } from 'api/muhajir/quote'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  confirmation,
  categories,
  wishlistCategories,
  carriers,
  natures,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr'
        ? 'Shopteamize - Demander votre devis'
        : 'Shopteamize - Request your quote'
    }
    description={
      language === 'fr'
        ? 'Bénéficiez de vente en gros de produits musulmans sur Shopteamize. Nous traiterons votre devis rapidement'
        : 'Enjoy wholesale Muslim products on Shopteamize. We will process your quote quickly'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? (
      <Error statusCode={404} />
    ) : (
      <Quote
        confirmation={confirmation}
        categories={categories}
        carriers={carriers}
        natures={natures}
        language={language}
      />
    )}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  query: { confirmation, language },
  store,
  prefferedLanguage,
  pathname,
  res
}) => {
  const lang = language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] },
    { data: natures = [] },
    { data: carriers = [] }
  ] = await Promise.all(
    [
      getProductCategories({ lang }),
      getWishlistCategories(),
      getNatures(),
      getCarriers()
    ].map((promise = Promise.reject()) =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    categories,
    wishlistCategories,
    natures,
    carriers,
    confirmation,
    error
  }
}

export default withStore(Page)
