import App, { withStore } from 'containers/App'
import Login from 'components/Login'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'
import { Router } from 'router'

const Page = ({
  categories,
  wishlistCategories,
  redirect,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr' ? 'Shopteamize - Connexion' : 'Shopteamize - Login'
    }
    description={
      language === 'fr'
        ? "Sur Shopteamize, vous bénéficiez d'un service adapté aux professionnels comme aux particuliers   - un service accessible et non couteux. N'attendez plus, optimiser votre budget avec Shopteamize."
        : 'On ShopTeamize, you benefit from a service adapted to the professionals as to the private individuals - an accessible and inexpensive service. Do not wait any longer, optimize your budget with Shopteamize.'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? <Error statusCode={404} /> : <Login redirect={redirect} />}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  query,
  store,
  prefferedLanguage,
  pathname,
  res
}) => {
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const { redirect = Router.linkPage('account', { language: lang }).as } = query
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] }
  ] = await Promise.all(
    [getProductCategories({ lang }), getWishlistCategories()].map(
      (promise = Promise.reject()) =>
        promise.catch(() => ({
          data: undefined
        }))
    )
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    redirect,
    categories,
    wishlistCategories,
    error
  }
}

export default withStore(Page)
