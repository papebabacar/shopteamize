import App, { withStore } from 'containers/App'
import {
  getProductCategories,
  getProducts,
  searchProducts,
  getAttributes
} from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import Catalog from 'components/Catalog'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setCatalog } from 'store/actions/catalog'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  category,
  categories,
  wishlistCategories,
  user: { language },
  params: { query },
  error
}) => (
  <App
    language={language}
    title={category.metaTitle}
    description={category.metaDescription}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? (
      <Error statusCode={404} />
    ) : (
      <Catalog
        itemType="products"
        getItems={query ? searchProducts : getProducts}
      />
    )}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  query,
  store,
  prefferedLanguage,
  pathname,
  req,
  res
}) => {
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const params = {
    lang,
    ...query,
    page: query.page || 0,
    size: query.size || 12
  }
  const [
    {
      data = [],
      headers: { ['x-total-count']: totalCount } = { 'x-total-count': 0 }
    },
    { data: categories = [] },
    { data: wishlistCategories = [] },
    { data: attributes = [] }
  ] = await Promise.all(
    [
      params.query ? searchProducts(params) : getProducts(params),
      getProductCategories({ lang }),
      getWishlistCategories(),
      getAttributes({ category: params.category })
    ].map((promise = Promise.reject()) =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  const category = categories.find(
    category => category.code === params.category
  ) || {
    code: null,
    label: 'Catalogue'
  }
  store.dispatch(
    setCatalog({
      data,
      category,
      params,
      totalCount,
      attributes,
      hasMore: data.length < totalCount
    })
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  } else {
    res &&
      res.setHeader(
        'Link',
        `<https://${req.headers.host}/${lang}/catalog>; rel="canonical"`
      )
  }
  return {
    category,
    categories,
    wishlistCategories,
    error
  }
}

export default withStore(Page)
