import App, { withStore } from 'containers/App'
import { getProduct } from 'api/muhajir/catalog'
import Detail from 'components/Product/Detail'
import Popular from 'components/Push/WishList/Popular'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { getWishlists } from 'api/muhajir/wishlist'
import { getTerms } from 'api/muhajir/cms'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setDefaultCartQuantity } from 'store/actions/cart'
import { setLanguage, setPathname, setPathParams } from 'store/actions/user'
import Error from 'components/Error'
import slugify from '@sindresorhus/slugify'

const Page = ({
  product,
  wishlists,
  categories,
  wishlistCategories,
  terms,
  user: { language },
  error
}) => (
  <App
    title={product.metaTitle}
    description={product.metaDescription}
    image={product.mainImageUrl}
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? (
      <Error statusCode={404} wishlists={wishlists} />
    ) : (
      <>
        <Detail
          terms={terms}
          product={{
            ...product,
            category: categories.find(
              category => category.code === product.categories.find(() => true)
            ) || { code: null, label: 'Catalogue' }
          }}
        />
        <Popular items={wishlists} />
      </>
    )}
    <Footer />
  </App>
)

Page.getInitialProps = async ({
  query: { id, name, language },
  store,
  prefferedLanguage,
  pathname,
  req,
  res
}) => {
  const lang = language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  store.dispatch(setPathParams({ id, name }))
  const params = { lang, size: 20, enable: true }
  const [
    {
      data: product = {
        images: [],
        attributes: {},
        categories: []
      }
    },
    { data: wishlists = [] },
    { data: categories = [] },
    { data: wishlistCategories = [] },
    { data: terms = [] }
  ] = await Promise.all(
    [
      getProduct(id, lang),
      getWishlists(params),
      getProductCategories({ lang }),
      getWishlistCategories(),
      getTerms(lang)
    ].map(promise =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  store.dispatch(setDefaultCartQuantity(1))
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  } else if (Number(product.id) !== Number(id)) {
    res.statusCode = error = 404
  } else {
    res &&
      res.setHeader(
        'Link',
        `<https://${req.headers.host}/${lang}/product/${product.id}/${slugify(
          product.name
        )}>; rel="canonical"`
      )
    res &&
      res.setHeader(
        'Link',
        `<https://${req.headers.host}/en/product/${product.id}/${slugify(
          product.name
        )}>; rel="alternate"; hreflang="en",<https://${
          req.headers.host
        }/fr/product/${product.id}/${slugify(
          product.name
        )}>; rel="alternate"; hreflang="fr"`
      )
  }
  return {
    wishlists,
    categories,
    wishlistCategories,
    terms,
    product,
    error
  }
}

export default withStore(Page)
