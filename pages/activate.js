import App, { withStore } from 'containers/App'
import { activate } from 'api/account'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import Activation from 'components/Activation'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  success,
  categories,
  wishlistCategories,
  user: { language },
  error
}) => (
  <App title={`Account | Shopteamize`} language={language}>
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? <Error statusCode={404} /> : <Activation success={success} />}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  query: { key, language },
  store,
  prefferedLanguage,
  pathname,
  res
}) => {
  const lang = language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  let success = false
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] }
  ] = await Promise.all(
    [getProductCategories({ lang }), getWishlistCategories()].map(
      (promise = Promise.reject()) =>
        promise.catch(() => ({
          data: undefined
        }))
    )
  )
  try {
    await activate({ key })
    success = true
  } catch (error) {
    success = false
  }
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    success,
    categories,
    wishlistCategories,
    error
  }
}

export default withStore(Page)
