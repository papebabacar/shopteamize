import Document, { Head, Main, NextScript } from 'next/document'
import { ServerStyleSheet } from 'styled-components'

export default class SiteDocument extends Document {
  static async getInitialProps({ renderPage, store }) {
    const language = store && store.getState().user.language
    const sheet = new ServerStyleSheet()
    const page = await renderPage(Page => props =>
      sheet.collectStyles(<Page {...props} />)
    )
    const styleTags = sheet.getStyleElement()
    return { ...page, styleTags, language }
  }
  render() {
    const { language, styleTags } = this.props
    return (
      <html lang={language}>
        <Head>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <link
            rel="stylesheet"
            type="text/css"
            href="/static/css/global.css"
          />
          <link rel="stylesheet" href="/static/css/main.css" />
          {styleTags}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
