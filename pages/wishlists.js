import App, { withStore } from 'containers/App'
import { getWishlists, searchWishlists } from 'api/muhajir/wishlist'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { getProduct } from 'api/muhajir/catalog'
import { setCatalog } from 'store/actions/catalog'
import Catalog from 'components/Catalog'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  categories,
  category: { name = '' },
  wishlistCategories,
  user: { language },
  params: { query },
  error
}) => (
  <App
    title={
      language === 'fr'
        ? `Shopteamize - ${name} | Coffrets cadeaux , wishlist, commande groupée`
        : `Shopteamize - ${name} | Gift boxes, wishlist, group order`
    }
    description={
      language === 'fr'
        ? `${name} Ramadan et Aïd el Fitr un clic avec Shopteamize. Invitez vos amis à participer à des commandes groupées et bénéficiez jusqu’à 70% de réduction chacun sur les frais de livraison. Le shopping à Dubaï comme si vous-y-étiez !`
        : `Take part in shopping ideas or create your own wishlist for Ramadan and Eid el Fitr in one click with Shopteamize. Invite your friends to participate in bulk orders and get up to 70% off each on shipping costs. Shopping in Dubai as if you were there!`
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? (
      <Error statusCode={404} />
    ) : (
      <Catalog
        itemType="wishlists"
        getItems={query ? searchWishlists : getWishlists}
      />
    )}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  query,
  store,
  prefferedLanguage,
  pathname,
  req,
  res
}) => {
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const params = {
    lang,
    ...query,
    page: query.page || 0,
    size: query.size || 12
  }
  const [
    {
      data = [],
      headers: { ['x-total-count']: totalCount } = { 'x-total-count': 0 }
    },
    { data: categories = [] },
    { data: wishlistCategories = [] },
    { data: product }
  ] = await Promise.all(
    [
      params.query ? searchWishlists(params) : getWishlists(params),
      getProductCategories({ lang }),
      getWishlistCategories(),
      params.id ? getProduct(params.id, lang) : undefined
    ].map((promise = Promise.reject()) =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  const category = categories.find(
    category =>
      product
        ? category.code === product.categories.find(() => true)
        : category.code === params.category
  ) || {
    code: null,
    label: ''
  }
  store.dispatch(
    setCatalog({
      data,
      category,
      params,
      product,
      totalCount: Number(totalCount),
      hasMore: data.length < totalCount
    })
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  } else {
    res &&
      res.setHeader(
        'Link',
        `<https://${req.headers.host}/${lang}/wishlists>; rel="canonical"`
      )
  }
  return {
    category,
    categories,
    wishlistCategories,
    error
  }
}

export default withStore(Page)
