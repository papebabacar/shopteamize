import App, { withStore } from 'containers/App'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import Header from 'components/Header'
import Footer from 'components/Footer'
import Error from 'components/Error'
import { getWishlists } from 'api/muhajir/wishlist'
import { setLanguage } from 'store/actions/user'

const Page = ({
  wishlists,
  statusCode,
  categories,
  wishlistCategories,
  user: { language }
}) => (
  <App
    title={
      language === 'fr'
        ? 'Shopteamize - Toutes les idées et envies shopping de Dubaï en un clic'
        : 'Shopteamize - All Dubai shopping ideas and desires in one click'
    }
    description={
      language === 'fr'
        ? 'Shopteamize est un site marchand spécialisé dans l’achat groupé de produits en provenance des pays musulmans ( Dubai, Arabie Saoudite, Maroc…). Shopteamize vous propose des idées shopping ramadan, avec des produits minutieusement sélectionnés pour inspirer vos envies shopping. Shop qui peut !'
        : 'Shopteamize is a merchant site specializing in the group purchase of products from Muslim countries (Dubai, Saudi Arabia, Morocco ...). Shopteamize offers you ramadan shopping ideas, with products carefully selected to inspire your shopping desires. Shop who can!'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    <Error statusCode={statusCode} wishlists={wishlists} />
    <Footer />
  </App>
)

Page.getInitialProps = async ({ store, prefferedLanguage, res, err }) => {
  const { statusCode } = res || err
  const lang = store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  const params = { lang, size: 20 }
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] },
    { data: wishlists = [] }
  ] = await Promise.all(
    [
      getProductCategories({ lang }),
      getWishlistCategories(),
      getWishlists(params)
    ].map((promise = Promise.reject()) =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  return {
    categories,
    wishlistCategories,
    wishlists,
    statusCode
  }
}

export default withStore(Page)
