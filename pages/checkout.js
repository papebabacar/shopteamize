import App, { withStore } from 'containers/App'
import Header from 'components/Header'
import MiniHeader from 'components/MiniHeader'
import Footer from 'components/Footer'
import MiniFooter from 'components/MiniFooter'
import Checkout from 'components/Checkout'
import { setCart } from 'store/actions/cart'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'
import { Router } from 'router'

const Page = ({
  categories,
  wishlistCategories,
  steps,
  currentStep,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr'
        ? 'Shopteamize - Checkout | La plateforme qui optimise vos achats groupés'
        : 'Shopteamize - Checkout | The platform which optimize your group shopping.'
    }
    description={
      language === 'fr'
        ? 'Vous souhaitez vous procurer des produits musulmans tels que les Emirats Arabes unis, le Maroc, l’Arabie Saoudite…Participer aux commandes groupées sur Shopteamize et économiser jusqu’à 70% sur les frais de livraison. Vos colis vous sont livrés en 48h avec DHL.'
        : 'You want to buy Muslim products such as the United Arab Emirates, Morocco, Saudi Arabia ... Participate in bulk orders on Shopteamize and save up to 70% on shipping costs. Your packages are delivered to you in 48h with DHL.'
    }
    language={language}
  >
    {currentStep === 'basket' ? (
      <Header categories={categories} wishlistCategories={wishlistCategories} />
    ) : (
      <MiniHeader />
    )}

    {error ? <Error statusCode={404} /> : <Checkout steps={steps} />}
    {currentStep === 'basket' ? <Footer /> : <MiniFooter />}
  </App>
)

Page.getInitialProps = async ({
  query,
  store,
  prefferedLanguage,
  res,
  pathname
}) => {
  res && res.setHeader('Cache-Control', 'no-cache, no-store')
  const { step } = query
  const {
    cart: { totalItems },
    order: { checked }
  } = store.getState()
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  if (checked && step !== 'basket') {
    res
      ? res.redirect(Router.linkPage('index', { language: lang }).as)
      : (document.location = Router.linkPage('index', { language: lang }).as)
    return
  }
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const steps = ['basket', 'login', 'delivery', 'payment', 'confirmation']
  const currentStep = steps.includes(step) && totalItems ? step : 'basket'
  store.dispatch(
    setCart({
      currentStep
    })
  )
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] }
  ] = await Promise.all(
    [getProductCategories({ lang }), getWishlistCategories()].map(
      (promise = Promise.reject()) =>
        promise.catch(() => ({
          data: undefined
        }))
    )
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    steps,
    currentStep,
    categories,
    wishlistCategories,
    error
  }
}

export default withStore(Page)
