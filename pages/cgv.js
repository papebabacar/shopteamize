import App, { withStore } from 'containers/App'
import Cgv from 'components/Cms/Cgv'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { getPagesCms } from 'api/muhajir/cms'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  content,
  categories,
  wishlistCategories,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr'
        ? 'Shopteamize - Conditions générales de vente'
        : 'Shopteamize - Terms of sales'
    }
    description={
      language === 'fr'
        ? 'Nous vous invitons à consulter nos Conditions Générales de vente (CGV) pour en savoir plus sur nos produits, les envies shopping, nos offres et services de livraison et de paiement.'
        : 'We invite you to consult our General Conditions of Sale (GTC) to learn more about our products, shopping desires, our offers and delivery and payment services.'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? <Error statusCode={404} /> : <Cgv {...content} />}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  store,
  prefferedLanguage,
  pathname,
  query,
  res
}) => {
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] },
    { data: content = [] }
  ] = await Promise.all(
    [
      getProductCategories({ lang }),
      getWishlistCategories(),
      getPagesCms(lang, 'cgv')
    ].map((promise = Promise.reject()) =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    categories,
    wishlistCategories,
    content,
    error
  }
}

export default withStore(Page)
