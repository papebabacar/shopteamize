import App, { withStore } from 'containers/App'
import Register from 'components/Register'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  redirect,
  categories,
  wishlistCategories,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr'
        ? 'Shopteamize - Créer votre compte'
        : 'Shopteamize - Register'
    }
    description={
      language === 'fr'
        ? 'Créer votre compte su Shopteamize , compléter vos envies shopping et inviter vos amis à participer à votre commande groupée. '
        : 'Create your Shopteamize account, complete your shopping desires and invite your friends to participate in your bulk order.'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? <Error statusCode={404} /> : <Register redirect={redirect} />}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  store,
  prefferedLanguage,
  pathname,
  query,
  res
}) => {
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] }
  ] = await Promise.all(
    [getProductCategories({ lang }), getWishlistCategories()].map(
      (promise = Promise.reject()) =>
        promise.catch(() => ({
          data: undefined
        }))
    )
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    categories,
    wishlistCategories,
    error
  }
}

export default withStore(Page)
