import App, { withStore } from 'containers/App'
import Shipping from 'components/Cms/Shipping'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { getPagesCms } from 'api/muhajir/cms'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  content,
  categories,
  wishlistCategories,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr'
        ? 'Shopteamize - En savoir plus sur la livraison'
        : 'Shopteamize - Learn more about the shipping'
    }
    description={
      language === 'fr'
        ? 'Vous souhaitez vous procurer des produits musulmans tels que les Emirats Arabes unis, le Maroc, l’Arabie Saoudite…Participer aux commandes groupées sur Shopteamize et économiser jusqu’à 70% sur les frais de livraison. Vos colis vous sont livrés en 48h avec DHL.'
        : 'You want to buy Muslim products such as the United Arab Emirates, Morocco, Saudi Arabia ... Participate in bulk orders on Shopteamize and save up to 70% on shipping costs. Your packages are delivered to you in 48h with DHL.'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? <Error statusCode={404} /> : <Shipping {...content} />}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  store,
  prefferedLanguage,
  pathname,
  query,
  res
}) => {
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] },
    { data: content = [] }
  ] = await Promise.all(
    [
      getProductCategories({ lang }),
      getWishlistCategories(),
      getPagesCms(lang, 'shipping')
    ].map((promise = Promise.reject()) =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    categories,
    wishlistCategories,
    content,
    error
  }
}

export default withStore(Page)
