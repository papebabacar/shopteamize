import App, { Container } from 'next/app'
import Negotiator from 'negotiator'

export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}
    if (Component.getInitialProps) {
      const { req } = ctx
      const language = new Negotiator(req).language(['en', 'fr'])
      pageProps = await Component.getInitialProps({
        ...ctx,
        prefferedLanguage: language
      })
    }
    const { query } = ctx
    return { pageProps, params: query }
  }
  render() {
    const { Component, pageProps, params } = this.props
    return (
      <Container>
        <Component {...pageProps} params={params} />
      </Container>
    )
  }
}
