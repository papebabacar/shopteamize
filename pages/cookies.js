import App, { withStore } from 'containers/App'
import Cookies from 'components/Cms/Cookies'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { getPagesCms } from 'api/muhajir/cms'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  content,
  categories,
  wishlistCategories,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr'
        ? 'Shopteamize - Données personnelles et cookies'
        : 'Shopteamize - Cookies'
    }
    description={
      language === 'fr'
        ? "Shopteamize, spécialiste de l'achat groupé vous invite à consulter sa page d'utilisation des cookies. Découvrez nos coffrets ramadan 2018"
        : 'Shopteamize, a group purchasing specialist, invites you to consult its cookie usage page. Discover our Ramadan 2018 gift sets'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? <Error statusCode={404} /> : <Cookies {...content} />}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  store,
  prefferedLanguage,
  pathname,
  query,
  res
}) => {
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] },
    { data: content = [] }
  ] = await Promise.all(
    [
      getProductCategories({ lang }),
      getWishlistCategories(),
      getPagesCms(lang, 'privacy_notice')
    ].map((promise = Promise.reject()) =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    categories,
    wishlistCategories,
    content,
    error
  }
}

export default withStore(Page)
