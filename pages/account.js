import App, { withStore } from 'containers/App'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { getUser } from 'api/account'
import Header from 'components/Header'
import Footer from 'components/Footer'
import Account from 'components/Account'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'
import { Router } from 'router'

const Page = ({
  params,
  categories,
  wishlistCategories,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr' ? 'Shopteamize - Mon compte' : 'Shopteamize - Account'
    }
    description={
      language === 'fr'
        ? 'Shopteamize est un site marchand spécialisé dans l’achat groupé de produits en provenance des pays musulmans ( Dubai, Arabie Saoudite, Maroc…). Shopteamize vous propose des idées shopping ramadan, avec des produits minutieusement sélectionnés pour inspirer vos envies shopping. Shop qui peut !'
        : 'Shopteamize is a merchant site specializing in the group purchase of products from Muslim countries (Dubai, Saudi Arabia, Morocco ...). Shopteamize offers you ramadan shopping ideas, with products carefully selected to inspire your shopping desires. Shop who can!'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? <Error statusCode={404} /> : <Account params={params} />}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  res,
  store,
  query: params,
  prefferedLanguage,
  pathname
}) => {
  const {
    user: { token }
  } = store.getState()
  const lang =
    params.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] }
  ] = await Promise.all(
    [getProductCategories({ lang }), getWishlistCategories()].map(
      (promise = Promise.reject()) =>
        promise.catch(() => ({
          data: undefined
        }))
    )
  )
  try {
    await getUser(token)
  } catch (error) {
    res && res.redirect(Router.linkPage('login', { language: lang }).as)
  }
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    params,
    categories,
    wishlistCategories,
    error
  }
}

export default withStore(Page)
