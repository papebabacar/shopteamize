import App, { withStore } from 'containers/App'
import Header from 'components/Header'
import Footer from 'components/Footer'
import Banner from 'components/Home/Banner'
import BannerPromo from 'components/Home/BannerPromo'
// import BannerPub from 'components/Home/BannerPub'
import QuickStart from 'components/Home/QuickStart'
import BannerSteps from 'components/Home/BannerSteps'
import Latest from 'components/Push/Product/Latest'
import Popular from 'components/Push/WishList/Popular'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { getNewProducts } from 'api/muhajir/catalog'
import { getWishlists } from 'api/muhajir/wishlist'
import { getSlides } from 'api/muhajir/cms'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  products,
  wishlists,
  slides,
  categories,
  wishlistCategories,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr'
        ? 'Shopteamize – La plateforme qui optimise vos achats groupés'
        : 'Shopteamize - The platform which optimize your group shopping.'
    }
    description={
      language === 'fr'
        ? 'Shopteamize est un site marchand spécialisé dans l’achat groupé de produits en provenance des  ( Dubai, Arabie Saoudite, Maroc…). Shopteamize vous propose des idées shopping ramadan, avec des produits minutieusement sélectionnés pour inspirer vos envies shopping. Shop qui peut !'
        : 'Shopteamize is a merchant site specializing in the group purchase of products from Muslim countries (Dubai, Saudi Arabia, Morocco ...). Shopteamize offers you ramadan shopping ideas, with products carefully selected to inspire your shopping desires. Shop who can!'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? (
      <Error statusCode={404} wishlists={wishlists} />
    ) : (
      <>
        <Banner slides={slides} />
        <QuickStart />
        <BannerSteps />
        <BannerPromo />
        <Popular items={wishlists} />
        <Latest items={products} />
        {/* <BannerPub /> */}
      </>
    )}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  store,
  prefferedLanguage,
  pathname,
  query,
  req,
  res
}) => {
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const params = { lang, size: 20 }
  const [
    { data: slides = [] },
    { data: products = [] },
    { data: wishlists = [] },
    { data: categories = [] },
    { data: wishlistCategories = [] }
  ] = await Promise.all(
    [
      getSlides(lang),
      getNewProducts(params),
      getWishlists(params),
      getProductCategories({ lang }),
      getWishlistCategories()
    ].map(promise =>
      promise.catch(() => ({
        data: []
      }))
    )
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  } else {
    res &&
      res.setHeader(
        'Link',
        `<https://${
          req.headers.host
        }/${lang}>; rel="alternate"; hreflang="x-default",<https://${
          req.headers.host
        }/en>; rel="alternate"; hreflang="en",<https://${
          req.headers.host
        }/fr>; rel="alternate"; hreflang="fr"`
      )
  }
  return {
    products,
    wishlists,
    slides,
    categories,
    wishlistCategories,
    error
  }
}

export default withStore(Page)
