import App, { withStore } from 'containers/App'
import Detail from 'components/WishList/Detail'
import Popular from 'components/Push/WishList/Popular'
import { getWishlist, getWishlists } from 'api/muhajir/wishlist'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setDefaultCartQuantity } from 'store/actions/cart'
import { setLanguage, setPathname, setPathParams } from 'store/actions/user'
import Error from 'components/Error'
import slugify from '@sindresorhus/slugify'

const Page = ({
  wishlist,
  wishlists,
  categories,
  wishlistCategories,
  user: { language },
  error
}) => (
  <App
    title={getTitle(wishlist.labels, language)}
    description={
      wishlist.labels.length > 1
        ? wishlist.labels.find(({ isoCodeLang }) => isoCodeLang === language)
            .description
        : wishlist.labels[0].description
    }
    image={wishlist.mainImageUrl}
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? (
      <Error statusCode={404} wishlists={wishlists} />
    ) : (
      <>
        <Detail
          wishlist={{
            ...wishlist,
            category: categories.find(
              category => category.code === wishlist.categories.find(() => true)
            ) || {
              code: null,
              label: "Listes d'envie"
            }
          }}
        />
        <Popular items={wishlists} />
      </>
    )}
    <Footer />
  </App>
)

Page.getInitialProps = async ({
  query: { id, name, language },
  store,
  prefferedLanguage,
  pathname,
  req,
  res
}) => {
  const lang = language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  store.dispatch(setPathParams({ id, name }))
  const params = { lang, size: 20 }
  const [
    {
      data: wishlist = {
        items: [],
        labels: [{ title: '', description: '' }]
      }
    },
    { data: wishlists = [] },
    { data: categories = [] },
    { data: wishlistCategories = [] }
  ] = await Promise.all(
    [
      getWishlist(id, lang),
      getWishlists(params),
      getProductCategories({ lang }),
      getWishlistCategories()
    ].map(promise =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  store.dispatch(setDefaultCartQuantity(1))
  let error
  res &&
    res.setHeader(
      'X-Robots-Tag',
      'unavailable_after: ' + new Date(wishlist.endDate).toUTCString()
    )
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  } else if (Number(wishlist.id) !== Number(id)) {
    res.statusCode = error = 404
  } else {
    res &&
      res.setHeader(
        'Link',
        `<https://${req.headers.host}/${lang}/wishlist/${wishlist.id}/${slugify(
          getTitle(wishlist.labels, lang)
        )}>; "rel=canonical"`
      )
    res &&
      res.setHeader(
        'Link',
        `<https://${req.headers.host}/en/wishlist/${wishlist.id}/${slugify(
          getTitle(wishlist.labels, lang)
        )}>; rel="alternate"; hreflang="en",<https://${
          req.headers.host
        }/fr/wishlist/${wishlist.id}/${slugify(
          getTitle(wishlist.labels, lang)
        )}>; rel="alternate"; hreflang="fr"`
      )
  }
  return {
    wishlist,
    wishlists,
    categories,
    wishlistCategories,
    error
  }
}
export default withStore(Page)

const getTitle = (labels, language) =>
  labels.length > 1
    ? labels.find(({ isoCodeLang }) => isoCodeLang === language).title
    : labels[0].title
