import App, { withStore } from 'containers/App'
import Payment from 'components/Cms/Payment'
import { getProductCategories } from 'api/muhajir/catalog'
import { getWishlistCategories } from 'api/muhajir/wishlist'
import { getPagesCms } from 'api/muhajir/cms'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { setLanguage, setPathname } from 'store/actions/user'
import Error from 'components/Error'

const Page = ({
  content,
  categories,
  wishlistCategories,
  user: { language },
  error
}) => (
  <App
    title={
      language === 'fr'
        ? 'Shopteamize - Les moyens de paiement'
        : 'Shopteamize - Payment methods'
    }
    description={
      language === 'fr'
        ? 'Shopteamize est un site marchand spécialisé dans l’achat groupé de produits en provenance des pays musulmans ( Dubai, Arabie Saoudite, Maroc…). Shopteamize vous propose des idées shopping ramadan, avec des produits minutieusement sélectionnés pour inspirer vos envies shopping. Shop qui peut !'
        : 'Shopteamize is a merchant site specializing in the group purchase of products from Muslim countries (Dubai, Saudi Arabia, Morocco ...). Shopteamize offers you ramadan shopping ideas, with products carefully selected to inspire your shopping desires. Shop who can!'
    }
    language={language}
  >
    <Header categories={categories} wishlistCategories={wishlistCategories} />
    {error ? <Error statusCode={404} /> : <Payment {...content} />}

    <Footer />
  </App>
)

Page.getInitialProps = async ({
  store,
  prefferedLanguage,
  pathname,
  query,
  res
}) => {
  const lang =
    query.language || store.getState().user.language || prefferedLanguage
  store.dispatch(setLanguage(lang))
  store.dispatch(setPathname(pathname))
  const [
    { data: categories = [] },
    { data: wishlistCategories = [] },
    { data: content = [] }
  ] = await Promise.all(
    [
      getProductCategories({ lang }),
      getWishlistCategories(),
      getPagesCms(lang, 'payment')
    ].map((promise = Promise.reject()) =>
      promise.catch(() => ({
        data: undefined
      }))
    )
  )
  let error
  if (lang !== 'fr' && lang !== 'en') {
    res.statusCode = error = 404
    store.dispatch(setLanguage('en'))
  }
  return {
    categories,
    wishlistCategories,
    content,
    error
  }
}

export default withStore(Page)
