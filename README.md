# Market place front


# library

* [Redux/Flux -> for application architecture ](https://redux.js.org/)
* [next.js -> for server-rendered React applications](https://github.com/zeit/next.js/)
* [Recompose -> React library for higher-order components Pattern (HOC)](https://github.com/acdlite/recompose)
* [Axios -> Promise based HTTP client for nodeJS](https://github.com/axios/axios)
* [keymetrics/docker-pm2](https://github.com/keymetrics/docker-pm2)
* [https://react.rocks/example/nuka-carousel]


# Build & run in dev mod

npm install
npm run dev

> http://localhost:3000

* build 
npm run build

* Windows > clean & build
rm -rf %appdata%\Temp\react-native-*
rm -rf node_modules/ .next && npm cache clean --force && npm install && npm run build -- --reset-cache

# test

npm test

## Docker 

* Build
> docker build --tag snecommerce/market-ui-store:dev .

* Run

> docker run -it -p 8000:3000 --name market-ui-store -d snecommerce/market-ui-store:dev
> docker logs -f market-ui-store

* Push to docker hub

> docker login

(snecommerce / aqwzsx123)

> docker push snecommerce/market-ui-store:latest

# TODO : jenkinsfile

* payment > Checkout with PayPal does not provide the ability to store a customer’s PayPal account in the Vault
    * [PayPal Vault](https://developers.braintreepayments.com/guides/paypal/vault/javascript/v3)

* jenkinsfile
    * https://jenkins.io/doc/tutorials/building-a-node-js-and-react-app-with-npm/
    * https://jenkins.io/blog/2016/08/08/docker-pipeline-environments/

# Ressources materials

* Payment > Braintree Dropin-UI
https://braintree.github.io/braintree-web-drop-in/docs/current/Dropin.html


# Monitoring 
http://pm2.keymetrics.io/docs/usage/docker-pm2-nodejs/
https://github.com/keymetrics/docker-pm2
http://docs.keymetrics.io/docs/pages/faq-troubleshooting/#troubleshooting-for-keymetrics-pm2

docker build -t ui-store .
docker run -p 80:80 --name market-ui-store -d ui-store -e "KEYMETRICS_PUBLIC="ouhmprt10sq9tyc" -e "KEYMETRICS_SECRET="elpqy1hdfpk9gfd"


